//获取浏览器相关信息
var Tjtv5UA = window.navigator.userAgent.toLowerCase();
var isIphone = Tjtv5UA.match(/(tjtv5iphone);?/i);//判断是iphone客户端
var isAndroid = Tjtv5UA.match(/(tjtv5android);?/i);//判断是Android客户端
/*
打开APP新窗体（webview）
参数1：title 窗体标题
参数2：linkUrl 打开的网址
参数3：type 新窗体的显示状态 0 竖屏 1 横屏
*/
function APP_OpenWin(title, linkUrl, type) {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://OpenWin/?" + encodeURI(title) + "," + encodeURI(linkUrl) + "," + encodeURI(type);
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
		//方法2：WKwebview网页与客户端JS交互方法，直接通过数据接口
		//try {
//			window.webkit.messageHandlers.OpenWin.postMessage(encodeURI(title) + "," + encodeURI(linkUrl) + "," + encodeURI(type));
//		} catch (ex) {
//		    //alert("Interface error.");
//		}
	} else if (isAndroid){
		window.Tjtv5API.OpenWin(title, linkUrl, type);
	}
}
/*
打开APP新窗体（webview）
参数1：title 窗体标题
参数2：linkUrl 打开的网址
参数3：type 新窗体的显示状态 0 竖屏 1 横屏
*/
function APP_OpenMemberWin(title, linkUrl, type) {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://OpenMemberWin/?" + encodeURI(title) + "," + encodeURI(linkUrl) + "," + encodeURI(type);
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
		//方法2：WKwebview网页与客户端JS交互方法，直接通过数据接口
		//try {
//			window.webkit.messageHandlers.OpenWin.postMessage(encodeURI(title) + "," + encodeURI(linkUrl) + "," + encodeURI(type));
//		} catch (ex) {
//		    //alert("Interface error.");
//		}
	} else if (isAndroid){
		window.Tjtv5API.OpenMemberWin(title, linkUrl, type);
	}
}
/*
打开APP新窗体（webview）
参数1：title 窗体标题
参数2：linkUrl 打开的网址
参数3：type 新窗体的显示状态 0 竖屏 1 横屏
*/
function APP_OpenLocalWin(title, linkUrl) {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://OpenLocalWin/?" + encodeURI(title) + "," + encodeURI(linkUrl);
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
		//方法2：WKwebview网页与客户端JS交互方法，直接通过数据接口
		//try {
//			window.webkit.messageHandlers.OpenWin.postMessage(encodeURI(title) + "," + encodeURI(linkUrl) + "," + encodeURI(type));
//		} catch (ex) {
//		    //alert("Interface error.");
//		}
	} else if (isAndroid){
		window.Tjtv5API.OpenLocalWin(title, linkUrl);
	}
}
/*
打开评论列表（原生）
参数：id 内容ID
*/
function APP_OpenComment(id) {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://OpenComment/?" + encodeURI(id);
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
		//方法2：WKwebview网页与客户端JS交互方法，直接通过数据接口
		//try {
//			window.webkit.messageHandlers.OpenComment.postMessage(encodeURI(id));
//		} catch (ex) {
//		    //alert("Interface error.");
//		}
	} else if (isAndroid){
		window.Tjtv5API.OpenComment(id);
	}
}
/*
关闭新闻详细的loading提示框
参数：sum 评论总数
*/
function APP_News_CloseLoading(sum) {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://NewsCloseLoading/?" + encodeURI(sum);
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
		//方法2：WKwebview网页与客户端JS交互方法，直接通过数据接口
		//try {
//			window.webkit.messageHandlers.NewsCloseLoading.postMessage(encodeURI(sum));
//		} catch (ex) {
//		    //alert("Interface error.");
//		}
	} else if (isAndroid){
		window.Tjtv5API.NewsCloseLoading(sum);
	}
}
/*
加载T5直播或T5多机位
参数：steamID 媒体ID，为0时取T5直播，大于0是多机位
*/
function APP_Live_Video(steamID) {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://LiveVideo/?" + encodeURI(steamID);
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
		//方法2：WKwebview网页与客户端JS交互方法，直接通过数据接口
		//try {
//			window.webkit.messageHandlers.LiveVideo.postMessage(encodeURI(steamID));
//		} catch (ex) {
//		    //alert("Interface error.");
//		}
	} else if (isAndroid){
		window.Tjtv5API.LiveVideo(steamID);
	}
}
/*
会员签到，需要登录
*/
function APP_signin(status, msg) {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://signin/?" + encodeURI(status);
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.signin(status);
	} else {
		alert(msg);
	}
}
/*
会员签到，需要登录
*/
function APP_signinpage() {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://signinpage/?";
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.signinpage();
	} else {
		alert(msg);
	}
}
/*
会员充值，需要登录
*/
function APP_payment() {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://payment/?";
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.payment();
	} else {
		alert(msg);
	}
}
/*
我的关注，需要登录
*/
function APP_myFollow() {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://myFollow/?";
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.myFollow();
	} else {
		alert("我的关注");
	}
}
/*
我的收藏，需要登录
*/
function APP_myLike() {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://myLike/?";
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.myLike();
	} else {
		alert("我的收藏");
	}
}
/*
我的竞猜，需要登录
*/
function APP_PayGuessing() {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://myPayGuessing/?";
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.myPayGuessing();
	} else {
		alert("我的竞猜");
	}
}
/*
我的直播，需要登录
*/
function APP_PayVideo() {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://myPayVideo/?";
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.myPayVideo();
	} else {
		alert("我的直播");
	}
}
/*
我的兑换，需要登录
*/
function APP_myIntegral() {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://myIntegral/?";
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.myIntegral();
	} else {
		alert("我的兑换");
	}
}
/*
我的提醒，需要登录
*/
function APP_myClock() {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://myClock/?";
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.myClock();
	} else {
		alert("我的提醒");
	}
}
/*
充值历史，需要登录
*/
function APP_myPayHistory() {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://myPayHistory/?";
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.myPayHistory();
	} else {
		alert("我的历史");
	}
}
/*
我的彩票，需要登录
*/
function APP_myLottery() {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://myLottery/?";
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.myLottery();
	} else {
		alert("我的彩票");
	}
}
/*
我的订单，需要登录
*/
function APP_myOrder(status) {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://myOrder/?" + encodeURI(status);
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.myOrder(status);
	} else {
		alert("我的订单" + status);
	}
}
/*
我的资料，需要登录
*/
function APP_myData() {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://myData/?";
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.myData();
	} else {
		alert("我的资料");
	}
}
/*
收货地址，需要登录
*/
function APP_myAddress() {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://myAddress/?";
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.myAddress();
	} else {
		alert("我的地址");
	}
}
/*
商品评论，需要登录
*/
function APP_myShopComment() {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://myShopComment/?";
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.myShopComment();
	} else {
		alert("商品评论");
	}
}
/*
商品收藏，需要登录
*/
function APP_myShopCollect() {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://myShopCollect/?";
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.myShopCollect();
	} else {
		alert("商品收藏");
	}
}
/*
退出登陆，需要登录
*/
function APP_loginOut() {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://loginOut/?";
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.loginOut();
	} else {
		alert("退出登陆");
	}
}
/*
打开会员中心页，需要登录
*/
function APP_memberCenter() {
	if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://memberCenter/?";
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.memberCenter();
	} else {
		alert("商品收藏");
	}
}
//进入登录页面
function APP_memberLogin() {	
  if (isIphone) {
		//方法1：UIwebview网页与客户端JS交互方法，通过创建隐藏iframe，伪造地址协议的方法
		var ifr = document.createElement('iframe');
		ifr.src = "Tjtv5API://memberLogin/?";
		ifr.style.width = "0";
		ifr.style.height = "0";
		document.body.appendChild(ifr);
		setTimeout(function(){
			if (null != ifr) ifr.parentNode.removeChild(ifr);
			ifr = null;
		},500);
	} else if (isAndroid){
		window.Tjtv5API.memberLogin();
	} else {
		alert("弹出登录提示");
	}
}