package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class IntegrationList {

    /**
     * status : 1
     * returnMsg : null
     * IntegrationCount : 3
     * memberIntegrationList : [{"integrationJJ":"-","integrationNumber":"200.00","integrationRecord":"订单【201806292004081874】积分兑换商品","integrationJE":"0.00","integrationType":"兑换商品","integrationDate":"2018-06-29 20:04"},{"integrationJJ":"+","integrationNumber":"80000.00","integrationRecord":"管理员后台操作","integrationJE":"0.00","integrationType":"管理员操作","integrationDate":"2018-06-29 19:27"},{"integrationJJ":"+","integrationNumber":"1.00","integrationRecord":"每日签到获取积分","integrationJE":"0.00","integrationType":"每日签到","integrationDate":"2017-11-01 10:41"}]
     */

    private int status;
    private Object returnMsg;
    private int IntegrationCount;
    private List<MemberIntegrationListBean> memberIntegrationList;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getIntegrationCount() {
        return IntegrationCount;
    }

    public void setIntegrationCount(int IntegrationCount) {
        this.IntegrationCount = IntegrationCount;
    }

    public List<MemberIntegrationListBean> getMemberIntegrationList() {
        return memberIntegrationList;
    }

    public void setMemberIntegrationList(List<MemberIntegrationListBean> memberIntegrationList) {
        this.memberIntegrationList = memberIntegrationList;
    }

    public static class MemberIntegrationListBean {
        /**
         * integrationJJ : -
         * integrationNumber : 200.00
         * integrationRecord : 订单【201806292004081874】积分兑换商品
         * integrationJE : 0.00
         * integrationType : 兑换商品
         * integrationDate : 2018-06-29 20:04
         */

        private String integrationJJ;
        private String integrationNumber;
        private String integrationRecord;
        private String integrationJE;
        private String integrationType;
        private String integrationDate;

        public String getIntegrationJJ() {
            return integrationJJ;
        }

        public void setIntegrationJJ(String integrationJJ) {
            this.integrationJJ = integrationJJ;
        }

        public String getIntegrationNumber() {
            return integrationNumber;
        }

        public void setIntegrationNumber(String integrationNumber) {
            this.integrationNumber = integrationNumber;
        }

        public String getIntegrationRecord() {
            return integrationRecord;
        }

        public void setIntegrationRecord(String integrationRecord) {
            this.integrationRecord = integrationRecord;
        }

        public String getIntegrationJE() {
            return integrationJE;
        }

        public void setIntegrationJE(String integrationJE) {
            this.integrationJE = integrationJE;
        }

        public String getIntegrationType() {
            return integrationType;
        }

        public void setIntegrationType(String integrationType) {
            this.integrationType = integrationType;
        }

        public String getIntegrationDate() {
            return integrationDate;
        }

        public void setIntegrationDate(String integrationDate) {
            this.integrationDate = integrationDate;
        }
    }
}
