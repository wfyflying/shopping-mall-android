package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.Order;

public interface IntegralOrderListView extends View{
    void onSuccessGetIntegralOrderList(Order order);
    void onSuccessAffirmOrder(CommonResult commonResult);
    void onError(String result);
}
