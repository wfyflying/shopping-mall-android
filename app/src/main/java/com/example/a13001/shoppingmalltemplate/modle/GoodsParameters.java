package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class GoodsParameters {

    /**
     * status : 1
     * returnMsg : null
     * shop_title1 : 口味
     * shop_title2 : 包装
     * shop_attribute : [{"shop_attribute_xh":"原味","shop_attribute_smallpic":"","shop_attribute_pic":"","shop_attribute_data":[{"shop_attribute_gg":"200克","shop_attribute_price":"25.00","shop_attribute_price2":"40.00","shop_attribute_stock":989,"shop_attribute_hh":"A36E72001"},{"shop_attribute_gg":"500克","shop_attribute_price":"60.00","shop_attribute_price2":"90.00","shop_attribute_stock":986,"shop_attribute_hh":"A36E72002"}]},{"shop_attribute_xh":"蔓越梅味","shop_attribute_smallpic":"","shop_attribute_pic":"","shop_attribute_data":[{"shop_attribute_gg":"200克","shop_attribute_price":"25.00","shop_attribute_price2":"40.00","shop_attribute_stock":998,"shop_attribute_hh":"A36E72003"},{"shop_attribute_gg":"500克","shop_attribute_price":"60.00","shop_attribute_price2":"90.00","shop_attribute_stock":999,"shop_attribute_hh":"A36E72004"}]},{"shop_attribute_xh":"奥利奥味","shop_attribute_smallpic":"","shop_attribute_pic":"","shop_attribute_data":[{"shop_attribute_gg":"200克","shop_attribute_price":"25.00","shop_attribute_price2":"40.00","shop_attribute_stock":995,"shop_attribute_hh":"A36E72005"},{"shop_attribute_gg":"500克","shop_attribute_price":"60.00","shop_attribute_price2":"90.00","shop_attribute_stock":990,"shop_attribute_hh":"A36E72006"}]},{"shop_attribute_xh":"杏仁味","shop_attribute_smallpic":"","shop_attribute_pic":"","shop_attribute_data":[{"shop_attribute_gg":"200克","shop_attribute_price":"35.00","shop_attribute_price2":"60.00","shop_attribute_stock":999,"shop_attribute_hh":"A36E72007"},{"shop_attribute_gg":"500克","shop_attribute_price":"85.00","shop_attribute_price2":"110.00","shop_attribute_stock":992,"shop_attribute_hh":"A36E72008"}]}]
     * comment : 0
     * evaluate : 3
     * evaluate_good : 2
     * evaluate_ordinary : 1
     * evaluate_bad : 0
     * collection : 0
     * sales : 32
     */

    private int status;
    private Object returnMsg;
    private String shop_title1;
    private String shop_title2;
    private int comment;
    private int evaluate;
    private int evaluate_good;
    private int evaluate_ordinary;
    private int evaluate_bad;
    private int collection;
    private int sales;
    private List<ShopAttributeBean> shop_attribute;

    @Override
    public String toString() {
        return "GoodsParameters{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", shop_title1='" + shop_title1 + '\'' +
                ", shop_title2='" + shop_title2 + '\'' +
                ", comment=" + comment +
                ", evaluate=" + evaluate +
                ", evaluate_good=" + evaluate_good +
                ", evaluate_ordinary=" + evaluate_ordinary +
                ", evaluate_bad=" + evaluate_bad +
                ", collection=" + collection +
                ", sales=" + sales +
                ", shop_attribute=" + shop_attribute +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public String getShop_title1() {
        return shop_title1;
    }

    public void setShop_title1(String shop_title1) {
        this.shop_title1 = shop_title1;
    }

    public String getShop_title2() {
        return shop_title2;
    }

    public void setShop_title2(String shop_title2) {
        this.shop_title2 = shop_title2;
    }

    public int getComment() {
        return comment;
    }

    public void setComment(int comment) {
        this.comment = comment;
    }

    public int getEvaluate() {
        return evaluate;
    }

    public void setEvaluate(int evaluate) {
        this.evaluate = evaluate;
    }

    public int getEvaluate_good() {
        return evaluate_good;
    }

    public void setEvaluate_good(int evaluate_good) {
        this.evaluate_good = evaluate_good;
    }

    public int getEvaluate_ordinary() {
        return evaluate_ordinary;
    }

    public void setEvaluate_ordinary(int evaluate_ordinary) {
        this.evaluate_ordinary = evaluate_ordinary;
    }

    public int getEvaluate_bad() {
        return evaluate_bad;
    }

    public void setEvaluate_bad(int evaluate_bad) {
        this.evaluate_bad = evaluate_bad;
    }

    public int getCollection() {
        return collection;
    }

    public void setCollection(int collection) {
        this.collection = collection;
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }

    public List<ShopAttributeBean> getShop_attribute() {
        return shop_attribute;
    }

    public void setShop_attribute(List<ShopAttributeBean> shop_attribute) {
        this.shop_attribute = shop_attribute;
    }

    public static class ShopAttributeBean {
        /**
         * shop_attribute_xh : 原味
         * shop_attribute_smallpic :
         * shop_attribute_pic :
         * shop_attribute_data : [{"shop_attribute_gg":"200克","shop_attribute_price":"25.00","shop_attribute_price2":"40.00","shop_attribute_stock":989,"shop_attribute_hh":"A36E72001"},{"shop_attribute_gg":"500克","shop_attribute_price":"60.00","shop_attribute_price2":"90.00","shop_attribute_stock":986,"shop_attribute_hh":"A36E72002"}]
         */

        private String shop_attribute_xh;
        private String shop_attribute_smallpic;
        private String shop_attribute_pic;
        private List<ShopAttributeDataBean> shop_attribute_data;

        @Override
        public String toString() {
            return "ShopAttributeBean{" +
                    "shop_attribute_xh='" + shop_attribute_xh + '\'' +
                    ", shop_attribute_smallpic='" + shop_attribute_smallpic + '\'' +
                    ", shop_attribute_pic='" + shop_attribute_pic + '\'' +
                    ", shop_attribute_data=" + shop_attribute_data +
                    '}';
        }

        public String getShop_attribute_xh() {
            return shop_attribute_xh;
        }

        public void setShop_attribute_xh(String shop_attribute_xh) {
            this.shop_attribute_xh = shop_attribute_xh;
        }

        public String getShop_attribute_smallpic() {
            return shop_attribute_smallpic;
        }

        public void setShop_attribute_smallpic(String shop_attribute_smallpic) {
            this.shop_attribute_smallpic = shop_attribute_smallpic;
        }

        public String getShop_attribute_pic() {
            return shop_attribute_pic;
        }

        public void setShop_attribute_pic(String shop_attribute_pic) {
            this.shop_attribute_pic = shop_attribute_pic;
        }

        public List<ShopAttributeDataBean> getShop_attribute_data() {
            return shop_attribute_data;
        }

        public void setShop_attribute_data(List<ShopAttributeDataBean> shop_attribute_data) {
            this.shop_attribute_data = shop_attribute_data;
        }

        public static class ShopAttributeDataBean {
            /**
             * shop_attribute_gg : 200克
             * shop_attribute_price : 25.00
             * shop_attribute_price2 : 40.00
             * shop_attribute_stock : 989
             * shop_attribute_hh : A36E72001
             */

            private String shop_attribute_gg;
            private String shop_attribute_price;
            private String shop_attribute_price2;
            private int shop_attribute_stock;
            private String shop_attribute_hh;

            @Override
            public String toString() {
                return "ShopAttributeDataBean{" +
                        "shop_attribute_gg='" + shop_attribute_gg + '\'' +
                        ", shop_attribute_price='" + shop_attribute_price + '\'' +
                        ", shop_attribute_price2='" + shop_attribute_price2 + '\'' +
                        ", shop_attribute_stock=" + shop_attribute_stock +
                        ", shop_attribute_hh='" + shop_attribute_hh + '\'' +
                        '}';
            }

            public String getShop_attribute_gg() {
                return shop_attribute_gg;
            }

            public void setShop_attribute_gg(String shop_attribute_gg) {
                this.shop_attribute_gg = shop_attribute_gg;
            }

            public String getShop_attribute_price() {
                return shop_attribute_price;
            }

            public void setShop_attribute_price(String shop_attribute_price) {
                this.shop_attribute_price = shop_attribute_price;
            }

            public String getShop_attribute_price2() {
                return shop_attribute_price2;
            }

            public void setShop_attribute_price2(String shop_attribute_price2) {
                this.shop_attribute_price2 = shop_attribute_price2;
            }

            public int getShop_attribute_stock() {
                return shop_attribute_stock;
            }

            public void setShop_attribute_stock(int shop_attribute_stock) {
                this.shop_attribute_stock = shop_attribute_stock;
            }

            public String getShop_attribute_hh() {
                return shop_attribute_hh;
            }

            public void setShop_attribute_hh(String shop_attribute_hh) {
                this.shop_attribute_hh = shop_attribute_hh;
            }
        }
    }
}
