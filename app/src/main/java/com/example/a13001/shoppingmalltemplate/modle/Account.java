package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class Account {

    /**
     * status : 1
     * returnMsg : null
     * TransactionCount : 3
     * memberTransactionList : [{"operatingRecord":"确认收货,普通付费从不可用金额移到消费总额","recordDate":"2018-07-17 11:40","recordType":"线下商品支付交易","recordPrice":"0.01","recordOrder":"201707201735002007"},{"operatingRecord":"确认收货,货到付款付费从消费总额增加","recordDate":"2018-07-01 18:28","recordType":"商品货到付款交易","recordPrice":"200.00","recordOrder":"201806292004081874"},{"operatingRecord":"关闭操作,从不可用金额移到账户余额中","recordDate":"2018-06-13 08:26","recordType":"商品交易订单取消","recordPrice":"0.01","recordOrder":"201806060921305708"}]
     */

    private int status;
    private String returnMsg;
    private int TransactionCount;
    private List<MemberTransactionListBean> memberTransactionList;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getTransactionCount() {
        return TransactionCount;
    }

    public void setTransactionCount(int TransactionCount) {
        this.TransactionCount = TransactionCount;
    }

    public List<MemberTransactionListBean> getMemberTransactionList() {
        return memberTransactionList;
    }

    public void setMemberTransactionList(List<MemberTransactionListBean> memberTransactionList) {
        this.memberTransactionList = memberTransactionList;
    }

    public static class MemberTransactionListBean {
        /**
         * operatingRecord : 确认收货,普通付费从不可用金额移到消费总额
         * recordDate : 2018-07-17 11:40
         * recordType : 线下商品支付交易
         * recordPrice : 0.01
         * recordOrder : 201707201735002007
         */

        private String operatingRecord;
        private String recordDate;
        private String recordType;
        private String recordPrice;
        private String recordOrder;

        public String getOperatingRecord() {
            return operatingRecord;
        }

        public void setOperatingRecord(String operatingRecord) {
            this.operatingRecord = operatingRecord;
        }

        public String getRecordDate() {
            return recordDate;
        }

        public void setRecordDate(String recordDate) {
            this.recordDate = recordDate;
        }

        public String getRecordType() {
            return recordType;
        }

        public void setRecordType(String recordType) {
            this.recordType = recordType;
        }

        public String getRecordPrice() {
            return recordPrice;
        }

        public void setRecordPrice(String recordPrice) {
            this.recordPrice = recordPrice;
        }

        public String getRecordOrder() {
            return recordOrder;
        }

        public void setRecordOrder(String recordOrder) {
            this.recordOrder = recordOrder;
        }
    }
}
