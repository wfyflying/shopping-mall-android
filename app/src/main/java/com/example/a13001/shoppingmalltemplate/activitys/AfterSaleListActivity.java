package com.example.a13001.shoppingmalltemplate.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.adapters.AfterSaleLvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.modle.AddressOrderId;
import com.example.a13001.shoppingmalltemplate.modle.AfterSale;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.ShouHouDetail;
import com.example.a13001.shoppingmalltemplate.modle.UpFile;
import com.example.a13001.shoppingmalltemplate.mvpview.ApplyAfterSaleView;
import com.example.a13001.shoppingmalltemplate.presenter.ApplyAfterSalePredenter;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AfterSaleListActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_aftersalelist_daishenhe)
    TextView tvAftersalelistDaishenhe;
    @BindView(R.id.view1)
    View view1;
    @BindView(R.id.tv_aftersalelist_yibohui)
    TextView tvAftersalelistYibohui;
    @BindView(R.id.view2)
    View view2;
    @BindView(R.id.tv_aftersalelist_daiyouji)
    TextView tvAftersalelistDaiyouji;
    @BindView(R.id.view3)
    View view3;
    @BindView(R.id.tv_aftersalelist_yishouhuo)
    TextView tvAftersalelistYishouhuo;
    @BindView(R.id.view4)
    View view4;
    @BindView(R.id.tv_aftersalelist_chulizhong)
    TextView tvAftersalelistChulizhong;
    @BindView(R.id.view5)
    View view5;
    @BindView(R.id.tv_aftersalelist_yiwancheng)
    TextView tvAftersalelistYiwancheng;
    @BindView(R.id.view6)
    View view6;
    @BindView(R.id.lv_aftersalelist)
    ListView lvAftersalelist;
    @BindView(R.id.tv_title_right)
    TextView tvTitleRight;
    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;
    @BindView(R.id.srfl_aftersale)
    SmartRefreshLayout srflAftersale;
    private ApplyAfterSalePredenter applyAfterSalePredenter = new ApplyAfterSalePredenter(AfterSaleListActivity.this);
    private static final String TAG = "AfterSaleListActivity";
    private List<AfterSale.RepairsListBean> mList;
    private AfterSaleLvAdapter mAdapter;
    private String code;
    private String timeStamp;
    private int pageindex = 1;
    private int status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_sale_list);
        ButterKnife.bind(this);
        String safetyCode = MyUtils.getMetaValue(AfterSaleListActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        timeStamp = Utils.getTimeStamp();
        tvTitleCenter.setText("商品售后");
        applyAfterSalePredenter.onCreate();
        applyAfterSalePredenter.attachView(applyAfterSaleView);
        mList = new ArrayList<>();
        mAdapter = new AfterSaleLvAdapter(AfterSaleListActivity.this, mList);
        lvAftersalelist.setAdapter(mAdapter);
        tvAftersalelistDaishenhe.performClick();
        lvAftersalelist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(AfterSaleListActivity.this, ShouHouDetailActivity.class);
                intent.putExtra("cartid", mList.get(i).getCartId());
                intent.putExtra("commodityid", mList.get(i).getContentid());
                intent.putExtra("orderid", mList.get(i).getOrdersId());
                startActivity(intent);
            }
        });
        setRefresh();
    }

    private void setRefresh() {
        //刷新
        srflAftersale.setRefreshHeader(new ClassicsHeader(AfterSaleListActivity.this));
        srflAftersale.setRefreshFooter(new ClassicsFooter(AfterSaleListActivity.this));
//        mRefresh.setEnablePureScrollMode(true);
        srflAftersale.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                pageindex = 1;
                if (mList != null) {
                    mList.clear();
                }
                applyAfterSalePredenter.getAfterSaleList(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.PAGE_SIZE, pageindex, status, "", "", "", AppConstants.FROM_MOBILE);
                refreshlayout.finishRefresh(2000/*,true*/);//传入false表示刷新失败
            }
        });
        srflAftersale.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                pageindex++;
                applyAfterSalePredenter.getAfterSaleList(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.PAGE_SIZE, pageindex, status, "", "", "", AppConstants.FROM_MOBILE);
                refreshlayout.finishLoadMore(2000/*,true*/);//传入false表示加载失败
            }
        });
    }

    ApplyAfterSaleView applyAfterSaleView = new ApplyAfterSaleView() {
        @Override
        public void onSuccess(CommonResult commonResult) {

        }

        @Override
        public void onSuccessGetAfterSaleList(AfterSale afterSale) {
            Log.e(TAG, "onSuccessGetAfterSaleList: " + afterSale);
            int status = afterSale.getStatus();
            if (status > 0) {
                mList.addAll(afterSale.getRepairsList());
                mAdapter.notifyDataSetChanged();
            } else {

            }
        }

        @Override
        public void onSuccessGetAddressOrderId(AddressOrderId addressOrderId) {

        }

        @Override
        public void onSuccessGetShouHouDetail(ShouHouDetail shouHouDetail) {

        }

        @Override
        public void onSuccessUpFile(UpFile upFile) {

        }


        @Override
        public void onError(String result) {
            Log.e(TAG, "onError: " + result);
        }
    };

    @OnClick({R.id.iv_title_back, R.id.tv_aftersalelist_daishenhe, R.id.tv_aftersalelist_yibohui, R.id.tv_aftersalelist_daiyouji, R.id.tv_aftersalelist_yishouhuo, R.id.tv_aftersalelist_chulizhong, R.id.tv_aftersalelist_yiwancheng})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_title_back:
                onBackPressed();
                break;
            //待审核
            case R.id.tv_aftersalelist_daishenhe:
                initColor();
                tvAftersalelistDaishenhe.setTextColor(getResources().getColor(R.color.ff2828));
                view1.setVisibility(View.VISIBLE);
                if (mList != null) {
                    mList.clear();
                }
                status = 0;
                applyAfterSalePredenter.getAfterSaleList(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.PAGE_SIZE, pageindex, status, "", "", "", AppConstants.FROM_MOBILE);
                lvAftersalelist.setAdapter(mAdapter);
                break;
            //已驳回
            case R.id.tv_aftersalelist_yibohui:
                initColor();
                tvAftersalelistYibohui.setTextColor(getResources().getColor(R.color.ff2828));
                view2.setVisibility(View.VISIBLE);
                if (mList != null) {
                    mList.clear();
                }
                status = 1;
                applyAfterSalePredenter.getAfterSaleList(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.PAGE_SIZE, pageindex, status, "", "", "", AppConstants.FROM_MOBILE);
                lvAftersalelist.setAdapter(mAdapter);
                break;
            //待邮寄
            case R.id.tv_aftersalelist_daiyouji:
                initColor();
                tvAftersalelistDaiyouji.setTextColor(getResources().getColor(R.color.ff2828));
                view3.setVisibility(View.VISIBLE);
                if (mList != null) {
                    mList.clear();
                }
                status = 2;
                applyAfterSalePredenter.getAfterSaleList(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.PAGE_SIZE, pageindex, status, "", "", "", AppConstants.FROM_MOBILE);
                lvAftersalelist.setAdapter(mAdapter);
                break;
            //已收货
            case R.id.tv_aftersalelist_yishouhuo:
                initColor();
                tvAftersalelistYishouhuo.setTextColor(getResources().getColor(R.color.ff2828));
                view4.setVisibility(View.VISIBLE);
                if (mList != null) {
                    mList.clear();
                }
                status = 3;
                applyAfterSalePredenter.getAfterSaleList(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.PAGE_SIZE, pageindex, status, "", "", "", AppConstants.FROM_MOBILE);
                lvAftersalelist.setAdapter(mAdapter);
                break;
            //处理中
            case R.id.tv_aftersalelist_chulizhong:
                initColor();
                tvAftersalelistChulizhong.setTextColor(getResources().getColor(R.color.ff2828));
                view5.setVisibility(View.VISIBLE);
                if (mList != null) {
                    mList.clear();
                }
                status = 4;
                applyAfterSalePredenter.getAfterSaleList(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.PAGE_SIZE, pageindex, status, "", "", "", AppConstants.FROM_MOBILE);
                lvAftersalelist.setAdapter(mAdapter);
                break;
            case R.id.tv_aftersalelist_yiwancheng:
                initColor();
                tvAftersalelistYiwancheng.setTextColor(getResources().getColor(R.color.ff2828));
                view6.setVisibility(View.VISIBLE);
                if (mList != null) {
                    mList.clear();
                }
                status = 5;
                applyAfterSalePredenter.getAfterSaleList(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.PAGE_SIZE, pageindex, status, "", "", "", AppConstants.FROM_MOBILE);
                lvAftersalelist.setAdapter(mAdapter);
                break;
        }
    }

    private void initColor() {
        tvAftersalelistDaishenhe.setTextColor(getResources().getColor(R.color.t333));
        tvAftersalelistYibohui.setTextColor(getResources().getColor(R.color.t333));
        tvAftersalelistDaiyouji.setTextColor(getResources().getColor(R.color.t333));
        tvAftersalelistYishouhuo.setTextColor(getResources().getColor(R.color.t333));
        tvAftersalelistChulizhong.setTextColor(getResources().getColor(R.color.t333));
        tvAftersalelistYiwancheng.setTextColor(getResources().getColor(R.color.t333));
        view1.setVisibility(View.INVISIBLE);
        view2.setVisibility(View.INVISIBLE);
        view3.setVisibility(View.INVISIBLE);
        view4.setVisibility(View.INVISIBLE);
        view5.setVisibility(View.INVISIBLE);
        view6.setVisibility(View.INVISIBLE);
    }
}
