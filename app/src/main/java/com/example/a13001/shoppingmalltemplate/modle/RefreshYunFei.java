package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class RefreshYunFei {

    @Override
    public String toString() {
        return "RefreshYunFei{" +
                "status=" + status +
                ", returnMsg='" + returnMsg + '\'' +
                ", expressCount=" + expressCount +
                ", express=" + express +
                '}';
    }

    /**
     * status : 1
     * returnMsg : null
     * expressCount : 3
     * express : [{"kdname":"快递","KdYfPrice":15},{"kdname":"EMS","KdYfPrice":20},{"kdname":"平邮","KdYfPrice":10}]
     */

    private int status;
    private String returnMsg;
    private int expressCount;
    private List<ExpressBean> express;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getExpressCount() {
        return expressCount;
    }

    public void setExpressCount(int expressCount) {
        this.expressCount = expressCount;
    }

    public List<ExpressBean> getExpress() {
        return express;
    }

    public void setExpress(List<ExpressBean> express) {
        this.express = express;
    }

    public static class ExpressBean {
        @Override
        public String toString() {
            return "ExpressBean{" +
                    "kdname='" + kdname + '\'' +
                    ", KdYfPrice=" + KdYfPrice +
                    '}';
        }

        /**
         * kdname : 快递
         * KdYfPrice : 15
         */

        private String kdname;
        private int KdYfPrice;

        public String getKdname() {
            return kdname;
        }

        public void setKdname(String kdname) {
            this.kdname = kdname;
        }

        public int getKdYfPrice() {
            return KdYfPrice;
        }

        public void setKdYfPrice(int KdYfPrice) {
            this.KdYfPrice = KdYfPrice;
        }
    }
}
