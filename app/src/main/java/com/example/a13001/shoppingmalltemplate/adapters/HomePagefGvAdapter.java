package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.modle.HomePageTitle;

import java.util.List;

public class HomePagefGvAdapter extends BaseAdapter {
    private Context mContext;
    private List<HomePageTitle> mList;

    public HomePagefGvAdapter(Context mContext, List<HomePageTitle> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {

        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view= LayoutInflater.from(mContext).inflate(R.layout.item_gv_homepage,null);
            holder=new ViewHolder();
            holder.tvTitle=(TextView) view.findViewById(R.id.tv_itemgvhomepage_title);
            holder.ivLogo=(ImageView) view.findViewById(R.id.iv_itemgvhomepage_logo);
            view.setTag(holder);
        }else{
            holder= (ViewHolder) view.getTag();
        }
        holder.tvTitle.setText(mList.get(i).getTitle());
        holder.ivLogo.setImageResource(mList.get(i).getLogo());
        return view;
    }
    class ViewHolder{
        TextView tvTitle;
        ImageView ivLogo;
    }
}
