package com.example.a13001.shoppingmalltemplate.manager;

import android.content.Context;

import com.example.a13001.shoppingmalltemplate.modle.Account;
import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.AddressOrderId;
import com.example.a13001.shoppingmalltemplate.modle.AfterSale;
import com.example.a13001.shoppingmalltemplate.modle.Alipay;
import com.example.a13001.shoppingmalltemplate.modle.AnswerList;
import com.example.a13001.shoppingmalltemplate.modle.AppConfig;
import com.example.a13001.shoppingmalltemplate.modle.Banner;
import com.example.a13001.shoppingmalltemplate.modle.Book;
import com.example.a13001.shoppingmalltemplate.modle.Classify;
import com.example.a13001.shoppingmalltemplate.modle.CloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.Collect;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.ContentFilter;
import com.example.a13001.shoppingmalltemplate.modle.CouponCount;
import com.example.a13001.shoppingmalltemplate.modle.DetailGoods;
import com.example.a13001.shoppingmalltemplate.modle.EvaluateList;
import com.example.a13001.shoppingmalltemplate.modle.FilterGoods;
import com.example.a13001.shoppingmalltemplate.modle.GoodsDetail;
import com.example.a13001.shoppingmalltemplate.modle.GoodsEvaluateDetail;
import com.example.a13001.shoppingmalltemplate.modle.GoodsEvaluateList;
import com.example.a13001.shoppingmalltemplate.modle.GoodsList;
import com.example.a13001.shoppingmalltemplate.modle.GoodsParameters;
import com.example.a13001.shoppingmalltemplate.modle.IntegralCloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.IntegrationList;
import com.example.a13001.shoppingmalltemplate.modle.LoginStatus;
import com.example.a13001.shoppingmalltemplate.modle.Message;
import com.example.a13001.shoppingmalltemplate.modle.MessageDetail;
import com.example.a13001.shoppingmalltemplate.modle.MyCouponList;
import com.example.a13001.shoppingmalltemplate.modle.NewBanner;
import com.example.a13001.shoppingmalltemplate.modle.News;
import com.example.a13001.shoppingmalltemplate.modle.Notice;
import com.example.a13001.shoppingmalltemplate.modle.NoticeDetail;
import com.example.a13001.shoppingmalltemplate.modle.Order;
import com.example.a13001.shoppingmalltemplate.modle.OrderDetail;
import com.example.a13001.shoppingmalltemplate.modle.OrderNum;
import com.example.a13001.shoppingmalltemplate.modle.Recharge;
import com.example.a13001.shoppingmalltemplate.modle.RefreshYunFei;
import com.example.a13001.shoppingmalltemplate.modle.Result;
import com.example.a13001.shoppingmalltemplate.modle.ShopCarGoods;
import com.example.a13001.shoppingmalltemplate.modle.ShopCouponList;
import com.example.a13001.shoppingmalltemplate.modle.ShouHouDetail;
import com.example.a13001.shoppingmalltemplate.modle.SignIn;
import com.example.a13001.shoppingmalltemplate.modle.UpFile;
import com.example.a13001.shoppingmalltemplate.modle.User;
import com.example.a13001.shoppingmalltemplate.modle.UserInfo;
import com.example.a13001.shoppingmalltemplate.modle.WechatPay;


import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit.RetrofitHelper;
import retrofit.RetrofitService;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;

public class DataManager {
    private RetrofitService mRetrofitService;

    public DataManager(Context context) {
        this.mRetrofitService = RetrofitHelper.getIntance(context).getService();
    }
    /**
     * 获取资讯列表
     * @param companyid  站点ID
     * @param channelid  频道ID，指定多个频道用“|”线分割
     * @param pagesize  每页显示数量
     * @param pageindex  当前页数
     * @return
     */
    public Observable<News> getNewsList(String companyid, String channelid,String keyword,int pagesize, int pageindex){
        return mRetrofitService.getSearchNews(companyid,channelid,keyword,pagesize,pageindex);
    }
    /**
     * 获取首页轮播图
     * @param companyid  站点ID
     * @param label     碎片文件标识ID
     * @return
     */
    public Observable<Banner> getBannerList(String companyid, String label){
        return mRetrofitService.getBannerList(companyid,label);
    }
    /**
     * 获取首页底部分类商品列表
     * @param companyid   站点ID
     * @param channelid   频道ID，指定多个频道用“|”线分割
     * @param pagesize    每页显示数量
     * @param pageindex   当前页数
     * @return
     */
    public Observable<GoodsDetail> getClassifyList(String companyid, String channelid,int pagesize, int pageindex){
        return mRetrofitService.getClassifyList(companyid,channelid,pagesize,pageindex);
    }
    /**
     * 获取特定栏目下商品列表
     * @param companyid 站点ID
     * @param channelid  频道ID，指定多个频道用“|”线分割
     * @param classid   栏目ID，指定多个栏目用“|”线分割
     * @param elite    推荐调用方法 1 调用有推荐 2 调用不推荐，为空调用所有
     * @param xinpin   新品调用方法 1 调用新品 2 调用非新品，为空调用所有
     * @param pagesize  每页显示数量
     * @param pageindex  当前页数
     * @param order  	排序方式 1 上架时间正序 2 上架时间倒序 3 价格从低到高 4 价格从高到低
     *                  5 销量从低到高 6 销量从高到低 7 评价从低到高 8 评价从高到低 9 浏览次数从低到高 10 浏览次数从高到低
     * @return
     */

    public Observable<GoodsList> getGoodsList(String companyid, String channelid,String classid,String elite,String hot,String xinpin,String cuxiao,String order,
                                              String specialid,String price1,String price2,String keyword,String excludeid, int pagesize, int pageindex){
        return mRetrofitService.getGoodsList(companyid,channelid,classid,elite,hot,xinpin,cuxiao,order,specialid,price1,price2,keyword,excludeid,pagesize,pageindex);
    }

    /**
     * 获取商品详情
     * @param companyid  站点ID
     * @param id    内容ID
     * @return
     */
    public Observable<DetailGoods> getGoodsDetail(String companyid,int id){
        return mRetrofitService.getGoodsDetail(companyid,id);
    }
    /**
     * 获取商品属性
     * @param companyid  站点ID
     * @param id   内容ID
     * @return
     */
    public Observable<GoodsParameters> getGoodsParemters(String companyid,int id){
        return mRetrofitService.getGoodsPramter(companyid,id);
    }
    //注册
    public Observable<User> doRegister(String companyid,String code,String timestamp,Map<String, String> map){
        return mRetrofitService.doRegister(companyid,code,timestamp,map);
    }
    /**
     * ★ 获取图形验证码
     */

    public Observable<CommonResult> getPicCode(String companyid,String code,String timestamp){
        return mRetrofitService.getPicCode(companyid,code,timestamp);
    }
    /**
     * ★  验证会员注册信息-发送手机校验码
     */
    public Observable<CommonResult> getPhoneCode(String companyid,String code,String timestamp,String chkdata1,String chkdata2,String from){
        return mRetrofitService.getPhoneCode(companyid,code,timestamp,chkdata1,chkdata2,from);
    }
    /**
     * 找回密码-发送手机校验码
     * @param companyid
     * @param code
     * @param timestamp
     * @param chkData1       手机号
     * @param chkData2      图形验证码
     * @return
     */

    public Observable<CommonResult> getForgetPwdPhoneCode(String companyid,String code,String timestamp,String chkData1,String chkData2){
        return mRetrofitService.getForgetPwdPhoneCode(companyid,code,timestamp,chkData1,chkData2);
    }
    /**
     *★ 找回密码-设置新密码
     * @param companyid
     * @param code
     * @param timestamp
     * @param chkData1        手机（邮箱）校验码
     * @param chkData2       密码
     * @param chkData3       确认密码
     * @return
     */

    public Observable<CommonResult> doResetPwd(String companyid,String code,String timestamp,String chkData1,String chkData2,String chkData3){
        return mRetrofitService.doResetPwd(companyid,code,timestamp,chkData1,chkData2,chkData3);
    }
    /**
     * ★ 修改会员安全密码
     * @param companyid
     * @param code
     * @param timestamp
     * @param oldpass      旧密码，未设置交易密码时允许为空
     * @param newpass     新密码
     * @param confirmpass   确认新密码
     * @return
     */

    public Observable<CommonResult> doResetSafePwd(String companyid,String code,String timestamp,String oldpass,String newpass,String confirmpass){
        return mRetrofitService.doResetSafePwd(companyid,code,timestamp,oldpass,newpass,confirmpass);
    }
    /**
     * 登录
     * @param companyid  站点ID
     * @param code   安全校验码
     * @param timestamp  时间戳
     * @param name    会员名称，用户名/手机/邮箱
     * @param pwd    登录密码
     * @param from   来源，pc 电脑端 mobile 移动端
     * @return
     */
    public Observable<User> doLogin(String companyid,String code,String timestamp,String name,String pwd,String from){
        return mRetrofitService.doLogin(companyid,code,timestamp,name,pwd,from);
    }
    /**
     * 获取一级分类
     * @param companyid
     * @param channelid
     * @return
     */
    public Observable<Classify> getClassifyList(String companyid,String channelid){
        return mRetrofitService.getClassify(companyid,channelid);
    }

    /**
     * 加入购物车
     * @param companyid 站点ID
     * @param id   内容ID
     * @param h    货号
     * @param s     数量
     * @param r    操作类型，1 立刻购买 2 购买并进入购物车页面 3 加入购物车成功
     * @param t    商品类型 0 普通商品 1 促销秒杀 3 积分兑换
     * @param from  来源，pc 电脑端 mobile 移动端
     * @return
     */
    public Observable<Result> addShopCar(String companyid, int id, String h, int s, int r, int t, String from){
        return mRetrofitService.addShopCar(companyid,id,h,s,r,t,from);
    }
    /**
     *删除购物车商品
     * @param companyid  站点ID
     * @param id         商品ID，多个用“,”号分割
     * @return
     */
    public Observable<CommonResult> deleteShopCar(String companyid, String id){
        return mRetrofitService.deleteShopCar(companyid,id);
    }
    /**
     * 获取购物车商品信息
     * @param companyid  站点ID
     * @param from   来源，pc 电脑端 mobile 移动端
     * @return
     */
    public Observable<ShopCarGoods> getShopCarGoods(String companyid, String from){
        return mRetrofitService.getShopCarGoods(companyid,from);
    }
    /**
     * 更新购物车数量
     * @param companyid   站点ID
     * @param id          内容ID
     * @param sum        更新后数量
     * @return
     */
    public Observable<CommonResult> updateShopCarNum(String companyid,int id,int sum){
        return mRetrofitService.updateShopCarNum(companyid,id,sum);
    };
    /**
     * 结算
     * @param companyid   站点ID
     * @param code        安全校验码
     * @param timestamp   时间戳
     * @param cartid     购物车结算ID，多个用“,”号分割
     * @param from       来源，pc 电脑端 mobile 移动端
     * @return
     */
    public Observable<CloaseAccount> doCloaseAccount(String companyid, String code, String timestamp, String cartid, String from){
        return mRetrofitService.doCloaseAccount(companyid,code,timestamp,cartid,from);
    }
    /**
     * 提交商品订单（实物商品）
     * @param companyid    站点ID
     * @param code         安全校验码
     * @param timestamp 时间戳
     * @param cartid      购物车结算ID，多个用“,”号分割
     * @param addressid    收货人地址ID
     * @param paymentname    支付方式ID
     * @param ordertype    送货方式
     * @param invoicestatus   是否开票发票 1 开发票 2 不开发票
     * @param invoicename     发票抬头，invoicestatus为1时必填
     * @param remark           备注
     * @param from           来源，pc 电脑端 mobile 移动端
     * @return
     */
    public Observable<CommonResult> doCommitOrder(String companyid, String code, String timestamp, String cartid,int addressid,String paymentname,String  ordertype,int invoicestatus,String invoicename,String snph,String remark,String from){
        return mRetrofitService.doCommitOrder(companyid,code,timestamp,cartid,addressid,paymentname,ordertype,invoicestatus,invoicename,snph,remark,from);
    }
    /**
     * 判断登录状态
     * @param companyid  站点ID
     * @param code    安全校验码
     * @param timestamp  时间戳
     * @param from   来源，pc 电脑端 mobile 移动端
     * @return
     */
    public Observable<LoginStatus> getLoginStatus(String companyid,String code,String timestamp, String from) {
        return mRetrofitService.getLoginStatus(companyid,code,timestamp,from);
    }
    /**
     * 第三方登录
     * @param companyid   站点ID
     * @param code        安全校验码
     * @param timestamp   时间戳
     * @param type        qqlogin QQ登录， wxlogin 微信登录，sinalogin 新浪微博登录
     * @param openid      第三方登录用户唯一值
     * @param nickname    会员昵称
     * @param sex          会员性别，1 男，2 女
     * @param headimgurl   头像地址
     * @return
     */
    public Observable<User> doThirdLogin(String companyid,String code,String timestamp, String type,String openid,String nickname,String sex,String headimgurl,String appos,String appkey ) {
        return mRetrofitService.doThirdLogin(companyid,code,timestamp,type,openid,nickname,sex,headimgurl,appos,appkey);
    }
    /**
     * ★ 修改会员登录密码
     * @param companyid
     * @param code
     * @param timestamp
     * @param oldpass       旧密码，如果是第三方登录首次修改密码，值可以为空
     * @param newpass       新密码
     * @param confirmpass    确认新密码
     * @return
     */

    public Observable<CommonResult> modifyPwd(String companyid,String code,String timestamp,String oldpass,String newpass,String confirmpass) {
        return mRetrofitService.modifyPwd(companyid,code,timestamp,oldpass,newpass,confirmpass);
    }
    /**
     *★ 绑定手机号码
     * @param companyid
     * @param code
     * @param timestamp
     * @param chkData1     手机号
     * @param chkData2      手机验证码
     * @param chkData3       安全密码
     * @return
     */
    public Observable<CommonResult> bindPhone(String companyid,String code,String timestamp,String chkData1,String chkData2,String chkData3) {
        return mRetrofitService.bindPhone(companyid,code,timestamp,chkData1,chkData2,chkData3);
    }
    /**
     *★ 绑定手机号码-发送手机校验码
     * @param companyid
     * @param code
     * @param timestamp
     * @param chkData1     手机号
     * @param chkData2      安全密码
     *
     * @return
     */
    public Observable<CommonResult> bindPhoneSendCode(String companyid,String code,String timestamp,String chkData1,String chkData2) {
        return mRetrofitService.bindPhoneSendCode(companyid,code,timestamp,chkData1,chkData2);
    }
    /**
     * 获取会员详细信息
     * @param companyid    站点ID
     * @param code          安全校验码
     * @param timestamp     时间戳
     * @return
     */
    public Observable<UserInfo> getUserInfo(String companyid, String code, String timestamp) {
        return mRetrofitService.getuserInfo(companyid,code,timestamp);
    }

    /**
     * 会员资料修改
     * @param companyid
     * @param code
     * @param timestamp
     * @param map
     * @return
     */
    public Observable<CommonResult> ModifyUserInfo(String companyid, String code, String timestamp,Map<String,String> map) {
        return mRetrofitService.modifyUserInfo(companyid,code,timestamp,map);
    }
    /**
     *  获取会员收货地址
     * @param companyid   站点ID
     * @param code   安全校验码
     * @param timestamp  时间戳
     * @param pagesize   每页显示数量
     * @param pageindex   当前页数
     * @param addressid   收货地址ID
     * @param main   为1时加载默认收货地址，其它调用所有
     * @return
     */

    public Observable<Address> getAddressList(String companyid,String code,String timestamp,int pagesize,int pageindex,int main,String addressid){
        return mRetrofitService.getAddress(companyid,code,timestamp,pagesize,pageindex,main,addressid);
    }
    /**
     * 编辑收货地址
     * @param companyid    站点ID
     * @param code         安全校验码
     * @param timestamp    时间戳
     * @param addressid 收货地址ID，为0时是添加新地址，大于0是修改指定ID收货地址
     * @param addressName  收货人名字
     * @param addressPhone  收货人手机
     * @param addressProvince  收货人所在省/市
     * @param addressCity      收货人所在市/区
     * @param addressArea      收货人所在区/县
     * @param addressXX        收货人详细地址
     * @param addressZipcode   收货人邮编
     * @param addressDefault    默认收货地址 0 否 1 是

     * @return
     */

    public Observable<CommonResult> editAddress(String companyid,String code,String timestamp,int addressid,String addressName,String addressPhone,String addressProvince,String addressCity,String addressArea,String addressXX,String addressZipcode,int addressDefault){
        return mRetrofitService.editAddress(companyid,code,timestamp,addressid,addressName,addressPhone,addressProvince,addressCity,addressArea,addressXX,addressZipcode,addressDefault);
    }
    /**
     * 删除会员收货地址
     * @param companyid   站点ID
     * @param code      安全校验码
     * @param timestamp  时间戳
     * @param addressid   收货地址ID
     * @return
     */

    public Observable<CommonResult> deleteAddress(String companyid,String code,String timestamp,String addressid){
        return mRetrofitService.deleteAddress(companyid,code,timestamp,addressid);
    }
    /**
     * 设置默认会员收货地址
     * @param companyid   站点ID
     * @param code        安全校验码
     * @param timestamp   时间戳
     * @param addressid   收货地址ID
     * @return
     */
    public Observable<CommonResult> setDefaultAddress(String companyid,String code,String timestamp,String addressid){
        return mRetrofitService.setDefaultAddress(companyid,code,timestamp,addressid);
    }
    /**
     *  获取普通订单列表
     * @param companyid   站点ID
     * @param code        安全校验码
     * @param timestamp   时间戳
     * @param pagesize    每页显示数量
     * @param pageindex   当前页数
     * @param status    查询订单状态，1 待支付 2 未发货 3 已发货 4 已签收
     * @param ordersNumber   查询订单号，支持模糊查询
     * @param starttime    指定开始时间（订单下单时间区间查询）
     * @param endtime     指定结束时间（订单下单时间区间查询）
     * @param from    来源，pc 电脑端 mobile 移动端
     * @return
     */

    public Observable<Order> getOrderList(String companyid, String code, String timestamp, int pagesize, int pageindex, String status, String ordersNumber, String starttime, String endtime,String type, String from){
        return mRetrofitService.getOrderList(companyid,code,timestamp,pagesize,pageindex,status,ordersNumber,starttime,endtime,type,from);
    }
    /**
     * 查看普通订单详情
     * @param companyid   站点ID
     * @param code         安全校验码
     * @param timestamp    时间戳
     * @param ordersNumber  订单号
     * @return
     */
    public Observable<OrderDetail> getOrderDetail(String companyid, String code, String timestamp, String ordersNumber, String from){
        return mRetrofitService.getOrderDetail(companyid,code,timestamp,ordersNumber,from);
    }
    /**
     * 取消未支付订单
     * @param companyid   站点ID
     * @param code         安全校验码
     * @param timestamp    时间戳
     * @param ordersNumber  订单号
     * @return
     */
    public Observable<CommonResult> cancelOrder(String companyid, String code, String timestamp,String ordersNumber){
        return mRetrofitService.cancelOrder(companyid,code,timestamp,ordersNumber);
    }
    /**
     * 订单确认收货
     * @param companyid   站点ID
     * @param code         安全校验码
     * @param timestamp    时间戳
     * @param ordersNumber  订单号
     * @return
     */
    public Observable<CommonResult> affirmOrder(String companyid, String code, String timestamp,String ordersNumber){
        return mRetrofitService.affirmOrder(companyid,code,timestamp,ordersNumber);
    }
    /**
     * 内容收藏
     * @param companyid
     * @param id   内容ID
     * @return
     */

    public Observable<CommonResult> setCollect(String companyid,int id){
        return mRetrofitService.setCollect(companyid,id);
    }
    /**
     * 内容收藏判断
     * @param companyid
     * @param id   内容ID
     * @return
     */
    @GET("/api/json/content/content.ashx?action=checkcollection")
    public Observable<CommonResult> ifCollect(String companyid,int id){
        return mRetrofitService.ifCollect(companyid,id);
    }
    /**
     * 微信支付
     * @param companyid   站点ID
     * @param code    安全校验码
     * @param timestamp  timestamp
     * @param ordernumber ordernumber
     * @param ip   ip
     * @return
     */
    public Observable<WechatPay> wechatPay(String companyid, String code, String timestamp, String ordernumber, String ip){
        return mRetrofitService.wechatPay(companyid,code,timestamp,ordernumber,ip);
    }
    /**
     * 获取会员收藏列表
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @return
     */
    public Observable<Collect> getLoveList(String companyid,String code,String timestamp,int pagesize,int pageindex){
        return mRetrofitService.getLoveList(companyid,code,timestamp,pagesize,pageindex);
    }
    /**
     *  删除会员收藏内容
     * @param companyid
     * @param code
     * @param timestamp
     * @param collectlId
     * @return
     */
    public Observable<CommonResult> doDeleteCollection(String companyid,String code,String timestamp,int collectlId){
        return mRetrofitService.doDeleteCollection(companyid,code,timestamp,collectlId);
    }
    /**
     * 上传会员头像（APP）
     * @param companyid
     * @param code
     * @param timestamp
     * @param face  头像数据，base64码
     * @return
     */
    public Observable<CommonResult> upLoadHeadImg(String companyid,String code,String timestamp,String face){
        return mRetrofitService.upLoadHeadImg(companyid,code,timestamp,face);
    }
    /**
     * ★ 获取会员消息列表
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @return
     */

    public Observable<Message> getSmsList(String companyid,String code,String timestamp,String status,int pagesize,int pageindex){
        return mRetrofitService.getSmsList(companyid,code,timestamp,status,pagesize,pageindex);
    }
    /**
     * ★ 获取会员消息详情
     * @param companyid
     * @param code
     * @param timestamp
     * @param smsid   会员消息ID
     * @return
     */

    public Observable<MessageDetail> getSmsContent(String companyid,String code,String timestamp,int smsid){
        return mRetrofitService.getSmsContent(companyid,code,timestamp,smsid);
    }
    /**
     * ★ 删除会员消息
     * @param companyid
     * @param code
     * @param timestamp
     * @param smsid   会员消息ID
     * @return
     */

    public Observable<CommonResult> doSmsDelete(String companyid,String code,String timestamp,int smsid){
        return mRetrofitService.doSmsDelete(companyid,code,timestamp,smsid);
    }
    /**
     * 获取会员通知列表
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @return
     */

    public Observable<Notice> getNoticeList(String companyid,String code,String timestamp,int pagesize,int pageindex){
        return mRetrofitService.getNoticeList(companyid,code,timestamp,pagesize,pageindex);
    }
    /**
     *
     * @param companyid
     * @param code
     * @param timestamp
     * @param noticeid 会员通知ID
     * @return
     */

    public Observable<NoticeDetail> getNoticeDetail(String companyid,String code,String timestamp,int noticeid){
        return mRetrofitService.getNoticeDetail(companyid,code,timestamp,noticeid);
    }
    /**
     * 内容高级筛选
     * @param companyid
     * @param channelid          频道ID
     * @param orderstatus        是否开启排序搜索，1 开启 0 关闭
     * @param orderlist          指定排序方法，请参考示例格式，可以隐藏部分
     * @param classstatus       是否开启栏目搜索，1 开启 0 关闭
     * @param parentclassid      上级栏目ID，为0和为空调用一级栏目
     * @param classtitle         指定栏目搜索名称，不指定则返回默认值
     * @param specialstatus     是否开启专题搜索，1 开启 0 关闭
     * @param specialtitle      指定专题搜索名称，不指定则返回默认值
     * @param fieldstatus          是否开启自定义字段筛选，1 开启 0 关闭
     * @param pricestatus           是否开启价格区间搜索，1 开启 0 关闭
     * @param pricetitle          指定价格区间搜索名称，不指定则返回默认值
     * @param pricelist           指定价格区间金额，请参考示例格式，可以隐藏部分
     * @param attributestatus        是否开启属性搜索，1 开启 0 关闭
     * @param attributetitle       指定属性方法搜索名称，不指定则返回默认值
     * @param attributelist        指定属性方法，请参考示例格式，可以隐藏部分
     * @return
     */

    public Observable<ContentFilter> getFilterList(String companyid,String channelid,String orderstatus,String orderlist,String classstatus,String parentclassid,String classtitle,String specialstatus,String specialtitle,
                                                   String fieldstatus,String pricestatus,String pricetitle,String pricelist,String attributestatus,String attributetitle,String attributelist){
        return mRetrofitService.getFilterList(companyid,channelid,orderstatus,orderlist,classstatus,parentclassid,classtitle,specialstatus,specialtitle,fieldstatus,pricestatus,pricetitle,pricelist,attributestatus,attributetitle,attributelist);
    }
    /**
     * 签到
     * @param companyid
     * @param code
     * @param timestamp
     * @return
     */

    public Observable<SignIn> doSignIn(String companyid,String code,String timestamp){
        return mRetrofitService.doSignIn(companyid,code,timestamp);
    }
    /**
     * ★ 提交积分兑换订单
     * @param companyid    站点ID
     * @param code         安全校验码
     * @param timestamp 时间戳
     * @param cartid      购物车结算ID，多个用“,”号分割
     * @param addressid    收货人地址ID
     * @param remark           备注
     * @param from           来源，pc 电脑端 mobile 移动端
     * @return
     */

    public Observable<CommonResult> doCommitIntegralOrder(String companyid,String code,String timestamp,String cartid,int addressid,String remark,String from){
        return mRetrofitService.doCommitIntegralOrder(companyid,code,timestamp,cartid,addressid,remark,from);
    }
    /**
     * ★ 进行积分兑换结算
     * @param companyid   站点ID
     * @param code        安全校验码
     * @param timestamp   时间戳
     * @param cartid     购物车结算ID，多个用“,”号分割
     * @param from       来源，pc 电脑端 mobile 移动端
     * @return
     */

    public Observable<CloaseAccount> doCloaseIntegralAccount(String companyid, String code, String timestamp, String cartid, String from){
        return mRetrofitService.doCloaseIntegralAccount(companyid,code,timestamp,cartid,from);
    }
    /**
     *★ 获取积分兑换订单列表
     * @param companyid  站点ID
     * @param code           安全校验码
     * @param timestamp     时间戳
     * @param pagesize     每页显示数量
     * @param pageindex    当前页数
     * @param status        查询订单状态，1 待支付 2 未发货 3 已发货 4 已签收
     * @param ordersNumber  查询订单号，支持模糊查询
     * @param starttime      指定开始时间（订单下单时间区间查询）
     * @param endtime       指定结束时间（订单下单时间区间查询）
     * @param from            来源，pc 电脑端 mobile 移动端
     * @return
     */
    public Observable<Order> getIntegralOrderList(String companyid, String code, String timestamp, int pagesize,int pageindex,String status,String ordersNumber,String starttime,String endtime, String from){
        return mRetrofitService.getIntegralOrderList(companyid,code,timestamp,pagesize,pageindex,status,ordersNumber,starttime,endtime,from);
    }
    /**
     * ★ 查看积分兑换订单详情
     * @param companyid    站点ID
     * @param code          安全校验码
     * @param timestamp       时间戳
     * @param ordersNumber    订单号
     * @param from      来源，pc 电脑端 mobile 移动端
     * @return
     */

    public Observable<OrderDetail> getIntegralOrderDetail(String companyid, String code, String timestamp,String ordersNumber, String from){
        return mRetrofitService.getIntegralOrderDetail(companyid,code,timestamp,ordersNumber,from);
    }
    /**
     * ★ 积分兑换订单确认收货
     * @param companyid
     * @param code
     * @param timestamp
     * @param ordersNumber
     * @return
     */

    public Observable<CommonResult> doSureIntegralOrder(String companyid, String code, String timestamp,String ordersNumber){
        return mRetrofitService.doSureIntegralOrder(companyid,code,timestamp,ordersNumber);
    }
    /**
     * ★ 发布评论/咨询信息
     * @param companyid
     * @param code
     * @param timestamp
     * @param id                     评论内容ID
     * @param commentname            评论人名字，为空时会取会员昵称，如果未登录，则默认为游客
     * @param commentcontent           评论内容
     * @param commentimages              评论图片，多个用“,”号分割
     * @return
     */

    public Observable<CommonResult> doCommentAdd(String companyid, String code, String timestamp,String id,String commentname,String commentcontent,String commentimages){
        return mRetrofitService.doCommentAdd(companyid,code,timestamp,id,commentname,commentcontent,commentimages);
    }

    /**
     * 获取评论/咨询列表信息
     * @param companyid      站点ID
     * @param id            评论内容ID
     * @param keyword       搜索关键词，模糊搜索评论人或评论内容
     * @param order         排序方式 0 按评论时间正序 1 按评论时间倒序 2 按支持数正序 3 按支持数倒序
     * @param pagesize      每页显示数量
     * @param pageindex      当前页数
     * @param from           来源，pc 电脑端 mobile 移动端 app app端 xcx 微信小程序
     * @return
     */

    public Observable<AnswerList> getCommentList(String companyid, String id, String keyword, String order, int pagesize, int pageindex, String from){
        return mRetrofitService.getCommentList(companyid,id,keyword,order,pagesize,pageindex,from);
    }
    /**
     * 获取商城优惠券列表
     * @param companyid       站点ID(必传)
     * @param storeid        商家分店ID（非必传）
     * @param type           限制类型 1 仅限注册会员 2 仅限新会员（新注册，未消费过）（非必传）
     * @param pagesize      每页显示数量（非必传）
     * @param pageindex         当前页数	（非必传）
     * @return
     */
    public Observable<ShopCouponList> getCouponList(String companyid, String storeid, String type, int pagesize, int pageindex){
        return mRetrofitService.getCouponList(companyid,storeid,type,pagesize,pageindex);
    }
    /**
     * ★ 领取商城优惠券
     * @param companyid   站点ID
     * @param code
     * @param timestamp
     * @param aid           优惠券活动ID
     * @return
     */

    public Observable<CommonResult> obtainCoupon(String companyid, String code, String timestamp,int aid){
        return mRetrofitService.obtainCoupon(companyid,code,timestamp,aid);
    }
    /**
     * ★ 获取会员商城优惠券数量
     * @param companyid
     * @param code
     * @param timestamp
     * @param storeid             商家分店ID
     * @param money            筛选符合订单金额的优惠券（大于等于该金额）
     * @param status          优惠券状态，1 未使用 2 已使用，为空则所有
     * @return
     */

    public Observable<CouponCount> getCouponCount(String companyid, String code, String timestamp, int storeid, int money, int status){
        return mRetrofitService.getCouponCount(companyid,code,timestamp,storeid,money,status);
    }
    /**
     * ★ 获取会员商城优惠券列表
     * @param companyid
     * @param code
     * @param timestamp
     * @param storeid              商家分店ID
     * @param money             筛选符合订单金额的优惠券（大于等于该金额）
     * @param status         优惠券状态，1 未使用 2 已使用，为空则所有
     * @param pagesize        每页显示数量
     * @param pageindex      当前页数
     * @return
     */

    public Observable<MyCouponList> getMyCouponList(String companyid, String code, String timestamp,
                                                    String storeid, String money, String status, int pagesize, int pageindex){
        return mRetrofitService.getMyCouponList(companyid,code,timestamp,storeid,money,status,pagesize,pageindex);
    }
    /**
     * 退出登录
     * @param companyid
     * @return
     */
    public Observable<CommonResult> doLoginOut(String companyid){
        return mRetrofitService.doLoginOut(companyid);
    }
    /**
     * ★ 获取会员积分明细
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @param startdate    指定开始时间
     * @param overdate       指定结束时间
     * @return
     */

    public Observable<IntegrationList> getIntegrationList(String companyid, String code, String timestamp, int pagesize, int pageindex, String startdate, String overdate){
        return mRetrofitService.getIntegrationList(companyid,code,timestamp,pagesize,pageindex,startdate,overdate);
    }
    /**
     ★ 获取会员资金明细
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @param startdate    指定开始时间
     * @param overdate       指定结束时间
     * @return
     */
    public Observable<Account> getTransactionList(String companyid, String code, String timestamp, int pagesize, int pageindex, String startdate, String overdate){
        return mRetrofitService.getTransactionList(companyid,code,timestamp,pagesize,pageindex,startdate,overdate);
    }
    /**
     * ★ 获取会员充值交易列表
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @param startdate
     * @param overdate
     * @return
     */

    public Observable<Recharge> getRechargeList(String companyid, String code, String timestamp, int pagesize, int pageindex, String startdate, String overdate){
        return mRetrofitService.getRechargeList(companyid,code,timestamp,pagesize,pageindex,startdate,overdate);
    }
    /**
     * 获取商品评价列表信息
     * @param companyid
     * @param id         评价商品ID
     * @param level      评价等级，1 好评 2 中评 3 差评，为空则调用所有
     * @param pagesize
     * @param pageindex
     * @param order           排序方式 0 按评价时间正序 1 按评价时间倒序 2 按支持数正序 3 按支持数倒序
     * @param from
     * @return
     */

    public Observable<EvaluateList> getEvaluateList(String companyid, int id,String level, String order,int pagesize, int pageindex, String from){
        return mRetrofitService.getEvaluateList(companyid,id,level,order,pagesize,pageindex,from);
    }
    /**
     * ★ 提交商品评价信息
     * @param companyid
     * @param code
     * @param timestamp
     * @param cartid           商品购物车ID
     * @param commodityid      商品ID
     * @param commentcontent   商品评价内容
     * @param commentlevel        商品评价星级
     * @param commentlabel       商品评价标签，多个用“|”线分割
     * @param commentimages        商品评价晒图
     * @return
     */
    public Observable<CommonResult> commitEvaluate(String companyid, String code, String timestamp,int cartid,int commodityid,String commentcontent,int commentlevel,String commentlabel,String commentimages){
        return mRetrofitService.commitEvaluate(companyid,code,timestamp,cartid,commodityid,commentcontent,commentlevel,commentlabel,commentimages);
    }

    /**
     * 上传文件
     * @param companyid

     * @return
     */
    public Observable<UpFile> upFile(String companyid,String code,String timestamp,MultipartBody.Part file){
        return mRetrofitService.upFile(companyid,code,timestamp,file);
    }
    /**
     *售后商品列表
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @param status
     * @param repairsid
     * @param startdate
     * @param overdate
     * @param from
     * @return
     */

    public Observable<AfterSale> getAfterSaleList(String companyid, String code, String timestamp, int pagesize, int pageindex, int status,
                                                  String repairsid, String startdate, String overdate, String from){
        return mRetrofitService.getAfterSaleList(companyid,code,timestamp,pagesize,pageindex,status,repairsid,startdate,overdate,from);
    }
    /**
     * ★ 订单号获取商品收货地址
     * @param companyid
     * @param code
     * @param timestamp
     * @param ordersid
     * @return
     */
    public Observable<AddressOrderId> getAddressOrderId(String companyid, String code, String timestamp, String ordersid){
        return mRetrofitService.getAddressOrderId(companyid,code,timestamp,ordersid);
    }
    /**
     * ★ 提交商品售后服务
     * @param companyid
     * @param code
     * @param timestamp
     * @param cartid        商品购物车ID
     * @param commodityid    商品ID
     * @param ordersid       商品订单ID
     * @param repairstype   售后服务类型，1 退货 2 换货 3 维修
     * @param repairsnumber      申请售后商品数量
     * @param repairsproof          申请售后商品申请凭据
     * @param repairscontent     申请售后商品原因
     * @param repairsimages      申请售后商品图片，多张图片用“|”线分割
     * @param repairsname       申请人名字
     * @param repairsphone           申请人电话
     * @param Receiptaddress      申请人地址（格式：省/市,市/区,区/县,详细地址）
     * @param repairszipcode     申请人邮编
     * @return
     */

    public Observable<CommonResult> commitShouHou(String companyid,String code,String timestamp,int cartid,int commodityid,String ordersid,
                                                  int repairstype,int repairsnumber,String repairsproof,String repairscontent,String repairsimages,
                                                  String repairsname,String repairsphone,String Receiptaddress,String repairszipcode){
        return mRetrofitService.commitShouHou(companyid,code,timestamp,cartid,commodityid,ordersid,repairstype,repairsnumber,repairsproof,repairscontent,repairsimages,
                repairsname,repairsphone,Receiptaddress,repairszipcode);
    }
    /**
     * ★ 运费刷新（更换收货地址时）
     * @param companyid
     * @param code
     * @param timestamp
     * @param cartid
     * @param addresslid
     * @return
     */

    public Observable<RefreshYunFei> refreshYunFei(String companyid, String code, String timestamp, String cartid, String addresslid){
        return mRetrofitService.refreshYunFei(companyid,code,timestamp,cartid,addresslid);
    }
    /**
     * ★ 获取售后服务单详情
     * @param companyid
     * @param code
     * @param timestamp
     * @param cartid
     * @param commodityid
     * @param from
     * @return
     */

    public Observable<ShouHouDetail> getShouHouDetail(String companyid, String code, String timestamp, String cartid, String commodityid, String from){
        return mRetrofitService.getShouHouDetail(companyid,code,timestamp,cartid,commodityid,from);
    }
    /**
     * ★ 支付宝支付（APP）
     * @param companyid
     * @param code
     * @param timestamp
     * @param ordernumber
     * @return
     */
    public Observable<Alipay> doAlipay(String companyid, String code, String timestamp, String ordernumber){
        return mRetrofitService.doAlipay(companyid,code,timestamp, ordernumber);
    }
    /**
     * ★ 获取评价商品列表
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @param status            查询评价状态，0 待评价 1 已评价，为空则所有
     * @param from
     * @return
     */

    public Observable<GoodsEvaluateList> getGoodsEvaluateList(String companyid, String code, String timestamp, int pagesize, int pageindex, String status, String from){
        return mRetrofitService.getGoodsEvaluateList(companyid,code,timestamp, pagesize,pageindex,status,from);
    }
    /**
     *★ 获取评价商品详情
     * @param companyid
     * @param code
     * @param timestamp
     * @param cartid    商品购物车ID
     * @param from
     * @return
     */

    public Observable<GoodsEvaluateDetail> getGoodsEvaluateDetail(String companyid, String code, String timestamp, String cartid, String from){
        return mRetrofitService.getGoodsEvaluateDetail(companyid,code,timestamp, cartid,from);
    }
    /**
     *获取原生版定制APP配置
     * @param companyid
     * @return
     */

    public Observable<AppConfig> getAppConfig(String companyid){
        return mRetrofitService.getAppConfig(companyid);
    }
    /**
     *获取原生版定制APP轮播图
     * @param companyid
     * @return
     */
    public Observable<NewBanner> getNewBanner(String companyid){
        return mRetrofitService.getNewBanner(companyid);
    }
    public Observable<OrderNum> getOrderNum(String companyid, String code, String timestamp, String type, String status){
        return mRetrofitService.getOrderNum(companyid,code,timestamp, type,status);
    }
    /**
     * 统计原生版定制APP安装
     * @param companyid
     * @param code
     * @param timestamp
     * @param appos             APP类型，IOS 苹果 Android 安卓
     * @param appkey            APP唯一编码
     * @param appipaddress     IP地址
     * @return
     */

    public Observable<CommonResult> getAppTongji(String companyid, String code, String timestamp,String appos,String appkey,String appipaddress){
        return mRetrofitService.getAppTongji(companyid,code,timestamp,appos,appkey,appipaddress);
    }
}
