package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class Specification {
    private String title;
    private List<GoodsParameters.ShopAttributeBean> mList;

    public Specification(String title, List<GoodsParameters.ShopAttributeBean> mList) {
        this.title = title;
        this.mList = mList;
    }

    public List<GoodsParameters.ShopAttributeBean> getmList() {
        return mList;
    }

    public void setmList(List<GoodsParameters.ShopAttributeBean> mList) {
        this.mList = mList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
