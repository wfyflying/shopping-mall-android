package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.activitys.GoEvaluateActivity;
import com.example.a13001.shoppingmalltemplate.activitys.GooodsEvaluateDetailActivity;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.GoodsEvaluateList;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Administrator on 2017/8/9.
 */

public class GoodsEvaluateListLvAdapter extends BaseAdapter {
    private Context mContext;
    private List<GoodsEvaluateList.OrderGoodsListBean> mList;
    private int mOrderStatus;

    public GoodsEvaluateListLvAdapter(Context context, List<GoodsEvaluateList.OrderGoodsListBean> mList) {
        this.mContext = context;
        this.mList = mList;

    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolder vh = null;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_lv_goodsevaluate, parent, false);
            vh = new ViewHolder(view);
            view.setTag(vh);
            AutoUtils.autoSize(view);
        } else {
            vh = (ViewHolder) view.getTag();
        }
        vh.tvGoodsevaluateGoodsname.setText(mList.get(position).getCommodityName()!=null?mList.get(position).getCommodityName():"");
        GlideUtils.setNetImage(mContext, AppConstants.INTERNET_HEAD+mList.get(position).getCartImg(), vh.ivGoodsevaluateGoodsimage);
        vh.tvGoodsevaluateGoodsguige.setText(mList.get(position).getCommodityProperty() != null ? mList.get(position).getCommodityProperty() : "");
        vh.tvGoodsevaluateGoodsdate.setText(mList.get(position).getOrderDate()!= null ? mList.get(position).getOrderDate() : "");
        vh.tvGoodsevaluateGoodscount.setText("数量X" + mList.get(position).getCommodityNumber());

        //评价状态：0 未评价 >0 已评价
        int status = mList.get(position).getCommentStatus();
            if (status>0){
                vh.tvGoodsevaluateState.setText("已评价");
            }else{
                vh.tvGoodsevaluateState.setText("去评价");
            }
        final ViewHolder finalVh = vh;
        vh.tvGoodsevaluateState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String trim = finalVh.tvGoodsevaluateState.getText().toString().trim();
                if ("已评价".equals(trim)){
                    Intent intent=new Intent(mContext, GooodsEvaluateDetailActivity.class);
                    intent.putExtra("cartid",mList.get(position).getCartId());
                    mContext.startActivity(intent);
                }else if ("去评价".equals(trim)){
                    Intent intent=new Intent(mContext, GoEvaluateActivity.class);
                    intent.putExtra("cartid",mList.get(position).getCartId());
                    intent.putExtra("commodityId",mList.get(position).getCommodityId());
                    intent.putExtra("cartimg",mList.get(position).getCartImg());
                    intent.putExtra("number",mList.get(position).getCommodityNumber());
//                    intent.putExtra("price",mList.get(position).getCommodityXPrice());
                    intent.putExtra("property",mList.get(position).getCommodityProperty());
                    intent.putExtra("name",mList.get(position).getCommodityName());
                    mContext.startActivity(intent);
                }

            }
        });
        return view;
    }


    static class ViewHolder {
        @BindView(R.id.tv_goodsevaluate_goodsname)
        TextView tvGoodsevaluateGoodsname;
        @BindView(R.id.tv_goodsevaluate_state)
        TextView tvGoodsevaluateState;
        @BindView(R.id.iv_goodsevaluate_goodsimage)
        ImageView ivGoodsevaluateGoodsimage;
        @BindView(R.id.tv_goodsevaluate_goodsguige)
        TextView tvGoodsevaluateGoodsguige;
        @BindView(R.id.tv_goodsevaluate_goodscount)
        TextView tvGoodsevaluateGoodscount;
        @BindView(R.id.tv_goodsevaluate_goodsdate)
        TextView tvGoodsevaluateGoodsdate;
        @BindView(R.id.tv_count2)
        TextView tvCount2;
        @BindView(R.id.tv_totalprice)
        TextView tvTotalprice;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
