package com.example.a13001.shoppingmalltemplate.modle;

public class MessageDetail {

    /**
     * status : 1
     * returnMsg : null
     * smsId : 5
     * smsTitle : 你有一个新消息，请仔细查收。
     * smsAddtime : 2018-06-15 09:32
     * smsContent : <p>你有一个新消息，请仔细查收。</p><p>你有一个新消息，请仔细查收。</p><p>你有一个新消息，请仔细查收。</p>
     * smsStatus : 1
     */

    private int status;
    private Object returnMsg;
    private int smsId;
    private String smsTitle;
    private String smsAddtime;
    private String smsContent;
    private int smsStatus;

    @Override
    public String toString() {
        return "MessageDetail{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", smsId=" + smsId +
                ", smsTitle='" + smsTitle + '\'' +
                ", smsAddtime='" + smsAddtime + '\'' +
                ", smsContent='" + smsContent + '\'' +
                ", smsStatus=" + smsStatus +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getSmsId() {
        return smsId;
    }

    public void setSmsId(int smsId) {
        this.smsId = smsId;
    }

    public String getSmsTitle() {
        return smsTitle;
    }

    public void setSmsTitle(String smsTitle) {
        this.smsTitle = smsTitle;
    }

    public String getSmsAddtime() {
        return smsAddtime;
    }

    public void setSmsAddtime(String smsAddtime) {
        this.smsAddtime = smsAddtime;
    }

    public String getSmsContent() {
        return smsContent;
    }

    public void setSmsContent(String smsContent) {
        this.smsContent = smsContent;
    }

    public int getSmsStatus() {
        return smsStatus;
    }

    public void setSmsStatus(int smsStatus) {
        this.smsStatus = smsStatus;
    }
}
