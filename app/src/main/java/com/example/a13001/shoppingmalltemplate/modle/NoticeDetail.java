package com.example.a13001.shoppingmalltemplate.modle;

public class NoticeDetail {

    /**
     * status : 1
     * returnMsg : null
     * noticeId : 2
     * noticeTitle : 所有人来看看
     * noticeAddtime : 2017-07-19 16:03
     * noticeContent : <p>所有人来看看</p>
     */

    private int status;
    private Object returnMsg;
    private int noticeId;
    private String noticeTitle;
    private String noticeAddtime;
    private String noticeContent;

    @Override
    public String toString() {
        return "NoticeDetail{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", noticeId=" + noticeId +
                ", noticeTitle='" + noticeTitle + '\'' +
                ", noticeAddtime='" + noticeAddtime + '\'' +
                ", noticeContent='" + noticeContent + '\'' +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(int noticeId) {
        this.noticeId = noticeId;
    }

    public String getNoticeTitle() {
        return noticeTitle;
    }

    public void setNoticeTitle(String noticeTitle) {
        this.noticeTitle = noticeTitle;
    }

    public String getNoticeAddtime() {
        return noticeAddtime;
    }

    public void setNoticeAddtime(String noticeAddtime) {
        this.noticeAddtime = noticeAddtime;
    }

    public String getNoticeContent() {
        return noticeContent;
    }

    public void setNoticeContent(String noticeContent) {
        this.noticeContent = noticeContent;
    }
}
