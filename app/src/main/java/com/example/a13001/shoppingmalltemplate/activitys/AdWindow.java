package com.example.a13001.shoppingmalltemplate.activitys;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.MainActivity;
import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.ProgressWebView;
import com.example.a13001.shoppingmalltemplate.application.ShoppingMallTemplateApplication;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.utils.Utils;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 2017/5/8.
 */

public class AdWindow extends BaseActivity {
    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_title_right)
    TextView tvTitleRight;
    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;

    @BindView(R.id.activityRootView)
    LinearLayout activityRootView;
    @BindView(R.id.web_adinfo)
    WebView webAdinfo;
//    private ProgressWebView webview;
    //    private Toolbar toolbar;
    private String ThisUrl;
    private String ThisTitle;
    private String winType;
    private ShoppingMallTemplateApplication model;
    //调用友盟分享插件
    private String weixin_api_key;
    private String weixin_api_secret;
    private String qq_api_key;
    private String qq_api_secret;
    private String sina_api_key;
    private String sina_api_secret;
    private String sina_api_callback;
    public static AdWindow instance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//竖屏
        instance = AdWindow.this;
        setContentView(R.layout.activity_adwindow);
        ButterKnife.bind(this);
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
        model = (ShoppingMallTemplateApplication) getApplicationContext();
        //加载分享配置参数
//        weixin_api_key= Utils.getMetaValue(AdWindow.this, "weixin_api_key");
//        weixin_api_secret=Utils.getMetaValue(AdWindow.this, "weixin_api_secret");
//        qq_api_key=Utils.getMetaValue(AdWindow.this, "qq_api_key");
//        qq_api_secret=Utils.getMetaValue(AdWindow.this, "qq_api_secret");
//        sina_api_key=Utils.getMetaValue(AdWindow.this, "sina_api_key");
//        sina_api_secret=Utils.getMetaValue(AdWindow.this, "sina_api_secret");
//        sina_api_callback=Utils.getMetaValue(AdWindow.this, "sina_api_callback");

        //读取传递的参数
        ThisTitle = getIntent().getStringExtra("content_title");
        ThisUrl = getIntent().getStringExtra("content_url");
        winType = getIntent().getStringExtra("content_type");
        //设置标题的文字
//        toolbar.setTitle(ThisTitle);
        tvTitleCenter.setText(ThisTitle);
//        setSupportActionBar(toolbar);
        //显示左边的图标
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //绑定WebView控件
//        webview = (ProgressWebView) findViewById(R.id.webview);
        WebSettings webSettings = webAdinfo.getSettings();
        webSettings.setSaveFormData(false);
        webSettings.setJavaScriptEnabled(true);//能够执行javascript脚本

        webSettings.setSupportZoom(false);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        webAdinfo.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY); //阻止滚动条出现白边
        if (!Utils.isNetworkAvailable(AdWindow.this)) {
            webAdinfo.loadUrl("file:///android_asset/404.html");
        } else {
            Log.e("ccc", "onCreate: " + ThisUrl);
            //加载URL
            webAdinfo.loadUrl(ThisUrl);
//            webview.setWebViewClient(new AdWebViewClient(AdWindow.this, toolbar, ThisTitle, "0", winType));
            webAdinfo.setWebViewClient(new WebViewClient());
        }
    }

    //创建右上角菜单
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        //创建标题栏菜单
//        getMenuInflater().inflate(R.menu.menu_adwindow, menu);
//        return true;
//    }

    //右上角菜单点击事件
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        if (id == android.R.id.home) {
//            QuitWindows();
//            return true;
//        } else if (id == R.id.action_refresh) {
//            ClearCache();
//            webview.loadUrl(ThisUrl);
//            return true;
//        } else if (id == R.id.action_browser_open) {
//            Intent intent = new Intent();
//            intent.setAction("android.intent.action.VIEW");
//            Uri content_url = Uri.parse(ThisUrl);
//            intent.setData(content_url);
//            startActivity(intent);
//            return true;
//        } else if (id == R.id.action_close) {
//            QuitWindows();
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }


    //清空webview缓存
    public void ClearCache() {
        File file = getCacheDir();
        if (file != null && file.exists() && file.isDirectory()) {
            for (File item : file.listFiles()) {
                item.delete();
            }
            file.delete();
        }
        AdWindow.this.deleteDatabase("webview.db");
        AdWindow.this.deleteDatabase("webviewCache.db");

        webAdinfo.clearCache(true);
        webAdinfo.clearHistory();
        webAdinfo.clearFormData();
        webAdinfo.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
//        webAdinfo.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
    }

    //捕捉返回键功能
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (webAdinfo.canGoBack()) {
                webAdinfo.goBack();   //goBack()表示返回webView的上一页面
            } else {
                finish();
            }
            return true;
        }
        return false;
    }

    //关闭窗体
    public void QuitWindows() {
        if (winType.equals("admainwebview")) {
            Intent intent = new Intent(AdWindow.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);  //注意本行的FLAG设置
            startActivity(intent);
        }
        ClearCache();
        finish();
    }

    @OnClick(R.id.iv_title_back)
    public void onViewClicked() {
        onBackPressed();
    }
}
