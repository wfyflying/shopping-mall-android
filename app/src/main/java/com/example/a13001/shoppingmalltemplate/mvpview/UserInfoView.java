package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.CloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.User;
import com.example.a13001.shoppingmalltemplate.modle.UserInfo;

public interface UserInfoView extends View{
    void onSuccessUserInfo(UserInfo user);
    void onSuccessModifyUserInfo(CommonResult commonResult);
    void onSuccessModifyHeadImg(CommonResult  commonResult);
    void onError(String result);
}
