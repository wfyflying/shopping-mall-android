package com.example.a13001.shoppingmalltemplate.activitys;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.MyListView;
import com.example.a13001.shoppingmalltemplate.adapters.ShouHouDetailLvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.modle.AddressOrderId;
import com.example.a13001.shoppingmalltemplate.modle.AfterSale;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.ShouHouDetail;
import com.example.a13001.shoppingmalltemplate.modle.UpFile;
import com.example.a13001.shoppingmalltemplate.mvpview.ApplyAfterSaleView;
import com.example.a13001.shoppingmalltemplate.presenter.ApplyAfterSalePredenter;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShouHouDetailActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_title_right)
    TextView tvTitleRight;
    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;
    @BindView(R.id.tv_shouhoudetail_ordernumber)
    TextView tvShouhoudetailOrdernumber;
    @BindView(R.id.tv_shouhoudetail_orderstate)
    TextView tvShouhoudetailOrderstate;
    @BindView(R.id.tv_shouhoudetail_goodsname)
    TextView tvShouhoudetailGoodsname;
    @BindView(R.id.iv_shouhoudetail_logo)
    ImageView ivShouhoudetailLogo;
    @BindView(R.id.tv_shouhoudetail_price)
    TextView tvShouhoudetailPrice;
    @BindView(R.id.tv_shouhoudetail_count)
    TextView tvShouhoudetailCount;
    @BindView(R.id.tv_shouhoudetail_date)
    TextView tvShouhoudetailDate;
    @BindView(R.id.tv_shouhoudetail_fuwudanhao)
    TextView tvShouhoudetailFuwudanhao;
    @BindView(R.id.tv_shouhoudetail_shenqingshijian)
    TextView tvShouhoudetailShenqingshijian;
    @BindView(R.id.tv_shouhoudetail_fuwuleiixng)
    TextView tvShouhoudetailFuwuleiixng;
    @BindView(R.id.tv_shouhoudetail_shenqingpingjv)
    TextView tvShouhoudetailShenqingpingjv;
    @BindView(R.id.tv_shouhoudetail_lianxiren)
    TextView tvShouhoudetailLianxiren;
    @BindView(R.id.tv_shouhoudetail_phone)
    TextView tvShouhoudetailPhone;
    @BindView(R.id.tv_shouhoudetail_address)
    TextView tvShouhoudetailAddress;
    @BindView(R.id.tv_shouhoudetail_question)
    TextView tvShouhoudetailQuestion;
    @BindView(R.id.mlv_tupian)
    MyListView mLvTuPian;
    private int mCartid;
    private int mCommodityid;
    private String code;
    private String timeStamp;
    private ApplyAfterSalePredenter applyAfterSalePredenter = new ApplyAfterSalePredenter(ShouHouDetailActivity.this);
    private static final String TAG = "ShouHouDetailActivity";
    private String mOrderid;
    private List<String> mList;
    private ShouHouDetailLvAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shou_hou_detail);
        ButterKnife.bind(this);
        initData();
    }

    /**
     * 初始化数据源
     */
    private void initData() {
        tvTitleCenter.setText("售后详情");

        String safetyCode = MyUtils.getMetaValue(ShouHouDetailActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        timeStamp = Utils.getTimeStamp();

        if (getIntent() != null) {
            mCartid = getIntent().getIntExtra("cartid", 0);
            mCommodityid = getIntent().getIntExtra("commodityid", 0);
            mOrderid = getIntent().getStringExtra("orderid");
            tvShouhoudetailOrdernumber.setText("订单号："+mOrderid);
        }
        mLvTuPian.setFocusable(false);
        applyAfterSalePredenter.onCreate();
        applyAfterSalePredenter.attachView(applyAfterSaleView);
        Log.e(TAG, "initData: "+ code+"=timeStamp="+timeStamp+"=mCartid="+String.valueOf(mCartid)+"=mCommodityid="+String.valueOf(mCommodityid) );
        applyAfterSalePredenter.getShouHouDetail(AppConstants.COMPANY_ID, code, timeStamp, String.valueOf(mCartid), String.valueOf(mCommodityid), AppConstants.FROM_MOBILE);



    }

    ApplyAfterSaleView applyAfterSaleView = new ApplyAfterSaleView() {
        @Override
        public void onSuccess(CommonResult commonResult) {

        }

        @Override
        public void onSuccessGetAfterSaleList(AfterSale afterSale) {

        }

        @Override
        public void onSuccessGetShouHouDetail(ShouHouDetail shouHouDetail) {
            Log.e(TAG, "onSuccessGetShouHouDetail: " + shouHouDetail.toString());
            int status = shouHouDetail.getStatus();
            if (status > 0) {
                //售后服务单状态 0 待审核 1 已驳回 2 等待商品寄回 3 已收货 4 售后处理中 5 已完成
                int repairsStatus = shouHouDetail.getRepairsStatus();
                switch (repairsStatus){
                    case 0:tvShouhoudetailOrderstate.setText("待审核");break;
                    case 1:tvShouhoudetailOrderstate.setText("已驳回");break;
                    case 2:tvShouhoudetailOrderstate.setText("等待商品寄回");break;
                    case 3:tvShouhoudetailOrderstate.setText("已收货");break;
                    case 4:tvShouhoudetailOrderstate.setText("售后处理中");break;
                    case 5:tvShouhoudetailOrderstate.setText("已完成");break;
                }
                tvShouhoudetailGoodsname.setText(shouHouDetail.getCommodityName()!=null?shouHouDetail.getCommodityName():"");
                GlideUtils.setNetImage2(ShouHouDetailActivity.this,AppConstants.INTERNET_HEAD+shouHouDetail.getCartImg(),ivShouhoudetailLogo);
                tvShouhoudetailPrice.setText(shouHouDetail.getCommodityProperty()!=null?shouHouDetail.getCommodityProperty():"");
                tvShouhoudetailCount.setText("数量X"+shouHouDetail.getCommodityNumber()+"");
                tvShouhoudetailDate.setText(shouHouDetail.getOrderDate()!=null?shouHouDetail.getOrderDate():"");
                tvShouhoudetailFuwudanhao.setText(shouHouDetail.getRepairsId()!=null?shouHouDetail.getRepairsId():"");
                tvShouhoudetailShenqingshijian.setText(shouHouDetail.getRepairsCreatTime()!=null?shouHouDetail.getRepairsCreatTime():"");
                int repairsProof = shouHouDetail.getRepairsProof();//售后服务单凭证类型 1 发票信息 2 检测报告 3 其它文件
                switch (repairsProof){
                    case 1:tvShouhoudetailShenqingpingjv.setText("发票信息");break;
                    case 2:tvShouhoudetailShenqingpingjv.setText("检测报告");break;
                    case 3:tvShouhoudetailShenqingpingjv.setText("其它文件");break;
                }
                tvShouhoudetailLianxiren.setText(shouHouDetail.getRepairsName()!=null?shouHouDetail.getRepairsName():"");
                tvShouhoudetailPhone.setText(shouHouDetail.getRepairsPhone()!=null?shouHouDetail.getRepairsPhone():"");
                tvShouhoudetailAddress.setText(shouHouDetail.getAddressProvince()+shouHouDetail.getAddressCity()+shouHouDetail.getAddressArea()+shouHouDetail.getAddressXX());
                tvShouhoudetailQuestion.setText(shouHouDetail.getRepairsContent()!=null?shouHouDetail.getRepairsContent():"");
                mList=new ArrayList<>();
                String repairsImages = shouHouDetail.getRepairsImages();
                String[] split = repairsImages.split(",");
                for (int i = 0; i < split.length; i++) {
                    mList.add(AppConstants.INTERNET_HEAD+split[i]);
                }
                mAdapter=new ShouHouDetailLvAdapter(ShouHouDetailActivity.this,mList);
                mLvTuPian.setAdapter(mAdapter);
            } else {

            }
        }

        @Override
        public void onSuccessGetAddressOrderId(AddressOrderId addressOrderId) {

        }

        @Override
        public void onSuccessUpFile(UpFile upFile) {

        }

        @Override
        public void onError(String result) {
            Log.e(TAG, "onError: " + result);
        }
    };
}
