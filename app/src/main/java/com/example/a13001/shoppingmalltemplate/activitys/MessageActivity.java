package com.example.a13001.shoppingmalltemplate.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.adapters.MessageLvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.Message;
import com.example.a13001.shoppingmalltemplate.modle.Notice;
import com.example.a13001.shoppingmalltemplate.mvpview.MessageView;
import com.example.a13001.shoppingmalltemplate.presenter.MessagePredenter;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Cookie;

public class MessageActivity extends BaseActivity implements MessageLvAdapter.doDeleteMessageInterface {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_message_unread)
    TextView tvMessageUnread;
    @BindView(R.id.tv_message_read)
    TextView tvMessageRead;
    @BindView(R.id.lv_message)
    ListView lvMessage;
    private List<Message.ListBean> mList;
    private MessageLvAdapter mAdapter;
    MessagePredenter messagePredenter=new MessagePredenter(MessageActivity.this);
    private static final String TAG = "MessageActivity";
    private String safetyCode;
    private String code;
    private String timeStamp;
    private int pageindex=1;
    private String smsStatus="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        ButterKnife.bind(this);
        initData();
    }
    MessageView messageView=new MessageView() {
        @Override
        public void onSuccess(Message message) {
            Log.e(TAG, "onSuccess: "+message.toString());
           int status= message.getStatus();
           if (status>0){
               mList.addAll(message.getList());
               mAdapter.notifyDataSetChanged();
           }else{
               Toast.makeText(MessageActivity.this, ""+message.getReturnMsg(), Toast.LENGTH_SHORT).show();
           }
        }

        @Override
        public void onSuccessNotice(Notice notice) {

        }

        @Override
        public void onSuccessDoSmsDelete(CommonResult commonResult) {
            Log.e(TAG, "onSuccessDoSmsDelete: "+commonResult.toString() );
            int status=commonResult.getStatus();
            if (status>0){
                if (mList != null) {
                    mList.clear();
                }
                messagePredenter.getSmsList(AppConstants.COMPANY_ID,code,timeStamp,smsStatus,AppConstants.PAGE_SIZE,pageindex);
            }else{
                Toast.makeText(MessageActivity.this, ""+commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onError(String result) {

        }
    };
    /**
     * 初始化数据源
     */
    private void initData() {
        safetyCode = MyUtils.getMetaValue(MessageActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        timeStamp = Utils.getTimeStamp();
        tvTitleCenter.setText("消息");
        messagePredenter.onCreate();
        messagePredenter.attachView(messageView);
        mList=new ArrayList<>();
        mAdapter=new MessageLvAdapter(MessageActivity.this,mList);
        mAdapter.setDoDeleteMessageInterface(this);
        Log.e(TAG, "initData: "+code+"===timeStamp==="+timeStamp);
//        messagePredenter.getSmsList(AppConstants.COMPANY_ID,code,timeStamp,AppConstants.PAGE_SIZE,pageindex);
        tvMessageUnread.performClick();
        lvMessage.setAdapter(mAdapter);

        lvMessage.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(MessageActivity.this,MessageDetailActivity.class).putExtra("smsid",mList.get(i).getSmsId()));
            }
        });
    }

    @OnClick({R.id.iv_title_back, R.id.tv_message_unread, R.id.tv_message_read})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_title_back:
                onBackPressed();
                break;
                //未读消息
            case R.id.tv_message_unread:
                initTxtColor();
                tvMessageUnread.setTextColor(getResources().getColor(R.color.white));
                tvMessageUnread.setBackgroundColor(getResources().getColor(R.color.ffb422));
                if (mList != null) {
                    mList.clear();
                }
                smsStatus="0";
                messagePredenter.getSmsList(AppConstants.COMPANY_ID,code,timeStamp,smsStatus,AppConstants.PAGE_SIZE,pageindex);
                lvMessage.setAdapter(mAdapter);
                break;
                //已读消息
            case R.id.tv_message_read:
                initTxtColor();
                tvMessageRead.setTextColor(getResources().getColor(R.color.white));
                tvMessageRead.setBackgroundColor(getResources().getColor(R.color.ffb422));
                if (mList != null) {
                    mList.clear();
                }
                smsStatus="1";
                messagePredenter.getSmsList(AppConstants.COMPANY_ID,code,timeStamp,smsStatus,AppConstants.PAGE_SIZE,pageindex);
                lvMessage.setAdapter(mAdapter);
                break;
        }
    }

    /**
     * 初始化顶部标题颜色
     */
    private void initTxtColor() {
        tvMessageUnread.setTextColor(getResources().getColor(R.color.t333));
        tvMessageUnread.setBackgroundColor(getResources().getColor(R.color.white));
        tvMessageRead.setTextColor(getResources().getColor(R.color.t333));
        tvMessageRead.setBackgroundColor(getResources().getColor(R.color.white));
    }
    static Cookie appCookie;

    @Override
    public void doDelete(int position) {
        messagePredenter.doSmsDelete(AppConstants.COMPANY_ID,code,timeStamp,mList.get(position).getSmsId());
    }

    @Override
    public void doStartDetail(int position) {
        startActivity(new Intent(MessageActivity.this,MessageDetailActivity.class).putExtra("smsid",mList.get(position).getSmsId()));

    }
//    public static Cookie getAppCookie() {
//        Log.e(TAG, "getAppCookie: "+appCookie.name()+appCookie.toString() );
//        return appCookie;
//    }
}
