package com.example.a13001.shoppingmalltemplate.modle;

public class UpFile {

    /**
     * status : 1
     * returnMsg : null
     * data : //file.site.ify2.cn/site/153/upload/gspd/upload/201805/20185211457198421.png
     */

    private int status;
    private String returnMsg;
    private String data;

    @Override
    public String toString() {
        return "UpFile{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", data='" + data + '\'' +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
