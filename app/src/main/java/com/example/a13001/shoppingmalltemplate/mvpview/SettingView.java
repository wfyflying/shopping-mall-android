package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.AppConfig;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;

public interface SettingView extends View{
    void onSuccessDoLoginOut(CommonResult commonResult);
    void onSuccessGetAppConfig(AppConfig appConfig);
    void onSuccessGetAppConfig1(AppConfig appConfig);
    void onError(String result);
}
