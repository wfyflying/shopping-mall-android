package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class Order {

    /**
     * status : 1
     * returnMsg : null
     * OrderCount : 4
     * OrderList : [{"ordersId":228,"orderTotalprice":0.03,"orderStatus":1,"orderStatusName":"等待买家付款","ordersNumber":"201806061739027900","orderDate":"2018-06-06 17:39","orderOddnumber":"","orderFreight":0,"ordersType":"普通订单","ordersZffs":"微信支付","OrderGoodsCount":1,"OrderGoodsList":[{"CartId":100073,"commodityId":33,"CartImg":"//file.site.ify.cn/site/144/upload/xnsp/upload/201806/2018669559781.jpg","CommodityName":"测试商品","commodityLink":"//849948.net/mobile/channel/content.aspx?id=33&preview=99a677b67554dbad50e7bd9b8082b46b","CommodityProperty":"产品:天津,型号:中型","CommodityNumber":1}]},{"ordersId":131,"orderTotalprice":25,"orderStatus":1,"orderStatusName":"等待买家付款","ordersNumber":"201707261355044907","orderDate":"2017-07-26 13:55","orderOddnumber":"","orderFreight":10,"ordersType":"普通订单","ordersZffs":"微信支付","OrderGoodsCount":1,"OrderGoodsList":[{"CartId":100031,"commodityId":3,"CartImg":"//file.site.ify.cn/site/144/upload/spzs/upload/201701/2017171221372511.jpg","CommodityName":"手工牛轧糖","commodityLink":"//849948.net/mobile/channel/content.aspx?id=3&preview=99a677b67554dbad50e7bd9b8082b46b","CommodityProperty":"口味:原味,包装:200克","CommodityNumber":1}]},{"ordersId":129,"orderTotalprice":30,"orderStatus":1,"orderStatusName":"等待买家付款","ordersNumber":"201707261159064239","orderDate":"2017-07-26 11:59","orderOddnumber":"","orderFreight":10,"ordersType":"普通订单","ordersZffs":"银行转账","OrderGoodsCount":1,"OrderGoodsList":[{"CartId":100030,"commodityId":6,"CartImg":"//file.site.ify.cn/site/144/upload/spzs/upload/201705/20175311536312751.jpg","CommodityName":"自然莎 水能量营养修护发膜","commodityLink":"//849948.net/mobile/channel/content.aspx?id=6&preview=99a677b67554dbad50e7bd9b8082b46b","CommodityProperty":"规格:盒","CommodityNumber":1}]},{"ordersId":128,"orderTotalprice":25,"orderStatus":1,"orderStatusName":"等待买家付款","ordersNumber":"201707261156270174","orderDate":"2017-07-26 11:56","orderOddnumber":"","orderFreight":18,"ordersType":"普通订单","ordersZffs":"银行转账","OrderGoodsCount":1,"OrderGoodsList":[{"CartId":100029,"commodityId":3,"CartImg":"//file.site.ify.cn/site/144/upload/spzs/upload/201701/2017171221372511.jpg","CommodityName":"手工牛轧糖","commodityLink":"//849948.net/mobile/channel/content.aspx?id=3&preview=99a677b67554dbad50e7bd9b8082b46b","CommodityProperty":"口味:原味,包装:200克","CommodityNumber":1}]}]
     */

    private int status;
    private Object returnMsg;
    private int OrderCount;
    private List<OrderListBean> OrderList;

    @Override
    public String toString() {
        return "Order{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", OrderCount=" + OrderCount +
                ", OrderList=" + OrderList +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getOrderCount() {
        return OrderCount;
    }

    public void setOrderCount(int OrderCount) {
        this.OrderCount = OrderCount;
    }

    public List<OrderListBean> getOrderList() {
        return OrderList;
    }

    public void setOrderList(List<OrderListBean> OrderList) {
        this.OrderList = OrderList;
    }

    public static class OrderListBean {
        /**
         * ordersId : 228
         * orderTotalprice : 0.03
         * orderStatus : 1
         * orderStatusName : 等待买家付款
         * ordersNumber : 201806061739027900
         * orderDate : 2018-06-06 17:39
         * orderOddnumber :
         * orderFreight : 0
         * ordersType : 普通订单
         * ordersZffs : 微信支付
         * OrderGoodsCount : 1
         * OrderGoodsList : [{"CartId":100073,"commodityId":33,"CartImg":"//file.site.ify.cn/site/144/upload/xnsp/upload/201806/2018669559781.jpg","CommodityName":"测试商品","commodityLink":"//849948.net/mobile/channel/content.aspx?id=33&preview=99a677b67554dbad50e7bd9b8082b46b","CommodityProperty":"产品:天津,型号:中型","CommodityNumber":1}]
         */

        private int ordersId;
        private double orderTotalprice;
        private int orderStatus;
        private String orderStatusName;
        private String ordersNumber;
        private String orderDate;
        private String orderOddnumber;
        private int orderFreight;
        private String ordersType;
        private String ordersZffs;
        private int OrderGoodsCount;
        private List<OrderGoodsListBean> OrderGoodsList;

        @Override
        public String toString() {
            return "OrderListBean{" +
                    "ordersId=" + ordersId +
                    ", orderTotalprice=" + orderTotalprice +
                    ", orderStatus=" + orderStatus +
                    ", orderStatusName='" + orderStatusName + '\'' +
                    ", ordersNumber='" + ordersNumber + '\'' +
                    ", orderDate='" + orderDate + '\'' +
                    ", orderOddnumber='" + orderOddnumber + '\'' +
                    ", orderFreight=" + orderFreight +
                    ", ordersType='" + ordersType + '\'' +
                    ", ordersZffs='" + ordersZffs + '\'' +
                    ", OrderGoodsCount=" + OrderGoodsCount +
                    ", OrderGoodsList=" + OrderGoodsList +
                    '}';
        }

        public int getOrdersId() {
            return ordersId;
        }

        public void setOrdersId(int ordersId) {
            this.ordersId = ordersId;
        }

        public double getOrderTotalprice() {
            return orderTotalprice;
        }

        public void setOrderTotalprice(double orderTotalprice) {
            this.orderTotalprice = orderTotalprice;
        }

        public int getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(int orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getOrderStatusName() {
            return orderStatusName;
        }

        public void setOrderStatusName(String orderStatusName) {
            this.orderStatusName = orderStatusName;
        }

        public String getOrdersNumber() {
            return ordersNumber;
        }

        public void setOrdersNumber(String ordersNumber) {
            this.ordersNumber = ordersNumber;
        }

        public String getOrderDate() {
            return orderDate;
        }

        public void setOrderDate(String orderDate) {
            this.orderDate = orderDate;
        }

        public String getOrderOddnumber() {
            return orderOddnumber;
        }

        public void setOrderOddnumber(String orderOddnumber) {
            this.orderOddnumber = orderOddnumber;
        }

        public int getOrderFreight() {
            return orderFreight;
        }

        public void setOrderFreight(int orderFreight) {
            this.orderFreight = orderFreight;
        }

        public String getOrdersType() {
            return ordersType;
        }

        public void setOrdersType(String ordersType) {
            this.ordersType = ordersType;
        }

        public String getOrdersZffs() {
            return ordersZffs;
        }

        public void setOrdersZffs(String ordersZffs) {
            this.ordersZffs = ordersZffs;
        }

        public int getOrderGoodsCount() {
            return OrderGoodsCount;
        }

        public void setOrderGoodsCount(int OrderGoodsCount) {
            this.OrderGoodsCount = OrderGoodsCount;
        }

        public List<OrderGoodsListBean> getOrderGoodsList() {
            return OrderGoodsList;
        }

        public void setOrderGoodsList(List<OrderGoodsListBean> OrderGoodsList) {
            this.OrderGoodsList = OrderGoodsList;
        }

        public static class OrderGoodsListBean {
            /**
             * CartId : 100073
             * commodityId : 33
             * CartImg : //file.site.ify.cn/site/144/upload/xnsp/upload/201806/2018669559781.jpg
             * CommodityName : 测试商品
             * commodityLink : //849948.net/mobile/channel/content.aspx?id=33&preview=99a677b67554dbad50e7bd9b8082b46b
             * CommodityProperty : 产品:天津,型号:中型
             * CommodityNumber : 1
             */

            private int CartId;
            private int commodityId;
            private String CartImg;
            private String CommodityName;
            private String commodityLink;
            private String CommodityProperty;
            private int CommodityNumber;

            @Override
            public String toString() {
                return "OrderGoodsListBean{" +
                        "CartId=" + CartId +
                        ", commodityId=" + commodityId +
                        ", CartImg='" + CartImg + '\'' +
                        ", CommodityName='" + CommodityName + '\'' +
                        ", commodityLink='" + commodityLink + '\'' +
                        ", CommodityProperty='" + CommodityProperty + '\'' +
                        ", CommodityNumber=" + CommodityNumber +
                        '}';
            }

            public int getCartId() {
                return CartId;
            }

            public void setCartId(int CartId) {
                this.CartId = CartId;
            }

            public int getCommodityId() {
                return commodityId;
            }

            public void setCommodityId(int commodityId) {
                this.commodityId = commodityId;
            }

            public String getCartImg() {
                return CartImg;
            }

            public void setCartImg(String CartImg) {
                this.CartImg = CartImg;
            }

            public String getCommodityName() {
                return CommodityName;
            }

            public void setCommodityName(String CommodityName) {
                this.CommodityName = CommodityName;
            }

            public String getCommodityLink() {
                return commodityLink;
            }

            public void setCommodityLink(String commodityLink) {
                this.commodityLink = commodityLink;
            }

            public String getCommodityProperty() {
                return CommodityProperty;
            }

            public void setCommodityProperty(String CommodityProperty) {
                this.CommodityProperty = CommodityProperty;
            }

            public int getCommodityNumber() {
                return CommodityNumber;
            }

            public void setCommodityNumber(int CommodityNumber) {
                this.CommodityNumber = CommodityNumber;
            }
        }
    }
}
