package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.modle.Account;
import com.example.a13001.shoppingmalltemplate.modle.IntegrationList;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Administrator on 2017/8/9.
 */

public class AccountBalanceLvAdapter extends BaseAdapter {
    private Context context;
    private List<Account.MemberTransactionListBean> mList;

    public AccountBalanceLvAdapter(Context context, List<Account.MemberTransactionListBean> mList) {
        this.context = context;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder vh = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_lv_accountbalance, parent, false);
            vh = new ViewHolder(view);
            view.setTag(vh);
            AutoUtils.autoSize(view);
        } else {
            vh = (ViewHolder) view.getTag();
        }
        vh.tvItemaccountbalanceTitle.setText(mList.get(position).getRecordType()!=null?mList.get(position).getRecordType():"");
        vh.tvItemaccountbalanceTxt1.setText(mList.get(position).getOperatingRecord()!=null?mList.get(position).getOperatingRecord():"");
        vh.tvItemaccountbalanceTxt2.setText("关联订单："+mList.get(position).getRecordOrder());
        vh.tvItemaccountbalanceTxt3.setText("操作时间："+mList.get(position).getRecordDate());
        vh.tvItemaccountbalanceIntegral.setText(mList.get(position).getRecordPrice()!=null?mList.get(position).getRecordPrice():"");

        return view;
    }



    class ViewHolder {
        @BindView(R.id.tv_itemaccountbalance_title)
        TextView tvItemaccountbalanceTitle;
        @BindView(R.id.tv_itemaccountbalance_txt1)
        TextView tvItemaccountbalanceTxt1;
        @BindView(R.id.tv_itemaccountbalance_txt2)
        TextView tvItemaccountbalanceTxt2;
        @BindView(R.id.tv_itemaccountbalance_txt3)
        TextView tvItemaccountbalanceTxt3;
        @BindView(R.id.tv_itemaccountbalance_integral)
        TextView tvItemaccountbalanceIntegral;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
