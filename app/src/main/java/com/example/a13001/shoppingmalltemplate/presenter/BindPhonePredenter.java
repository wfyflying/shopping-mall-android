package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;

import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.mvpview.AdressManagerView;
import com.example.a13001.shoppingmalltemplate.mvpview.BindPhoneView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class BindPhonePredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private BindPhoneView mBindPhoneView;
    private CommonResult mCommonResult;



    public BindPhonePredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        mCompositeSubscription=new CompositeSubscription();
        manager=new DataManager(mContext);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mBindPhoneView=(BindPhoneView) view;
    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }

    /**
     *★ 绑定手机号码
     * @param companyid
     * @param code
     * @param timestamp
     * @param chkData1     手机号
     * @param chkData2      手机验证码
     * @param chkData3       安全密码
     * @return
     */
    public void bindPhone(String companyid,String code,String timestamp,String chkData1,String chkData2,String chkData3) {

        mCompositeSubscription.add(manager.bindPhone(companyid,code,timestamp,chkData1,chkData2,chkData3)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mBindPhoneView!=null){
                            mBindPhoneView.onSuccessBindPhone(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mBindPhoneView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
    /**
     *★ 绑定手机号码-发送手机校验码
     * @param companyid
     * @param code
     * @param timestamp
     * @param chkData1     手机号
     * @param chkData2      安全密码
     *
     * @return
     */
    public void bindPhoneSendCode(String companyid,String code,String timestamp,String chkData1,String chkData2) {

        mCompositeSubscription.add(manager.bindPhoneSendCode(companyid,code,timestamp,chkData1,chkData2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mBindPhoneView!=null){
                            mBindPhoneView.onSuccessGetCode(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mBindPhoneView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }

}
