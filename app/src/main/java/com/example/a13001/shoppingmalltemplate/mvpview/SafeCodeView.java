package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.UserInfo;

public interface SafeCodeView extends View{
    void onSuccessGetUserInfo(UserInfo userInfo);
    void onSuccessDoResetSafepwd(CommonResult commonResult);
    void onError(String result);
}
