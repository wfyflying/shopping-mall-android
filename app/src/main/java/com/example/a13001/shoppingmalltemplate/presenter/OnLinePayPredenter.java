package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;

import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.Alipay;
import com.example.a13001.shoppingmalltemplate.modle.CloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.WechatPay;
import com.example.a13001.shoppingmalltemplate.mvpview.AffirmOrderView;
import com.example.a13001.shoppingmalltemplate.mvpview.OnLinePayView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class OnLinePayPredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private OnLinePayView mOnLinePayView;
    private CommonResult mCommonResult;
    private WechatPay mWechatPay;
    private Alipay mAlipay;
    public OnLinePayPredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        mCompositeSubscription=new CompositeSubscription();
        manager=new DataManager(mContext);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mOnLinePayView=(OnLinePayView) view;
    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }


    /**
     * 微信支付
     * @param companyid   站点ID
     * @param code    安全校验码
     * @param timestamp  timestamp
     * @param ordernumber ordernumber
     * @param ip   ip
     * @return
     */
    public void wechatPay(String companyid, String code, String timestamp, String ordernumber, String ip) {

        mCompositeSubscription.add(manager.wechatPay(companyid,code,timestamp,ordernumber,ip)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<WechatPay>() {
                    @Override
                    public void onCompleted() {
                        if (mOnLinePayView!=null){
                            mOnLinePayView.onSuccessWechatPay(mWechatPay);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mOnLinePayView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(WechatPay wechatPay) {
                        mWechatPay=wechatPay;
                    }
                }));
    }
    /**
     * ★ 支付宝支付（APP）
     * @param companyid
     * @param code
     * @param timestamp
     * @param ordernumber
     * @return
     */
    public void doAlipay(String companyid, String code, String timestamp, String ordernumber) {

        mCompositeSubscription.add(manager.doAlipay(companyid,code,timestamp,ordernumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Alipay>() {
                    @Override
                    public void onCompleted() {
                        if (mOnLinePayView!=null){
                            mOnLinePayView.onSuccessAliPay(mAlipay);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mOnLinePayView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(Alipay alipay) {
                        mAlipay=alipay;
                    }
                }));
    }
}
