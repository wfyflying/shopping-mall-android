package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;

import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.User;
import com.example.a13001.shoppingmalltemplate.mvpview.ForgetPwdView;
import com.example.a13001.shoppingmalltemplate.mvpview.RegisterView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import java.util.Map;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ForgetPwdPredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private ForgetPwdView mForgetPwdView;
    private CommonResult mCommonResult;



    public ForgetPwdPredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        mCompositeSubscription=new CompositeSubscription();
        manager=new DataManager(mContext);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mForgetPwdView=(ForgetPwdView) view;
    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }
    /**
     *★ 找回密码-设置新密码
     * @param companyid
     * @param code
     * @param timestamp
     * @param chkData1        手机（邮箱）校验码
     * @param chkData2       密码
     * @param chkData3       确认密码
     * @return
     */
    public void doResetPwd(String companyid,String code,String timestamp,String chkData1,String chkData2,String chkData3) {
        mCompositeSubscription.add(manager.doResetPwd(companyid,code,timestamp,chkData1,chkData2,chkData3)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                               @Override
                               public void onCompleted() {
                                   if (mForgetPwdView!=null){
                                       mForgetPwdView.onSuccessDoResetPwd(mCommonResult);
                                   }
                               }

                               @Override
                               public void onError(Throwable e) {
                                   mForgetPwdView.onError("请求失败"+e.toString());
                               }

                               @Override
                               public void onNext(CommonResult commonResult) {
                                    mCommonResult=commonResult;
                               }
                           }

                ));
    }
    /**
     * ★ 获取图形验证码
     */

    public void getPicCode(String companyid,String code,String timestamp) {

        mCompositeSubscription.add(manager.getPicCode(companyid,code,timestamp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mForgetPwdView!=null){
                            mForgetPwdView.onSuccessGetPicCode(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mForgetPwdView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
    /**
     * 找回密码-发送手机校验码
     * @param companyid
     * @param code
     * @param timestamp
     * @param chkData1       手机号
     * @param chkData2      图形验证码
     * @return
     */
    public void getPhoneCode(String companyid,String code,String timestamp,String chkData1,String chkData2) {

        mCompositeSubscription.add(manager.getForgetPwdPhoneCode(companyid,code,timestamp,chkData1,chkData2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mForgetPwdView!=null){
                            mForgetPwdView.onSuccessGetPhoneCode(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mForgetPwdView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
    /**
     * 登录
     *
     * @param companyid 站点ID
     * @param code      安全校验码
     * @param timestamp 时间戳
     * @param name      会员名称，用户名/手机/邮箱
     * @param pwd       登录密码
     * @param from      来源，pc 电脑端 mobile 移动端
     */
//    public void doLogin(String companyid, String code, String timestamp, final String name, final String pwd, String from) {
//        mCompositeSubscription.add(manager.doLogin(companyid, code, timestamp, name, pwd, from)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Observer<User>() {
//                               @Override
//                               public void onCompleted() {
//                                   if (mForgetPwdView!=null){
//                                       mForgetPwdView.onSuccessDoLogin(mUser);
//                                   }
//
//                               }
//
//                               @Override
//                               public void onError(Throwable e) {
//                                   mForgetPwdView.onError("请求失败"+e.toString());
//                               }
//
//                               @Override
//                               public void onNext(User user) {
//                                   mUser=user;
//
//                               }
//                           }
//
//                ));
//    }
}
