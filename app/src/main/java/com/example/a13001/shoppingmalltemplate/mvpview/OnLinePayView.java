package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.Alipay;
import com.example.a13001.shoppingmalltemplate.modle.CloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.WechatPay;

public interface OnLinePayView extends View{
    void onSuccessWechatPay(WechatPay wechatPay);
    void onSuccessAliPay(Alipay alipay);
    void onError(String result);
}
