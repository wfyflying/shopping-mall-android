package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.MyListView;
import com.example.a13001.shoppingmalltemplate.modle.AnswerList;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuestionLvAdapter extends BaseAdapter {
    private Context mContext;
    private List<AnswerList.ListBean> mList;

    public QuestionLvAdapter(Context mContext, List<AnswerList.ListBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_lv_qanda, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
            AutoUtils.autoSize(view);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.tvQuestion.setText(mList.get(i).getCommentContent()!=null?mList.get(i).getCommentContent():"");
        holder.tvAnswe.setText(mList.get(i).getCommentReplay()!=null?mList.get(i).getCommentReplay():"");

        return view;
    }


    static class ViewHolder {
        @BindView(R.id.tv_question)
        TextView tvQuestion;
        @BindView(R.id.tv_answe)
        TextView tvAnswe;
        @BindView(R.id.mlv)
        MyListView mlv;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
