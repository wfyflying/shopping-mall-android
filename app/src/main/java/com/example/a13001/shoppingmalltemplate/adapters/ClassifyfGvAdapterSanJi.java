package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.activitys.GoodsListActivity;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.Classify;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;


/**
 *
 */
public class ClassifyfGvAdapterSanJi extends BaseAdapter {
    private Context mContext;
    private List<Classify.ListBeanXX.ListBeanX.ListBean> mList;

    public ClassifyfGvAdapterSanJi(Context mContext, List<Classify.ListBeanXX.ListBeanX.ListBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (mList != null && mList.size() > 0) {
            return mList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_gv_homepage1, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
            AutoUtils.autoSize(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        GlideUtils.setNetImage(mContext, AppConstants.INTERNET_HEAD+mList.get(position).getClassImages(),holder.ivLogo);
        holder.tvTitle.setText(mList.get(position).getClassName()!=null?mList.get(position).getClassName():"");
        holder.ivLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mContext, GoodsListActivity.class);
                intent.putExtra("id",mList.get(position).getClassid());
                intent.putExtra("searchword","");
                mContext.startActivity(intent);
            }
        });
        return convertView;
    }


    static class ViewHolder {
        ImageView ivLogo;
        TextView tvTitle;

        ViewHolder(View view) {
            ivLogo=view.findViewById(R.id.iv_itemgvhomepage_logo);
            tvTitle=view.findViewById(R.id.tv_itemgvhomepage_title);
        }
    }
}
