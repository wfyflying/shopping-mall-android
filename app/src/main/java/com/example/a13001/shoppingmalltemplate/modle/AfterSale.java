package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class AfterSale {

    /**
     * status : 1
     * returnMsg : null
     * RepairsCount : 1
     * RepairsList : [{"rid":2,"repairsId":"20180919150709848","ordersId":"201808060926569775","CartImg":"http://cdn.qkk.cn/site/495/upload/shop/upload/201808/2018821424501721.jpg","CommodityName":"葡萄干","CommodityProperty":"重量:1kg,:","commodityLink":"//www.hxptsc.com/channel/content.aspx?id=9&preview=440addcdf8f4966b22b26b53f29c8277","repairsStatus":0,"repairsCreatTime":"2018-09-19 15:07","CartId":100008,"Contentid":9}]
     */

    private int status;
    private String returnMsg;
    private int RepairsCount;
    private List<RepairsListBean> RepairsList;

    @Override
    public String toString() {
        return "AfterSale{" +
                "status=" + status +
                ", returnMsg='" + returnMsg + '\'' +
                ", RepairsCount=" + RepairsCount +
                ", RepairsList=" + RepairsList +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getRepairsCount() {
        return RepairsCount;
    }

    public void setRepairsCount(int RepairsCount) {
        this.RepairsCount = RepairsCount;
    }

    public List<RepairsListBean> getRepairsList() {
        return RepairsList;
    }

    public void setRepairsList(List<RepairsListBean> RepairsList) {
        this.RepairsList = RepairsList;
    }

    public static class RepairsListBean {
        /**
         * rid : 2
         * repairsId : 20180919150709848
         * ordersId : 201808060926569775
         * CartImg : http://cdn.qkk.cn/site/495/upload/shop/upload/201808/2018821424501721.jpg
         * CommodityName : 葡萄干
         * CommodityProperty : 重量:1kg,:
         * commodityLink : //www.hxptsc.com/channel/content.aspx?id=9&preview=440addcdf8f4966b22b26b53f29c8277
         * repairsStatus : 0
         * repairsCreatTime : 2018-09-19 15:07
         * CartId : 100008
         * Contentid : 9
         */

        private int rid;
        private String repairsId;
        private String ordersId;
        private String CartImg;
        private String CommodityName;
        private String CommodityProperty;
        private String commodityLink;
        private int repairsStatus;
        private String repairsCreatTime;
        private int CartId;
        private int Contentid;

        @Override
        public String toString() {
            return "RepairsListBean{" +
                    "rid=" + rid +
                    ", repairsId='" + repairsId + '\'' +
                    ", ordersId='" + ordersId + '\'' +
                    ", CartImg='" + CartImg + '\'' +
                    ", CommodityName='" + CommodityName + '\'' +
                    ", CommodityProperty='" + CommodityProperty + '\'' +
                    ", commodityLink='" + commodityLink + '\'' +
                    ", repairsStatus=" + repairsStatus +
                    ", repairsCreatTime='" + repairsCreatTime + '\'' +
                    ", CartId=" + CartId +
                    ", Contentid=" + Contentid +
                    '}';
        }

        public int getRid() {
            return rid;
        }

        public void setRid(int rid) {
            this.rid = rid;
        }

        public String getRepairsId() {
            return repairsId;
        }

        public void setRepairsId(String repairsId) {
            this.repairsId = repairsId;
        }

        public String getOrdersId() {
            return ordersId;
        }

        public void setOrdersId(String ordersId) {
            this.ordersId = ordersId;
        }

        public String getCartImg() {
            return CartImg;
        }

        public void setCartImg(String CartImg) {
            this.CartImg = CartImg;
        }

        public String getCommodityName() {
            return CommodityName;
        }

        public void setCommodityName(String CommodityName) {
            this.CommodityName = CommodityName;
        }

        public String getCommodityProperty() {
            return CommodityProperty;
        }

        public void setCommodityProperty(String CommodityProperty) {
            this.CommodityProperty = CommodityProperty;
        }

        public String getCommodityLink() {
            return commodityLink;
        }

        public void setCommodityLink(String commodityLink) {
            this.commodityLink = commodityLink;
        }

        public int getRepairsStatus() {
            return repairsStatus;
        }

        public void setRepairsStatus(int repairsStatus) {
            this.repairsStatus = repairsStatus;
        }

        public String getRepairsCreatTime() {
            return repairsCreatTime;
        }

        public void setRepairsCreatTime(String repairsCreatTime) {
            this.repairsCreatTime = repairsCreatTime;
        }

        public int getCartId() {
            return CartId;
        }

        public void setCartId(int CartId) {
            this.CartId = CartId;
        }

        public int getContentid() {
            return Contentid;
        }

        public void setContentid(int Contentid) {
            this.Contentid = Contentid;
        }
    }
}
