package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.AppConfig;
import com.example.a13001.shoppingmalltemplate.modle.CloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.DetailGoods;
import com.example.a13001.shoppingmalltemplate.modle.GoodsParameters;
import com.example.a13001.shoppingmalltemplate.modle.LoginStatus;
import com.example.a13001.shoppingmalltemplate.modle.ShopCarGoods;
import com.example.a13001.shoppingmalltemplate.mvpview.GoodsDetailView;
import com.example.a13001.shoppingmalltemplate.mvpview.ShopCarView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ShopCarPredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private ShopCarView mShopCarView;
    private ShopCarGoods mShopCarGoods;
    private LoginStatus mLoginStatus;
    private CommonResult mCommonResult;
    private CloaseAccount mCloaseAccount;
    private AppConfig mAppConfig;


    public ShopCarPredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        mCompositeSubscription=new CompositeSubscription();
        manager=new DataManager(mContext);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mShopCarView=(ShopCarView) view;
    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }

    /**
     * 获取购物车商品信息
     * @param companyid  站点ID
     * @param from   来源，pc 电脑端 mobile 移动端
     * @return
     */
    public void getShopCarGoods(String companyid,String from) {

        mCompositeSubscription.add(manager.getShopCarGoods(companyid, from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ShopCarGoods>() {
                    @Override
                    public void onCompleted() {
                        if (mShopCarView!=null){
                            mShopCarView.onSuccess(mShopCarGoods);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mShopCarView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(ShopCarGoods shopCarGoods) {
                        mShopCarGoods=shopCarGoods;
                    }
                }));
    }
    /**
     * 判断登录状态
     * @param companyid  站点ID
     * @param code    安全校验码
     * @param timestamp  时间戳
     * @param from   来源，pc 电脑端 mobile 移动端
     * @return
     */
    public void getLoginStatus(String companyid,String code,String timestamp, String from) {

        mCompositeSubscription.add(manager.getLoginStatus(companyid,code,timestamp, from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<LoginStatus>() {
                    @Override
                    public void onCompleted() {
                        if (mShopCarView!=null){
                            mShopCarView.onSuccessLoginStatus(mLoginStatus);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mShopCarView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(LoginStatus loginStatus) {
                        mLoginStatus=loginStatus;
                    }
                }));
    }
    /**
     *删除购物车商品
     * @param companyid  站点ID
     * @param id         商品ID，多个用“,”号分割
     * @return
     */
    public void deleteShopCar(String companyid,String id) {

        mCompositeSubscription.add(manager.deleteShopCar(companyid,id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mShopCarView!=null){
                            mShopCarView.onSuccessDeleteShopCar(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mShopCarView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
    /**
     * 更新购物车数量
     * @param companyid   站点ID
     * @param id          内容ID
     * @param sum        更新后数量
     * @return
     */
    public void updateShopCarNum(String companyid,int id,int sum) {

        mCompositeSubscription.add(manager.updateShopCarNum(companyid,id,sum)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mShopCarView!=null){
                            mShopCarView.onSuccessUpdateShopCarNum(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mShopCarView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
    /**
     * 结算
     * @param companyid   站点ID
     * @param code        安全校验码
     * @param timestamp   时间戳
     * @param cartid     购物车结算ID，多个用“,”号分割
     * @param from       来源，pc 电脑端 mobile 移动端
     * @return
     */
    public void doCloaseAccount(String companyid, String code, String timestamp, String cartid, String from) {

        mCompositeSubscription.add(manager.doCloaseAccount(companyid,code,timestamp,cartid,from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CloaseAccount>() {
                    @Override
                    public void onCompleted() {
                        if (mShopCarView!=null){
                            mShopCarView.onSuccessDoCloaseAccount(mCloaseAccount);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mShopCarView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CloaseAccount cloaseAccount) {
                        mCloaseAccount=cloaseAccount;
                    }
                }));
    }

    public void getAppConfig(String companyid) {

        mCompositeSubscription.add(manager.getAppConfig(companyid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AppConfig>() {
                    @Override
                    public void onCompleted() {
                        if (mShopCarView!=null){
                            mShopCarView.onSuccessGetAppConfig(mAppConfig);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mShopCarView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(AppConfig appConfig) {
                        mAppConfig=appConfig;
                    }
                }));
    }
}
