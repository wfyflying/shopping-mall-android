package com.example.a13001.shoppingmalltemplate.modle;


import java.util.List;

/**
 * Created by axehome on 2018/1/19.
 */

public class FenLeiJsonBean {

    /**
     * name : 男装
     * erji : ["T恤","衬衫","休闲裤","卫衣"]
     */

    private String name;
    private List<String> erji;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getErji() {
        return erji;
    }

    public void setErji(List<String> erji) {
        this.erji = erji;
    }


}
