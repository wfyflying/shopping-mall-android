package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;

import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.CloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.LoginStatus;
import com.example.a13001.shoppingmalltemplate.modle.ShopCarGoods;
import com.example.a13001.shoppingmalltemplate.modle.User;
import com.example.a13001.shoppingmalltemplate.modle.UserInfo;
import com.example.a13001.shoppingmalltemplate.mvpview.ShopCarView;
import com.example.a13001.shoppingmalltemplate.mvpview.UserInfoView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import java.util.Map;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class UserInfoPredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private UserInfoView mUserInfoView;
    private UserInfo mUser;
    private CommonResult mCommonResult;
    private String ss="";


    public UserInfoPredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        mCompositeSubscription=new CompositeSubscription();
        manager=new DataManager(mContext);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mUserInfoView=(UserInfoView) view;
    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }

    /**
     * 获取会员详细信息
     * @param companyid    站点ID
     * @param code          安全校验码
     * @param timestamp     时间戳
     * @return
     */
    public void getUserInfo(String companyid,String code,String timestamp) {

        mCompositeSubscription.add(manager.getUserInfo(companyid, code,timestamp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserInfo>() {
                    @Override
                    public void onCompleted() {
                        if (mUserInfoView!=null){
                            mUserInfoView.onSuccessUserInfo(mUser);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mUserInfoView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(UserInfo userInfo) {
                        mUser=userInfo;
                    }
                }));
    }
    /**
     * 会员资料修改
     * @param companyid    站点ID
     * @param code          安全校验码
     * @param timestamp     时间戳
     * @return
     */
    public void modifyUserInfo(String companyid, String code, String timestamp, Map<String,String> map) {

        mCompositeSubscription.add(manager.ModifyUserInfo(companyid, code,timestamp,map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mUserInfoView!=null){
                            mUserInfoView.onSuccessModifyUserInfo(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mUserInfoView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
    /**
     * 上传会员头像（APP）
     * @param companyid
     * @param code
     * @param timestamp
     * @param face  头像数据，base64码
     * @return
     */
    public void modifyHeadImg(String companyid, String code, String timestamp, String face) {

        mCompositeSubscription.add(manager.upLoadHeadImg(companyid, code,timestamp,face)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mUserInfoView!=null){
                            mUserInfoView.onSuccessModifyHeadImg(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mUserInfoView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
}
