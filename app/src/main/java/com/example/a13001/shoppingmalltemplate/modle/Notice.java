package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class Notice {

    @Override
    public String toString() {
        return "Notice{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", count=" + count +
                ", pagesize=" + pagesize +
                ", pageindex=" + pageindex +
                ", list=" + list +
                '}';
    }

    /**
     * status : 1
     * returnMsg : null
     * count : 0
     * pagesize : 20
     * pageindex : 1
     * list : [{"noticeId":2,"noticeTitle":"所有人来看看","noticeAddtime":"2017-07-19 16:03"}]
     */

    private int status;
    private Object returnMsg;
    private int count;
    private int pagesize;
    private int pageindex;
    private List<ListBean> list;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public int getPageindex() {
        return pageindex;
    }

    public void setPageindex(int pageindex) {
        this.pageindex = pageindex;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * noticeId : 2
         * noticeTitle : 所有人来看看
         * noticeAddtime : 2017-07-19 16:03
         */

        private int noticeId;
        private String noticeTitle;
        private String noticeAddtime;

        @Override
        public String toString() {
            return "ListBean{" +
                    "noticeId=" + noticeId +
                    ", noticeTitle='" + noticeTitle + '\'' +
                    ", noticeAddtime='" + noticeAddtime + '\'' +
                    '}';
        }

        public int getNoticeId() {
            return noticeId;
        }

        public void setNoticeId(int noticeId) {
            this.noticeId = noticeId;
        }

        public String getNoticeTitle() {
            return noticeTitle;
        }

        public void setNoticeTitle(String noticeTitle) {
            this.noticeTitle = noticeTitle;
        }

        public String getNoticeAddtime() {
            return noticeAddtime;
        }

        public void setNoticeAddtime(String noticeAddtime) {
            this.noticeAddtime = noticeAddtime;
        }
    }
}
