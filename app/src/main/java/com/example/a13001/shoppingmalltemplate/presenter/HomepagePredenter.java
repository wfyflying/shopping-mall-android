package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;

import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.Banner;
import com.example.a13001.shoppingmalltemplate.modle.Classify;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.GoodsDetail;
import com.example.a13001.shoppingmalltemplate.modle.NewBanner;
import com.example.a13001.shoppingmalltemplate.modle.News;
import com.example.a13001.shoppingmalltemplate.modle.ShopCouponList;
import com.example.a13001.shoppingmalltemplate.mvpview.NewsView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class HomepagePredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private NewsView mNewsView;
    private News mNews;
    private Banner mBanner;
    private NewBanner mNewBanner;
    private GoodsDetail mGoodsDetail;
    private ShopCouponList mShopCouponList;
    private CommonResult mCommonResult;


    public HomepagePredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        manager=new DataManager(mContext);
        mCompositeSubscription=new CompositeSubscription();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mNewsView=(NewsView) view;

    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }

    /**
     * 获取资讯列表
     * @param companyid  站点id
     * @param channelid  频道ID，指定多个频道用“|”线分割
     * @param pagesize  每页显示数量
     * @param pageindex   当前页数
     */
    public void getSearchNews(String companyid,String channelid,String keyword,int pagesize,int pageindex){
        mCompositeSubscription.add(manager.getNewsList(companyid,channelid,keyword,pagesize,pageindex)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<News>() {
                               @Override
                               public void onCompleted() {
                                   if (mNews!=null){
                                       mNewsView.onSuccess(mNews);
                                   }
                               }

                               @Override
                               public void onError(Throwable e) {
                                   mNewsView.onError("请求失败");
                               }

                               @Override
                               public void onNext(News news) {
                                   mNews=news;
                               }
                           }

                ));
    }

    /**
     * 获取banner列表
     * @param companyid  站点id
     * @param label   碎片文件标识ID


     */
    public void getBannerList(String companyid,String label){
        mCompositeSubscription.add(manager.getBannerList(companyid,label)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Banner>() {
                               @Override
                               public void onCompleted() {
                                   if (mNewsView!=null){
                                       mNewsView.onSuccessBanner(mBanner);
                                   }
                               }

                               @Override
                               public void onError(Throwable e) {
                                   mNewsView.onError("请求失败");
                               }

                               @Override
                               public void onNext(Banner banner) {
                                   mBanner=banner;
                               }
                           }

                ));
    }
    /**
     * 获取banner列表
     * @param companyid  站点id

     */
    public void getNewBannerList(String companyid){
        mCompositeSubscription.add(manager.getNewBanner(companyid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NewBanner>() {
                               @Override
                               public void onCompleted() {
                                   if (mNewsView!=null){
                                       mNewsView.onSuccessNewBanner(mNewBanner);
                                   }
                               }

                               @Override
                               public void onError(Throwable e) {
                                   mNewsView.onError("请求失败");
                               }

                               @Override
                               public void onNext(NewBanner newBanner) {
                                   mNewBanner=newBanner;
                               }
                           }

                ));
    }
    /**
     * 获取首页商品分类列表
     * @param companyid  站点id
     * @param channelid   频道ID
     * @param pagesize   每页显示数量
     * @param pageindex   当前页数
     */
    public void getClassifyList(String companyid,String channelid,int pagesize,int pageindex) {
        mCompositeSubscription.add(manager.getClassifyList(companyid,channelid,pagesize,pageindex)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GoodsDetail>() {
                               @Override
                               public void onCompleted() {
                                   if (mNewsView!=null){
                                       mNewsView.onSuccessClassify(mGoodsDetail);
                                   }
                               }

                               @Override
                               public void onError(Throwable e) {
                                   mNewsView.onError("请求失败");
                               }

                               @Override
                               public void onNext(GoodsDetail goodsDetail) {
                                   mGoodsDetail=goodsDetail;
                               }
                           }

                ));
    }
    /**
     * ★ 领取商城优惠券
     * @param companyid   站点ID
     * @param code
     * @param timestamp
     * @param aid           优惠券活动ID
     * @return
     */
    public void obTainCoupon(String companyid, String code, String timestamp,int aid) {
        mCompositeSubscription.add(manager.obtainCoupon(companyid,code,timestamp,aid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                               @Override
                               public void onCompleted() {
                                   if (mNewsView!=null){
                                       mNewsView.onSuccessObtainCoupon(mCommonResult);
                                   }
                               }

                               @Override
                               public void onError(Throwable e) {
                                   mNewsView.onError("请求失败");
                               }

                               @Override
                               public void onNext(CommonResult commonResult) {
                                   mCommonResult=commonResult;
                               }
                           }

                ));
    }

    public void getCouponList(String companyid, String storeid, String type, int pagesize, int pageindex) {
        mCompositeSubscription.add(manager.getCouponList(companyid,storeid,type,pagesize,pageindex)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ShopCouponList>() {
                               @Override
                               public void onCompleted() {
                                   if (mNewsView!=null){
                                       mNewsView.onSuccessGetCouponList(mShopCouponList);
                                   }
                               }

                               @Override
                               public void onError(Throwable e) {
                                   mNewsView.onError("请求失败");
                               }

                               @Override
                               public void onNext(ShopCouponList shopCouponList) {
                                   mShopCouponList=shopCouponList;
                               }
                           }

                ));
    }
}
