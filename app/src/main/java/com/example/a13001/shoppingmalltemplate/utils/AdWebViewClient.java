package com.example.a13001.shoppingmalltemplate.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.R;


/**
 * Created by Administrator on 2017/3/6.
 */

public class AdWebViewClient extends WebViewClient {
    Context obj;
    Toolbar toolbar;
    String ThisTitle;
    //加载JSON需要参数
    String JsonUrl;
    String IfySiteKey="";
    String IfyTimeStamp="";
    String id="0";
    String contentType="";

    public AdWebViewClient(Context context, Toolbar toolbar, String ThisTitle, String id, String contentType){
        super();
        this.obj=context;
        this.toolbar=toolbar;
        this.ThisTitle=ThisTitle;
        this.id=id;
        this.contentType=contentType;
        this.JsonUrl=MyUtils.getMetaValue(context, "companyJSON");
        this.IfyTimeStamp=Utils.getTimeStamp();
//        this.IfySiteKey=Utils.md5(MyUtils.getMetaValue(context, "IfySiteKey") + this.IfyTimeStamp);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        super.shouldOverrideUrlLoading(view, url);
        if (url.startsWith("tel:")) {
            //调用手机拨号盘
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            obj.startActivity(intent);
        } else if (url.startsWith("smsto:")) {
            //调用短信发送
            Uri uri = Uri.parse(url);
            Intent it = new Intent(Intent.ACTION_SENDTO, uri);
            it.putExtra("sms_body", "");
            obj.startActivity(it);
        } else{
            //判断网络是否正常
            if (!Utils.isNetworkAvailable(obj)){
                //弹出提示窗口
                Toast.makeText(obj, obj.getResources().getString(R.string.error002), Toast.LENGTH_SHORT).show();
                view.loadUrl("file:///android_asset/404.html");//加载404错误页面
            } else {
                if (contentType.length()>0 && contentType.equals("adwebview")){
                    //广告页允许超链接
                    view.loadUrl(url);
                }
            }
        }
        return true;
    }

    @Override
    public  void onPageFinished(WebView view, String url){
        super.onPageFinished(view, url);
        //如果默认标题为空的话，就使用网页标题
        if (TextUtils.isEmpty(ThisTitle)) {
            toolbar.setTitle(view.getTitle());
        }
        Log.v("Window","contentType:" + contentType + " url:" + url);
        if (contentType.length()>0 && contentType.equals("webview")){
            //页面加载完皆后执行指定命令
            //Log.v("Window","contentType:javascript:GetJson('" + JsonUrl + "','" + IfySiteKey + "','" + IfyTimeStamp + "','" + id + "');");
            view.loadUrl("javascript:GetJson('" + JsonUrl + "','" + IfySiteKey + "','" + IfyTimeStamp + "','" + id + "');");
            //appendJs(view);

        }
    }

    @Override
    public  void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        super.onReceivedError(view, request, error);
        view.stopLoading();
        view.loadUrl("file:///android_asset/404.html");//加载404错误页面
    }


}
