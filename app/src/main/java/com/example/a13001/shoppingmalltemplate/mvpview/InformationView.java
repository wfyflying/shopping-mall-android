package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.Banner;
import com.example.a13001.shoppingmalltemplate.modle.GoodsDetail;
import com.example.a13001.shoppingmalltemplate.modle.News;

public interface InformationView extends View {
    void onSuccess(News mNews);
    void onSuccessBanner(Banner banner);
    void onError(String result);

}
