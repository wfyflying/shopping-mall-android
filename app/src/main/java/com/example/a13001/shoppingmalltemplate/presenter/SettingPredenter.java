package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;

import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.AppConfig;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.mvpview.SettingView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class SettingPredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private SettingView mSettingView;
    private CommonResult mCommonResult;
    private AppConfig mAppConfig;


    public SettingPredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        mCompositeSubscription=new CompositeSubscription();
        manager=new DataManager(mContext);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mSettingView=(SettingView) view;
    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }

    /**
     * 退出登录
     * @param companyid
     * @return
     */
    public void doLoginOut(String companyid) {

        mCompositeSubscription.add(manager.doLoginOut(companyid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mSettingView!=null){
                            mSettingView.onSuccessDoLoginOut(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mSettingView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }

    public void getAppConfig(String companyid) {

        mCompositeSubscription.add(manager.getAppConfig(companyid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AppConfig>() {
                    @Override
                    public void onCompleted() {
                        if (mSettingView!=null){
                            mSettingView.onSuccessGetAppConfig(mAppConfig);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mSettingView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(AppConfig appConfig) {
                        mAppConfig=appConfig;
                    }
                }));
    }
    public void getAppConfig1(String companyid) {

        mCompositeSubscription.add(manager.getAppConfig(companyid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AppConfig>() {
                    @Override
                    public void onCompleted() {
                        if (mSettingView!=null){
                            mSettingView.onSuccessGetAppConfig1(mAppConfig);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mSettingView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(AppConfig appConfig) {
                        mAppConfig=appConfig;
                    }
                }));
    }
}
