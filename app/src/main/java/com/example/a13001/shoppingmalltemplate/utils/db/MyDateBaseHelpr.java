package com.example.a13001.shoppingmalltemplate.utils.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.content.ContentValues.TAG;

public class MyDateBaseHelpr extends SQLiteOpenHelper {
    private final static String DATABASE_NAME="shopmall";
    private final static int DATABASE_VERSION = 1;
    public MyDateBaseHelpr(Context context) {
        super(context,DATABASE_NAME , null, DATABASE_VERSION);
    }



    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //创建表：选择的属性
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS select_parameter(goodsId interger, position1 interger, position interger, pa1 nvarchar(150), pa2 nvarchar(150))");
        //创建表：zfy_config，用于记录APP广告配置信息
        sqLiteDatabase.execSQL("CREATE TABLE ad_config(status interger, adSatus interger, adid interger, adTitle nvarchar(150), adImages nvarchar(150), adLink nvarchar(150))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        //如果数据库版本更新，重建各表
        if(newVersion > oldVersion) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS select_parameter");
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS ad_config");
            onCreate(sqLiteDatabase);
        }
    }
    public Cursor select(String tabName, String[] text1, String text2, String[] text3, String text4, String text5, String text6) {
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.query(tabName, text1, text2, text3, text4, text5,  text6);
        return cursor;
    }

    public Cursor select(String table, String[] columns, String selection,
                         String[] selectionArgs, String groupBy, String having,
                         String orderBy, String limit) {
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.query(table, columns, selection, selectionArgs, groupBy, having,  orderBy, limit);
        return cursor;
    }

    //数据表插入数据
    public long insert(String tabName, ContentValues cv) {
        SQLiteDatabase db=this.getWritableDatabase();
        long row=db.insert(tabName, null, cv);
        return row;
    }

    //数据表更新数据
    public long update(String tabName, ContentValues cv, String whereClause, String[] whereArgs) {
        SQLiteDatabase db=this.getWritableDatabase();
        long row=db.update(tabName, cv, whereClause, whereArgs);
        return row;
    }

    //删除数据表数据
    public long delete(String tabName, String whereClause, String[] whereArgs) {
        SQLiteDatabase db=this.getWritableDatabase();
        long row = db.delete(tabName, whereClause, whereArgs);
        return row;
    }


    //判断指定数据表是否存在
    public boolean tabIsExist(String tabName){
        SQLiteDatabase db=this.getReadableDatabase();
        boolean result = false;
        if(tabName == null){
            return false;
        }
        Cursor cursor = null;
        try {
            String sql = "select count(*) as c from sqlite_master where type='table' and name='" + tabName + "' ";
            cursor = db.rawQuery(sql, null);
            if(cursor.moveToNext()){
                int count = cursor.getInt(0);
                if(count>0){
                    result = true;
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
        return result;
    }

    //创建数据表
    public void createTable(String tabName) {
        SQLiteDatabase db=this.getReadableDatabase();
        String sql="";
        Log.d(tabName,TAG);
        if (tabName.equals("ad_config")){
            //创建表：
            sql="Create table " + tabName + " (status interger, adSatus interger, adid interger, adTitle nvarchar(150), adImages nvarchar(150), adLink nvarchar(150));";
        }
        db.execSQL(sql);
    }

    //删除一个数据表
    public void dropTable(String tabName) {
        SQLiteDatabase db=this.getReadableDatabase();
        String sql="DROP TABLE IF EXISTS " + tabName;
        db.execSQL(sql);
    }
}
