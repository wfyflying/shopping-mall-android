package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.modle.ContentFilter;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

public class FilterChildAttributeadapter extends BaseAdapter {
    private Context mContext;
    private List<ContentFilter.AttributeDataBean> mList;
    private int selectorPosition;
    public FilterChildAttributeadapter(Context mContext, List<ContentFilter.AttributeDataBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view= LayoutInflater.from(mContext).inflate(R.layout.iten_gv_filter,viewGroup,false);
            holder=new ViewHolder();
            holder.mTvTitle =view.findViewById(R.id.tv_title);
           view.setTag(holder);
            AutoUtils.autoSize(view);
        }else{
            holder= (ViewHolder) view.getTag();
        }
        if (selectorPosition==i){
            holder.mTvTitle.setBackground(mContext.getResources().getDrawable(R.drawable.shape_filter_true));
            holder.mTvTitle.setTextColor(mContext.getResources().getColor(R.color.a009944));
            holder.mTvTitle.setText(mList.get(i).getName());
        }else{
            holder.mTvTitle.setBackground(mContext.getResources().getDrawable(R.drawable.shape_filter_false));
            holder.mTvTitle.setTextColor(mContext.getResources().getColor(R.color.t333));
            holder.mTvTitle.setText(mList.get(i).getName());
        }


        /**
         * 筛选子列表
         */
        return view;
    }
    class ViewHolder{
        TextView mTvTitle;
    }
    public void changeState(int pos) {
        selectorPosition = pos;
        notifyDataSetChanged();

    }
}
