package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.CloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.Goods;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;

import java.text.DecimalFormat;
import java.util.List;

public class AffirmOrderLvAdapter extends BaseAdapter {
    private Context mContext;
    private List<CloaseAccount.ShopListBean> mList;

    public AffirmOrderLvAdapter(Context mContext, List<CloaseAccount.ShopListBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            view= LayoutInflater.from(mContext).inflate(R.layout.item_lv_affirmorder,parent,false);
            holder=new ViewHolder(view);
            view.setTag(holder);
        }else{
            holder= (ViewHolder) view.getTag();
        }
        GlideUtils.setNetImage(mContext, AppConstants.INTERNET_HEAD+mList.get(i).getCartImg(),holder.ivLogo);
        holder.mTvGoodsName.setText(mList.get(i).getCommodityName()!=null?mList.get(i).getCommodityName():"");
        holder.mTvDestribe.setText(mList.get(i).getCommodityProperty()!=null?mList.get(i).getCommodityProperty():"");

        DecimalFormat df=new DecimalFormat("0.00");
        String ss= df.format(mList.get(i).getCommodityXPrice());
        if (".00".equals(ss)){
            holder.mTvPrice.setText("原价：¥0.00"  );
        }else{
            holder.mTvPrice.setText("原价：¥" +ss );
        }
        String ss1= df.format(mList.get(i).getCommodityYhPrice());
        if (".00".equals(ss1)){
            holder.tvYouHuiJia.setText("(优惠价：-¥0.00)");
        }else{
            holder.tvYouHuiJia.setText("(优惠价：-¥"+ss1+")");
        }
        String ss2= df.format(mList.get(i).getCommodityZPrice());
        if (".00".equals(ss2)){
            holder.tvDangqianjia.setText("当前价：¥0.00");
        }else{
            holder.tvDangqianjia.setText("当前价：¥"+ss2);
        }
        holder.mTvCount.setText("数量"+"X"+mList.get(i).getCommodityNumber()+"");

        return view;
    }
    class ViewHolder{

        TextView mTvGoodsName,mTvDestribe,mTvPrice,mTvCount,tvYouHuiJia,tvDangqianjia;
        ImageView ivLogo;
        public ViewHolder(View view) {
            mTvGoodsName = view.findViewById(R.id.tv_itemaffirmorder_goodsname);
            mTvDestribe = view.findViewById(R.id.tv_itemaffirmorder_goodsdescribe);
            mTvPrice = view.findViewById(R.id.tv_itemaffirmorder_price);
            mTvCount = view.findViewById(R.id.tv_itemaffirmorder_count);
            ivLogo = view.findViewById(R.id.iv_itemaffirmorder_logo);
            tvYouHuiJia = view.findViewById(R.id.tv_itemaffirmorder_youhuiprice);
            tvDangqianjia = view.findViewById(R.id.tv_itemaffirmorder_dangqiangjia);
        }
    }
}
