package com.example.a13001.shoppingmalltemplate.mvpview;


import com.example.a13001.shoppingmalltemplate.modle.AddressOrderId;
import com.example.a13001.shoppingmalltemplate.modle.AfterSale;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.ShouHouDetail;
import com.example.a13001.shoppingmalltemplate.modle.UpFile;

public interface ApplyAfterSaleView extends View{
    void onSuccess(CommonResult commonResult);
    void onSuccessGetAfterSaleList(AfterSale afterSale);
    void onSuccessGetAddressOrderId(AddressOrderId addressOrderId);
    void onSuccessGetShouHouDetail(ShouHouDetail shouHouDetail);
    void onSuccessUpFile(UpFile upFile);
    void onError(String result);
}
