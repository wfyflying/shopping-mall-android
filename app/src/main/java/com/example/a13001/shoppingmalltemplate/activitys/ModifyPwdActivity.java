package com.example.a13001.shoppingmalltemplate.activitys;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.CloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ModifyPwdActivity extends BaseActivity {
    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_title_right)
    TextView tvTitleRight;
    @BindView(R.id.et_modifypwd_oldpwd)
    EditText etModifypwdOldpwd;
    @BindView(R.id.et_modifypwd_newpwd)
    EditText etModifypwdNewpwd;
    @BindView(R.id.et_modifypwd_surenewpwd)
    EditText etModifypwdSurenewpwd;
    private RelativeLayout mRlBack;
    private TextView mTvTitle;
    private TextView mTvRight;
    private EditText mEtOldPwd;
    private EditText mEtNewPwd;
    private EditText mEtSureNewPwd;
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;

    private CommonResult mCommonResult;
    private static final String TAG = "ModifyPwdActivity";
    private String code;
    private String timestamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_pwd);
        ButterKnife.bind(this);
        initData();
    }

    /**
     * 初始化控件
     */
    private void initData() {
        tvTitleCenter.setText("修改密码");
        tvTitleRight.setText("保存");
        tvTitleRight.setVisibility(View.VISIBLE);

        mCompositeSubscription=new CompositeSubscription();
        manager=new DataManager(ModifyPwdActivity.this);

        String safetyCode = MyUtils.getMetaValue(ModifyPwdActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        timestamp = Utils.getTimeStamp();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    /**
     * 各控件的点击事件
     *
     * @param view
     */


    @OnClick({R.id.iv_title_back, R.id.tv_title_right})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_title_back:
                onBackPressed();
                break;
                //保存
            case R.id.tv_title_right:
               String oldPwd=etModifypwdOldpwd.getText().toString().trim();
               String newPwd=etModifypwdNewpwd.getText().toString().trim();
               String confirmPwd=etModifypwdSurenewpwd.getText().toString().trim();
               if (TextUtils.isEmpty(oldPwd)){
                   Toast.makeText(this, "更新失败，旧密码不能为空", Toast.LENGTH_SHORT).show();
                   return;
               }
                if (TextUtils.isEmpty(newPwd)){
                    Toast.makeText(this, "更新失败，新密码不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(confirmPwd)){
                    Toast.makeText(this, "更新失败，确认新密码不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }
                modifyPwd(AppConstants.COMPANY_ID, code, timestamp,oldPwd,newPwd,confirmPwd);
                break;
        }
    }
    /**
     * ★ 修改会员登录密码
     * @param companyid
     * @param code
     * @param timestamp
     * @param oldpass       旧密码，如果是第三方登录首次修改密码，值可以为空
     * @param newpass       新密码
     * @param confirmpass    确认新密码
     * @return
     */
    public void modifyPwd(String companyid,String code,String timestamp,String oldpass,String newpass,String confirmpass) {

        mCompositeSubscription.add(manager.modifyPwd(companyid,code,timestamp,oldpass,newpass,confirmpass)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        int status=mCommonResult.getStatus();
                        if (status>0){
                            Toast.makeText(ModifyPwdActivity.this, ""+mCommonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            Toast.makeText(ModifyPwdActivity.this, ""+mCommonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();
                        }
                }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: "+"请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
}
