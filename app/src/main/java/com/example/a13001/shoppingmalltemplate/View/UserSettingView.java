package com.example.a13001.shoppingmalltemplate.View;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;


public class UserSettingView extends LinearLayout {

    private ImageView mIvLeft;
    private ImageView mIvRight;
    private TextView mTvLeft;
    private TextView mTvRight;
    private String txtLeft;
    private String txtRight;
    private boolean isRightTxtVisible;
    private int leftImage;
    private int rightImage;
    private boolean isRightIvVisible;
    private int leftColor;
    private int rightColor;
    private boolean isLeftIvVisible;

    public UserSettingView(Context context) {
        super(context);
        init(context);
    }

    public UserSettingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
        initTypeArray(context,attrs);
    }


    public UserSettingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
        initTypeArray(context,attrs);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.layout_usersetting,this,true);
        mIvLeft = ((ImageView) findViewById(R.id.iv_usview_left));
        mIvRight = ((ImageView) findViewById(R.id.iv_usview_right));
        mTvLeft = ((TextView) findViewById(R.id.tv_usview_left));
        mTvRight = ((TextView) findViewById(R.id.tv_usview_right));
    }

    private void initTypeArray(Context context, AttributeSet attrs) {
         TypedArray typedArray= context.obtainStyledAttributes(attrs,R.styleable.UserSettingView);
        txtLeft = typedArray.getString(R.styleable.UserSettingView_txt_left);
        txtRight = typedArray.getString(R.styleable.UserSettingView_txt_right);
        isRightTxtVisible = typedArray.getBoolean(R.styleable.UserSettingView_txt_right_visible,true);
        if (isRightTxtVisible){
            mTvRight.setVisibility(VISIBLE);
        }else{
            mTvRight.setVisibility(INVISIBLE);
        }
        isRightIvVisible = typedArray.getBoolean(R.styleable.UserSettingView_iv_right_visible,true);
        if (isRightIvVisible){
            mIvRight.setVisibility(VISIBLE);
        }else{
            mIvRight.setVisibility(GONE);
        }
        isLeftIvVisible = typedArray.getBoolean(R.styleable.UserSettingView_iv_left_visible,true);
        if (isLeftIvVisible){
            mIvLeft.setVisibility(VISIBLE);
        }else{
            mIvLeft.setVisibility(GONE);
        }
        leftImage = typedArray.getResourceId(R.styleable.UserSettingView_image_left,R.mipmap.ic_launcher_round);
        rightImage = typedArray.getResourceId(R.styleable.UserSettingView_image_right,R.drawable.arrow_right);
        leftColor = typedArray.getColor(R.styleable.UserSettingView_txt_left_color, Color.BLACK);
        rightColor = typedArray.getColor(R.styleable.UserSettingView_txt_right_color,Color.BLACK);
        setTxtLeftColor(leftColor);
        setTxtRightColor(rightColor);
//        int bgColor = typedArray.getResourceId(R.styleable.UserSettingView_br_color, Color.WHITE);
//        setBackgroundColor(bgColor);

        setTxtLeft(txtLeft);
        setTxtRight(txtRight);
        setLeftImage(leftImage);
        setRightImage(rightImage);
    }
    public void setTxtLeft(String sss){
        if (!TextUtils.isEmpty(sss)){
            mTvLeft.setText(sss);
        }
    }
    public void setTxtLeftSize(float sss){

            mTvLeft.setTextSize(sss);

    }
    public void setTxtLeftColor(int color){

            mTvLeft.setTextColor(color);

    }
    public void setTxtRightColor(int color){

        mTvRight.setTextColor(color);

    }
    public void setTxtRight(String txtRight){
        if (!TextUtils.isEmpty(txtRight)){
            mTvRight.setText(txtRight);
        }
    }
    public void setLeftImage(int leftImage){
        mIvLeft.setImageResource(leftImage);
    }
    public void setRightImage(int rightImage){
        mIvRight.setImageResource(rightImage);
    }
}
