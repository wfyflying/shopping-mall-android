package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;

public interface BindPhoneView extends View{
    void onSuccessGetCode(CommonResult commonResult);
    void onSuccessBindPhone(CommonResult commonResult);
    void onError(String result);
}
