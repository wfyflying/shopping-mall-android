package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.activitys.GoodsDetailActivity;
import com.example.a13001.shoppingmalltemplate.activitys.GoodsListActivity;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.Banner;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.DetailGoods;
import com.example.a13001.shoppingmalltemplate.modle.GoodsDetail;
import com.example.a13001.shoppingmalltemplate.modle.GoodsList;
import com.example.a13001.shoppingmalltemplate.modle.GoodsParameters;
import com.example.a13001.shoppingmalltemplate.modle.LoginStatus;
import com.example.a13001.shoppingmalltemplate.modle.News;
import com.example.a13001.shoppingmalltemplate.modle.Result;
import com.example.a13001.shoppingmalltemplate.modle.ShopCarGoods;
import com.example.a13001.shoppingmalltemplate.mvpview.GoodsDetailView;
import com.example.a13001.shoppingmalltemplate.mvpview.NewsView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.google.gson.Gson;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class GoodsDetailPredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private GoodsDetailView mGoodsDetailView;
    private GoodsParameters mGoodsParameters;
    private DetailGoods mDetailGoods;
    private GoodsList mGoodsList;
    private Result mResult;
    private CommonResult mCommonResult;
    private LoginStatus mLoginStatus;
    private ShopCarGoods mShopCarGoods;
    public GoodsDetailPredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        mCompositeSubscription=new CompositeSubscription();
        manager=new DataManager(mContext);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mGoodsDetailView=(GoodsDetailView) view;
    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }

    /**
     * 获取商品详情
     * @param companyid
     * @param id
     */
    public void getGoodDetail(String companyid,int id) {

        mCompositeSubscription.add(manager.getGoodsDetail(companyid, id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<DetailGoods>() {
                    @Override
                    public void onCompleted() {
                        if (mGoodsDetailView!=null){
                            mGoodsDetailView.onSuccessGoodsDetail(mDetailGoods);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                      mGoodsDetailView.onError("请求失败");
                    }

                    @Override
                    public void onNext(DetailGoods detailGoods) {
                        mDetailGoods=detailGoods;
                    }
                }));
    }

    /**
     * 获取商品规格参数
     * @param companyid
     * @param id
     */
    public void getGoodsParameters(String companyid,int id){
        mCompositeSubscription.add(manager.getGoodsParemters(companyid, id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GoodsParameters>() {
                    @Override
                    public void onCompleted() {
                        if (mGoodsDetailView!=null){
                            mGoodsDetailView.onSuccess(mGoodsParameters);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mGoodsDetailView.onError("请求失败");
                    }

                    @Override
                    public void onNext(GoodsParameters goodsParameters) {
                        mGoodsParameters=goodsParameters;
                    }
                }));
    }
    /**
     * 加入购物车
     * @param companyid 站点ID
     * @param id   内容ID
     * @param h    货号
     * @param s     数量
     * @param r    操作类型，1 立刻购买 2 购买并进入购物车页面 3 加入购物车成功
     * @param t    商品类型 0 普通商品 1 促销秒杀 3 积分兑换
     * @param from  来源，pc 电脑端 mobile 移动端
     * @return
     */
    public void addShopCar(String companyid,int id,String h,int s,int r,int t,String from){
        mCompositeSubscription.add(manager.addShopCar(companyid,id,h,s,r,t,from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Result>() {
                    @Override
                    public void onCompleted() {
                        if (mGoodsDetailView!=null){
                            mGoodsDetailView.onSuccessAddShopCar(mResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mGoodsDetailView.onError("请求失败"+e.toString()+"fffff"+e.getCause()+e.getMessage());
                    }

                    @Override
                    public void onNext(Result json) {
                        mResult=json;
                    }
                }));
    }
    /**
     * 内容收藏判断
     * @param companyid
     * @param id   内容ID
     * @return
     */
    public void ifCollect(String companyid,int id){
        mCompositeSubscription.add(manager.ifCollect(companyid,id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mGoodsDetailView!=null){
                            mGoodsDetailView.onSuccessIfCollect(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mGoodsDetailView.onError("请求失败"+e.toString()+"fffff"+e.getCause()+e.getMessage());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
    /**
     * 内容收藏
     * @param companyid
     * @param id   内容ID
     * @return
     */
    public void setCollect(String companyid,int id){
        mCompositeSubscription.add(manager.setCollect(companyid,id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mGoodsDetailView!=null){
                            mGoodsDetailView.onSuccessSetCollect(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mGoodsDetailView.onError("请求失败"+e.toString()+"fffff"+e.getCause()+e.getMessage());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
    public void getGoodList(String companyid, String channelid,String classid,String elite,String hot,String xinpin,String cuxiao,String order,
                             String specialid,String price1,String price2,String keyword,String excludeid, int pagesize, int pageindex) {

        mCompositeSubscription.add(manager.getGoodsList(companyid, channelid, classid, elite,hot
                , xinpin,cuxiao, order,specialid,price1,price2,keyword,excludeid, pagesize, pageindex)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GoodsList>() {
                    @Override
                    public void onCompleted() {
                        if (mGoodsDetailView!=null){
                            mGoodsDetailView.onSuccessGetGoodList(mGoodsList);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mGoodsDetailView.onError("请求失败"+e.toString()+"fffff"+e.getCause()+e.getMessage());
                    }

                    @Override
                    public void onNext(GoodsList goodsList) {

                        mGoodsList=goodsList;

                    }
                }));
    }
    /**
     * 判断登录状态
     * @param companyid  站点ID
     * @param code    安全校验码
     * @param timestamp  时间戳
     * @param from   来源，pc 电脑端 mobile 移动端
     * @return
     */
    public void getLoginStatus(String companyid,String code,String timestamp, String from) {

        mCompositeSubscription.add(manager.getLoginStatus(companyid,code,timestamp, from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<LoginStatus>() {
                    @Override
                    public void onCompleted() {
                        if (mGoodsDetailView!=null){
                            mGoodsDetailView.onSuccessLoginStatus(mLoginStatus);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mGoodsDetailView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(LoginStatus loginStatus) {
                        mLoginStatus=loginStatus;
                    }
                }));
    }
    /**
     * 获取购物车商品信息
     * @param companyid  站点ID
     * @param from   来源，pc 电脑端 mobile 移动端
     * @return
     */
    public void getShopCarGoods(String companyid,String from) {

        mCompositeSubscription.add(manager.getShopCarGoods(companyid, from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ShopCarGoods>() {
                    @Override
                    public void onCompleted() {
                        if (mGoodsDetailView!=null){
                            mGoodsDetailView.onSuccessGetShopCarGoods(mShopCarGoods);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mGoodsDetailView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(ShopCarGoods shopCarGoods) {
                        mShopCarGoods=shopCarGoods;
                    }
                }));
    }
}
