package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.User;

public interface ForgetPwdView extends View{
    void onSuccessDoResetPwd(CommonResult commonResult);
    void onSuccessGetPicCode(CommonResult commonResult);
    void onSuccessGetPhoneCode(CommonResult commonResult);
    void onError(String result);
}
