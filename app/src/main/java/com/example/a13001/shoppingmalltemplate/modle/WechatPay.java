package com.example.a13001.shoppingmalltemplate.modle;

public class WechatPay {

    /**
     * status : 1
     * returnMsg : null
     * appid : wxfb5cbd375373fe1d
     * partnerid : 1491031792
     * nonceStr : EBB23524DDA2382C21B5F8D9B82B018C
     * timeStamp : 1533622071
     * prepayid : 4200000144201808072564544386
     * paySign : C66983D6F20212E43BC3C057907B8186
     */

    private int status;
    private String returnMsg;
    private String appid;
    private int partnerid;
    private String nonceStr;
    private String timeStamp;
    private String prepayid;
    private String paySign;

    @Override
    public String toString() {
        return "WechatPay{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", appid='" + appid + '\'' +
                ", partnerid=" + partnerid +
                ", nonceStr='" + nonceStr + '\'' +
                ", timeStamp='" + timeStamp + '\'' +
                ", prepayid='" + prepayid + '\'' +
                ", paySign='" + paySign + '\'' +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public int getPartnerid() {
        return partnerid;
    }

    public void setPartnerid(int partnerid) {
        this.partnerid = partnerid;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getPrepayid() {
        return prepayid;
    }

    public void setPrepayid(String prepayid) {
        this.prepayid = prepayid;
    }

    public String getPaySign() {
        return paySign;
    }

    public void setPaySign(String paySign) {
        this.paySign = paySign;
    }
}
