package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.MyListView;
import com.example.a13001.shoppingmalltemplate.activitys.ApplyAfterSaleActivity;
import com.example.a13001.shoppingmalltemplate.activitys.GoEvaluateActivity;
import com.example.a13001.shoppingmalltemplate.activitys.IntegralOrderDetailActivity;
import com.example.a13001.shoppingmalltemplate.activitys.LogisticsInfoActivity;
import com.example.a13001.shoppingmalltemplate.activitys.OrderDetailActivity;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.Order;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Administrator on 2017/8/9.
 */

public class IntegralOrderListLvAdapter extends BaseAdapter {
    private Context context;
    private List<Order.OrderListBean> mList;
    private changeOrderStateInterface changeOrderStateInterface;

    public IntegralOrderListLvAdapter(Context context, List<Order.OrderListBean> mList) {
        this.context = context;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolder vh = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.integralorder_item, parent, false);
            vh = new ViewHolder(view);
            view.setTag(vh);
            AutoUtils.autoSize(view);
        } else {
            vh = (ViewHolder) view.getTag();
        }
        vh.tvIntegralorderitemOrderid.setText(mList.get(position).getOrdersNumber() != null ? "订单号：" + mList.get(position).getOrdersNumber() : "");
        GlideUtils.setNetImage(context, AppConstants.INTERNET_HEAD+mList.get(position).getOrderGoodsList().get(0).getCartImg(),vh.ivIntegralorderitemLogo);
        vh.tvIntegralorderitemGoodsanme.setText(mList.get(position).getOrderGoodsList().get(0).getCommodityName()!=null?mList.get(position).getOrderGoodsList().get(0).getCommodityName():"");
        vh.tvIntegralorderitemNum.setText("数量：X"+mList.get(position).getOrderGoodsList().get(0).getCommodityNumber()+"");
        vh.tvIntegralorderitemTotalprice.setText( mList.get(position).getOrderTotalprice() + "");
        vh.tvIntegralorderitemDate.setText(mList.get(position).getOrderDate() != null ? mList.get(position).getOrderDate() : "");
        vh.llitem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, IntegralOrderDetailActivity.class).putExtra("ordernumber", mList.get(position).getOrdersNumber()));

            }
        });
        vh.tvIntegralorderitemSureorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeOrderStateInterface.doAffirmOrder(position,mList.get(position).getOrdersNumber());
            }
        });
        //订单状态，1 待支付 2 未发货 3 已发货 4 已签收
        int state = mList.get(position).getOrderStatus();
        switch (state) {
            case 1:
                vh.tvIntegralorderitemSureorder.setVisibility(View.GONE);
                break;
            case 2:
                vh.tvIntegralorderitemSureorder.setVisibility(View.GONE);
                break;
            case 3:
               vh.tvIntegralorderitemSureorder.setVisibility(View.VISIBLE);
                break;
            case 4:
                vh.tvIntegralorderitemSureorder.setVisibility(View.GONE);
                break;
        }

        return view;
    }

    /**
     * 改变订单状态接口
     */
    public interface changeOrderStateInterface {
        void doAffirmOrder(int position, String orderNumber);
    }

    public void setChangeOrderStateInterface(changeOrderStateInterface changeOrderStateInterface) {
        this.changeOrderStateInterface = changeOrderStateInterface;
    }



    class ViewHolder {
        @BindView(R.id.tv_integralorderitem_orderid)
        TextView tvIntegralorderitemOrderid;
        @BindView(R.id.tv_integralorderitem_sureorder)
        TextView tvIntegralorderitemSureorder;
        @BindView(R.id.iv_integralorderitem_logo)
        ImageView ivIntegralorderitemLogo;
        @BindView(R.id.tv_integralorderitem_goodsanme)
        TextView tvIntegralorderitemGoodsanme;
        @BindView(R.id.tv_integralorderitem_num)
        TextView tvIntegralorderitemNum;
        @BindView(R.id.tv_integralorderitem_date)
        TextView tvIntegralorderitemDate;
        @BindView(R.id.tv_count2)
        TextView tvCount2;
        @BindView(R.id.tv_integralorderitem_totalprice)
        TextView tvIntegralorderitemTotalprice;
        @BindView(R.id.ll_item)
        LinearLayout llitem;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
