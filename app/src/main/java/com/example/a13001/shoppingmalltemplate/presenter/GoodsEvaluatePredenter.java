package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;


import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.GoodsEvaluateDetail;
import com.example.a13001.shoppingmalltemplate.modle.GoodsEvaluateList;
import com.example.a13001.shoppingmalltemplate.mvpview.GoodsEvaluateView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class GoodsEvaluatePredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private GoodsEvaluateView mGoodsEvaluateView;
    private GoodsEvaluateList mGoodsEvaluateList;
    private GoodsEvaluateDetail mGoodsEvaluateDetail;




    public GoodsEvaluatePredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        mCompositeSubscription=new CompositeSubscription();
        manager=new DataManager(mContext);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mGoodsEvaluateView=(GoodsEvaluateView) view;
    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }

    /**
     * ★ 获取评价商品列表
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @param status            查询评价状态，0 待评价 1 已评价，为空则所有
     * @param from
     * @return
     */

    public void getGoodsEvaluateList(String companyid, String code, String timestamp, int pagesize,int pageindex,String status,String from) {

        mCompositeSubscription.add(manager.getGoodsEvaluateList(companyid,code,timestamp, pagesize,pageindex,status,from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GoodsEvaluateList>() {
                    @Override
                    public void onCompleted() {
                        if (mGoodsEvaluateView!=null){
                            mGoodsEvaluateView.onSuccessGetGoodsEvaluateList(mGoodsEvaluateList);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mGoodsEvaluateView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(GoodsEvaluateList goodsEvaluateList) {
                        mGoodsEvaluateList=goodsEvaluateList;
                    }
                }));
    }
    /**
     *★ 获取评价商品详情
     * @param companyid
     * @param code
     * @param timestamp
     * @param cartid    商品购物车ID
     * @param from
     * @return
     */

    public void getGoodsEvaluateDetail(String companyid, String code, String timestamp, String cartid,String from) {

        mCompositeSubscription.add(manager.getGoodsEvaluateDetail(companyid,code,timestamp,cartid,from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GoodsEvaluateDetail>() {
                    @Override
                    public void onCompleted() {
                        if (mGoodsEvaluateView!=null){
                            mGoodsEvaluateView.onSuccessGetGoodsEvaluateDetail(mGoodsEvaluateDetail);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mGoodsEvaluateView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(GoodsEvaluateDetail goodsEvaluateDetail) {
                        mGoodsEvaluateDetail=goodsEvaluateDetail;
                    }
                }));
    }
}
