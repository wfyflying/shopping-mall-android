package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.modle.Message;
import com.example.a13001.shoppingmalltemplate.modle.Notice;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Administrator on 2017/8/9.
 */

public class NoticeLvAdapter extends BaseAdapter {
    private Context context;
    private List<Notice.ListBean> mList;

    public NoticeLvAdapter(Context context, List<Notice.ListBean> mList) {
        this.context = context;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder vh = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_lv_message, parent, false);
            vh = new ViewHolder(view);
            view.setTag(vh);
            AutoUtils.autoSize(view);
        } else {
            vh = (ViewHolder) view.getTag();
        }
        vh.tvItemmessageContent.setText(mList.get(position).getNoticeTitle()!=null?mList.get(position).getNoticeTitle():"");
        vh.tvItemmessageDate.setText(mList.get(position).getNoticeAddtime()!=null?mList.get(position).getNoticeAddtime():"");
        return view;
    }


    static class ViewHolder {
        @BindView(R.id.tv_itemmessage_content)
        TextView tvItemmessageContent;
        @BindView(R.id.tv_itemmessage_date)
        TextView tvItemmessageDate;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
