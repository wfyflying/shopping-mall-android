package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.User;

public interface RegisterView extends View{
    void onSuccessDoRegister(User user);
    void onSuccessDoLogin(User user);
    void onSuccessGetPicCode(CommonResult commonResult);
    void onSuccessGetPhoneCode(CommonResult commonResult);
    void onError(String result);
}
