package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;


import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.AddressOrderId;
import com.example.a13001.shoppingmalltemplate.modle.AfterSale;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.ShouHouDetail;
import com.example.a13001.shoppingmalltemplate.modle.UpFile;
import com.example.a13001.shoppingmalltemplate.mvpview.ApplyAfterSaleView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import okhttp3.MultipartBody;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ApplyAfterSalePredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private ApplyAfterSaleView mApplyAfterSaleView;
    private CommonResult mCommonResult;
    private AfterSale mAfterSale;
    private AddressOrderId mAddressOrderId;
    private UpFile mUpFile;
    private ShouHouDetail mShouHouDetail;


    public ApplyAfterSalePredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        mCompositeSubscription=new CompositeSubscription();
        manager=new DataManager(mContext);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mApplyAfterSaleView=(ApplyAfterSaleView) view;
    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }

    /**
     * ★ 提交商品售后服务
     * @param companyid
     * @param code
     * @param timestamp
     * @param cartid        商品购物车ID
     * @param commodityid    商品ID
     * @param ordersid       商品订单ID
     * @param repairstype   售后服务类型，1 退货 2 换货 3 维修
     * @param repairsnumber      申请售后商品数量
     * @param repairsproof          申请售后商品申请凭据
     * @param repairscontent     申请售后商品原因
     * @param repairsimages      申请售后商品图片，多张图片用“|”线分割
     * @param repairsname       申请人名字
     * @param repairsphone           申请人电话
     * @param Receiptaddress      申请人地址（格式：省/市,市/区,区/县,详细地址）
     * @param repairszipcode     申请人邮编
     * @return
     */
    public void commitShouHou(String companyid,String code,String timestamp,int cartid,int commodityid,String ordersid,
                              int repairstype,int repairsnumber,String repairsproof,String repairscontent,String repairsimages,
                              String repairsname,String repairsphone,String Receiptaddress,String repairszipcode) {

        mCompositeSubscription.add(manager.commitShouHou(companyid,code,timestamp,cartid,commodityid,ordersid,repairstype,repairsnumber,repairsproof,repairscontent,repairsimages,
                repairsname,repairsphone,Receiptaddress,repairszipcode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mApplyAfterSaleView!=null){
                            mApplyAfterSaleView.onSuccess(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mApplyAfterSaleView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
    /**
     *售后商品列表
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @param status
     * @param repairsid
     * @param startdate
     * @param overdate
     * @param from
     * @return
     */

    public void getAfterSaleList(String companyid,String code,String timestamp,int pagesize,int pageindex,int status,
                              String repairsid,String startdate,String overdate,String from) {

        mCompositeSubscription.add(manager.getAfterSaleList(companyid,code,timestamp,pagesize,pageindex,status,repairsid,startdate,overdate,from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AfterSale>() {
                    @Override
                    public void onCompleted() {
                        if (mApplyAfterSaleView!=null){
                            mApplyAfterSaleView.onSuccessGetAfterSaleList(mAfterSale);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mApplyAfterSaleView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(AfterSale afterSale) {
                        mAfterSale=afterSale;
                    }
                }));
    }
    /**
     * ★ 订单号获取商品收货地址
     * @param companyid
     * @param code
     * @param timestamp
     * @param ordersid
     * @return
     */
    public void getAddressOrderId(String companyid,String code,String timestamp,String ordersid) {

        mCompositeSubscription.add(manager.getAddressOrderId(companyid,code,timestamp,ordersid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AddressOrderId>() {
                    @Override
                    public void onCompleted() {
                        if (mApplyAfterSaleView!=null){
                            mApplyAfterSaleView.onSuccessGetAddressOrderId(mAddressOrderId);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mApplyAfterSaleView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(AddressOrderId addressOrderId) {
                        mAddressOrderId=addressOrderId;
                    }
                }));
    }
    public void upFile(String companyid, String code, String timestamp, MultipartBody.Part file) {

        mCompositeSubscription.add(manager.upFile(companyid,code,timestamp,file)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UpFile>() {
                    @Override
                    public void onCompleted() {
                        if (mApplyAfterSaleView!=null){
                            mApplyAfterSaleView.onSuccessUpFile(mUpFile);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mApplyAfterSaleView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(UpFile upFile) {
                        mUpFile=upFile;
                    }
                }));
    }
    /**
     * ★ 获取售后服务单详情
     * @param companyid
     * @param code
     * @param timestamp
     * @param cartid
     * @param commodityid
     * @param from
     * @return
     */

    public void getShouHouDetail(String companyid, String code, String timestamp, String cartid, String commodityid, String from) {

        mCompositeSubscription.add(manager.getShouHouDetail(companyid,code,timestamp,cartid,commodityid,from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ShouHouDetail>() {
                    @Override
                    public void onCompleted() {
                        if (mApplyAfterSaleView!=null){
                            mApplyAfterSaleView.onSuccessGetShouHouDetail(mShouHouDetail);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mApplyAfterSaleView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(ShouHouDetail shouHouDetail) {
                        mShouHouDetail=shouHouDetail;
                    }
                }));
    }
}
