package com.example.a13001.shoppingmalltemplate.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.GoodsList;
import com.example.a13001.shoppingmalltemplate.modle.ShopCouponList;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

public class HomeCouponRvAdapter extends RecyclerView.Adapter<HomeCouponRvAdapter.ViewHolder>{
    private Context mContext;
    private List<ShopCouponList.ListBean> mList;
    GoodsListRvAdapter.onItemClickListener onItemClickListener;
    public HomeCouponRvAdapter(Context mContext, List<ShopCouponList.ListBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }
    public interface onItemClickListener{
        void onClick(int position);
    }
    public void setOnItemClickListener(GoodsListRvAdapter.onItemClickListener onItemClickListener){
        this.onItemClickListener=onItemClickListener;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.item_rv_homecoupon,parent,false);
        ViewHolder holder=new ViewHolder(view);
        return holder;
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.tvMoney.setText(mList.get(position).getQmoney()+"");
        holder.tvContent.setText(mList.get(position).getAtitle()!=null?mList.get(position).getAtitle():"");
        if (position%2==0){
            holder.mLlBg.setBackground(mContext.getResources().getDrawable(R.drawable.img_coupon_home1));
        }else{
            holder.mLlBg.setBackground(mContext.getResources().getDrawable(R.drawable.img_coupon_home2));
        }


        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private final TextView tvMoney;
        private final TextView tvContent;
        private final LinearLayout mLlBg;

        public ViewHolder(View itemView) {
            super(itemView);
            AutoUtils.autoSize(itemView);
            tvContent = itemView.findViewById(R.id.tv_homecoupon_content);
            tvMoney = itemView.findViewById(R.id.tv_homecoupon_money);
            mLlBg = itemView.findViewById(R.id.ll_couponhome_bg);

        }
    }
}
