package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.CloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.IntegralCloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.LoginStatus;
import com.example.a13001.shoppingmalltemplate.modle.MyCouponList;
import com.example.a13001.shoppingmalltemplate.modle.RefreshYunFei;
import com.example.a13001.shoppingmalltemplate.modle.ShopCarGoods;
import com.example.a13001.shoppingmalltemplate.mvpview.AffirmOrderView;
import com.example.a13001.shoppingmalltemplate.mvpview.ShopCarView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class AffirmOrderPredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private AffirmOrderView mAffirmOrderView;
    private CloaseAccount mCloaseAccount;
    private CommonResult mCommonResult;
    private Address mAddress;
    private MyCouponList mMyCouponList;
    private RefreshYunFei mRefreshYunFei;
    public AffirmOrderPredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        mCompositeSubscription=new CompositeSubscription();
        manager=new DataManager(mContext);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mAffirmOrderView=(AffirmOrderView) view;
    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }


    /**
     * 结算
     * @param companyid   站点ID
     * @param code        安全校验码
     * @param timestamp   时间戳
     * @param cartid     购物车结算ID，多个用“,”号分割
     * @param from       来源，pc 电脑端 mobile 移动端
     * @return
     */
    public void doCloaseAccount(String companyid, String code, String timestamp, String cartid, String from) {

        mCompositeSubscription.add(manager.doCloaseAccount(companyid,code,timestamp,cartid,from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CloaseAccount>() {
                    @Override
                    public void onCompleted() {
                        if (mAffirmOrderView!=null){
                            mAffirmOrderView.onSuccessDoCloaseAccount(mCloaseAccount);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mAffirmOrderView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CloaseAccount cloaseAccount) {
                        mCloaseAccount=cloaseAccount;
                    }
                }));
    }
    /**
     * 结算
     * @param companyid   站点ID
     * @param code        安全校验码
     * @param timestamp   时间戳
     * @param cartid     购物车结算ID，多个用“,”号分割
     * @param from       来源，pc 电脑端 mobile 移动端
     * @return
     */
    public void doCloaseAccountIntegral(String companyid, String code, String timestamp, String cartid, String from) {

        mCompositeSubscription.add(manager.doCloaseIntegralAccount(companyid,code,timestamp,cartid,from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CloaseAccount>() {
                    @Override
                    public void onCompleted() {
                        if (mAffirmOrderView!=null){
                            mAffirmOrderView.onSuccessDoCloaseAccount(mCloaseAccount);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mAffirmOrderView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CloaseAccount cloaseAccount) {
                        mCloaseAccount=cloaseAccount;
                    }
                }));
    }
    /**
     * 提交商品订单（实物商品）
     * @param companyid    站点ID
     * @param code         安全校验码
     * @param timestamp 时间戳
     * @param cartid      购物车结算ID，多个用“,”号分割
     * @param addressid    收货人地址ID
     * @param paymentname    支付方式
     * @param ordertype    送货方式
     * @param invoicestatus   是否开票发票 1 开发票 2 不开发票
     * @param invoicename     发票抬头，invoicestatus为1时必填
     * @param remark           备注
     * @param from           来源，pc 电脑端 mobile 移动端
     * @return
     */
    public void doCommitOrder(String companyid, String code, String timestamp, String cartid,int addressid,String paymentname,String  ordertype,int invoicestatus,String invoicename,String snph,String remark,String from) {

        mCompositeSubscription.add(manager.doCommitOrder(companyid,code,timestamp,cartid,addressid,paymentname,ordertype,invoicestatus,invoicename,snph,remark,from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mAffirmOrderView!=null){
                            mAffirmOrderView.onSuccessDoCommitOrder(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mAffirmOrderView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
    /**
     * ★ 提交积分兑换订单
     * @param companyid    站点ID
     * @param code         安全校验码
     * @param timestamp 时间戳
     * @param cartid      购物车结算ID，多个用“,”号分割
     * @param addressid    收货人地址ID
     * @param remark           备注
     * @param from           来源，pc 电脑端 mobile 移动端
     * @return
     */

    public void doCommitIntegralOrder(String companyid, String code, String timestamp, String cartid,int addressid,String remark,String from) {

        mCompositeSubscription.add(manager.doCommitIntegralOrder(companyid,code,timestamp,cartid,addressid,remark,from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mAffirmOrderView!=null){
                            mAffirmOrderView.onSuccessDoCommitOrder(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mAffirmOrderView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
    /**
     *  获取会员收货地址
     * @param companyid   站点ID
     * @param code   安全校验码
     * @param timestamp  时间戳
     * @param pagesize   每页显示数量
     * @param pageindex   当前页数
     * @param addressid   收货地址ID
     * @param main   为1时加载默认收货地址，其它调用所有
     * @return
     */
    public void getAdressList(String companyid,String code,String timestamp,int pagesize,int pageindex,int main,String addressid) {

        mCompositeSubscription.add(manager.getAddressList(companyid,code,timestamp,pagesize,pageindex,main,addressid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Address>() {
                    @Override
                    public void onCompleted() {
                        if (mAffirmOrderView!=null){
                            mAffirmOrderView.onSuccessGetAddress(mAddress);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mAffirmOrderView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(Address address) {
                        mAddress=address;
                    }
                }));
    }
    /**
     * ★ 获取会员商城优惠券列表
     *
     * @param companyid
     * @param code
     * @param timestamp
     * @param storeid
     * @param money
     * @param status
     * @param pagesize
     * @param pageindex
     * @return
     */
    public void getMyCouponList(String companyid, String code, String timestamp,
                                String storeid, String money, String status, int pagesize, int pageindex) {

        mCompositeSubscription.add(manager.getMyCouponList(companyid, code, timestamp, storeid, money, status, pagesize, pageindex)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MyCouponList>() {
                    @Override
                    public void onCompleted() {
                        if (mAffirmOrderView!=null){
                            mAffirmOrderView.onSuccessGetMyCouponList(mMyCouponList);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mAffirmOrderView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(MyCouponList myCouponList) {
                        mMyCouponList = myCouponList;
                    }
                }));
    }
    /**
     * ★ 运费刷新（更换收货地址时）
     * @param companyid
     * @param code
     * @param timestamp
     * @param cartid
     * @param addresslid
     * @return
     */
    public void refreshYunFei(String companyid, String code, String timestamp, String cartid, String addresslid) {

        mCompositeSubscription.add(manager.refreshYunFei(companyid,code,timestamp,cartid,addresslid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RefreshYunFei>() {
                    @Override
                    public void onCompleted() {
                        if (mAffirmOrderView!=null){
                            mAffirmOrderView.onSuccessRefreshYunFei(mRefreshYunFei);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mAffirmOrderView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(RefreshYunFei refreshYunFei) {
                        mRefreshYunFei = refreshYunFei;
                    }
                }));
    }
}
