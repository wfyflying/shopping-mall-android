package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.OrderDetail;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderDetailLvAdapter extends BaseAdapter {
    private Context mContext;
    private List<OrderDetail.OrderGoodsListBean> mList;
    private int mOrderStatus;
    private int mOrderId;
    private String mOrderNumber;
    private boolean ifSHouHou;
    private btnClickInterface btnClickInterface;

    public void setBtnClickInterface(OrderDetailLvAdapter.btnClickInterface btnClickInterface) {
        this.btnClickInterface = btnClickInterface;
    }

    public boolean isIfSHouHou() {
        return ifSHouHou;
    }

    public void setIfSHouHou(boolean ifSHouHou) {
        this.ifSHouHou = ifSHouHou;
    }

    public String getmOrderNumber() {
        return mOrderNumber;
    }

    public void setmOrderNumber(String mOrderNumber) {
        this.mOrderNumber = mOrderNumber;
    }

    public int getmOrderId() {
        return mOrderId;
    }

    public void setmOrderId(int mOrderId) {
        this.mOrderId = mOrderId;
    }

    public OrderDetailLvAdapter(Context mContext, List<OrderDetail.OrderGoodsListBean> mList, int orderstatus) {
        this.mContext = mContext;
        this.mList = mList;
        this.mOrderStatus = orderstatus;
    }

    public int getmOrderStatus() {
        return mOrderStatus;
    }

    public void setmOrderStatus(int mOrderStatus) {
        this.mOrderStatus = mOrderStatus;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_lv_oederdetail, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        GlideUtils.setNetImage(mContext, AppConstants.INTERNET_HEAD + mList.get(i).getCartImg(), holder.ivItemorderdetailLogo);
        holder.tvItemorderdetailGoodsname.setText(mList.get(i).getCommodityName() != null ? mList.get(i).getCommodityName() : "");
        holder.tvItemorderdetailGoodsdescribe.setText(mList.get(i).getCommodityProperty() != null ? mList.get(i).getCommodityProperty() : "");

        DecimalFormat df = new DecimalFormat("0.00");
        String ss2 = df.format(mList.get(i).getCommodityXPrice());
        if (".00".equals(ss2)) {
            holder.tvItemorderdetailPrice.setText("¥0.00");
        } else {
            holder.tvItemorderdetailPrice.setText("¥" + ss2);
        }
        holder.tvItemorderdetailCount.setText("数量" + "X" + mList.get(i).getCommodityNumber() + "");
        holder.btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnClickInterface.doClickBtn1(i);
//                Intent intent=new Intent(mContext, GoEvaluateActivity.class);
//                intent.putExtra("cartid",mList.get(i).getCartId());
//                intent.putExtra("commodityId",mList.get(i).getCommodityId());
//                intent.putExtra("cartimg",mList.get(i).getCartImg());
//                intent.putExtra("number",mList.get(i).getCommodityNumber());
//                intent.putExtra("price",mList.get(i).getCommodityXPrice());
//                intent.putExtra("property",mList.get(i).getCommodityProperty());
//                intent.putExtra("name",mList.get(i).getCommodityName());
//                mContext.startActivity(intent);
            }
        });
        holder.btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean ifSHouHou = isIfSHouHou();
                Log.e("xxx", "onClick: "+ifSHouHou);
                btnClickInterface.doClickBtn2(i);
//                if (ifSHouHou){
//                    Intent intent = new Intent(mContext, ShouHouDetailActivity.class);
//                    intent.putExtra("cartid", mList.get(i).getCartId());
//                    intent.putExtra("commodityid", mList.get(i).getCommodityId());
//                    intent.putExtra("orderid", getmOrderId());
//                    mContext.startActivity(intent);
//                }else{
//                    Intent intent=new Intent(mContext, ApplyAfterSaleActivity.class);
//                    intent.putExtra("cartid",mList.get(i).getCartId());
//                    intent.putExtra("commodityId",mList.get(i).getCommodityId());
//                    intent.putExtra("cartimg",mList.get(i).getCartImg());
//                    intent.putExtra("number",mList.get(i).getCommodityNumber());
//                    intent.putExtra("price",mList.get(i).getCommodityXPrice());
//                    intent.putExtra("property",mList.get(i).getCommodityProperty());
//                    intent.putExtra("name",mList.get(i).getCommodityName());
//                    intent.putExtra("orderid",getmOrderId());
//                    intent.putExtra("ordernumber",getmOrderNumber());
//
//                    mContext.startActivity(intent);
//                }

            }
        });
        Log.e("aaa", "getView: "+mOrderStatus);
        mOrderStatus=getmOrderStatus();
        switch (mOrderStatus){
            case 1:
                holder.llButton.setVisibility(View.GONE);
                break;
            case 2:
                holder.llButton.setVisibility(View.GONE);
                break;
            case 3:
                holder.llButton.setVisibility(View.GONE);
                break;
            case 4:
                holder.llButton.setVisibility(View.VISIBLE);
                break;
        }
        return view;
    }
    /**
     * 改变数量的接口
     */
    public interface btnClickInterface {
        void doClickBtn2(int position);
        void doClickBtn1(int position);


    }

    class ViewHolder {
        @BindView(R.id.iv_itemorderdetail_logo)
        ImageView ivItemorderdetailLogo;
        @BindView(R.id.tv_itemorderdetail_goodsname)
        TextView tvItemorderdetailGoodsname;
        @BindView(R.id.tv_itemorderdetail_goodsdescribe)
        TextView tvItemorderdetailGoodsdescribe;
        @BindView(R.id.tv_itemorderdetail_price)
        TextView tvItemorderdetailPrice;
        @BindView(R.id.tv_itemorderdetail_count)
        TextView tvItemorderdetailCount;
        @BindView(R.id.btn1)
        TextView btn1;
        @BindView(R.id.btn2)
        TextView btn2;
        @BindView(R.id.ll_button)
        LinearLayout llButton;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
