package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class News {
    /**
     * status : 1
     * returnMsg : null
     * count : 25
     * pagesize : 1
     * pageindex : 1
     * list : [{"id":302,"title":"【人文】天津人都有的三种特征","Subheading":"","images":"//cdn.qkk.cn/site/215/upload/tour-information/upload/201805/20185181030355641.jpg","images100":"//cdn.qkk.cn/site/215/upload/tour-information/upload/201805/20185181030355641.jpg","images200":"//cdn.qkk.cn/site/215/upload/tour-information/upload/201805/20185181030355641.jpg","images300":"//cdn.qkk.cn/site/215/upload/tour-information/upload/201805/20185181030355641.jpg","imagesAll":"","imagesAll100":"","imagesAll200":"","imagesAll300":"","LinkUrl":"","LinkUrlTo":false,"Intro":"一方水土，养一方人。不同地方的人都有不同的特征，天津本身是个移民城市，从燕王扫北时赐名的那个小土城，发展成目前的直辖市。它的人口构成早就换了又换，很多土生土长的天津人他们的爷爷都来自全国各地。但是如果你细细品...","hits":17,"updatetime":"2018-05-18 09:34","OnTop":true,"Elite":true,"Hot":true,"Xinpin":true,"Cuxiao":false,"classid":10024,"className":"旅游新闻","channelid":106,"channelName":"旅游资讯","text1":"小久九","text2":"天津","text3":"","channelProperty":0,"price":0,"price2":0,"discount":0,"startTime":null,"overTime":null,"integral":0,"fields":[]}]
     */

    private int status;
    private Object returnMsg;
    private int count;
    private int pagesize;
    private int pageindex;
    private List<ListBean> list;

    @Override
    public String toString() {
        return "News{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", count=" + count +
                ", pagesize=" + pagesize +
                ", pageindex=" + pageindex +
                ", list=" + list +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public int getPageindex() {
        return pageindex;
    }

    public void setPageindex(int pageindex) {
        this.pageindex = pageindex;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 302
         * title : 【人文】天津人都有的三种特征
         * Subheading :
         * images : //cdn.qkk.cn/site/215/upload/tour-information/upload/201805/20185181030355641.jpg
         * images100 : //cdn.qkk.cn/site/215/upload/tour-information/upload/201805/20185181030355641.jpg
         * images200 : //cdn.qkk.cn/site/215/upload/tour-information/upload/201805/20185181030355641.jpg
         * images300 : //cdn.qkk.cn/site/215/upload/tour-information/upload/201805/20185181030355641.jpg
         * imagesAll :
         * imagesAll100 :
         * imagesAll200 :
         * imagesAll300 :
         * LinkUrl :
         * LinkUrlTo : false
         * Intro : 一方水土，养一方人。不同地方的人都有不同的特征，天津本身是个移民城市，从燕王扫北时赐名的那个小土城，发展成目前的直辖市。它的人口构成早就换了又换，很多土生土长的天津人他们的爷爷都来自全国各地。但是如果你细细品...
         * hits : 17
         * updatetime : 2018-05-18 09:34
         * OnTop : true
         * Elite : true
         * Hot : true
         * Xinpin : true
         * Cuxiao : false
         * classid : 10024
         * className : 旅游新闻
         * channelid : 106
         * channelName : 旅游资讯
         * text1 : 小久九
         * text2 : 天津
         * text3 :
         * channelProperty : 0
         * price : 0
         * price2 : 0
         * discount : 0
         * startTime : null
         * overTime : null
         * integral : 0
         * fields : []
         */

        private int id;
        private String title;
        private String Subheading;
        private String images;
        private String images100;
        private String images200;
        private String images300;
        private String imagesAll;
        private String imagesAll100;
        private String imagesAll200;
        private String imagesAll300;
        private String LinkUrl;
        private boolean LinkUrlTo;
        private String Intro;
        private int hits;
        private String updatetime;
        private boolean OnTop;
        private boolean Elite;
        private boolean Hot;
        private boolean Xinpin;
        private boolean Cuxiao;
        private int classid;
        private String className;
        private int channelid;
        private String channelName;
        private String text1;
        private String text2;
        private String text3;
        private int channelProperty;
        private int price;
        private int price2;
        private int discount;
        private Object startTime;
        private Object overTime;
        private int integral;
        private List<?> fields;

        @Override
        public String toString() {
            return "ListBean{" +
                    "id=" + id +
                    ", title='" + title + '\'' +
                    ", Subheading='" + Subheading + '\'' +
                    ", images='" + images + '\'' +
                    ", images100='" + images100 + '\'' +
                    ", images200='" + images200 + '\'' +
                    ", images300='" + images300 + '\'' +
                    ", imagesAll='" + imagesAll + '\'' +
                    ", imagesAll100='" + imagesAll100 + '\'' +
                    ", imagesAll200='" + imagesAll200 + '\'' +
                    ", imagesAll300='" + imagesAll300 + '\'' +
                    ", LinkUrl='" + LinkUrl + '\'' +
                    ", LinkUrlTo=" + LinkUrlTo +
                    ", Intro='" + Intro + '\'' +
                    ", hits=" + hits +
                    ", updatetime='" + updatetime + '\'' +
                    ", OnTop=" + OnTop +
                    ", Elite=" + Elite +
                    ", Hot=" + Hot +
                    ", Xinpin=" + Xinpin +
                    ", Cuxiao=" + Cuxiao +
                    ", classid=" + classid +
                    ", className='" + className + '\'' +
                    ", channelid=" + channelid +
                    ", channelName='" + channelName + '\'' +
                    ", text1='" + text1 + '\'' +
                    ", text2='" + text2 + '\'' +
                    ", text3='" + text3 + '\'' +
                    ", channelProperty=" + channelProperty +
                    ", price=" + price +
                    ", price2=" + price2 +
                    ", discount=" + discount +
                    ", startTime=" + startTime +
                    ", overTime=" + overTime +
                    ", integral=" + integral +
                    ", fields=" + fields +
                    '}';
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubheading() {
            return Subheading;
        }

        public void setSubheading(String Subheading) {
            this.Subheading = Subheading;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public String getImages100() {
            return images100;
        }

        public void setImages100(String images100) {
            this.images100 = images100;
        }

        public String getImages200() {
            return images200;
        }

        public void setImages200(String images200) {
            this.images200 = images200;
        }

        public String getImages300() {
            return images300;
        }

        public void setImages300(String images300) {
            this.images300 = images300;
        }

        public String getImagesAll() {
            return imagesAll;
        }

        public void setImagesAll(String imagesAll) {
            this.imagesAll = imagesAll;
        }

        public String getImagesAll100() {
            return imagesAll100;
        }

        public void setImagesAll100(String imagesAll100) {
            this.imagesAll100 = imagesAll100;
        }

        public String getImagesAll200() {
            return imagesAll200;
        }

        public void setImagesAll200(String imagesAll200) {
            this.imagesAll200 = imagesAll200;
        }

        public String getImagesAll300() {
            return imagesAll300;
        }

        public void setImagesAll300(String imagesAll300) {
            this.imagesAll300 = imagesAll300;
        }

        public String getLinkUrl() {
            return LinkUrl;
        }

        public void setLinkUrl(String LinkUrl) {
            this.LinkUrl = LinkUrl;
        }

        public boolean isLinkUrlTo() {
            return LinkUrlTo;
        }

        public void setLinkUrlTo(boolean LinkUrlTo) {
            this.LinkUrlTo = LinkUrlTo;
        }

        public String getIntro() {
            return Intro;
        }

        public void setIntro(String Intro) {
            this.Intro = Intro;
        }

        public int getHits() {
            return hits;
        }

        public void setHits(int hits) {
            this.hits = hits;
        }

        public String getUpdatetime() {
            return updatetime;
        }

        public void setUpdatetime(String updatetime) {
            this.updatetime = updatetime;
        }

        public boolean isOnTop() {
            return OnTop;
        }

        public void setOnTop(boolean OnTop) {
            this.OnTop = OnTop;
        }

        public boolean isElite() {
            return Elite;
        }

        public void setElite(boolean Elite) {
            this.Elite = Elite;
        }

        public boolean isHot() {
            return Hot;
        }

        public void setHot(boolean Hot) {
            this.Hot = Hot;
        }

        public boolean isXinpin() {
            return Xinpin;
        }

        public void setXinpin(boolean Xinpin) {
            this.Xinpin = Xinpin;
        }

        public boolean isCuxiao() {
            return Cuxiao;
        }

        public void setCuxiao(boolean Cuxiao) {
            this.Cuxiao = Cuxiao;
        }

        public int getClassid() {
            return classid;
        }

        public void setClassid(int classid) {
            this.classid = classid;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public int getChannelid() {
            return channelid;
        }

        public void setChannelid(int channelid) {
            this.channelid = channelid;
        }

        public String getChannelName() {
            return channelName;
        }

        public void setChannelName(String channelName) {
            this.channelName = channelName;
        }

        public String getText1() {
            return text1;
        }

        public void setText1(String text1) {
            this.text1 = text1;
        }

        public String getText2() {
            return text2;
        }

        public void setText2(String text2) {
            this.text2 = text2;
        }

        public String getText3() {
            return text3;
        }

        public void setText3(String text3) {
            this.text3 = text3;
        }

        public int getChannelProperty() {
            return channelProperty;
        }

        public void setChannelProperty(int channelProperty) {
            this.channelProperty = channelProperty;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public int getPrice2() {
            return price2;
        }

        public void setPrice2(int price2) {
            this.price2 = price2;
        }

        public int getDiscount() {
            return discount;
        }

        public void setDiscount(int discount) {
            this.discount = discount;
        }

        public Object getStartTime() {
            return startTime;
        }

        public void setStartTime(Object startTime) {
            this.startTime = startTime;
        }

        public Object getOverTime() {
            return overTime;
        }

        public void setOverTime(Object overTime) {
            this.overTime = overTime;
        }

        public int getIntegral() {
            return integral;
        }

        public void setIntegral(int integral) {
            this.integral = integral;
        }

        public List<?> getFields() {
            return fields;
        }

        public void setFields(List<?> fields) {
            this.fields = fields;
        }
    }
}
