package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.modle.Goods;
import com.example.a13001.shoppingmalltemplate.modle.Specification;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class PopLvAdapter extends BaseAdapter{
    private Context mContext;
    private List<Specification> mList;
    private List<String> mListTitle=new ArrayList<>();
    setTagclick setTagclick;

    public PopLvAdapter(Context mContext, List<Specification> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder=null;
        if (view == null) {
            view= LayoutInflater.from(mContext).inflate(R.layout.item_lv_pop,viewGroup,false);
            holder=new ViewHolder(view);
            view.setTag(holder);
        }else{
            holder= (ViewHolder) view.getTag();
        }
       holder.tvTitle.setText(mList.get(i).getTitle());
        for (int j = 0; j < mList.get(i).getmList().size(); j++) {
            mListTitle.add(mList.get(i).getmList().get(j).getShop_attribute_xh());
        }

        final ViewHolder finalHolder = holder;
        holder.mTfTag.setAdapter(new TagAdapter<String>(mListTitle) {
            @Override
            public View getView(FlowLayout parent, int position, String s) {
                TextView tv = (TextView) LayoutInflater.from(mContext).inflate(R.layout.tv,
                        finalHolder.mTfTag, false);
                tv.setText(s);
                return tv;
            }
        });
        holder.mTfTag.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {
//                Toast.makeText(mContext, ""+mList.get(i).getmList().get(position), Toast.LENGTH_SHORT).show();
//               setTagclick.getSelecred(mList.get(i).getmList().get(position));
                EventBus.getDefault().post(mList.get(i).getmList().get(position));
                return false;
            }
        });


        return view;
    }

    public interface setTagclick{
        void getSelecred(String  str);
    }
    public void getSelectde(setTagclick setTagclick){
        this.setTagclick=setTagclick;
    }

    class ViewHolder{

        TextView tvTitle;
        TagFlowLayout mTfTag;

        public ViewHolder(View itemView){
            tvTitle=itemView.findViewById(R.id.tv_lvpop_title);//标题
            mTfTag=itemView.findViewById(R.id.tf_lvpop);//标题
        }
    }
}
