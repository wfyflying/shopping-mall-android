package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.Banner;
import com.example.a13001.shoppingmalltemplate.modle.Book;
import com.example.a13001.shoppingmalltemplate.modle.Classify;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.GoodsDetail;
import com.example.a13001.shoppingmalltemplate.modle.NewBanner;
import com.example.a13001.shoppingmalltemplate.modle.News;
import com.example.a13001.shoppingmalltemplate.modle.ShopCouponList;

public interface NewsView extends View {
    void onSuccess(News mNews);
    void onSuccessBanner(Banner banner);
    void onSuccessNewBanner(NewBanner newBanner);
    void onSuccessClassify(GoodsDetail goodsDetail);
    void onSuccessGetCouponList(ShopCouponList shopCouponList);
    void onSuccessObtainCoupon(CommonResult commonResult);
    void onError(String result);

}
