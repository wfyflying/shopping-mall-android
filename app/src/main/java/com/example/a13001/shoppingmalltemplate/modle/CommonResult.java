package com.example.a13001.shoppingmalltemplate.modle;

import android.os.Parcel;
import android.os.Parcelable;

public class CommonResult implements Parcelable{




    /**
     * status : 1
     * returnMsg : null
     */

    private int status;
    private int chkType;
    private String returnMsg;
    private String code;
    private int sum;
    private int ordersType;//返回订单类型，1 普通商品（含实物商品和虚拟商品） 2 促销秒杀商品 9 积分兑换商品
    private int paymentId;//返回支付模式，0 余额支付 1 货到付款 2 银行转账 3 邮局汇款 4 支付宝 5 微信支付 6 财付通 8 中国银联 9 银联电子支付
    private String  orderNumber;//返回订单号
    private String  paymentUrl;//跳转支付链接，仅用于微信公众号及网页版
    private String  returnHTML;//跳转支付链接，仅用于微信公众号及网页版
    private String  reUrl;//跳转订单详情页，，仅用于微信公众号及网页版
    private int  collectlId;
    private int  commodityId;
    private int  commentstatus;//评论状态：0 评论成功并通过审核 1 评论成功等待管理员审核

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "CommonResult{" +
                "status=" + status +
                ", chkType=" + chkType +
                ", returnMsg='" + returnMsg + '\'' +
                ", code='" + code + '\'' +
                ", sum=" + sum +
                ", ordersType=" + ordersType +
                ", paymentId=" + paymentId +
                ", orderNumber='" + orderNumber + '\'' +
                ", paymentUrl='" + paymentUrl + '\'' +
                ", returnHTML='" + returnHTML + '\'' +
                ", reUrl='" + reUrl + '\'' +
                ", collectlId=" + collectlId +
                ", commodityId=" + commodityId +
                ", commentstatus=" + commentstatus +
                '}';
    }

    public int getCommentstatus() {
        return commentstatus;
    }

    public void setCommentstatus(int commentstatus) {
        this.commentstatus = commentstatus;
    }

    protected CommonResult(Parcel in) {
        status = in.readInt();
        returnMsg = in.readString();
        code = in.readString();
        sum = in.readInt();
        ordersType = in.readInt();
        paymentId = in.readInt();
        orderNumber = in.readString();
        paymentUrl = in.readString();
        collectlId = in.readInt();
        commodityId = in.readInt();
        chkType = in.readInt();
    }

    public static final Creator<CommonResult> CREATOR = new Creator<CommonResult>() {
        @Override
        public CommonResult createFromParcel(Parcel in) {
            return new CommonResult(in);
        }

        @Override
        public CommonResult[] newArray(int size) {
            return new CommonResult[size];
        }
    };


    public String getReUrl() {
        return reUrl;
    }

    public void setReUrl(String reUrl) {
        this.reUrl = reUrl;
    }

    public String getReturnHTML() {
        return returnHTML;
    }

    public void setReturnHTML(String returnHTML) {
        this.returnHTML = returnHTML;
    }

    public int getOrdersType() {
        return ordersType;
    }

    public void setOrdersType(int ordersType) {
        this.ordersType = ordersType;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getPaymentUrl() {
        return paymentUrl;
    }

    public void setPaymentUrl(String paymentUrl) {
        this.paymentUrl = paymentUrl;
    }

    public int getCollectlId() {
        return collectlId;
    }

    public void setCollectlId(int collectlId) {
        this.collectlId = collectlId;
    }

    public int getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(int commodityId) {
        this.commodityId = commodityId;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getChkType() {
        return chkType;
    }

    public void setChkType(int chkType) {
        this.chkType = chkType;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(status);
        parcel.writeInt(chkType);
        parcel.writeString(returnMsg);
        parcel.writeString(code);
        parcel.writeInt(sum);
        parcel.writeInt(ordersType);
        parcel.writeInt(paymentId);
        parcel.writeString(orderNumber);
        parcel.writeString(paymentUrl);
        parcel.writeInt(collectlId);
        parcel.writeInt(commodityId);
    }
}
