package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.Message;
import com.example.a13001.shoppingmalltemplate.modle.Notice;

public interface MessageView extends View{
    void onSuccess(Message message);
    void onSuccessNotice(Notice notice);
    void onSuccessDoSmsDelete(CommonResult commonResult);
    void onError(String result);
}
