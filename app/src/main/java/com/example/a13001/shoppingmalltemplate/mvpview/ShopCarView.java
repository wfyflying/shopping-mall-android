package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.AppConfig;
import com.example.a13001.shoppingmalltemplate.modle.CloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.DetailGoods;
import com.example.a13001.shoppingmalltemplate.modle.GoodsParameters;
import com.example.a13001.shoppingmalltemplate.modle.LoginStatus;
import com.example.a13001.shoppingmalltemplate.modle.ShopCarGoods;

public interface ShopCarView extends View{
    void onSuccess(ShopCarGoods shopCarGoods);
    void onSuccessGetAppConfig(AppConfig appConfig);
    void onSuccessLoginStatus(LoginStatus loginStatus);
    void onSuccessDeleteShopCar(CommonResult commonResult);
    void onSuccessDoCloaseAccount(CloaseAccount cloaseAccount);
    void onSuccessUpdateShopCarNum(CommonResult commonResult);
    void onError(String result);
}
