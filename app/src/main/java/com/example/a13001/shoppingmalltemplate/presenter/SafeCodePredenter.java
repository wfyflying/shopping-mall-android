package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;

import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.LoginStatus;
import com.example.a13001.shoppingmalltemplate.modle.SignIn;
import com.example.a13001.shoppingmalltemplate.modle.UserInfo;
import com.example.a13001.shoppingmalltemplate.mvpview.MyView;
import com.example.a13001.shoppingmalltemplate.mvpview.SafeCodeView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class SafeCodePredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private SafeCodeView mSafeCodeView;
    private UserInfo mUser;
    private CommonResult mCommonResult;
  
    public SafeCodePredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        manager=new DataManager(mContext);
        mCompositeSubscription=new CompositeSubscription();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mSafeCodeView=(SafeCodeView) view;

    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }

  
    /**
     * 获取会员详细信息
     * @param companyid    站点ID
     * @param code          安全校验码
     * @param timestamp     时间戳
     * @return
     */
    public void getUserInfo(String companyid,String code,String timestamp) {

        mCompositeSubscription.add(manager.getUserInfo(companyid, code,timestamp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserInfo>() {
                    @Override
                    public void onCompleted() {
                        if (mSafeCodeView!=null){
                            mSafeCodeView.onSuccessGetUserInfo(mUser);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mSafeCodeView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(UserInfo userInfo) {
                        mUser=userInfo;
                    }
                }));
    }

    /**
     * ★ 修改会员安全密码
     * @param companyid
     * @param code
     * @param timestamp
     * @param oldpass      旧密码，未设置交易密码时允许为空
     * @param newpass     新密码	
     * @param confirmpass   确认新密码	
     * @return
     */
    public void doResetSafePwd(String companyid,String code,String timestamp,String oldpass,String newpass,String confirmpass) {

        mCompositeSubscription.add(manager.doResetSafePwd(companyid,code,timestamp,oldpass,newpass,confirmpass)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mSafeCodeView!=null){
                            mSafeCodeView.onSuccessDoResetSafepwd(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mSafeCodeView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
}
