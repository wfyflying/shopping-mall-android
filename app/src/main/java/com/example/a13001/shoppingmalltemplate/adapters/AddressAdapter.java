package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.activitys.NewAddressActivity;
import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.presenter.AdressManagerPredenter;
import com.zhy.autolayout.utils.AutoUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;


import okhttp3.Call;

/**
 * Created by Administrator on 2018/5/2.
 */

public class AddressAdapter extends BaseAdapter {
    private Context context;
    private AdressManagerPredenter mAdressManagerPredenter;
    // 用于记录每个RadioButton的状态，并保证只可选一个
    HashMap<String, Boolean> states = new HashMap<String, Boolean>();
    private List<Address.DeliveryAddressListBean> mList;
    private deleteInterface deleteInterface;
    private defaultAddressInterface defaultAddressInterface;

    class ViewHolder {
        TextView tv_address_edit, tvName, tvAddress, tvDelete;
        CheckBox rb_state;
    }

    public AddressAdapter(Context context, List<Address.DeliveryAddressListBean> mList) {
        // TODO Auto-generated constructor stub

        this.context = context;
        this.mList = mList;

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        // 页面
        ViewHolder holder;

        LayoutInflater inflater = LayoutInflater.from(context);
        if (convertView == null) {
            convertView = inflater.inflate(
                    R.layout.address_item, parent, false);
            holder = new ViewHolder();
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_addressname);
            holder.tvAddress = (TextView) convertView.findViewById(R.id.tv_address);
            holder.tv_address_edit = (TextView) convertView.findViewById(R.id.tv_address_edit);
            holder.tvDelete = (TextView) convertView.findViewById(R.id.tv_address_delete);
            convertView.setTag(holder);
            AutoUtils.autoSize(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvName.setText(mList.get(position).getAddressName() + "    " + mList.get(position).getAddressPhone());

        holder.tvAddress.setText(mList.get(position).getAddressProvince() + mList.get(position).getAddressCity() + mList.get(position).getAddressArea() + mList.get(position).getAddressXX());
        holder.tv_address_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //地址编辑提交
                context.startActivity(new Intent(context, NewAddressActivity.class).putExtra("addressid", mList.get(position).getAddresslId()));
            }
        });
        /**
         * 删除
         */
        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteInterface.doDelete(position);
            }
        });
        holder.rb_state= (CheckBox) convertView.findViewById(R.id.rb_address);
        final boolean isDefault = mList.get(position).isAddressDefault();
        Log.e("aaa", "---isDefault---->" + isDefault);
        holder.rb_state.setChecked(isDefault);
        holder.rb_state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                defaultAddressInterface.setDefault(position);
            }
        });
//        if (isDefault)
//            states.put(String.valueOf(position), isDefault);
//
//        holder.rb_state.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//
//                // 重置，确保最多只有一项被选中
//                for (String key : states.keySet()) {
//                    states.put(key, false);
//
//                }
//                states.put(String.valueOf(position), isDefault);
//                AddressAdapter.this.notifyDataSetChanged();
//
//            }
//        });
//
//        boolean res = false;
//        if (states.get(String.valueOf(position)) == null
//                || states.get(String.valueOf(position)) == false) {
//            res = false;
//            states.put(String.valueOf(position), false);
//        } else {
//            res = true;
//        }
//        holder.rb_state.setChecked(res);
        return convertView;
    }

    public interface deleteInterface {
        void doDelete(int position);
    }
    public interface defaultAddressInterface {
        void setDefault(int position);
    }
    public void setDeleteInterface(deleteInterface deleteInterface) {
        this.deleteInterface = deleteInterface;
    }
    public void setDefaultInterface(defaultAddressInterface defaultAddressInterface) {
        this.defaultAddressInterface = defaultAddressInterface;
    }
}
