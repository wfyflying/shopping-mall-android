package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.Information;
import com.example.a13001.shoppingmalltemplate.modle.News;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

public class ImfromationLvAdapter extends BaseAdapter {
    private Context mContext;
    private List<News.ListBean> mList;

    public ImfromationLvAdapter(Context mContext, List<News.ListBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        if (mList != null) {
            return mList.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        if (mList != null) {
            return i;
        }
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder=null;
        if (view == null) {
            view= LayoutInflater.from(mContext).inflate(R.layout.item_lv_information,viewGroup,false);
            holder=new ViewHolder();
            holder.ivLogo=view.findViewById(R.id.iv_iteminfor_logo);
            holder.tvTitle=view.findViewById(R.id.tv_iteminfor_title);
            holder.tvContent=view.findViewById(R.id.tv_iteminfor_content);
            holder.tvInfoNum=view.findViewById(R.id.tv_iteminfor_infonum);
            holder.tvLikeNum=view.findViewById(R.id.tv_iteminfor_likenum);
            view.setTag(holder);
            AutoUtils.autoSize(view);
        }else{
            holder= (ViewHolder) view.getTag();
        }
        String images=mList.get(i).getImages();
        if (TextUtils.isEmpty(images)){
            holder.ivLogo.setVisibility(View.GONE);
        }else{
            holder.ivLogo.setVisibility(View.VISIBLE);
            GlideUtils.setNetImage(mContext, AppConstants.INTERNET_HEAD+mList.get(i).getImages(),holder.ivLogo);
        }
        holder.tvTitle.setText(mList.get(i).getTitle());
        holder.tvContent.setText(mList.get(i).getIntro());
        holder.tvInfoNum.setText(mList.get(i).getHits()+"");
//        holder.tvLikeNum.setText(mList.get(i).getLikeNum());
        return view;
    }
    class ViewHolder{
        ImageView ivLogo;
        TextView tvTitle,tvContent,tvInfoNum,tvLikeNum;
    }
}
