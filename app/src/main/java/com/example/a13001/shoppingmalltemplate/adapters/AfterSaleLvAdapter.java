package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.modle.AfterSale;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Administrator on 2017/8/9.
 */

public class AfterSaleLvAdapter extends BaseAdapter {
    private Context mContext;
    private List<AfterSale.RepairsListBean> mList;
    private int mOrderStatus;

    public AfterSaleLvAdapter(Context context, List<AfterSale.RepairsListBean> mList) {
        this.mContext = context;
        this.mList = mList;

    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolder vh = null;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_lv_aftersale, parent, false);
            vh = new ViewHolder(view);
            view.setTag(vh);
            AutoUtils.autoSize(view);
        } else {
            vh = (ViewHolder) view.getTag();
        }
        vh.tvOrderitemOrderid.setText("售后服务单号："+mList.get(position).getRepairsId());
        GlideUtils.setNetImage(mContext, mList.get(position).getCartImg(), vh.ivOrderitemGoodimage);
        vh.tvOrderitemGoodsname.setText(mList.get(position).getCommodityName() != null ? mList.get(position).getCommodityName() : "");
        vh.tvOrderitemDescribe.setText(mList.get(position).getCommodityProperty() != null ? mList.get(position).getCommodityProperty() : "");
        vh.tvTime.setText(mList.get(position).getRepairsCreatTime() != null ? mList.get(position).getRepairsCreatTime() : "");
//        vh.tvOrderitemPrice.setText(mList.get(position).getCommodityProperty()!=null?mList.get(position).getCommodityProperty():"");
//        vh.tvOrderitemCount.setText("X" + mList.get(position).getCommodityNumber());
//        vh.btn1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(mContext, GoEvaluateActivity.class);
//                intent.putExtra("cartid", mList.get(position).getCartId());
//                intent.putExtra("commodityId", mList.get(position).getCommodityId());
//                mContext.startActivity(intent);
//            }
//        });
//        vh.btn2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mContext.startActivity(new Intent(mContext, ApplyAfterSaleActivity.class));
//            }
//        });
        //售后服务单状态，0 待审核 1 已驳回 2 等待商品寄回 3 已收货 4 售后处理中 5 已完成
       int repairsId= mList.get(position).getRepairsStatus();
        switch (repairsId) {
            case 0:
                vh.tvOrderitemState.setText("待审核");
                break;
            case 1:
                vh.tvOrderitemState.setText("已驳回");
                break;
            case 2:
                vh.tvOrderitemState.setText("等待商品寄回");
                break;
            case 3:
                vh.tvOrderitemState.setText("已收货");
                break;
            case 4:
                vh.tvOrderitemState.setText("售后处理中");
                break;
            case 5:
                vh.tvOrderitemState.setText("已完成");
                break;
        }
        return view;
    }



    class ViewHolder {
        @BindView(R.id.tv_orderitem_orderid)
        TextView tvOrderitemOrderid;
        @BindView(R.id.tv_orderitem_state)
        TextView tvOrderitemState;
        @BindView(R.id.iv_orderitem_goodimage)
        ImageView ivOrderitemGoodimage;
        @BindView(R.id.tv_orderitem_goodsname)
        TextView tvOrderitemGoodsname;
        @BindView(R.id.tv_orderitem_price)
        TextView tvOrderitemPrice;
        @BindView(R.id.tv_orderitem_describe)
        TextView tvOrderitemDescribe;
        @BindView(R.id.tv_orderitem_count)
        TextView tvOrderitemCount;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.tv_count2)
        TextView tvCount2;
        @BindView(R.id.tv_totalprice)
        TextView tvTotalprice;
        @BindView(R.id.btn1)
        Button btn1;
        @BindView(R.id.btn2)
        Button btn2;
        @BindView(R.id.ll_button)
        LinearLayout llButton;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
