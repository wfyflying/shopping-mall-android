package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.MyListView;
import com.example.a13001.shoppingmalltemplate.activitys.ApplyAfterSaleActivity;
import com.example.a13001.shoppingmalltemplate.activitys.GoEvaluateActivity;
import com.example.a13001.shoppingmalltemplate.activitys.LogisticsInfoActivity;
import com.example.a13001.shoppingmalltemplate.activitys.OnLinePayActivity;
import com.example.a13001.shoppingmalltemplate.activitys.OrderDetailActivity;
import com.example.a13001.shoppingmalltemplate.activitys.OrderListActivity;
import com.example.a13001.shoppingmalltemplate.modle.Order;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Administrator on 2017/8/9.
 */

public class OrderListLvAdapter extends BaseAdapter {
    private Context context;
    private List<Order.OrderListBean> mList;
    private changeOrderStateInterface changeOrderStateInterface;
    private int payid;

    public OrderListLvAdapter(Context context, List<Order.OrderListBean> mList) {
        this.context = context;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolder vh = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.order_item, parent, false);
            vh = new ViewHolder(view);
            view.setTag(vh);
            AutoUtils.autoSize(view);
        } else {
            vh = (ViewHolder) view.getTag();
        }
        vh.tvOrderitemOrderid.setText(mList.get(position).getOrdersNumber()!=null?"订单号："+mList.get(position).getOrdersNumber():"");
        vh.tvOrderitemState.setText(mList.get(position).getOrderStatusName()!=null?mList.get(position).getOrderStatusName():"");
        vh.tvTotalprice.setText("¥"+mList.get(position).getOrderTotalprice()+"");
        vh.mTvTime.setText(mList.get(position).getOrderDate()!=null?mList.get(position).getOrderDate():"");
        OrderListLvLvAdapter mAdapter=new OrderListLvLvAdapter(context,mList.get(position).getOrderGoodsList(),mList.get(position).getOrderStatus());
        vh.mlvOrderitem.setAdapter(mAdapter);
        vh.mlvOrderitem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent=new Intent(context,OrderDetailActivity.class);
                intent.putExtra("orderNumber",mList.get(position).getOrdersNumber());
                intent.putExtra("orderStatus",mList.get(position).getOrderStatus());
                context.startActivity(intent);
            }
        });
        //订单状态，1 待支付 2 未发货 3 已发货 4 已签收
        int state=mList.get(position).getOrderStatus();
        switch (state){
            case 1:
                vh.btn1.setText("取消订单");
                vh.btn2.setText("支付");
                vh.btn1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        changeOrderStateInterface.doCancelOrder(position,mList.get(position).getOrdersNumber());
                    }
                });
                vh.btn2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent3 = new Intent(context, OnLinePayActivity.class);
                        intent3.putExtra("orderNumber", mList.get(position).getOrdersNumber());
                        intent3.putExtra("totalprice", String.valueOf(mList.get(position).getOrderTotalprice()));
                        String paytype=mList.get(position).getOrdersZffs();
                        switch (paytype){
                            case "微信支付":
                                payid = 5;
                                break;
                            case "余额支付":
                                payid = 0;
                                break;
                            case "银行转账":
                                payid = 2;
                                break;
                        }
                        intent3.putExtra("payid", payid);
                        context.startActivity(intent3);
                    }
                });
                break;
            case 2:
                vh.btn1.setVisibility(View.GONE);
                vh.btn2.setVisibility(View.GONE);
                vh.mLlButton.setVisibility(View.GONE);
                break;
            case 3:
                vh.btn1.setText("查看物流");
                vh.btn2.setText("确认收货");
                vh.btn1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        context.startActivity(new Intent(context,LogisticsInfoActivity.class));
                    }
                });
                vh.btn2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        changeOrderStateInterface.doAffirmOrder(position,mList.get(position).getOrdersNumber());
                    }
                });
                break;
            case 4:
                    vh.btn1.setVisibility(View.GONE);
                    vh.btn2.setVisibility(View.GONE);
                    vh.mLlButton.setVisibility(View.GONE);
//                vh.btn1.setText("评价");
//                vh.btn2.setText("申请售后");
//                vh.btn1.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent=new Intent(context, GoEvaluateActivity.class);
//                        context.startActivity(intent);
//                    }
//                });
//                vh.btn2.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        context.startActivity(new Intent(context, ApplyAfterSaleActivity.class));
//                    }
//                });

                break;
        }

        return view;
    }

    /**
     * 改变订单状态接口
     */
    public interface changeOrderStateInterface{
      void  doAffirmOrder(int position,String orderNumber);
      void  doCancelOrder(int position,String orderNumber);
    }

    public void setChangeOrderStateInterface(changeOrderStateInterface changeOrderStateInterface) {
        this.changeOrderStateInterface = changeOrderStateInterface;
    }

    static class ViewHolder {
        @BindView(R.id.tv_orderitem_orderid)
        TextView tvOrderitemOrderid;
        @BindView(R.id.tv_orderitem_state)
        TextView tvOrderitemState;
        @BindView(R.id.mlv_orderitem)
        MyListView mlvOrderitem;
        @BindView(R.id.tv_count2)
        TextView tvCount2;
        @BindView(R.id.tv_totalprice)
        TextView tvTotalprice;
        @BindView(R.id.btn1)
        Button btn1;
        @BindView(R.id.btn2)
        Button btn2;
        @BindView(R.id.ll_button)
        LinearLayout mLlButton;
        @BindView(R.id.tv_time)
        TextView mTvTime;
        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
