package com.example.a13001.shoppingmalltemplate.modle;

public class User {

    /**
     * status : 100014
     * GoUrl : //www.tjsrsh.site.ify2.cn/member/center/
     * returnMsg : 恭喜您，您已成功注册为本站会员
     * memberId : 100014
     * memberFace :
     * memberUser : flying00221
     * memberMobile :
     * memberEmail :
     * memberZsname : 子非鱼
     */

    private int status;
    private String GoUrl;
    private String returnMsg;
    private int memberId;
    private String memberFace;
    private String memberUser;
    private String memberMobile;
    private String memberEmail;
    private String memberZsname;

    @Override
    public String toString() {
        return "User{" +
                "status=" + status +
                ", GoUrl='" + GoUrl + '\'' +
                ", returnMsg='" + returnMsg + '\'' +
                ", memberId=" + memberId +
                ", memberFace='" + memberFace + '\'' +
                ", memberUser='" + memberUser + '\'' +
                ", memberMobile='" + memberMobile + '\'' +
                ", memberEmail='" + memberEmail + '\'' +
                ", memberZsname='" + memberZsname + '\'' +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getGoUrl() {
        return GoUrl;
    }

    public void setGoUrl(String GoUrl) {
        this.GoUrl = GoUrl;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public String getMemberFace() {
        return memberFace;
    }

    public void setMemberFace(String memberFace) {
        this.memberFace = memberFace;
    }

    public String getMemberUser() {
        return memberUser;
    }

    public void setMemberUser(String memberUser) {
        this.memberUser = memberUser;
    }

    public String getMemberMobile() {
        return memberMobile;
    }

    public void setMemberMobile(String memberMobile) {
        this.memberMobile = memberMobile;
    }

    public String getMemberEmail() {
        return memberEmail;
    }

    public void setMemberEmail(String memberEmail) {
        this.memberEmail = memberEmail;
    }

    public String getMemberZsname() {
        return memberZsname;
    }

    public void setMemberZsname(String memberZsname) {
        this.memberZsname = memberZsname;
    }
}
