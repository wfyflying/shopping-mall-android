package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class NewBanner {

    /**
     * status : 1
     * returnMsg : null
     * count : 2
     * list : [{"status":0,"returnMsg":null,"photoId":100020,"photoName":"我是轮播图1","photoPath":"//file.site.ify2.cn/site/153/upload/store/201811/2018115175657141.jpg","photoLowPath":"//file.site.ify2.cn/site/153/upload/store/201811/small-2018115175657141.jpg","photoPreviewPath":"//file.site.ify2.cn/site/153/upload/store/201811/preview-2018115175657141.jpg","photoDescript":"","photoSuffix":"jpg","photoDate":"2018-11-05 17:56","photoConType":0,"photoLink":""},{"status":0,"returnMsg":null,"photoId":100021,"photoName":"轮播图2","photoPath":"//file.site.ify2.cn/site/153/upload/app/201811/20181151758245871.jpg","photoLowPath":"//file.site.ify2.cn/site/153/upload/app/201811/small-20181151758245871.jpg","photoPreviewPath":"//file.site.ify2.cn/site/153/upload/app/201811/preview-20181151758245871.jpg","photoDescript":"","photoSuffix":"jpg","photoDate":"2018-11-05 17:58","photoConType":1,"photoLink":"http://tjsrsh.site.ify2.cn/"}]
     */

    private int status;
    private Object returnMsg;
    private int count;
    private List<ListBean> list;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * status : 0
         * returnMsg : null
         * photoId : 100020
         * photoName : 我是轮播图1
         * photoPath : //file.site.ify2.cn/site/153/upload/store/201811/2018115175657141.jpg
         * photoLowPath : //file.site.ify2.cn/site/153/upload/store/201811/small-2018115175657141.jpg
         * photoPreviewPath : //file.site.ify2.cn/site/153/upload/store/201811/preview-2018115175657141.jpg
         * photoDescript :
         * photoSuffix : jpg
         * photoDate : 2018-11-05 17:56
         * photoConType : 0
         * photoLink :
         */

        private int status;
        private Object returnMsg;
        private int photoId;
        private String photoName;
        private String photoPath;
        private String photoLowPath;
        private String photoPreviewPath;
        private String photoDescript;
        private String photoSuffix;
        private String photoDate;
        private int photoConType;
        private String photoLink;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public Object getReturnMsg() {
            return returnMsg;
        }

        public void setReturnMsg(Object returnMsg) {
            this.returnMsg = returnMsg;
        }

        public int getPhotoId() {
            return photoId;
        }

        public void setPhotoId(int photoId) {
            this.photoId = photoId;
        }

        public String getPhotoName() {
            return photoName;
        }

        public void setPhotoName(String photoName) {
            this.photoName = photoName;
        }

        public String getPhotoPath() {
            return photoPath;
        }

        public void setPhotoPath(String photoPath) {
            this.photoPath = photoPath;
        }

        public String getPhotoLowPath() {
            return photoLowPath;
        }

        public void setPhotoLowPath(String photoLowPath) {
            this.photoLowPath = photoLowPath;
        }

        public String getPhotoPreviewPath() {
            return photoPreviewPath;
        }

        public void setPhotoPreviewPath(String photoPreviewPath) {
            this.photoPreviewPath = photoPreviewPath;
        }

        public String getPhotoDescript() {
            return photoDescript;
        }

        public void setPhotoDescript(String photoDescript) {
            this.photoDescript = photoDescript;
        }

        public String getPhotoSuffix() {
            return photoSuffix;
        }

        public void setPhotoSuffix(String photoSuffix) {
            this.photoSuffix = photoSuffix;
        }

        public String getPhotoDate() {
            return photoDate;
        }

        public void setPhotoDate(String photoDate) {
            this.photoDate = photoDate;
        }

        public int getPhotoConType() {
            return photoConType;
        }

        public void setPhotoConType(int photoConType) {
            this.photoConType = photoConType;
        }

        public String getPhotoLink() {
            return photoLink;
        }

        public void setPhotoLink(String photoLink) {
            this.photoLink = photoLink;
        }
    }
}
