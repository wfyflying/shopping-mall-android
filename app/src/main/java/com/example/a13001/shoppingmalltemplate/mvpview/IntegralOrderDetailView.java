package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.OrderDetail;

public interface IntegralOrderDetailView extends View{
    void onSuccessGetIntegralOrderDetail(OrderDetail orderDetail);
    void onSuccessAffirmOrder(CommonResult commonResult);
    void onError(String result);
}
