package com.example.a13001.shoppingmalltemplate.modle;

public class HomePageTitle {
    private int logo;
    private String title;

    public HomePageTitle(int logo, String title) {
        this.logo = logo;
        this.title = title;
    }

    public HomePageTitle() {

    }

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
