package com.example.a13001.shoppingmalltemplate.activitys;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.adapters.MyCouponListRvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.MyCouponList;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class MyCouponListActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_title_right)
    TextView tvTitleRight;
    @BindView(R.id.rv_mycouponlist)
    RecyclerView rvMycouponlist;
    @BindView(R.id.tv_mycouponlist_unuse)
    TextView tvMycouponlistUnuse;
    @BindView(R.id.tv_mycouponlist_haveuse)
    TextView tvMycouponlistHaveuse;
    @BindView(R.id.srfl_mycouponlist)
    SmartRefreshLayout srflMycouponlist;
    @BindView(R.id.view1)
    View view1;
    @BindView(R.id.view2)
    View view2;
    @BindView(R.id.tv_mycouponlist_yishixiao)
    TextView tvMycouponlistYishixiao;
    @BindView(R.id.view3)
    View view3;
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private MyCouponList mMyCouponList;
    private List<MyCouponList.ListBean> mList;
    private MyCouponListRvAdapter mAdapter;
    private String code;
    private String timeStamp;
    private int pageindex;
    private String couponStatus = "";//优惠券状态，1 未使用 2 已使用，为空则所有
    private static final String TAG = "MyCouponListActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_coupon_list);
        ButterKnife.bind(this);
        mCompositeSubscription = new CompositeSubscription();
        manager = new DataManager(MyCouponListActivity.this);

        initData();
        setRefresh();
    }

    /**
     * 初始化数据源
     */
    private void initData() {
        tvTitleCenter.setText("优惠券");
        String safetyCode = MyUtils.getMetaValue(MyCouponListActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        timeStamp = Utils.getTimeStamp();


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MyCouponListActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvMycouponlist.setLayoutManager(linearLayoutManager);
        //添加自定义分割线
//        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
//        divider.setDrawable(ContextCompat.getDrawable(this, R.drawable.line_recycler));
//        rvMycouponlist.addItemDecoration(divider);
//        rvMycouponlist.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        mList = new ArrayList<>();
        mAdapter = new MyCouponListRvAdapter(MyCouponListActivity.this, mList);
        tvMycouponlistUnuse.performClick();

    }

    private void setRefresh() {
        //刷新
        srflMycouponlist.setRefreshHeader(new ClassicsHeader(MyCouponListActivity.this));
        srflMycouponlist.setRefreshFooter(new ClassicsFooter(MyCouponListActivity.this));
//        mRefresh.setEnablePureScrollMode(true);
        srflMycouponlist.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                pageindex = 1;
                if (mList != null) {
                    mList.clear();
                }
                getMyCouponList(AppConstants.COMPANY_ID, code, timeStamp, "", "", couponStatus, AppConstants.PAGE_SIZE, pageindex);
                refreshlayout.finishRefresh(2000/*,true*/);//传入false表示刷新失败
            }
        });
        srflMycouponlist.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                pageindex++;
                getMyCouponList(AppConstants.COMPANY_ID, code, timeStamp, "", "", couponStatus, AppConstants.PAGE_SIZE, pageindex);
                refreshlayout.finishLoadMore(2000/*,true*/);//传入false表示加载失败
            }
        });
    }

    @OnClick({R.id.iv_title_back, R.id.tv_title_right, R.id.tv_mycouponlist_unuse, R.id.tv_mycouponlist_haveuse,R.id.tv_mycouponlist_yishixiao})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_title_back:
                onBackPressed();
                break;
            case R.id.tv_title_right:
                break;
            //未使用
            case R.id.tv_mycouponlist_unuse:
               initColor();
                tvMycouponlistUnuse.setTextColor(getResources().getColor(R.color.ff2828));
                view1.setVisibility(View.VISIBLE);
                if (mList != null) {
                    mList.clear();
                }
                couponStatus = "1";
                getMyCouponList(AppConstants.COMPANY_ID, code, timeStamp, "", "", couponStatus, AppConstants.PAGE_SIZE, pageindex);
                rvMycouponlist.setAdapter(mAdapter);
                break;
            //已使用
            case R.id.tv_mycouponlist_haveuse:
                initColor();
                tvMycouponlistHaveuse.setTextColor(getResources().getColor(R.color.ff2828));
                view2.setVisibility(View.VISIBLE);
                if (mList != null) {
                    mList.clear();
                }
                couponStatus = "2";
                getMyCouponList(AppConstants.COMPANY_ID, code, timeStamp, "", "", couponStatus, AppConstants.PAGE_SIZE, pageindex);
                rvMycouponlist.setAdapter(mAdapter);
                break;
            //已失效
            case R.id.tv_mycouponlist_yishixiao:
                initColor();
                tvMycouponlistYishixiao.setTextColor(getResources().getColor(R.color.ff2828));
                view3.setVisibility(View.VISIBLE);
                if (mList != null) {
                    mList.clear();
                }
                couponStatus = "2";
                getMyCouponList(AppConstants.COMPANY_ID, code, timeStamp, "", "", couponStatus, AppConstants.PAGE_SIZE, pageindex);
                rvMycouponlist.setAdapter(mAdapter);
                break;
        }
    }

    /**
     * 初始化顶部标题颜色
     */
    private void initColor() {
        tvMycouponlistUnuse.setTextColor(getResources().getColor(R.color.t333));
        tvMycouponlistHaveuse.setTextColor(getResources().getColor(R.color.t333));
        tvMycouponlistYishixiao.setTextColor(getResources().getColor(R.color.t333));

        view1.setVisibility(View.INVISIBLE);
        view2.setVisibility(View.INVISIBLE);
        view3.setVisibility(View.INVISIBLE);

    }

    /**
     * ★ 获取会员商城优惠券列表
     *
     * @param companyid
     * @param code
     * @param timestamp
     * @param storeid
     * @param money
     * @param status
     * @param pagesize
     * @param pageindex
     * @return
     */
    public void getMyCouponList(String companyid, String code, String timestamp,
                                String storeid, String money, String status, int pagesize, int pageindex) {

        mCompositeSubscription.add(manager.getMyCouponList(companyid, code, timestamp, storeid, money, status, pagesize, pageindex)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MyCouponList>() {
                    @Override
                    public void onCompleted() {
                        if (mMyCouponList != null) {
                            int status = mMyCouponList.getStatus();
                            if (status > 0) {
                                mList.addAll(mMyCouponList.getList());
                                mAdapter.notifyDataSetChanged();
                            } else {
//                                Toast.makeText(MyCouponListActivity.this, "" + mMyCouponList.getReturnMsg(), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: " + e.toString());
                    }

                    @Override
                    public void onNext(MyCouponList myCouponList) {
                        mMyCouponList = myCouponList;
                    }
                }));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCompositeSubscription.hasSubscriptions()) {
            mCompositeSubscription.unsubscribe();
        }
    }

    @OnClick(R.id.tv_mycouponlist_yishixiao)
    public void onViewClicked() {
    }
}
