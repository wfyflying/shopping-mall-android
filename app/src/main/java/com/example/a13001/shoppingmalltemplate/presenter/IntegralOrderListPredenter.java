package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;

import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.Order;
import com.example.a13001.shoppingmalltemplate.mvpview.IntegralOrderListView;
import com.example.a13001.shoppingmalltemplate.mvpview.OrderListView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class IntegralOrderListPredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private IntegralOrderListView mIntegralOrderListView;
    private Order mOrder;
    private CommonResult mCommonResult;

    public IntegralOrderListPredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        mCompositeSubscription=new CompositeSubscription();
        manager=new DataManager(mContext);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mIntegralOrderListView=(IntegralOrderListView) view;
    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }


    /**
     *  获取普通订单列表
     * @param companyid   站点ID
     * @param code        安全校验码
     * @param timestamp   时间戳
     * @param pagesize    每页显示数量
     * @param pageindex   当前页数
     * @param status    查询订单状态，1 待支付 2 未发货 3 已发货 4 已签收
     * @param ordersNumber   查询订单号，支持模糊查询
     * @param starttime    指定开始时间（订单下单时间区间查询）
     * @param endtime     指定结束时间（订单下单时间区间查询）
     * @param from    来源，pc 电脑端 mobile 移动端
     * @return
     */
    public void getOrderList(String companyid, String code, String timestamp, int pagesize, int pageindex, String status, String ordersNumber, String starttime, String endtime, String from) {

        mCompositeSubscription.add(manager.getIntegralOrderList(companyid,code,timestamp,pagesize,pageindex,status,ordersNumber,starttime,endtime,from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Order>() {
                    @Override
                    public void onCompleted() {
                        if (mIntegralOrderListView!=null){
                            mIntegralOrderListView.onSuccessGetIntegralOrderList(mOrder);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mIntegralOrderListView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(Order order) {
                        mOrder=order;
                    }
                }));
    }

    /**
     * 订单确认收货
     * @param companyid   站点ID
     * @param code         安全校验码
     * @param timestamp    时间戳
     * @param ordersNumber  订单号
     * @return
     */
    public void affirmOrder(String companyid, String code, String timestamp, String ordersNumber) {

        mCompositeSubscription.add(manager.doSureIntegralOrder(companyid,code,timestamp,ordersNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mIntegralOrderListView!=null){
                            mIntegralOrderListView.onSuccessAffirmOrder(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mIntegralOrderListView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
}
