package com.example.a13001.shoppingmalltemplate.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.MyCouponList;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

import butterknife.BindView;

public class MyCouponListRvAdapter extends RecyclerView.Adapter<MyCouponListRvAdapter.ViewHolder> {

    private Context mContext;
    private List<MyCouponList.ListBean> mList;
    GoodsListRvAdapter.onItemClickListener onItemClickListener;

    public MyCouponListRvAdapter(Context mContext, List<MyCouponList.ListBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    public interface onItemClickListener {
        void onClick(int position);
    }

    public void setOnItemClickListener(GoodsListRvAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_rv_mycouponlist, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
       holder.tvMoney.setText(mList.get(position).getQmoney()+"");
       holder.tvTitle.setText(mList.get(position).getAtitle()!=null?mList.get(position).getAtitle():"");
       holder.tvContent.setText(mList.get(position).getQzjContent()!=null?mList.get(position).getQzjContent():"");
       holder.tvAddtime.setText(mList.get(position).getSnAddTime()!=null?mList.get(position).getSnAddTime():"");
       holder.tvEndTIme.setText(mList.get(position).getSnEndTime()!=null?mList.get(position).getSnEndTime():"");
      int snStatus= mList.get(position).getSnStatus();//SN码状态 1 未使用 2 已使用
        if (snStatus==1){
            holder.rlTop.setBackground(mContext.getResources().getDrawable(R.drawable.img_coupon_ljsy));
        }else if (snStatus==2){
            holder.rlTop.setBackground(mContext.getResources().getDrawable(R.drawable.img_coupon_haveuse));
        }else{
            holder.rlTop.setBackground(mContext.getResources().getDrawable(R.drawable.img_coupon_yishixiao));
        }
        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {


        private final TextView tvTitle;
        private final TextView tvMoney;
        private final TextView tvContent;
        private final TextView tvAddtime;
        private final TextView tvEndTIme;
        private RelativeLayout rlTop;

        public ViewHolder(View itemView) {
            super(itemView);
            AutoUtils.autoSize(itemView);
            tvTitle = itemView.findViewById(R.id.tv_itemmycouponlist_title);
            tvMoney = itemView.findViewById(R.id.tv_itemmycouponlist_money);
            tvContent = itemView.findViewById(R.id.tv_itemmycouponlist_content);
            tvAddtime = itemView.findViewById(R.id.tv_itemmycouponlist_addtime);
            tvEndTIme = itemView.findViewById(R.id.tv_itemmycouponlist_endtime);
            rlTop = itemView.findViewById(R.id.rl_itemmycoupon_top);
        }
    }
}
