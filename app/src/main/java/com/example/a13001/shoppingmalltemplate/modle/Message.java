package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class Message {

    /**
     * status : 1
     * returnMsg : null
     * count : 0
     * pagesize : 20
     * pageindex : 1
     * list : [{"smsId":5,"smsTitle":"你有一个新消息，请仔细查收。","smsAddtime":"2018-06-15 09:32","smsStatus":1}]
     */

    private int status;
    private Object returnMsg;
    private int count;
    private int pagesize;
    private int pageindex;
    private List<ListBean> list;

    @Override
    public String toString() {
        return "Message{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", count=" + count +
                ", pagesize=" + pagesize +
                ", pageindex=" + pageindex +
                ", list=" + list +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public int getPageindex() {
        return pageindex;
    }

    public void setPageindex(int pageindex) {
        this.pageindex = pageindex;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * smsId : 5
         * smsTitle : 你有一个新消息，请仔细查收。
         * smsAddtime : 2018-06-15 09:32
         * smsStatus : 1
         */

        private int smsId;
        private String smsTitle;
        private String smsAddtime;
        private int smsStatus;

        @Override
        public String toString() {
            return "ListBean{" +
                    "smsId=" + smsId +
                    ", smsTitle='" + smsTitle + '\'' +
                    ", smsAddtime='" + smsAddtime + '\'' +
                    ", smsStatus=" + smsStatus +
                    '}';
        }

        public int getSmsId() {
            return smsId;
        }

        public void setSmsId(int smsId) {
            this.smsId = smsId;
        }

        public String getSmsTitle() {
            return smsTitle;
        }

        public void setSmsTitle(String smsTitle) {
            this.smsTitle = smsTitle;
        }

        public String getSmsAddtime() {
            return smsAddtime;
        }

        public void setSmsAddtime(String smsAddtime) {
            this.smsAddtime = smsAddtime;
        }

        public int getSmsStatus() {
            return smsStatus;
        }

        public void setSmsStatus(int smsStatus) {
            this.smsStatus = smsStatus;
        }
    }
}
