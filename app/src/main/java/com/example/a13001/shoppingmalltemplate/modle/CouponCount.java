package com.example.a13001.shoppingmalltemplate.modle;

public class CouponCount {

    /**
     * status : 1
     * returnMsg : null
     * count : 2
     */

    private int status;
    private String returnMsg;
    private int count;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
