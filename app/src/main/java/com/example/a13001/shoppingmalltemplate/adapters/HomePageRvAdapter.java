package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.MyGridView;
import com.example.a13001.shoppingmalltemplate.activitys.GoodsDetailActivity;
import com.example.a13001.shoppingmalltemplate.activitys.GoodsListActivity;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.GoodsDetail;
import com.example.a13001.shoppingmalltemplate.modle.HomepageGoods;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomePageRvAdapter extends BaseAdapter {
    private Context mContext;
    private List<HomepageGoods> mList;

    private HomePageRvSubAdapter mAdapter;

    public HomePageRvAdapter(Context mContext, List<HomepageGoods> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_rv_homepage_top, viewGroup, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
            AutoUtils.autoSize(view);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.lltop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext, GoodsListActivity.class).putExtra("id",mList.get(i).getListgoodsdetail().get(0).getClassid()).putExtra("searchword",""));
            }
        });
        viewHolder.tvItemrvhomeTitle.setText(mList.get(i).getClassname()!=null?mList.get(i).getClassname():"");
        mAdapter=new HomePageRvSubAdapter(mContext,mList.get(i).getListgoodsdetail());
        viewHolder.rvItemrvhome.setAdapter(mAdapter);
        viewHolder.rvItemrvhome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//                Toast.makeText(mContext, ""+position, Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(mContext, GoodsDetailActivity.class);
                intent.putExtra("good_id",mList.get(i).getListgoodsdetail().get(position).getId());
                intent.putExtra("class_id",mList.get(i).getListgoodsdetail().get(position).getClassid());
                intent.putExtra("type","a");
                mContext.startActivity(intent);
            }
        });
        return view;
    }


    static class ViewHolder {
        @BindView(R.id.iv_itemrvhome_leftlogo)
        ImageView ivItemrvhomeLeftlogo;
        @BindView(R.id.tv_itemrvhome_title)
        TextView tvItemrvhomeTitle;
        @BindView(R.id.iv_itemrvhome_more)
        ImageView ivItemrvhomeMore;
        @BindView(R.id.rv_itemrvhome)
        MyGridView rvItemrvhome;
        @BindView(R.id.ll_top)
        LinearLayout lltop;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
