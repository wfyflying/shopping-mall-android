package com.example.a13001.shoppingmalltemplate.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.adapters.InformLvAdapter;
import com.example.a13001.shoppingmalltemplate.adapters.MessageLvAdapter;
import com.example.a13001.shoppingmalltemplate.adapters.NoticeLvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.Message;
import com.example.a13001.shoppingmalltemplate.modle.Notice;
import com.example.a13001.shoppingmalltemplate.mvpview.MessageView;
import com.example.a13001.shoppingmalltemplate.presenter.MessagePredenter;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InformActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_inform_unread)
    TextView tvInformUnread;
    @BindView(R.id.tv_inform_read)
    TextView tvInformRead;
    @BindView(R.id.lv_inform)
    ListView lvInform;
    private List<Notice.ListBean> mList;
    private InformLvAdapter mAdapter;
    MessagePredenter messagePredenter=new MessagePredenter(InformActivity.this);
    private String safetyCode;
    private String code;
    private String timeStamp;
    private int pageindex=1;
    private static final String TAG = "InformActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inform);
        ButterKnife.bind(this);
        initData();
    }
    MessageView messageView=new MessageView() {
        @Override
        public void onSuccess(Message message) {

        }

        @Override
        public void onSuccessNotice(Notice notice) {
            Log.e(TAG, "onSuccessNotice: "+notice.toString());
            int status=notice.getStatus();
            if (status>0){
                mList.addAll(notice.getList());
                mAdapter.notifyDataSetChanged();
            }else{
                Toast.makeText(InformActivity.this, ""+notice.getReturnMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onSuccessDoSmsDelete(CommonResult commonResult) {

        }

        @Override
        public void onError(String result) {

        }
    };
    /**
     * 初始化数据源
     */
    private void initData() {
        messagePredenter.onCreate();
        messagePredenter.attachView(messageView);
        safetyCode = MyUtils.getMetaValue(InformActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        timeStamp = Utils.getTimeStamp();
        tvTitleCenter.setText("站内通知");
        mList=new ArrayList<>();
        mAdapter=new InformLvAdapter(InformActivity.this,mList);
        messagePredenter.getNoticeList(AppConstants.COMPANY_ID,code,timeStamp,AppConstants.PAGE_SIZE,pageindex);
        lvInform.setAdapter(mAdapter);

        lvInform.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e(TAG, "onCreate: "+mList.get(i).getNoticeId());
                startActivity(new Intent(InformActivity.this,InformDetailActivity.class).putExtra("noticeid",mList.get(i).getNoticeId()));
            }
        });
    }
    @OnClick({R.id.iv_title_back, R.id.tv_inform_unread, R.id.tv_inform_read})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_title_back:
                onBackPressed();
                break;
                //未读消息
            case R.id.tv_inform_unread:
                initTxtColor();
                tvInformUnread.setTextColor(getResources().getColor(R.color.white));
                tvInformUnread.setBackgroundColor(getResources().getColor(R.color.ffb422));
                break;
                //已读消息
            case R.id.tv_inform_read:
                initTxtColor();
                tvInformRead.setTextColor(getResources().getColor(R.color.white));
                tvInformRead.setBackgroundColor(getResources().getColor(R.color.ffb422));
                break;
        }
    }
    /**
     * 初始化顶部标题颜色
     */
    private void initTxtColor() {
        tvInformUnread.setTextColor(getResources().getColor(R.color.t333));
        tvInformUnread.setBackgroundColor(getResources().getColor(R.color.white));
        tvInformRead.setTextColor(getResources().getColor(R.color.t333));
        tvInformRead.setBackgroundColor(getResources().getColor(R.color.white));
    }
}
