package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;

import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.OrderDetail;
import com.example.a13001.shoppingmalltemplate.mvpview.IntegralOrderDetailView;
import com.example.a13001.shoppingmalltemplate.mvpview.OrderDetailView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class IntegralOrderDetailPredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private IntegralOrderDetailView mIntegralOrderDetailView;
    private OrderDetail mOrderDetail;
    private CommonResult mCommonResult;

    public IntegralOrderDetailPredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        mCompositeSubscription=new CompositeSubscription();
        manager=new DataManager(mContext);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mIntegralOrderDetailView=(IntegralOrderDetailView) view;
    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }


    /**
     * 查看普通订单详情
     * @param companyid   站点ID
     * @param code         安全校验码
     * @param timestamp    时间戳
     * @param ordersNumber  订单号
     * @return
     */
    public void getIntegralOrderDetail(String companyid, String code, String timestamp, String ordersNumber, String from) {

        mCompositeSubscription.add(manager.getIntegralOrderDetail(companyid,code,timestamp,ordersNumber,from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<OrderDetail>() {
                    @Override
                    public void onCompleted() {
                        if (mIntegralOrderDetailView!=null){
                            mIntegralOrderDetailView.onSuccessGetIntegralOrderDetail(mOrderDetail);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mIntegralOrderDetailView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(OrderDetail orderDetail) {
                        mOrderDetail=orderDetail;
                    }
                }));
    }

    /**
     * 订单确认收货
     * @param companyid   站点ID
     * @param code         安全校验码
     * @param timestamp    时间戳
     * @param ordersNumber  订单号
     * @return
     */
    public void affirmOrder(String companyid, String code, String timestamp, String ordersNumber) {

        mCompositeSubscription.add(manager.doSureIntegralOrder(companyid,code,timestamp,ordersNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mIntegralOrderDetailView!=null){
                            mIntegralOrderDetailView.onSuccessAffirmOrder(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mIntegralOrderDetailView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
}
