package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;

import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.Banner;
import com.example.a13001.shoppingmalltemplate.modle.GoodsDetail;
import com.example.a13001.shoppingmalltemplate.modle.News;
import com.example.a13001.shoppingmalltemplate.mvpview.InformationView;
import com.example.a13001.shoppingmalltemplate.mvpview.NewsView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class InformationPredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private InformationView mInformationView;
    private News mNews;
    private Banner mBanner;



    public InformationPredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        manager=new DataManager(mContext);
        mCompositeSubscription=new CompositeSubscription();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mInformationView=(InformationView) view;

    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }

    /**
     * 获取资讯列表
     * @param companyid  站点id
     * @param channelid  频道ID，指定多个频道用“|”线分割
     * @param pagesize  每页显示数量
     * @param pageindex   当前页数
     */
    public void getSearchNews(String companyid,String channelid,String keyword,int pagesize,int pageindex){
        mCompositeSubscription.add(manager.getNewsList(companyid,channelid,keyword,pagesize,pageindex)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<News>() {
                               @Override
                               public void onCompleted() {
                                   if (mNews!=null){
                                       mInformationView.onSuccess(mNews);
                                   }
                               }

                               @Override
                               public void onError(Throwable e) {
                                   mInformationView.onError("请求失败");
                               }

                               @Override
                               public void onNext(News news) {
                                   mNews=news;
                               }
                           }

                ));
    }

    /**
     * 获取banner列表
     * @param companyid  站点id
     * @param label   碎片文件标识ID


     */
    public void getBannerList(String companyid,String label){
        mCompositeSubscription.add(manager.getBannerList(companyid,label)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Banner>() {
                               @Override
                               public void onCompleted() {
                                   if (mInformationView!=null){
                                       mInformationView.onSuccessBanner(mBanner);
                                   }
                               }

                               @Override
                               public void onError(Throwable e) {
                                   mInformationView.onError("请求失败");
                               }

                               @Override
                               public void onNext(Banner banner) {
                                   mBanner=banner;
                               }
                           }

                ));
    }


}
