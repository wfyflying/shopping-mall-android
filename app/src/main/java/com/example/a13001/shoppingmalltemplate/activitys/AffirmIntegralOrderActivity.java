package com.example.a13001.shoppingmalltemplate.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.MyListView;
import com.example.a13001.shoppingmalltemplate.adapters.AffirmIntegralOrderLvAdapter;
import com.example.a13001.shoppingmalltemplate.adapters.AffirmOrderLvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.event.AddShopCarEvent;
import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.CloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.IntegralCloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.MyCouponList;
import com.example.a13001.shoppingmalltemplate.modle.RefreshYunFei;
import com.example.a13001.shoppingmalltemplate.mvpview.AffirmOrderView;
import com.example.a13001.shoppingmalltemplate.presenter.AffirmOrderPredenter;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AffirmIntegralOrderActivity extends BaseActivity {


    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;

    @BindView(R.id.et_doorder_say)
    EditText etDoorderSay;

    @BindView(R.id.tv_doorder_heji)
    TextView tvDoorderHeji;
    @BindView(R.id.ll_noaddress)
    LinearLayout llNoaddress;
    @BindView(R.id.ll_haveaddress)
    LinearLayout llHaveaddress;
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.tv_useraddress)
    TextView tvUseraddress;
    @BindView(R.id.tv_title_right)
    TextView tvTitleRight;
    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;
    @BindView(R.id.tv_affirmintegralorder_default)
    TextView tvAffirmintegralorderDefault;
    @BindView(R.id.ll_affirmintegralorder_address)
    LinearLayout llAffirmintegralorderAddress;
    @BindView(R.id.iv_doorder_company)
    ImageView ivDoorderCompany;
    @BindView(R.id.tv_doorder_companyname)
    TextView tvDoorderCompanyname;
    @BindView(R.id.lv_affirmintegralorder)
    MyListView lvAffirmintegralorder;
    @BindView(R.id.btn_affirmintegralorder_commit)
    Button btnAffirmintegralorderCommit;

    private AffirmIntegralOrderLvAdapter mAdapter;
    private List<CloaseAccount.ShopListBean> mList;
    private String mCartId;
    private static final String TAG = "AffirmOrderActivity";
    AffirmOrderPredenter affirmOrderPredenter = new AffirmOrderPredenter(AffirmIntegralOrderActivity.this);
    CloaseAccount mCloaseAccount;
    private int pageindex = 1;
    private String code;
    private String timestamp;
    private int addressid = 0;
    private String paymentname = "";
    private String orderType = "";
    private int invoiceStatus = -1;
    private String invoicename = "";
    private String remark = "";
    private String totalPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affirm_integralorder);
        ButterKnife.bind(this);
        affirmOrderPredenter.onCreate();
        affirmOrderPredenter.attachView(affirmOrderView);
        initData();
    }


    AffirmOrderView affirmOrderView = new AffirmOrderView() {
        @Override
        public void onSuccessDoCloaseAccount(CloaseAccount cloaseAccount) {
            Log.e(TAG, "onSuccessDoCloaseAccount: " + cloaseAccount.toString());
            mCloaseAccount = cloaseAccount;
            int status = cloaseAccount.getStatus();
            if (status > 0) {
                        mList.addAll(cloaseAccount.getShopList());
                        mAdapter.notifyDataSetChanged();

                            tvDoorderHeji.setText(cloaseAccount.getTotalPrice()+ "");


            } else {
                Toast.makeText(AffirmIntegralOrderActivity.this, "" + cloaseAccount.getReturnMsg(), Toast.LENGTH_SHORT).show();
            }
        }


        //提交商品订单
        @Override
        public void onSuccessDoCommitOrder(CommonResult commonResult) {
            Log.e(TAG, "onSuccessDoCommitOrder: " + commonResult.toString());
            int status = commonResult.getStatus();
            if (status > 0) {
                Intent intent=new Intent(AffirmIntegralOrderActivity.this,IntegralOrderDetailActivity.class);
                intent.putExtra("ordernumber",commonResult.getOrderNumber());
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(AffirmIntegralOrderActivity.this, "" + commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();
            }

        }

        //获取默认收货地址
        @Override
        public void onSuccessGetAddress(Address address) {
            Log.e(TAG, "onSuccessGetAddress: " + address.toString());
            int status = address.getStatus();
            if (status > 0) {
                llHaveaddress.setVisibility(View.VISIBLE);
                llNoaddress.setVisibility(View.GONE);
                tvUsername.setText(address.getDelivery_AddressList().get(0).getAddressName() + "    " + address.getDelivery_AddressList().get(0).getAddressPhone());
                tvUseraddress.setText("地址:" + address.getDelivery_AddressList().get(0).getAddressProvince() + address.getDelivery_AddressList().get(0).getAddressCity() + address.getDelivery_AddressList().get(0).getAddressArea() + address.getDelivery_AddressList().get(0).getAddressXX());
                addressid = address.getDelivery_AddressList().get(0).getAddresslId();
            } else {
                llHaveaddress.setVisibility(View.GONE);
                llNoaddress.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onSuccessGetMyCouponList(MyCouponList myCouponList) {

        }

        @Override
        public void onSuccessRefreshYunFei(RefreshYunFei refreshYunFei) {

        }

        @Override
        public void onError(String result) {

        }
    };
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            switch (requestCode) {

                case 4:
                    if (resultCode == 44) {
                        Address.DeliveryAddressListBean addressListBean = (Address.DeliveryAddressListBean) data.getParcelableExtra("address");
                        Log.e(TAG, "onActivityResult: " + addressListBean.toString());
                        tvUsername.setText(addressListBean.getAddressName() + "    " + addressListBean.getAddressPhone());
                        tvUseraddress.setText("地址:" + addressListBean.getAddressProvince() + addressListBean.getAddressCity() + addressListBean.getAddressArea() + addressListBean.getAddressXX());
                        boolean isDefault = addressListBean.isAddressDefault();
                        if (isDefault) {
                            tvAffirmintegralorderDefault.setVisibility(View.VISIBLE);
                        } else {
                            tvAffirmintegralorderDefault.setVisibility(View.GONE);
                        }
                        addressid=addressListBean.getAddresslId();
                    }
                    break;
            }
        }

    }
    /**
     * 初始化数据源
     */
    private void initData() {
        mCartId = getIntent().getStringExtra("cartId");
        tvTitleCenter.setText("确认订单");
        mList = new ArrayList<>();
        mAdapter = new AffirmIntegralOrderLvAdapter(AffirmIntegralOrderActivity.this, mList);
        lvAffirmintegralorder.setAdapter(mAdapter);
        String safetyCode = MyUtils.getMetaValue(AffirmIntegralOrderActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        timestamp = Utils.getTimeStamp();
        Log.e(TAG, "initData: " + code + "==" + Utils.getTimeStamp() + "==" + mCartId);
        affirmOrderPredenter.doCloaseAccountIntegral(AppConstants.COMPANY_ID, code, timestamp, mCartId, AppConstants.FROM_MOBILE);
        affirmOrderPredenter.getAdressList(AppConstants.COMPANY_ID, code, timestamp, AppConstants.PAGE_SIZE, pageindex, 1, "");
    }


    @OnClick({R.id.iv_title_back, R.id.ll_affirmintegralorder_address, R.id.btn_affirmintegralorder_commit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_title_back:
                onBackPressed();
                break;

            //地址
            case R.id.ll_affirmintegralorder_address:
                Intent addressintent = new Intent(AffirmIntegralOrderActivity.this, AdressManagerActivity.class);
                startActivityForResult(addressintent, 4);
                break;

            //提交订单
            case R.id.btn_affirmintegralorder_commit:
                if (addressid == 0) {
                    Toast.makeText(this, "请先选择收货地址", Toast.LENGTH_SHORT).show();
                    return;
                }
                remark = etDoorderSay.getText().toString().trim();
                Log.e(TAG, "onViewClicked: " + AppConstants.COMPANY_ID + "code==" + code + "timestamp==" + timestamp + "mCartId==" + mCartId + "addressid==" + addressid + "paymentid==" + paymentname + "orderType==" + orderType + "invoiceStatus==" + invoiceStatus + "invoicename==" + invoicename + "remark==" + remark + "from==" + AppConstants.FROM_MOBILE);
                affirmOrderPredenter.doCommitIntegralOrder(AppConstants.COMPANY_ID, code, timestamp, mCartId, addressid, remark, AppConstants.FROM_MOBILE);
                break;
        }
    }


}
