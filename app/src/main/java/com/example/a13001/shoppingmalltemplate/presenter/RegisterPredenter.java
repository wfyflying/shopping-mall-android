package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.MainActivity;
import com.example.a13001.shoppingmalltemplate.activitys.LoginActivity;
import com.example.a13001.shoppingmalltemplate.application.ShoppingMallTemplateApplication;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.User;
import com.example.a13001.shoppingmalltemplate.mvpview.AdressManagerView;
import com.example.a13001.shoppingmalltemplate.mvpview.RegisterView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;

import java.util.Map;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class RegisterPredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private RegisterView mRegisterView;
    private User mUser;
    private CommonResult mCommonResult;



    public RegisterPredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        mCompositeSubscription=new CompositeSubscription();
        manager=new DataManager(mContext);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mRegisterView=(RegisterView) view;
    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }
    public void doRegister(String companyid,String code,String timestamp,Map<String, String> map) {
        mCompositeSubscription.add(manager.doRegister(companyid,code,timestamp,map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<User>() {
                               @Override
                               public void onCompleted() {
                                   if (mRegisterView!=null){
                                       mRegisterView.onSuccessDoRegister(mUser);
                                   }
                               }

                               @Override
                               public void onError(Throwable e) {
                                   mRegisterView.onError("请求失败"+e.toString());
                               }

                               @Override
                               public void onNext(User user) {
                                    mUser=user;
                               }
                           }

                ));
    }
    /**
     * ★ 获取图形验证码
     */

    public void getPicCode(String companyid,String code,String timestamp) {

        mCompositeSubscription.add(manager.getPicCode(companyid,code,timestamp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mRegisterView!=null){
                            mRegisterView.onSuccessGetPicCode(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mRegisterView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
    /**
     * ★  验证会员注册信息-发送手机校验码
     */
    public void getPhoneCode(String companyid,String code,String timestamp,String chkdata1,String chkdata2,String from) {

        mCompositeSubscription.add(manager.getPhoneCode(companyid,code,timestamp,chkdata1,chkdata2,from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mRegisterView!=null){
                            mRegisterView.onSuccessGetPhoneCode(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mRegisterView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
    /**
     * 登录
     *
     * @param companyid 站点ID
     * @param code      安全校验码
     * @param timestamp 时间戳
     * @param name      会员名称，用户名/手机/邮箱
     * @param pwd       登录密码
     * @param from      来源，pc 电脑端 mobile 移动端
     */
    public void doLogin(String companyid, String code, String timestamp, final String name, final String pwd, String from) {
        mCompositeSubscription.add(manager.doLogin(companyid, code, timestamp, name, pwd, from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<User>() {
                               @Override
                               public void onCompleted() {
                                   if (mRegisterView!=null){
                                       mRegisterView.onSuccessDoLogin(mUser);
                                   }

                               }

                               @Override
                               public void onError(Throwable e) {
                                   mRegisterView.onError("请求失败"+e.toString());
                               }

                               @Override
                               public void onNext(User user) {
                                   mUser=user;

                               }
                           }

                ));
    }
}
