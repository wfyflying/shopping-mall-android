package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;


import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

public class ShouHouDetailLvAdapter extends BaseAdapter {
    private Context mContext;
    private List<String> mList;

    public ShouHouDetailLvAdapter(Context mContext, List<String> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            view= LayoutInflater.from(mContext).inflate(R.layout.item_lvshouhoudetail,parent,false);
            holder=new ViewHolder(view);
            view.setTag(holder);
            AutoUtils.autoSize(view);
        }else{
            holder= (ViewHolder) view.getTag();
        }
       GlideUtils.setNetImage22(mContext,mList.get(i),holder.ivTuPian);

        return view;
    }
    class ViewHolder{

        ImageView ivTuPian;
        public ViewHolder(View view) {
            ivTuPian = view.findViewById(R.id.iv_tupian);
        }
    }
}
