package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.modle.Goods;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by axehome on 2017/11/17.
 */

public class LogisticsInfoLvAdapter extends BaseAdapter {
    private Context mContext;
    private List<Goods> mList;

    public LogisticsInfoLvAdapter(Context mContext, List<Goods> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (mList != null && mList.size() > 0) {
            return mList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_lv_freedection, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
            AutoUtils.autoSize(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (position == 0) {

            holder.ivItemfreedectionPoint.setImageResource(R.drawable.jianli_point_blue);
        }else{
            holder.ivItemfreedectionPoint.setImageResource(R.drawable.jianli_point_grey);
        }
        //最后一项时，竖线不再显示
        if (position == mList.size() - 1) {
            holder.ivItemfreedectionLine.setVisibility(View.GONE);

        }else{
            holder.ivItemfreedectionLine.setVisibility(View.VISIBLE);
        }
        return convertView;
    }


    static class ViewHolder {
        @BindView(R.id.iv_itemfreedection_point)
        ImageView ivItemfreedectionPoint;
        @BindView(R.id.iv_itemfreedection_line)
        ImageView ivItemfreedectionLine;
        @BindView(R.id.tv_itemfreedection_state)
        TextView tvItemfreedectionState;
        @BindView(R.id.tv_itemfreedection_currentstate)
        TextView tvItemfreedectionCurrentstate;
        @BindView(R.id.tv_itemfreedection_date)
        TextView tvItemfreedectionDate;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
