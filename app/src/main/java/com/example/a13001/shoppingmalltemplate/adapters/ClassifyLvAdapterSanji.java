package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.MyGridView;
import com.example.a13001.shoppingmalltemplate.modle.Classify;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClassifyLvAdapterSanji extends BaseAdapter {
    private Context mContext;
    private List<Classify.ListBeanXX.ListBeanX> mList;
    private List<Classify.ListBeanXX.ListBeanX.ListBean> mListChild;
    private ClassifyfGvAdapterSanJi mAdapterChild;
    public ClassifyLvAdapterSanji(Context mContext, List<Classify.ListBeanXX.ListBeanX> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_lv_classifysanjji, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
            AutoUtils.autoSize(view);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.tvClassifyfFenlei.setText(mList.get(i).getClassName()!=null?mList.get(i).getClassName():"");
        mListChild=new ArrayList<>();
        try {
            mListChild.addAll(mList.get(i).getList());
            mAdapterChild=new ClassifyfGvAdapterSanJi(mContext,mList.get(i).getList());
            holder.mgvSanji.setAdapter(mAdapterChild);
        }catch (Exception e){

        }

        return view;
    }

    static class ViewHolder {
        @BindView(R.id.tv_classifyf_fenlei)
        TextView tvClassifyfFenlei;
        @BindView(R.id.mgv_sanji)
        MyGridView mgvSanji;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
