package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class Banner {


    /**
     * status : 0
     * returnMsg : null
     * fragmentName : 手机首页banner
     * fragmentLabel : sjsybanner
     * fragmentRange : 2
     * fragmentType : 7
     * fragmentText :
     * fragmentWidth : 720
     * fragmentHeight : 360
     * fragmentPictureList : [{"fragmentPicture":"//file.site.ify.cn/site/144/upload/ad/images/sjsybanner.png","fragmentLink":""},{"fragmentPicture":"//file.site.ify.cn/site/144/upload/ad/images/sjsybanner2.png","fragmentLink":""},{"fragmentPicture":"//file.site.ify.cn/site/144/upload/ad/images/sjsybanner3.png","fragmentLink":""}]
     */

    private int status;
    private Object returnMsg;
    private String fragmentName;
    private String fragmentLabel;
    private int fragmentRange;
    private int fragmentType;
    private String fragmentText;
    private int fragmentWidth;
    private int fragmentHeight;
    private List<FragmentPictureListBean> fragmentPictureList;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public String getFragmentName() {
        return fragmentName;
    }

    public void setFragmentName(String fragmentName) {
        this.fragmentName = fragmentName;
    }

    public String getFragmentLabel() {
        return fragmentLabel;
    }

    public void setFragmentLabel(String fragmentLabel) {
        this.fragmentLabel = fragmentLabel;
    }

    public int getFragmentRange() {
        return fragmentRange;
    }

    public void setFragmentRange(int fragmentRange) {
        this.fragmentRange = fragmentRange;
    }

    public int getFragmentType() {
        return fragmentType;
    }

    public void setFragmentType(int fragmentType) {
        this.fragmentType = fragmentType;
    }

    public String getFragmentText() {
        return fragmentText;
    }

    public void setFragmentText(String fragmentText) {
        this.fragmentText = fragmentText;
    }

    public int getFragmentWidth() {
        return fragmentWidth;
    }

    public void setFragmentWidth(int fragmentWidth) {
        this.fragmentWidth = fragmentWidth;
    }

    public int getFragmentHeight() {
        return fragmentHeight;
    }

    public void setFragmentHeight(int fragmentHeight) {
        this.fragmentHeight = fragmentHeight;
    }

    public List<FragmentPictureListBean> getFragmentPictureList() {
        return fragmentPictureList;
    }

    public void setFragmentPictureList(List<FragmentPictureListBean> fragmentPictureList) {
        this.fragmentPictureList = fragmentPictureList;
    }

    public static class FragmentPictureListBean {
        /**
         * fragmentPicture : //file.site.ify.cn/site/144/upload/ad/images/sjsybanner.png
         * fragmentLink :
         */

        private String fragmentPicture;
        private String fragmentLink;

        public String getFragmentPicture() {
            return fragmentPicture;
        }

        public void setFragmentPicture(String fragmentPicture) {
            this.fragmentPicture = fragmentPicture;
        }

        public String getFragmentLink() {
            return fragmentLink;
        }

        public void setFragmentLink(String fragmentLink) {
            this.fragmentLink = fragmentLink;
        }
    }
}
