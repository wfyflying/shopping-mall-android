package com.example.a13001.shoppingmalltemplate.application;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Environment;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.a13001.shoppingmalltemplate.R;
import com.mob.MobSDK;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.umeng.commonsdk.UMConfigure;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import cn.jpush.android.api.JPushInterface;
import cn.sharesdk.framework.ShareSDK;

public class ShoppingMallTemplateApplication extends Application{
  
    private static ShoppingMallTemplateApplication application;
    private List<Activity> activityList=new LinkedList<>();
    public static RequestOptions options;
    public static RequestOptions options1;
    public static RequestOptions options2;
    public static  String loginType="";
    public static RequestOptions options22;

    @Override
    public void onCreate() {
        super.onCreate();
        application=this;
        MobSDK.init(getApplicationContext());
        JPushInterface.init(this);
//        UMConfigure.init(getApplicationContext(),"5afa7bd68f4a9d33c300007f",null,);
        initImageLoader(getApplicationContext());
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .priority(Priority.HIGH)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        options1 = new RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .priority(Priority.HIGH)
                .diskCacheStrategy(DiskCacheStrategy.NONE);
        options2 = new RequestOptions()
                .fitCenter()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .priority(Priority.HIGH)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        options22 = new RequestOptions()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .priority(Priority.HIGH)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
    }
    private void initImageLoader(Context applicationContext) {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.color.ee)    // 正在加载图片时显示的默认图片
                .showImageForEmptyUri(R.color.ee)    // 图片Url为空时显示的默认图片
                .showImageOnFail(R.color.ee)        // 图片加载失败时显示的默认图片
                .cacheInMemory(true)        // 缓存到内存
                .cacheOnDisk(true)        // 缓存到磁盘
                .considerExifParams(true)    //考虑exif参数
                //	.displayer(new RoundedBitmapDisplayer(20))	// 显示圆角
                .build();

        File cacheDir = new File(Environment.getExternalStorageDirectory(), "ccImage");
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(applicationContext)
                .threadPriority(Thread.NORM_PRIORITY - 2)        // 线程优先级
                .denyCacheImageMultipleSizesInMemory()        // 禁止在内存中把一张图片缓存多种尺寸
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())    // 缓存文件名的生成器
                .tasksProcessingOrder(QueueProcessingType.LIFO)    // 下载图片的任务的顺序
//                .writeDebugLogs() // Remove for release app	// 写出调试信息，发布apk时注释掉
                .defaultDisplayImageOptions(options)        // 默认显示图片显示选项
                .diskCacheFileCount(5000)
                .diskCacheSize(1000 * 1024 * 1024)
                .diskCache(new UnlimitedDiscCache(cacheDir, null, new Md5FileNameGenerator()))
                .build();
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);                // 初始化ImageLoader
    }
    public static Context getContext(){
        return application;
    }
    public static ShoppingMallTemplateApplication getInstance(){
        if (null==application){
            application=new ShoppingMallTemplateApplication();
        }
        return application;
    }
    public void addActivity(Activity activity){
        activityList.add(activity);
    }
    public void exit() {
        for(Activity activity : activityList) {
            activity.finish();
        }
        System.exit(0);
    }
    public void out() {
        for(Activity activity : activityList) {
            activity.finish();
        }

    }
    //广告ID
    private int strAdId=0;
    public void setAdId(int AdId){
        this.strAdId=AdId;
    }
    public int getAdId(){
        return this.strAdId;
    }
    //广告标题
    private String strAdTtitle="";
    public void setAdTtitle(String AdTtitle){
        this.strAdTtitle=AdTtitle;
    }
    public String getAdTtitle(){
        return this.strAdTtitle;
    }
    //广告图片
    private String strAdImages="";
    public void setAdImages(String AdImages){
        this.strAdImages=AdImages;
    }
    public String getAdImages(){
        return this.strAdImages;
    }
    //广告链接
    private String strAdLink="";
    public void setAdLink(String AdLink){
        this.strAdLink=AdLink;
    }
    public String getAdLink(){
        return this.strAdLink;
    }
    //APP初始化状态
    private int strStatus=0;
    public void setStatus(int Status){
        this.strStatus=Status;
    }
    public int getStatus(){
        return this.strStatus;
    }
    //广告状态
    private int strAdStatus=0;
    public void setAdStatus(int AdStatus){
        this.strAdStatus=AdStatus;
    }
    public int getAdStatus(){
        return this.strAdStatus;
    }
}
