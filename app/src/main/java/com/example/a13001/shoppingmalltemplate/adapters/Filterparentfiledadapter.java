package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.MyGridView;
import com.example.a13001.shoppingmalltemplate.activitys.GoodsListActivity;
import com.example.a13001.shoppingmalltemplate.modle.ContentFilter;
import com.example.a13001.shoppingmalltemplate.modle.FilterGoods;
import com.zhy.autolayout.utils.AutoUtils;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Filterparentfiledadapter extends BaseAdapter {
    private Context mContext;
    private List<ContentFilter.FieldsDataBean> mList;
    private FilterChildadapter mAdapter;

    public Filterparentfiledadapter(Context mContext, List<ContentFilter.FieldsDataBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_right_sideslip_layer, viewGroup, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
            AutoUtils.autoSize(view);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.tvRightslideFiletitle.setText(mList.get(i).getFieldtitle()!=null?mList.get(i).getFieldtitle():"");
        final ViewHolder finalHolder = holder;
        List<ContentFilter.FieldsDataBean.FieldlistBean> fieldlist = mList.get(i).getFieldlist();
        holder.itemSelectGvField.setAdapter(new TagAdapter<ContentFilter.FieldsDataBean.FieldlistBean>(fieldlist) {
            @Override
            public View getView(FlowLayout parent, int position, ContentFilter.FieldsDataBean.FieldlistBean s) {
                TextView tv = (TextView) LayoutInflater.from(mContext).inflate(R.layout.tv_filter,
                        finalHolder.itemSelectGvField, false);
                tv.setText(s.getName());
                return tv;
            }
        });
        return view;
    }



    class ViewHolder {
        @BindView(R.id.tv_rightslide_filetitle)
        TextView tvRightslideFiletitle;
        @BindView(R.id.item_selectGv_field)
        TagFlowLayout itemSelectGvField;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
