package com.example.a13001.shoppingmalltemplate.modle;

public class OrderNum {

    /**
     * status : 1
     * returnMsg : null
     * OrderCount : 1
     * OrderCount1 : 0
     * OrderCount2 : 1
     * OrderCount3 : 0
     * OrderCount4 : 0
     */

    private int status;
    private String returnMsg;
    private int OrderCount;
    private int OrderCount1;
    private int OrderCount2;
    private int OrderCount3;
    private int OrderCount4;

    @Override
    public String toString() {
        return "OrderNum{" +
                "status=" + status +
                ", returnMsg='" + returnMsg + '\'' +
                ", OrderCount=" + OrderCount +
                ", OrderCount1=" + OrderCount1 +
                ", OrderCount2=" + OrderCount2 +
                ", OrderCount3=" + OrderCount3 +
                ", OrderCount4=" + OrderCount4 +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getOrderCount() {
        return OrderCount;
    }

    public void setOrderCount(int OrderCount) {
        this.OrderCount = OrderCount;
    }

    public int getOrderCount1() {
        return OrderCount1;
    }

    public void setOrderCount1(int OrderCount1) {
        this.OrderCount1 = OrderCount1;
    }

    public int getOrderCount2() {
        return OrderCount2;
    }

    public void setOrderCount2(int OrderCount2) {
        this.OrderCount2 = OrderCount2;
    }

    public int getOrderCount3() {
        return OrderCount3;
    }

    public void setOrderCount3(int OrderCount3) {
        this.OrderCount3 = OrderCount3;
    }

    public int getOrderCount4() {
        return OrderCount4;
    }

    public void setOrderCount4(int OrderCount4) {
        this.OrderCount4 = OrderCount4;
    }
}
