package com.example.a13001.shoppingmalltemplate.modle;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

public class CloaseAccount {

    /**
     * status : 1
     * returnMsg : null
     * addressCount : 1
     * address : [{"addresslId":7,"addressName":"子非鱼","addressPhone":"18622470028","addressProvince":"天津市","addressCity":"天津市","addressArea":"河西区","addressXX":"建国楼","addressZipcode":300000,"addressDefault":true}]
     * Count : 2
     * shopSum : 3
     * shopList : [{"CartId":100001,"commodityId":134,"CartImg":"//file.site.ify2.cn/site/153/upload/sccs/upload/201801/2018112107373851.jpg","CommodityName":"添加个新商品","commodityLink":"//www.tjsrsh.site.ify2.cn/channel/content.aspx?id=134&preview=6e8c08bea0328d612569d30dc24ffb4e","CommodityProperty":"规格:10克","CommodityNumber":2,"CartListID":"E24BAB001","CommodityXPrice":100,"CommodityYhPrice":0,"CommodityZPrice":200},{"CartId":100002,"commodityId":134,"CartImg":"//file.site.ify2.cn/site/153/upload/sccs/upload/201801/2018112107373851.jpg","CommodityName":"添加个新商品","commodityLink":"//www.tjsrsh.site.ify2.cn/channel/content.aspx?id=134&preview=6e8c08bea0328d612569d30dc24ffb4e","CommodityProperty":"规格:20克","CommodityNumber":1,"CartListID":"E24BAB002","CommodityXPrice":200,"CommodityYhPrice":0,"CommodityZPrice":200}]
     * PaymentCount : 2
     * PaymentTypeList : [{"PaymentId":28,"PaymentDefault":1,"PaymentName":"余额支付","PaymentExplain":"平台内充值后用平台余额支付订单。","PaymentRange":"PC,手机,微信"},{"PaymentId":29,"PaymentDefault":0,"PaymentName":"货到付款","PaymentExplain":"货物送达后由顾客一次性付款。","PaymentRange":"PC,手机,微信"}]
     * expressCount : 2
     * express : [{"kdname":"快递","KdYfPrice":0},{"kdname":"EMS","KdYfPrice":0},{"kdname":"平邮","KdYfPrice":0}]
     * InvoiceStatus : 1
     * totalPrice : 400
     * totalZkPrice : 0
     * totalZongPrice : 400
     */

    private int status;
    private Object returnMsg;
    private int addressCount;
    private int Count;
    private int shopSum;
    private int PaymentCount;
    private int expressCount;
    private int InvoiceStatus;
    private double totalPrice;
    private double totalZkPrice;
    private double totalZongPrice;
    private List<AddressBean> address;
    private List<ShopListBean> shopList;
    private List<PaymentTypeListBean> PaymentTypeList;
    private List<ExpressBean> express;

    @Override
    public String toString() {
        return "CloaseAccount{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", addressCount=" + addressCount +
                ", Count=" + Count +
                ", shopSum=" + shopSum +
                ", PaymentCount=" + PaymentCount +
                ", expressCount=" + expressCount +
                ", InvoiceStatus=" + InvoiceStatus +
                ", totalPrice=" + totalPrice +
                ", totalZkPrice=" + totalZkPrice +
                ", totalZongPrice=" + totalZongPrice +
                ", address=" + address +
                ", shopList=" + shopList +
                ", PaymentTypeList=" + PaymentTypeList +
                ", express=" + express +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getAddressCount() {
        return addressCount;
    }

    public void setAddressCount(int addressCount) {
        this.addressCount = addressCount;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int Count) {
        this.Count = Count;
    }

    public int getShopSum() {
        return shopSum;
    }

    public void setShopSum(int shopSum) {
        this.shopSum = shopSum;
    }

    public int getPaymentCount() {
        return PaymentCount;
    }

    public void setPaymentCount(int PaymentCount) {
        this.PaymentCount = PaymentCount;
    }

    public int getExpressCount() {
        return expressCount;
    }

    public void setExpressCount(int expressCount) {
        this.expressCount = expressCount;
    }

    public int getInvoiceStatus() {
        return InvoiceStatus;
    }

    public void setInvoiceStatus(int InvoiceStatus) {
        this.InvoiceStatus = InvoiceStatus;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getTotalZkPrice() {
        return totalZkPrice;
    }

    public void setTotalZkPrice(double totalZkPrice) {
        this.totalZkPrice = totalZkPrice;
    }

    public double getTotalZongPrice() {
        return totalZongPrice;
    }

    public void setTotalZongPrice(double totalZongPrice) {
        this.totalZongPrice = totalZongPrice;
    }

    public List<AddressBean> getAddress() {
        return address;
    }

    public void setAddress(List<AddressBean> address) {
        this.address = address;
    }

    public List<ShopListBean> getShopList() {
        return shopList;
    }

    public void setShopList(List<ShopListBean> shopList) {
        this.shopList = shopList;
    }

    public List<PaymentTypeListBean> getPaymentTypeList() {
        return PaymentTypeList;
    }

    public void setPaymentTypeList(List<PaymentTypeListBean> PaymentTypeList) {
        this.PaymentTypeList = PaymentTypeList;
    }

    public List<ExpressBean> getExpress() {
        return express;
    }

    public void setExpress(List<ExpressBean> express) {
        this.express = express;
    }

    public static class AddressBean {
        /**
         * addresslId : 7
         * addressName : 子非鱼
         * addressPhone : 18622470028
         * addressProvince : 天津市
         * addressCity : 天津市
         * addressArea : 河西区
         * addressXX : 建国楼
         * addressZipcode : 300000
         * addressDefault : true
         */

        private int addresslId;
        private String addressName;
        private String addressPhone;
        private String addressProvince;
        private String addressCity;
        private String addressArea;
        private String addressXX;
        private int addressZipcode;
        private boolean addressDefault;

        @Override
        public String toString() {
            return "AddressBean{" +
                    "addresslId=" + addresslId +
                    ", addressName='" + addressName + '\'' +
                    ", addressPhone='" + addressPhone + '\'' +
                    ", addressProvince='" + addressProvince + '\'' +
                    ", addressCity='" + addressCity + '\'' +
                    ", addressArea='" + addressArea + '\'' +
                    ", addressXX='" + addressXX + '\'' +
                    ", addressZipcode=" + addressZipcode +
                    ", addressDefault=" + addressDefault +
                    '}';
        }

        public int getAddresslId() {
            return addresslId;
        }

        public void setAddresslId(int addresslId) {
            this.addresslId = addresslId;
        }

        public String getAddressName() {
            return addressName;
        }

        public void setAddressName(String addressName) {
            this.addressName = addressName;
        }

        public String getAddressPhone() {
            return addressPhone;
        }

        public void setAddressPhone(String addressPhone) {
            this.addressPhone = addressPhone;
        }

        public String getAddressProvince() {
            return addressProvince;
        }

        public void setAddressProvince(String addressProvince) {
            this.addressProvince = addressProvince;
        }

        public String getAddressCity() {
            return addressCity;
        }

        public void setAddressCity(String addressCity) {
            this.addressCity = addressCity;
        }

        public String getAddressArea() {
            return addressArea;
        }

        public void setAddressArea(String addressArea) {
            this.addressArea = addressArea;
        }

        public String getAddressXX() {
            return addressXX;
        }

        public void setAddressXX(String addressXX) {
            this.addressXX = addressXX;
        }

        public int getAddressZipcode() {
            return addressZipcode;
        }

        public void setAddressZipcode(int addressZipcode) {
            this.addressZipcode = addressZipcode;
        }

        public boolean isAddressDefault() {
            return addressDefault;
        }

        public void setAddressDefault(boolean addressDefault) {
            this.addressDefault = addressDefault;
        }
    }

    public static class ShopListBean {
        /**
         * CartId : 100001
         * commodityId : 134
         * CartImg : //file.site.ify2.cn/site/153/upload/sccs/upload/201801/2018112107373851.jpg
         * CommodityName : 添加个新商品
         * commodityLink : //www.tjsrsh.site.ify2.cn/channel/content.aspx?id=134&preview=6e8c08bea0328d612569d30dc24ffb4e
         * CommodityProperty : 规格:10克
         * CommodityNumber : 2
         * CartListID : E24BAB001
         * CommodityXPrice : 100
         * CommodityYhPrice : 0
         * CommodityZPrice : 200
         */

        private int CartId;
        private int commodityId;
        private String CartImg;
        private String CommodityName;
        private String commodityLink;
        private String CommodityProperty;
        private int CommodityNumber;
        private String CartListID;
        private double CommodityXPrice;
        private double CommodityYhPrice;
        private double CommodityZPrice;

        @Override
        public String toString() {
            return "ShopListBean{" +
                    "CartId=" + CartId +
                    ", commodityId=" + commodityId +
                    ", CartImg='" + CartImg + '\'' +
                    ", CommodityName='" + CommodityName + '\'' +
                    ", commodityLink='" + commodityLink + '\'' +
                    ", CommodityProperty='" + CommodityProperty + '\'' +
                    ", CommodityNumber=" + CommodityNumber +
                    ", CartListID='" + CartListID + '\'' +
                    ", CommodityXPrice=" + CommodityXPrice +
                    ", CommodityYhPrice=" + CommodityYhPrice +
                    ", CommodityZPrice=" + CommodityZPrice +
                    '}';
        }

        public int getCartId() {
            return CartId;
        }

        public void setCartId(int CartId) {
            this.CartId = CartId;
        }

        public int getCommodityId() {
            return commodityId;
        }

        public void setCommodityId(int commodityId) {
            this.commodityId = commodityId;
        }

        public String getCartImg() {
            return CartImg;
        }

        public void setCartImg(String CartImg) {
            this.CartImg = CartImg;
        }

        public String getCommodityName() {
            return CommodityName;
        }

        public void setCommodityName(String CommodityName) {
            this.CommodityName = CommodityName;
        }

        public String getCommodityLink() {
            return commodityLink;
        }

        public void setCommodityLink(String commodityLink) {
            this.commodityLink = commodityLink;
        }

        public String getCommodityProperty() {
            return CommodityProperty;
        }

        public void setCommodityProperty(String CommodityProperty) {
            this.CommodityProperty = CommodityProperty;
        }

        public int getCommodityNumber() {
            return CommodityNumber;
        }

        public void setCommodityNumber(int CommodityNumber) {
            this.CommodityNumber = CommodityNumber;
        }

        public String getCartListID() {
            return CartListID;
        }

        public void setCartListID(String CartListID) {
            this.CartListID = CartListID;
        }

        public double getCommodityXPrice() {
            return CommodityXPrice;
        }

        public void setCommodityXPrice(double CommodityXPrice) {
            this.CommodityXPrice = CommodityXPrice;
        }

        public double getCommodityYhPrice() {
            return CommodityYhPrice;
        }

        public void setCommodityYhPrice(double CommodityYhPrice) {
            this.CommodityYhPrice = CommodityYhPrice;
        }

        public double getCommodityZPrice() {
            return CommodityZPrice;
        }

        public void setCommodityZPrice(double CommodityZPrice) {
            this.CommodityZPrice = CommodityZPrice;
        }
    }

    public static class PaymentTypeListBean implements Serializable{
        /**
         * PaymentId : 28
         * PaymentDefault : 1
         * PaymentName : 余额支付
         * PaymentExplain : 平台内充值后用平台余额支付订单。
         * PaymentRange : PC,手机,微信
         */

        private int PaymentId;
        private int PaymentDefault;
        private String PaymentName;
        private String PaymentExplain;
        private String PaymentRange;

        protected PaymentTypeListBean(Parcel in) {
            PaymentId = in.readInt();
            PaymentDefault = in.readInt();
            PaymentName = in.readString();
            PaymentExplain = in.readString();
            PaymentRange = in.readString();
        }

//        public static final Creator<PaymentTypeListBean> CREATOR = new Creator<PaymentTypeListBean>() {
//            @Override
//            public PaymentTypeListBean createFromParcel(Parcel in) {
//                return new PaymentTypeListBean(in);
//            }
//
//            @Override
//            public PaymentTypeListBean[] newArray(int size) {
//                return new PaymentTypeListBean[size];
//            }
//        };

        @Override
        public String toString() {
            return "PaymentTypeListBean{" +
                    "PaymentId=" + PaymentId +
                    ", PaymentDefault=" + PaymentDefault +
                    ", PaymentName='" + PaymentName + '\'' +
                    ", PaymentExplain='" + PaymentExplain + '\'' +
                    ", PaymentRange='" + PaymentRange + '\'' +
                    '}';
        }

        public int getPaymentId() {
            return PaymentId;
        }

        public void setPaymentId(int PaymentId) {
            this.PaymentId = PaymentId;
        }

        public int getPaymentDefault() {
            return PaymentDefault;
        }

        public void setPaymentDefault(int PaymentDefault) {
            this.PaymentDefault = PaymentDefault;
        }

        public String getPaymentName() {
            return PaymentName;
        }

        public void setPaymentName(String PaymentName) {
            this.PaymentName = PaymentName;
        }

        public String getPaymentExplain() {
            return PaymentExplain;
        }

        public void setPaymentExplain(String PaymentExplain) {
            this.PaymentExplain = PaymentExplain;
        }

        public String getPaymentRange() {
            return PaymentRange;
        }

        public void setPaymentRange(String PaymentRange) {
            this.PaymentRange = PaymentRange;
        }

//        @Override
//        public int describeContents() {
//            return 0;
//        }
//
//        @Override
//        public void writeToParcel(Parcel parcel, int i) {
//            parcel.writeInt(PaymentId);
//            parcel.writeInt(PaymentDefault);
//            parcel.writeString(PaymentName);
//            parcel.writeString(PaymentExplain);
//            parcel.writeString(PaymentRange);
//        }
    }

    public static class ExpressBean implements Serializable{
        /**
         * kdname : 快递
         * KdYfPrice : 0
         */

        private String kdname;
        private double KdYfPrice;

        protected ExpressBean(Parcel in) {
            kdname = in.readString();
            KdYfPrice = in.readInt();
        }

//        public static final Creator<ExpressBean> CREATOR = new Creator<ExpressBean>() {
//            @Override
//            public ExpressBean createFromParcel(Parcel in) {
//                return new ExpressBean(in);
//            }
//
//            @Override
//            public ExpressBean[] newArray(int size) {
//                return new ExpressBean[size];
//            }
//        };

        @Override
        public String toString() {
            return "ExpressBean{" +
                    "kdname='" + kdname + '\'' +
                    ", KdYfPrice=" + KdYfPrice +
                    '}';
        }

        public String getKdname() {
            return kdname;
        }

        public void setKdname(String kdname) {
            this.kdname = kdname;
        }

        public double getKdYfPrice() {
            return KdYfPrice;
        }

        public void setKdYfPrice(double KdYfPrice) {
            this.KdYfPrice = KdYfPrice;
        }

//        @Override
//        public int describeContents() {
//            return 0;
//        }
//
//        @Override
//        public void writeToParcel(Parcel parcel, int i) {
//            parcel.writeString(kdname);
//            parcel.writeInt(KdYfPrice);
//        }
    }
}
