package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class Classify {

    /**
     * status : 1
     * returnMsg : null
     * channelid : 105
     * channelName : 商品展示
     * ModuleType : 1
     * channelOrder : 0
     * count : 7
     * list : [{"ParentID":0,"classid":10000,"className":"水貂衣","classEnName":"","classDir":"mwdnzt","classCount":2,"conCount":102,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"//cdn.qkk.cn/site/536/upload/spzs/upload/201809/2018918102934771.jpg","mobileclassImages":"//cdn.qkk.cn/site/536/upload/spzs/upload/201809/2018918102934771.jpg","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":2,"list":[{"ParentID":10000,"classid":10029,"className":"男款水貂衣","classEnName":"","classDir":"nksdy","classCount":3,"conCount":20,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":3,"list":[{"ParentID":10029,"classid":10030,"className":"高端时尚","classEnName":"","classDir":"gdss","classCount":0,"conCount":4,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10029,"classid":10031,"className":"大众畅销","classEnName":"","classDir":"dzcx","classCount":0,"conCount":8,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10029,"classid":10032,"className":"个性创意","classEnName":"","classDir":"gxcy","classCount":0,"conCount":8,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null}]},{"ParentID":10000,"classid":10035,"className":"女款水貂衣","classEnName":"","classDir":"nvksdy","classCount":3,"conCount":79,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":3,"list":[{"ParentID":10035,"classid":10042,"className":"高端时尚","classEnName":"","classDir":"gdss1","classCount":0,"conCount":30,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10035,"classid":10043,"className":"大众畅销","classEnName":"","classDir":"dzcx1","classCount":0,"conCount":49,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10035,"classid":10044,"className":"个性创意","classEnName":"","classDir":"gxcy1","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null}]}]},{"ParentID":0,"classid":10001,"className":"单皮衣","classEnName":"","classDir":"hcdbg","classCount":2,"conCount":18,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"//cdn.qkk.cn/site/536/upload/spzs/upload/201809/201891810522571.jpg","mobileclassImages":"//cdn.qkk.cn/site/536/upload/spzs/upload/201809/201891810522571.jpg","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":2,"list":[{"ParentID":10001,"classid":10033,"className":"男单皮","classEnName":"","classDir":"nsp","classCount":3,"conCount":17,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":3,"list":[{"ParentID":10033,"classid":10045,"className":"高端时尚","classEnName":"","classDir":"gdss2","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10033,"classid":10046,"className":"大众畅销","classEnName":"","classDir":"dzcx2","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10033,"classid":10047,"className":"个性创意","classEnName":"","classDir":"gxcy2","classCount":0,"conCount":17,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null}]},{"ParentID":10001,"classid":10034,"className":"女单皮","classEnName":"","classDir":"nsp1","classCount":3,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":3,"list":[{"ParentID":10034,"classid":10048,"className":"高端时尚","classEnName":"","classDir":"gdss3","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10034,"classid":10049,"className":"大众畅销","classEnName":"","classDir":"dzcx3","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10034,"classid":10050,"className":"个性创意","classEnName":"","classDir":"gxcy3","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null}]}]},{"ParentID":0,"classid":10002,"className":"皮毛类","classEnName":"","classDir":"jphbdx","classCount":2,"conCount":10,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":2,"list":[{"ParentID":10002,"classid":10036,"className":"男皮毛","classEnName":"","classDir":"npm","classCount":3,"conCount":5,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":3,"list":[{"ParentID":10036,"classid":10051,"className":"高端时尚","classEnName":"","classDir":"gdss4","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10036,"classid":10052,"className":"大众畅销","classEnName":"","classDir":"dzcx4","classCount":0,"conCount":5,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10036,"classid":10053,"className":"个性创意","classEnName":"","classDir":"gxcy4","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null}]},{"ParentID":10002,"classid":10037,"className":"女皮毛","classEnName":"","classDir":"nvpm","classCount":3,"conCount":4,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":3,"list":[{"ParentID":10037,"classid":10054,"className":"高端时尚","classEnName":"","classDir":"gdss5","classCount":0,"conCount":4,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10037,"classid":10055,"className":"大众畅销","classEnName":"","classDir":"dzcx5","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10037,"classid":10056,"className":"个性创意","classEnName":"","classDir":"gxcy5","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null}]}]},{"ParentID":0,"classid":10003,"className":"皮羽绒","classEnName":"","classDir":"kwxfc","classCount":2,"conCount":1,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":2,"list":[{"ParentID":10003,"classid":10038,"className":"男皮羽绒","classEnName":"","classDir":"npyr","classCount":6,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":6,"list":[{"ParentID":10038,"classid":10057,"className":"高端时尚","classEnName":"","classDir":"gdss6","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10038,"classid":10058,"className":"大众畅销","classEnName":"","classDir":"dzcx6","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10038,"classid":10059,"className":"个性创意","classEnName":"","classDir":"gxcy6","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10038,"classid":10060,"className":"高端时尚","classEnName":"","classDir":"gdss7","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10038,"classid":10061,"className":"大众畅销","classEnName":"","classDir":"dzcx7","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10038,"classid":10062,"className":"个性创意","classEnName":"","classDir":"gxcy7","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null}]},{"ParentID":10003,"classid":10039,"className":"女皮羽绒","classEnName":"","classDir":"nvpyr","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null}]},{"ParentID":0,"classid":10004,"className":"派克服","classEnName":"","classDir":"sgdzyb","classCount":2,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":2,"list":[{"ParentID":10004,"classid":10040,"className":"男派克服","classEnName":"","classDir":"npkf","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10004,"classid":10041,"className":"女派克服","classEnName":"","classDir":"nvpkf","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null}]},{"ParentID":0,"classid":10026,"className":"私人定制","classEnName":"","classDir":"srdz","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":0,"classid":10027,"className":"潮牌系列","classEnName":"","classDir":"cpxl","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null}]
     */

    private int status;
    private String returnMsg;
    private int channelid;
    private String channelName;
    private int ModuleType;
    private int channelOrder;
    private int count;
    private List<ListBeanXX> list;

    @Override
    public String toString() {
        return "Classify{" +
                "status=" + status +
                ", returnMsg='" + returnMsg + '\'' +
                ", channelid=" + channelid +
                ", channelName='" + channelName + '\'' +
                ", ModuleType=" + ModuleType +
                ", channelOrder=" + channelOrder +
                ", count=" + count +
                ", list=" + list +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getChannelid() {
        return channelid;
    }

    public void setChannelid(int channelid) {
        this.channelid = channelid;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public int getModuleType() {
        return ModuleType;
    }

    public void setModuleType(int ModuleType) {
        this.ModuleType = ModuleType;
    }

    public int getChannelOrder() {
        return channelOrder;
    }

    public void setChannelOrder(int channelOrder) {
        this.channelOrder = channelOrder;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<ListBeanXX> getList() {
        return list;
    }

    public void setList(List<ListBeanXX> list) {
        this.list = list;
    }

    public static class ListBeanXX {
        /**
         * ParentID : 0
         * classid : 10000
         * className : 水貂衣
         * classEnName :
         * classDir : mwdnzt
         * classCount : 2
         * conCount : 102
         * classAttributes : 0
         * classIcon :
         * mobileclassIcon :
         * classImages : //cdn.qkk.cn/site/536/upload/spzs/upload/201809/2018918102934771.jpg
         * mobileclassImages : //cdn.qkk.cn/site/536/upload/spzs/upload/201809/2018918102934771.jpg
         * classImagesAll :
         * mobileclassImagesAll :
         * classContent :
         * classRecommend : 0
         * commentStatus : 1
         * Meta_title :
         * Meta_Keywords :
         * Meta_Description :
         * count : 2
         * list : [{"ParentID":10000,"classid":10029,"className":"男款水貂衣","classEnName":"","classDir":"nksdy","classCount":3,"conCount":20,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":3,"list":[{"ParentID":10029,"classid":10030,"className":"高端时尚","classEnName":"","classDir":"gdss","classCount":0,"conCount":4,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10029,"classid":10031,"className":"大众畅销","classEnName":"","classDir":"dzcx","classCount":0,"conCount":8,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10029,"classid":10032,"className":"个性创意","classEnName":"","classDir":"gxcy","classCount":0,"conCount":8,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null}]},{"ParentID":10000,"classid":10035,"className":"女款水貂衣","classEnName":"","classDir":"nvksdy","classCount":3,"conCount":79,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":3,"list":[{"ParentID":10035,"classid":10042,"className":"高端时尚","classEnName":"","classDir":"gdss1","classCount":0,"conCount":30,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10035,"classid":10043,"className":"大众畅销","classEnName":"","classDir":"dzcx1","classCount":0,"conCount":49,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10035,"classid":10044,"className":"个性创意","classEnName":"","classDir":"gxcy1","classCount":0,"conCount":0,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null}]}]
         */

        private int ParentID;
        private int classid;
        private String className;
        private String classEnName;
        private String classDir;
        private int classCount;
        private int conCount;
        private int classAttributes;
        private String classIcon;
        private String mobileclassIcon;
        private String classImages;
        private String mobileclassImages;
        private String classImagesAll;
        private String mobileclassImagesAll;
        private String classContent;
        private int classRecommend;
        private int commentStatus;
        private String Meta_title;
        private String Meta_Keywords;
        private String Meta_Description;
        private int count;
        private List<ListBeanX> list;

        @Override
        public String toString() {
            return "ListBeanXX{" +
                    "ParentID=" + ParentID +
                    ", classid=" + classid +
                    ", className='" + className + '\'' +
                    ", classEnName='" + classEnName + '\'' +
                    ", classDir='" + classDir + '\'' +
                    ", classCount=" + classCount +
                    ", conCount=" + conCount +
                    ", classAttributes=" + classAttributes +
                    ", classIcon='" + classIcon + '\'' +
                    ", mobileclassIcon='" + mobileclassIcon + '\'' +
                    ", classImages='" + classImages + '\'' +
                    ", mobileclassImages='" + mobileclassImages + '\'' +
                    ", classImagesAll='" + classImagesAll + '\'' +
                    ", mobileclassImagesAll='" + mobileclassImagesAll + '\'' +
                    ", classContent='" + classContent + '\'' +
                    ", classRecommend=" + classRecommend +
                    ", commentStatus=" + commentStatus +
                    ", Meta_title='" + Meta_title + '\'' +
                    ", Meta_Keywords='" + Meta_Keywords + '\'' +
                    ", Meta_Description='" + Meta_Description + '\'' +
                    ", count=" + count +
                    ", list=" + list +
                    '}';
        }

        public int getParentID() {
            return ParentID;
        }

        public void setParentID(int ParentID) {
            this.ParentID = ParentID;
        }

        public int getClassid() {
            return classid;
        }

        public void setClassid(int classid) {
            this.classid = classid;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getClassEnName() {
            return classEnName;
        }

        public void setClassEnName(String classEnName) {
            this.classEnName = classEnName;
        }

        public String getClassDir() {
            return classDir;
        }

        public void setClassDir(String classDir) {
            this.classDir = classDir;
        }

        public int getClassCount() {
            return classCount;
        }

        public void setClassCount(int classCount) {
            this.classCount = classCount;
        }

        public int getConCount() {
            return conCount;
        }

        public void setConCount(int conCount) {
            this.conCount = conCount;
        }

        public int getClassAttributes() {
            return classAttributes;
        }

        public void setClassAttributes(int classAttributes) {
            this.classAttributes = classAttributes;
        }

        public String getClassIcon() {
            return classIcon;
        }

        public void setClassIcon(String classIcon) {
            this.classIcon = classIcon;
        }

        public String getMobileclassIcon() {
            return mobileclassIcon;
        }

        public void setMobileclassIcon(String mobileclassIcon) {
            this.mobileclassIcon = mobileclassIcon;
        }

        public String getClassImages() {
            return classImages;
        }

        public void setClassImages(String classImages) {
            this.classImages = classImages;
        }

        public String getMobileclassImages() {
            return mobileclassImages;
        }

        public void setMobileclassImages(String mobileclassImages) {
            this.mobileclassImages = mobileclassImages;
        }

        public String getClassImagesAll() {
            return classImagesAll;
        }

        public void setClassImagesAll(String classImagesAll) {
            this.classImagesAll = classImagesAll;
        }

        public String getMobileclassImagesAll() {
            return mobileclassImagesAll;
        }

        public void setMobileclassImagesAll(String mobileclassImagesAll) {
            this.mobileclassImagesAll = mobileclassImagesAll;
        }

        public String getClassContent() {
            return classContent;
        }

        public void setClassContent(String classContent) {
            this.classContent = classContent;
        }

        public int getClassRecommend() {
            return classRecommend;
        }

        public void setClassRecommend(int classRecommend) {
            this.classRecommend = classRecommend;
        }

        public int getCommentStatus() {
            return commentStatus;
        }

        public void setCommentStatus(int commentStatus) {
            this.commentStatus = commentStatus;
        }

        public String getMeta_title() {
            return Meta_title;
        }

        public void setMeta_title(String Meta_title) {
            this.Meta_title = Meta_title;
        }

        public String getMeta_Keywords() {
            return Meta_Keywords;
        }

        public void setMeta_Keywords(String Meta_Keywords) {
            this.Meta_Keywords = Meta_Keywords;
        }

        public String getMeta_Description() {
            return Meta_Description;
        }

        public void setMeta_Description(String Meta_Description) {
            this.Meta_Description = Meta_Description;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public List<ListBeanX> getList() {
            return list;
        }

        public void setList(List<ListBeanX> list) {
            this.list = list;
        }

        public static class ListBeanX {
            /**
             * ParentID : 10000
             * classid : 10029
             * className : 男款水貂衣
             * classEnName :
             * classDir : nksdy
             * classCount : 3
             * conCount : 20
             * classAttributes : 0
             * classIcon :
             * mobileclassIcon :
             * classImages :
             * mobileclassImages :
             * classImagesAll :
             * mobileclassImagesAll :
             * classContent :
             * classRecommend : 0
             * commentStatus : 1
             * Meta_title :
             * Meta_Keywords :
             * Meta_Description :
             * count : 3
             * list : [{"ParentID":10029,"classid":10030,"className":"高端时尚","classEnName":"","classDir":"gdss","classCount":0,"conCount":4,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10029,"classid":10031,"className":"大众畅销","classEnName":"","classDir":"dzcx","classCount":0,"conCount":8,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null},{"ParentID":10029,"classid":10032,"className":"个性创意","classEnName":"","classDir":"gxcy","classCount":0,"conCount":8,"classAttributes":0,"classIcon":"","mobileclassIcon":"","classImages":"","mobileclassImages":"","classImagesAll":"","mobileclassImagesAll":"","classContent":"","classRecommend":0,"commentStatus":1,"Meta_title":"","Meta_Keywords":"","Meta_Description":"","count":0,"list":null}]
             */

            private int ParentID;
            private int classid;
            private String className;
            private String classEnName;
            private String classDir;
            private int classCount;
            private int conCount;
            private int classAttributes;
            private String classIcon;
            private String mobileclassIcon;
            private String classImages;
            private String mobileclassImages;
            private String classImagesAll;
            private String mobileclassImagesAll;
            private String classContent;
            private int classRecommend;
            private int commentStatus;
            private String Meta_title;
            private String Meta_Keywords;
            private String Meta_Description;
            private int count;
            private List<ListBean> list;

            @Override
            public String toString() {
                return "ListBeanX{" +
                        "ParentID=" + ParentID +
                        ", classid=" + classid +
                        ", className='" + className + '\'' +
                        ", classEnName='" + classEnName + '\'' +
                        ", classDir='" + classDir + '\'' +
                        ", classCount=" + classCount +
                        ", conCount=" + conCount +
                        ", classAttributes=" + classAttributes +
                        ", classIcon='" + classIcon + '\'' +
                        ", mobileclassIcon='" + mobileclassIcon + '\'' +
                        ", classImages='" + classImages + '\'' +
                        ", mobileclassImages='" + mobileclassImages + '\'' +
                        ", classImagesAll='" + classImagesAll + '\'' +
                        ", mobileclassImagesAll='" + mobileclassImagesAll + '\'' +
                        ", classContent='" + classContent + '\'' +
                        ", classRecommend=" + classRecommend +
                        ", commentStatus=" + commentStatus +
                        ", Meta_title='" + Meta_title + '\'' +
                        ", Meta_Keywords='" + Meta_Keywords + '\'' +
                        ", Meta_Description='" + Meta_Description + '\'' +
                        ", count=" + count +
                        ", list=" + list +
                        '}';
            }

            public int getParentID() {
                return ParentID;
            }

            public void setParentID(int ParentID) {
                this.ParentID = ParentID;
            }

            public int getClassid() {
                return classid;
            }

            public void setClassid(int classid) {
                this.classid = classid;
            }

            public String getClassName() {
                return className;
            }

            public void setClassName(String className) {
                this.className = className;
            }

            public String getClassEnName() {
                return classEnName;
            }

            public void setClassEnName(String classEnName) {
                this.classEnName = classEnName;
            }

            public String getClassDir() {
                return classDir;
            }

            public void setClassDir(String classDir) {
                this.classDir = classDir;
            }

            public int getClassCount() {
                return classCount;
            }

            public void setClassCount(int classCount) {
                this.classCount = classCount;
            }

            public int getConCount() {
                return conCount;
            }

            public void setConCount(int conCount) {
                this.conCount = conCount;
            }

            public int getClassAttributes() {
                return classAttributes;
            }

            public void setClassAttributes(int classAttributes) {
                this.classAttributes = classAttributes;
            }

            public String getClassIcon() {
                return classIcon;
            }

            public void setClassIcon(String classIcon) {
                this.classIcon = classIcon;
            }

            public String getMobileclassIcon() {
                return mobileclassIcon;
            }

            public void setMobileclassIcon(String mobileclassIcon) {
                this.mobileclassIcon = mobileclassIcon;
            }

            public String getClassImages() {
                return classImages;
            }

            public void setClassImages(String classImages) {
                this.classImages = classImages;
            }

            public String getMobileclassImages() {
                return mobileclassImages;
            }

            public void setMobileclassImages(String mobileclassImages) {
                this.mobileclassImages = mobileclassImages;
            }

            public String getClassImagesAll() {
                return classImagesAll;
            }

            public void setClassImagesAll(String classImagesAll) {
                this.classImagesAll = classImagesAll;
            }

            public String getMobileclassImagesAll() {
                return mobileclassImagesAll;
            }

            public void setMobileclassImagesAll(String mobileclassImagesAll) {
                this.mobileclassImagesAll = mobileclassImagesAll;
            }

            public String getClassContent() {
                return classContent;
            }

            public void setClassContent(String classContent) {
                this.classContent = classContent;
            }

            public int getClassRecommend() {
                return classRecommend;
            }

            public void setClassRecommend(int classRecommend) {
                this.classRecommend = classRecommend;
            }

            public int getCommentStatus() {
                return commentStatus;
            }

            public void setCommentStatus(int commentStatus) {
                this.commentStatus = commentStatus;
            }

            public String getMeta_title() {
                return Meta_title;
            }

            public void setMeta_title(String Meta_title) {
                this.Meta_title = Meta_title;
            }

            public String getMeta_Keywords() {
                return Meta_Keywords;
            }

            public void setMeta_Keywords(String Meta_Keywords) {
                this.Meta_Keywords = Meta_Keywords;
            }

            public String getMeta_Description() {
                return Meta_Description;
            }

            public void setMeta_Description(String Meta_Description) {
                this.Meta_Description = Meta_Description;
            }

            public int getCount() {
                return count;
            }

            public void setCount(int count) {
                this.count = count;
            }

            public List<ListBean> getList() {
                return list;
            }

            public void setList(List<ListBean> list) {
                this.list = list;
            }

            public static class ListBean {
                /**
                 * ParentID : 10029
                 * classid : 10030
                 * className : 高端时尚
                 * classEnName :
                 * classDir : gdss
                 * classCount : 0
                 * conCount : 4
                 * classAttributes : 0
                 * classIcon :
                 * mobileclassIcon :
                 * classImages :
                 * mobileclassImages :
                 * classImagesAll :
                 * mobileclassImagesAll :
                 * classContent :
                 * classRecommend : 0
                 * commentStatus : 1
                 * Meta_title :
                 * Meta_Keywords :
                 * Meta_Description :
                 * count : 0
                 * list : null
                 */

                private int ParentID;
                private int classid;
                private String className;
                private String classEnName;
                private String classDir;
                private int classCount;
                private int conCount;
                private int classAttributes;
                private String classIcon;
                private String mobileclassIcon;
                private String classImages;
                private String mobileclassImages;
                private String classImagesAll;
                private String mobileclassImagesAll;
                private String classContent;
                private int classRecommend;
                private int commentStatus;
                private String Meta_title;
                private String Meta_Keywords;
                private String Meta_Description;
                private int count;
                private Object list;

                @Override
                public String toString() {
                    return "ListBean{" +
                            "ParentID=" + ParentID +
                            ", classid=" + classid +
                            ", className='" + className + '\'' +
                            ", classEnName='" + classEnName + '\'' +
                            ", classDir='" + classDir + '\'' +
                            ", classCount=" + classCount +
                            ", conCount=" + conCount +
                            ", classAttributes=" + classAttributes +
                            ", classIcon='" + classIcon + '\'' +
                            ", mobileclassIcon='" + mobileclassIcon + '\'' +
                            ", classImages='" + classImages + '\'' +
                            ", mobileclassImages='" + mobileclassImages + '\'' +
                            ", classImagesAll='" + classImagesAll + '\'' +
                            ", mobileclassImagesAll='" + mobileclassImagesAll + '\'' +
                            ", classContent='" + classContent + '\'' +
                            ", classRecommend=" + classRecommend +
                            ", commentStatus=" + commentStatus +
                            ", Meta_title='" + Meta_title + '\'' +
                            ", Meta_Keywords='" + Meta_Keywords + '\'' +
                            ", Meta_Description='" + Meta_Description + '\'' +
                            ", count=" + count +
                            ", list=" + list +
                            '}';
                }

                public int getParentID() {
                    return ParentID;
                }

                public void setParentID(int ParentID) {
                    this.ParentID = ParentID;
                }

                public int getClassid() {
                    return classid;
                }

                public void setClassid(int classid) {
                    this.classid = classid;
                }

                public String getClassName() {
                    return className;
                }

                public void setClassName(String className) {
                    this.className = className;
                }

                public String getClassEnName() {
                    return classEnName;
                }

                public void setClassEnName(String classEnName) {
                    this.classEnName = classEnName;
                }

                public String getClassDir() {
                    return classDir;
                }

                public void setClassDir(String classDir) {
                    this.classDir = classDir;
                }

                public int getClassCount() {
                    return classCount;
                }

                public void setClassCount(int classCount) {
                    this.classCount = classCount;
                }

                public int getConCount() {
                    return conCount;
                }

                public void setConCount(int conCount) {
                    this.conCount = conCount;
                }

                public int getClassAttributes() {
                    return classAttributes;
                }

                public void setClassAttributes(int classAttributes) {
                    this.classAttributes = classAttributes;
                }

                public String getClassIcon() {
                    return classIcon;
                }

                public void setClassIcon(String classIcon) {
                    this.classIcon = classIcon;
                }

                public String getMobileclassIcon() {
                    return mobileclassIcon;
                }

                public void setMobileclassIcon(String mobileclassIcon) {
                    this.mobileclassIcon = mobileclassIcon;
                }

                public String getClassImages() {
                    return classImages;
                }

                public void setClassImages(String classImages) {
                    this.classImages = classImages;
                }

                public String getMobileclassImages() {
                    return mobileclassImages;
                }

                public void setMobileclassImages(String mobileclassImages) {
                    this.mobileclassImages = mobileclassImages;
                }

                public String getClassImagesAll() {
                    return classImagesAll;
                }

                public void setClassImagesAll(String classImagesAll) {
                    this.classImagesAll = classImagesAll;
                }

                public String getMobileclassImagesAll() {
                    return mobileclassImagesAll;
                }

                public void setMobileclassImagesAll(String mobileclassImagesAll) {
                    this.mobileclassImagesAll = mobileclassImagesAll;
                }

                public String getClassContent() {
                    return classContent;
                }

                public void setClassContent(String classContent) {
                    this.classContent = classContent;
                }

                public int getClassRecommend() {
                    return classRecommend;
                }

                public void setClassRecommend(int classRecommend) {
                    this.classRecommend = classRecommend;
                }

                public int getCommentStatus() {
                    return commentStatus;
                }

                public void setCommentStatus(int commentStatus) {
                    this.commentStatus = commentStatus;
                }

                public String getMeta_title() {
                    return Meta_title;
                }

                public void setMeta_title(String Meta_title) {
                    this.Meta_title = Meta_title;
                }

                public String getMeta_Keywords() {
                    return Meta_Keywords;
                }

                public void setMeta_Keywords(String Meta_Keywords) {
                    this.Meta_Keywords = Meta_Keywords;
                }

                public String getMeta_Description() {
                    return Meta_Description;
                }

                public void setMeta_Description(String Meta_Description) {
                    this.Meta_Description = Meta_Description;
                }

                public int getCount() {
                    return count;
                }

                public void setCount(int count) {
                    this.count = count;
                }

                public Object getList() {
                    return list;
                }

                public void setList(Object list) {
                    this.list = list;
                }
            }
        }
    }
}
