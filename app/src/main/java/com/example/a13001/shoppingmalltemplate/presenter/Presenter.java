package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Intent;
import com.example.a13001.shoppingmalltemplate.mvpview.View ;

public interface Presenter {
    void onCreate();
    void onStart();
    void onStop();
    void pause();
    void attachView(View view);
    void attachIncomingIntent(Intent intent);
}
