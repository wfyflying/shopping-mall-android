package com.example.a13001.shoppingmalltemplate.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.utils.KeyboardUtils;
import com.example.a13001.shoppingmalltemplate.ItemDecoration.SpaceItemDecoration;
import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.MyListView;
import com.example.a13001.shoppingmalltemplate.View.RightSideslipLay;
import com.example.a13001.shoppingmalltemplate.adapters.Filterparentfiledadapter;
import com.example.a13001.shoppingmalltemplate.adapters.GoodsListRvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.base.ContentFilterConstants;
import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.ContentFilter;
import com.example.a13001.shoppingmalltemplate.modle.GoodsList;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.google.gson.Gson;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class GoodsListActivity extends BaseActivity {

    @BindView(R.id.tv_goodslist_synthesize)
    TextView tvGoodslistSynthesize;
    @BindView(R.id.iv_goodslist_synthesize)
    ImageView ivGoodslistSynthesize;
    @BindView(R.id.ll_goodslist_synthesize)
    LinearLayout llGoodslistSynthesize;
    @BindView(R.id.tv_goodslist_salesvolume)
    TextView tvGoodslistSalesvolume;
    @BindView(R.id.iv_goodslist_salesvolume)
    ImageView ivGoodslistSalesvolume;
    @BindView(R.id.ll_goodslist_salesvolume)
    LinearLayout llGoodslistSalesvolume;
    @BindView(R.id.tv_goodslist_price)
    TextView tvGoodslistPrice;
    @BindView(R.id.iv_goodslist_price)
    ImageView ivGoodslistPrice;
    @BindView(R.id.ll_goodslist_price)
    LinearLayout llGoodslistPrice;
    @BindView(R.id.tv_goodslist_filter)
    TextView tvGoodslistFilter;
    @BindView(R.id.iv_goodslist_filter)
    ImageView ivGoodslistFilter;
    @BindView(R.id.ll_goodslist_filter)
    LinearLayout llGoodslistFilter;
    @BindView(R.id.rv_goodlist)
    RecyclerView rvGoodlist;
    @BindView(R.id.nav_view)
    LinearLayout navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.select_brand_back_im)
    ImageView selectBrandBackIm;
    @BindView(R.id.refresh_goodlist)
    SmartRefreshLayout refreshGoodlist;
    @BindView(R.id.item_selectGv_class)
    TagFlowLayout itemSelectGvClass;
    @BindView(R.id.item_selectGv_special)
    TagFlowLayout itemSelectGvSpecial;
    @BindView(R.id.tv_filter_class)
    TextView tvFilterClass;
    @BindView(R.id.tv_filter_special)
    TextView tvFilterSpecial;
    @BindView(R.id.tv_filter_price)
    TextView tvFilterPrice;
    @BindView(R.id.item_selectGv_price)
    TagFlowLayout itemSelectGvPrice;
    @BindView(R.id.tv_filter_attribute)
    TextView tvFilterAttribute;
    @BindView(R.id.item_selectGv_attribute)
    TagFlowLayout itemSelectGvAttribute;
    @BindView(R.id.mlv_rightslide)
    MyListView mlvRightslide;
    @BindView(R.id.fram_reset_but)
    Button framResetBut;
    @BindView(R.id.fram_ok_but)
    Button framOkBut;
    @BindView(R.id.editText)
    EditText editText;
    @BindView(R.id.iv_goodslist_searchcloase)
    ImageView ivGoodslistSearchcloase;
    @BindView(R.id.topbar_lay)
    RelativeLayout topbarLay;
    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;
    @BindView(R.id.select_frame_lay)
    LinearLayout selectFrameLay;
    private RightSideslipLay menuHeaderView;
    private List<GoodsList.ListBean> mList;//商品列表集合
    private GoodsListRvAdapter mAdapter;//商品列表适配器
    private GridLayoutManager gridLayoutManager;
    private PopupWindow popWnd;
    private CompositeSubscription mCompositeSubscription;
    private DataManager manager;
    private int pageIndex = 1;
    /**
     * 右面滑出筛选相关
     *
     * @param savedInstanceState
     */

    private List<ContentFilter.ClassDataBean> mListClass;
    private List<ContentFilter.SpecialDataBean> mListSpecial;
    private List<ContentFilter.PriceDataBean> mListPrice;
    private List<ContentFilter.AttributeDataBean> mListAttribute;


    private String classId;
    private static final String TAG = "GoodsListActivity";
    private String elite = "";//推荐调用方法 1 调用有推荐 2 调用不推荐，为空调用所有
    private String hot = "";//热门调用方法 1 调用热门 2 调用不热门，为空调用所有
    private String xinpin = "";//新品调用方法 1 调用新品 2 调用非新品，为空调用所有
    private String cuxiao = "";//促销调用方法 1 调用促销 2 调用非促销，为空调用所有
    private String specialid = "";//专题ID，指定多个专题用“|”线分割
    private String price1 = "";//最低价格，用于价格区间查询，需要配合price2
    private String price2 = "";//最高价格，用于价格区间查询，需要配合price1
    private String keyword = "";//关键词搜索，模糊搜索
    private String excludeid = "";//不加载的ID，用“|”线分割

    private String order = "";
    private ContentFilter mContentFilter;
    private boolean flagXiaoLiang = true;
    private boolean flagPrice = true;
    //右滑筛选相关
    private Filterparentfiledadapter mAdapterFiled;
    private List<ContentFilter.FieldsDataBean> mListFiled;
    private String mBaseUrl;
    private String url = "";
    private String url1 = "";
    private int aaa;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods_list);
        ButterKnife.bind(this);
        if (getIntent() != null) {
            aaa = getIntent().getIntExtra("id", 0);
            classId = String.valueOf(aaa);
            elite = getIntent().getStringExtra("elite");
            xinpin = getIntent().getStringExtra("xinpin");
            keyword = getIntent().getStringExtra("searchword");
            editText.setText(keyword);
        }
        initView();
        initData();
    }

    private void getGoodList() {
        Log.e(TAG, "getGoodList: " + classId);
        mBaseUrl = MyUtils.getMetaValue(GoodsListActivity.this, "companyURL");
        Log.e(TAG, "getGoodList: " + mBaseUrl + "/api/json/content/content.ashx?action=list&companyid=" + AppConstants.COMPANY_ID
                + "&channelid=" + AppConstants.CLASSIFY_ID + "&classid=" + classId + "&order=" + order + "&specialid=" + specialid + "&keyword=" + keyword + "&excludeid=" + excludeid + url + url1);
        mBaseUrl = MyUtils.getMetaValue(GoodsListActivity.this, "companyURL");
        OkHttpUtils.get().url(mBaseUrl + "/api/json/content/content.ashx?action=list&companyid=" + AppConstants.COMPANY_ID
                + "&channelid=" + AppConstants.CLASSIFY_ID + "&classid=" + classId + "&order=" + order + "&specialid="
                + specialid + "&keyword=" + keyword + "&excludeid=" + excludeid + url+url1+"&pagesize="+AppConstants.PAGE_SIZE+"&pageindex="+pageIndex).build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {

            }

            @Override
            public void onResponse(String response, int id) {
                Log.e(TAG, "onResponse: aaa" + response.toString());
                Gson gson = new Gson();
                GoodsList goodsList = gson.fromJson(response, GoodsList.class);
                if (1 == goodsList.getStatus()) {
                    mList.addAll(goodsList.getList());
                    mAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(GoodsListActivity.this, "" + goodsList.getReturnMsg(), Toast.LENGTH_SHORT).show();
                }
            }
        });
//        mCompositeSubscription.add(manager.getGoodsList(AppConstants.COMPANY_ID, AppConstants.CLASSIFY_ID, classId, elite,hot
//                , xinpin,cuxiao, order,specialid,price1,price2,keyword,excludeid, AppConstants.PAGE_SIZE, pageIndex)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Observer<GoodsList>() {
//                    @Override
//                    public void onCompleted() {
//                        Log.e(TAG, "onCompleted: ");
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Log.e(TAG, "onError: " + e.toString());
//                    }
//
//                    @Override
//                    public void onNext(GoodsList goodsList) {
//                        Log.e(TAG, "onNext: " + goodsList.toString());
//                        if (1 == goodsList.getStatus()) {
//                            mList.addAll(goodsList.getList());
//                            mAdapter.notifyDataSetChanged();
//                        } else {
//                            Toast.makeText(GoodsListActivity.this, "" + goodsList.getReturnMsg(), Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                }));
    }

    private void getAttributeList() {
        Log.e(TAG, "getGoodList: " + classId);
        mCompositeSubscription.add(manager.getFilterList(AppConstants.COMPANY_ID, AppConstants.CLASSIFY_ID, ContentFilterConstants.orderstatus, ContentFilterConstants.orderlist
                , ContentFilterConstants.classstatus, ContentFilterConstants.parentclassid, ContentFilterConstants.classtitle, ContentFilterConstants.specialstatus, ContentFilterConstants.specialtitle,
                ContentFilterConstants.fieldstatus, ContentFilterConstants.pricestatus, ContentFilterConstants.pricetitle, ContentFilterConstants.pricelist,
                ContentFilterConstants.attributestatus, ContentFilterConstants.attributetitle, ContentFilterConstants.attributelist)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ContentFilter>() {
                    @Override
                    public void onCompleted() {
                        Log.e(TAG, "onCompleted: " + mContentFilter.toString());
                        if (1 == mContentFilter.getStatus()) {
                            if (mContentFilter.getClassData() != null) {
                                tvFilterClass.setVisibility(View.VISIBLE);
                                itemSelectGvClass.setVisibility(View.VISIBLE);
                                mListClass.addAll(mContentFilter.getClassData());
                                itemSelectGvClass.setAdapter(new TagAdapter<ContentFilter.ClassDataBean>(mListClass) {
                                    @Override
                                    public View getView(FlowLayout parent, int position, ContentFilter.ClassDataBean s) {
                                        TextView tv = (TextView) LayoutInflater.from(GoodsListActivity.this).inflate(R.layout.tv_filter,
                                                itemSelectGvClass, false);
                                        tv.setText(s.getName());
                                        return tv;
                                    }
                                });
                                Log.e(TAG, "onCompleteddddd: " + classId);
                                for (int i = 0; i < mListClass.size(); i++) {

                                    if (classId.equals(mListClass.get(i).getLabel())) {
                                        itemSelectGvClass.getChildAt(i).performClick();
                                    }
                                }

                                tvFilterClass.setText(mContentFilter.getClasstitle() != null ? mContentFilter.getClasstitle() : "");
                            } else {
                                tvFilterClass.setVisibility(View.GONE);
                                itemSelectGvClass.setVisibility(View.GONE);
                            }
                            if (mContentFilter.getSpecialData() != null) {
                                tvFilterSpecial.setVisibility(View.VISIBLE);
                                itemSelectGvSpecial.setVisibility(View.VISIBLE);
                                mListSpecial.addAll(mContentFilter.getSpecialData());
                                itemSelectGvSpecial.setAdapter(new TagAdapter<ContentFilter.SpecialDataBean>(mListSpecial) {
                                    @Override
                                    public View getView(FlowLayout parent, int position, ContentFilter.SpecialDataBean s) {
                                        TextView tv = (TextView) LayoutInflater.from(GoodsListActivity.this).inflate(R.layout.tv_filter,
                                                itemSelectGvSpecial, false);
                                        tv.setText(s.getName());
                                        return tv;
                                    }
                                });
                                itemSelectGvSpecial.getChildAt(0).performClick();
                                tvFilterSpecial.setText(mContentFilter.getSpecialtitle() != null ? mContentFilter.getSpecialtitle() : "");
                            } else {
                                tvFilterSpecial.setVisibility(View.GONE);
                                itemSelectGvSpecial.setVisibility(View.GONE);
                            }

                            if (mContentFilter.getPriceData() != null) {
                                tvFilterPrice.setVisibility(View.VISIBLE);
                                itemSelectGvPrice.setVisibility(View.VISIBLE);
                                mListPrice.addAll(mContentFilter.getPriceData());
                                itemSelectGvPrice.setAdapter(new TagAdapter<ContentFilter.PriceDataBean>(mListPrice) {
                                    @Override
                                    public View getView(FlowLayout parent, int position, ContentFilter.PriceDataBean s) {
                                        TextView tv = (TextView) LayoutInflater.from(GoodsListActivity.this).inflate(R.layout.tv_filter,
                                                itemSelectGvPrice, false);
                                        tv.setText(s.getName());
                                        return tv;
                                    }
                                });
                                itemSelectGvPrice.getChildAt(0).performClick();
                                tvFilterPrice.setText(mContentFilter.getPricetitle() != null ? mContentFilter.getPricetitle() : "");
                            } else {
                                tvFilterPrice.setVisibility(View.GONE);
                                itemSelectGvPrice.setVisibility(View.GONE);
                            }

                            if (mContentFilter.getAttributeData() != null) {
                                tvFilterAttribute.setVisibility(View.VISIBLE);
                                itemSelectGvAttribute.setVisibility(View.VISIBLE);
                                mListAttribute.addAll(mContentFilter.getAttributeData());
                                itemSelectGvAttribute.setAdapter(new TagAdapter<ContentFilter.AttributeDataBean>(mListAttribute) {
                                    @Override
                                    public View getView(FlowLayout parent, int position, ContentFilter.AttributeDataBean s) {
                                        TextView tv = (TextView) LayoutInflater.from(GoodsListActivity.this).inflate(R.layout.tv_filter,
                                                itemSelectGvAttribute, false);
                                        tv.setText(s.getName());
                                        return tv;
                                    }
                                });
                                itemSelectGvAttribute.getChildAt(0).performClick();
                                tvFilterAttribute.setText(mContentFilter.getAttributetitle() != null ? mContentFilter.getAttributetitle() : "");
                            } else {
                                tvFilterAttribute.setVisibility(View.GONE);
                                itemSelectGvAttribute.setVisibility(View.GONE);
                            }

                            if (mContentFilter.getFieldsData() != null) {
                                mlvRightslide.setVisibility(View.VISIBLE);
                                mListFiled.addAll(mContentFilter.getFieldsData());
                                mAdapterFiled.notifyDataSetChanged();
                            } else {
                                mlvRightslide.setVisibility(View.GONE);
                            }

                        } else {
                            Toast.makeText(GoodsListActivity.this, "" + mContentFilter.getReturnMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: " + e.toString());
                    }

                    @Override
                    public void onNext(ContentFilter contentFilter) {
                        Log.e(TAG, "onNext:11 " + contentFilter.toString());
                        mContentFilter = contentFilter;

                    }
                }));
    }

    /**
     * 初始化数据源
     */
    private void initData() {
        manager = new DataManager(GoodsListActivity.this);
        mCompositeSubscription = new CompositeSubscription();
        mList = new ArrayList<>();
        mAdapter = new GoodsListRvAdapter(GoodsListActivity.this, mList);
        getGoodList();
        rvGoodlist.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new GoodsListRvAdapter.onItemClickListener() {
            @Override
            public void onClick(int position) {
//                Toast.makeText(GoodsListActivity.this, "点击了" + position, Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(GoodsListActivity.this, GoodsDetailActivity.class);
                intent.putExtra("good_id",mList.get(position).getId());
                intent.putExtra("type","a");
                startActivity(intent);
            }
        });
        /**
         * 右面滑出筛选相关
         */


        mListClass = new ArrayList<>();
        mListSpecial = new ArrayList<>();
        mListPrice = new ArrayList<>();
        mListAttribute = new ArrayList<>();

        mListFiled = new ArrayList<>();
        mAdapterFiled = new Filterparentfiledadapter(GoodsListActivity.this, mListFiled);
        mlvRightslide.setAdapter(mAdapterFiled);
        getAttributeList();

        editText.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //判断是否是“完成”键
                if(actionId == EditorInfo.IME_ACTION_SEARCH){
                    keyword=editText.getText().toString().trim();
                    if (mList != null) {
                        mList.clear();
                    }
                    getGoodList();

//                    //隐藏软键盘
//                    InputMethodManager imm = (InputMethodManager) v
//                            .getContext().getSystemService(
//                                    Context.INPUT_METHOD_SERVICE);
//                    if (imm.isActive()) {
//                        imm.hideSoftInputFromWindow(
//                                v.getApplicationWindowToken(), 0);
//                    }
                    return true;
                }
                return false;
            }
        });
    }

    /**
     * 初始化控件
     */
    private void initView() {

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.space);
        int leftspace = getResources().getDimensionPixelSize(R.dimen.leftspace);
        rvGoodlist.addItemDecoration(new SpaceItemDecoration(spacingInPixels, leftspace));
        gridLayoutManager = new GridLayoutManager(GoodsListActivity.this, 2);
        rvGoodlist.setLayoutManager(gridLayoutManager);


        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.RIGHT);
        refreshGoodlist.setRefreshHeader(new ClassicsHeader(GoodsListActivity.this));
        refreshGoodlist.setRefreshFooter(new ClassicsFooter(GoodsListActivity.this));
//        mRefresh.setEnablePureScrollMode(true);
        refreshGoodlist.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                if (mList != null) {
                    mList.clear();
                }
                pageIndex=1;
                getGoodList();
                refreshlayout.finishRefresh(2000/*,false*/);//传入false表示刷新失败
            }
        });
        refreshGoodlist.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                pageIndex++;
                getGoodList();
                refreshlayout.finishLoadMore(2000/*,false*/);//传入false表示加载失败
            }
        });

    }

    public void closeMenu() {
        drawerLayout.closeDrawer(GravityCompat.END);
    }

    public void openMenu() {
        drawerLayout.openDrawer(GravityCompat.END);
    }

    /**
     * 各控件的点击事件
     *
     * @param view
     */
    @OnClick({R.id.iv_goodslist_searchcloase,R.id.select_brand_back_im, R.id.iv_goodslist_back, R.id.ll_goodslist_synthesize, R.id.ll_goodslist_salesvolume, R.id.ll_goodslist_price, R.id.ll_goodslist_filter, R.id.fram_reset_but, R.id.fram_ok_but})
    public void onClick(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_goodslist_back:
                closeMenu();
                onBackPressed();
                break;
            // 综合
            case R.id.ll_goodslist_synthesize:
                initColor();
                tvGoodslistSynthesize.setTextColor(getResources().getColor(R.color.ff2828));
                ivGoodslistSynthesize.setImageResource(R.drawable.arrow_down);
                order = "";
                if (mList != null) {
                    mList.clear();
                }
                getGoodList();
                break;
            //销量
            case R.id.ll_goodslist_salesvolume:
                initColor();
                tvGoodslistSalesvolume.setTextColor(getResources().getColor(R.color.ff2828));
                ivGoodslistSalesvolume.setImageResource(R.drawable.arrow_down);
                order = "6";
                if (mList != null) {
                    mList.clear();
                }
                getGoodList();
                break;
            //价格
            case R.id.ll_goodslist_price:
                initColor();
                tvGoodslistPrice.setTextColor(getResources().getColor(R.color.ff2828));
                // 3 价格从低到高 4 价格从高到低 5 销量从低到高 6 销量从高到低
                if (flagPrice) {
                    ivGoodslistPrice.setImageResource(R.drawable.price_up);
                    order = "3";
                    if (mList != null) {
                        mList.clear();
                    }
                    getGoodList();
                    flagPrice = !flagPrice;
                } else {
                    ivGoodslistPrice.setImageResource(R.drawable.price_down);
                    order = "4";
                    if (mList != null) {
                        mList.clear();
                    }
                    getGoodList();
                    flagPrice = !flagPrice;
                }

                break;
            //排序
            case R.id.ll_goodslist_filter:
                initColor();
                tvGoodslistFilter.setTextColor(getResources().getColor(R.color.ff2828));
                ivGoodslistFilter.setImageResource(R.drawable.filter);
                KeyboardUtils.hideSoftInput(GoodsListActivity.this);
                openMenu();
                break;
            //重置
            case R.id.fram_reset_but:
                if (mList != null) {
                    mList.clear();
                }
                url = "";
                url1 = "";
                specialid = "";
                classId = String.valueOf(aaa);
//                for (int i = 0; i < mListClass.size(); i++) {
//
//                    if (classId.equals(mListClass.get(i).getLabel())) {
//                        itemSelectGvClass.getChildAt(i).performClick();
//                    }
//                }
//                itemSelectGvSpecial.getChildAt(0).setSelected(true);
//                itemSelectGvPrice.getChildAt(0).performClick();
//                itemSelectGvAttribute.getChildAt(0).performClick();
                getGoodList();
                closeMenu();

                break;
            //确定
            case R.id.fram_ok_but:
                Set<Integer> selectedList = itemSelectGvClass.getSelectedList();
                Iterator<Integer> it = selectedList.iterator();
                while (it.hasNext()) {
                    Integer str = it.next();
                    if ("all".equals(mListClass.get(str).getLabel())) {
                        classId = "";
                    } else {
                        classId = mListClass.get(str).getLabel();
                    }
                }
                Set<Integer> selectedList1 = itemSelectGvSpecial.getSelectedList();
                Iterator<Integer> it1 = selectedList1.iterator();
                while (it1.hasNext()) {
                    Integer str = it1.next();
                    if ("all".equals(mListSpecial.get(str).getLabel())) {
                        specialid = "";
                    } else {
                        specialid = mListSpecial.get(str).getLabel();
                    }
                }
                Set<Integer> selectedList2 = itemSelectGvPrice.getSelectedList();
                Iterator<Integer> it2 = selectedList2.iterator();
                while (it2.hasNext()) {
                    Integer str = it2.next();
                    Log.e(TAG, "onClick: "+mListPrice.get(str).getWhere() );
                    url = mListPrice.get(str).getWhere()!=null?"&" + mListPrice.get(str).getWhere():"";
                }
                Log.e(TAG, "onClick: " + classId);
                if (mList != null) {
                    mList.clear();
                }
                Set<Integer> selectedList3 = itemSelectGvAttribute.getSelectedList();
                Iterator<Integer> it3 = selectedList3.iterator();
                while (it3.hasNext()) {
                    Integer str = it3.next();
                    String ss = mListAttribute.get(str).getWhere();
                    url1 = "&" + ss;
                }
                Log.e(TAG, "onClick: " + classId);
                if (mList != null) {
                    mList.clear();
                }
                getGoodList();
                rvGoodlist.setAdapter(mAdapter);
                closeMenu();
                break;
            case R.id.select_brand_back_im:
                closeMenu();
                break;
            case R.id.iv_goodslist_searchcloase:
                editText.setText("");
                break;
        }
    }

    /**
     * 初始化顶部按钮颜色
     */
    private void initColor() {
        ivGoodslistSynthesize.setImageResource(R.drawable.arrow_down_grey);
        ivGoodslistSalesvolume.setImageResource(R.drawable.arrow_down_grey);
        ivGoodslistPrice.setImageResource(R.drawable.price_default);
        ivGoodslistFilter.setImageResource(R.drawable.filter_grey);

        tvGoodslistSynthesize.setTextColor(getResources().getColor(R.color.t333));
        tvGoodslistSalesvolume.setTextColor(getResources().getColor(R.color.t333));
        tvGoodslistPrice.setTextColor(getResources().getColor(R.color.t333));
        tvGoodslistFilter.setTextColor(getResources().getColor(R.color.t333));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCompositeSubscription.hasSubscriptions()) {
            mCompositeSubscription.unsubscribe();
        }
    }

    @OnClick()
    public void onViewClicked() {
    }
}
