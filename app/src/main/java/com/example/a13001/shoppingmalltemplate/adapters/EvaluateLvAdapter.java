package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.CircleImageView;
import com.example.a13001.shoppingmalltemplate.View.MultiImageView;
import com.example.a13001.shoppingmalltemplate.activitys.ImagePagerActivity;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.EvaluateList;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Administrator on 2017/8/9.
 */

public class EvaluateLvAdapter extends BaseAdapter {
    private Context context;
    private List<EvaluateList.ListBean> mList;
    private static final String TAG = "EvaluateLvAdapter";

    public EvaluateLvAdapter(Context context, List<EvaluateList.ListBean> mList) {
        this.context = context;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder vh = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_lv_evaluate, parent, false);
            vh = new ViewHolder(view);
            view.setTag(vh);
            AutoUtils.autoSize(view);
        } else {
            vh = (ViewHolder) view.getTag();
        }
        GlideUtils.setNetImage(context, AppConstants.INTERNET_HEAD + mList.get(position).getMemberFace(), vh.civItemlvevaluateHeadimage);
        vh.tvItemlvevaluateUsername.setText(mList.get(position).getCommentName() != null ? mList.get(position).getCommentName() : "");
        if (TextUtils.isEmpty(mList.get(position).getMemberLevel())) {
            vh.tvItemlvevaluateMember.setText("普通会员");
        } else {
            vh.tvItemlvevaluateMember.setText(mList.get(position).getMemberLevel());
        }
        vh.tvItemlvevaluateContent.setText(mList.get(position).getCommentContent() != null ? mList.get(position).getCommentContent() : "");
        if (TextUtils.isEmpty(mList.get(position).getCommentReplay())){
//            vh.tvItemlvevaluateReplycontent.setText(mList.get(position).getCommentReplay() != null ? mList.get(position).getCommentReplay() : "");
            vh.llHuifu.setVisibility(View.GONE);

        }else{
            vh.llHuifu.setVisibility(View.VISIBLE);
            vh.tvItemlvevaluateReplycontent.setText(mList.get(position).getCommentReplay() != null ? mList.get(position).getCommentReplay() : "");

        }
        vh.tvItemlvevaluateDate.setText(mList.get(position).getCommentTime() != null ? mList.get(position).getCommentTime() : "");
        vh.tvItemlvevaluateBuydate.setText("购买时间：" + mList.get(position).getCommentTime() != null ? mList.get(position).getCommentTime() : "");
        int level = mList.get(position).getCommentLevel();
        switch (level) {
            case 1:
                vh.rbItemlvevaluateStar.setRating(1);
                break;
            case 2:
                vh.rbItemlvevaluateStar.setRating(2);
                break;
            case 3:
                vh.rbItemlvevaluateStar.setRating(3);
                break;
            case 4:
                vh.rbItemlvevaluateStar.setRating(4);
                break;
            case 5:
                vh.rbItemlvevaluateStar.setRating(5);
                break;
        }
        String images = mList.get(position).getCommentImages();
        String[] split = images.split(Pattern.quote("|"));
        final List<String> listImage = new ArrayList<>();

        Log.e(TAG, "getView11: " + images.toString());
        for (int i = 0; i < split.length; i++) {
            Log.e(TAG, "getView1: " + split[i]);
            listImage.add(AppConstants.INTERNET_HEAD + split[i]);
        }
        Log.e(TAG, "getView111: " + listImage.toString());
        vh.mivItemlvEvaluate.setList(listImage);
        vh.mivItemlvEvaluate.setOnItemClickListener(new MultiImageView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ImagePagerActivity.ImageSize imageSize = new ImagePagerActivity.ImageSize(view.getMeasuredWidth(), view.getMeasuredHeight());
                ImagePagerActivity.startImagePagerActivity(context, listImage, position, imageSize);

            }
        });

        return view;
    }



    class ViewHolder {
        @BindView(R.id.civ_itemlvevaluate_headimage)
        CircleImageView civItemlvevaluateHeadimage;
        @BindView(R.id.tv_itemlvevaluate_username)
        TextView tvItemlvevaluateUsername;
        @BindView(R.id.tv_itemlvevaluate_member)
        TextView tvItemlvevaluateMember;
        @BindView(R.id.tv_itemlvevaluate_date)
        TextView tvItemlvevaluateDate;
        @BindView(R.id.rb_itemlvevaluate_star)
        RatingBar rbItemlvevaluateStar;
        @BindView(R.id.tv_itemlvevaluate_content)
        TextView tvItemlvevaluateContent;
        @BindView(R.id.miv_itemlv_evaluate)
        MultiImageView mivItemlvEvaluate;
        @BindView(R.id.tv_itemlvevaluate_replycontent)
        TextView tvItemlvevaluateReplycontent;
        @BindView(R.id.ll_huifu)
        LinearLayout llHuifu;
        @BindView(R.id.tv_itemlvevaluate_buydate)
        TextView tvItemlvevaluateBuydate;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
