package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.Banner;
import com.example.a13001.shoppingmalltemplate.modle.Classify;
import com.example.a13001.shoppingmalltemplate.modle.GoodsDetail;
import com.example.a13001.shoppingmalltemplate.modle.GoodsList;
import com.example.a13001.shoppingmalltemplate.modle.News;
import com.example.a13001.shoppingmalltemplate.modle.Special;

public interface ClassifyView extends View {
    void onSuccess(Classify mClassify);
    void onSuccessGoodsList(GoodsList mGoodsList);
    void onSuccessGetSpecialList(Special special);
    void onError(String result);

}
