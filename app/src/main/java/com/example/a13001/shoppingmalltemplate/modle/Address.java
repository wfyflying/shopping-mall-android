package com.example.a13001.shoppingmalltemplate.modle;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

public class Address {

    /**
     * status : 1
     * returnMsg : null
     * AddressCount : 1
     * Delivery_AddressList : [{"addresslId":156,"addressName":"aaa","addressPhone":"18222215103","addressProvince":"aaa","addressCity":"aaa","addressArea":"aa","addressXX":"aa","addressZipcode":32200,"addressDefault":false}]
     */

    private int status;
    private Object returnMsg;
    private int AddressCount;
    private List<DeliveryAddressListBean> Delivery_AddressList;

    @Override
    public String toString() {
        return "Address{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", AddressCount=" + AddressCount +
                ", Delivery_AddressList=" + Delivery_AddressList +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getAddressCount() {
        return AddressCount;
    }

    public void setAddressCount(int AddressCount) {
        this.AddressCount = AddressCount;
    }

    public List<DeliveryAddressListBean> getDelivery_AddressList() {
        return Delivery_AddressList;
    }

    public void setDelivery_AddressList(List<DeliveryAddressListBean> Delivery_AddressList) {
        this.Delivery_AddressList = Delivery_AddressList;
    }

    public static class DeliveryAddressListBean implements Parcelable{
        /**
         * addresslId : 156
         * addressName : aaa
         * addressPhone : 18222215103
         * addressProvince : aaa
         * addressCity : aaa
         * addressArea : aa
         * addressXX : aa
         * addressZipcode : 32200
         * addressDefault : false
         */

        private int addresslId;
        private String addressName;
        private String addressPhone;
        private String addressProvince;
        private String addressCity;
        private String addressArea;
        private String addressXX;
        private int addressZipcode;
        private boolean addressDefault;

        protected DeliveryAddressListBean(Parcel in) {
            addresslId = in.readInt();
            addressName = in.readString();
            addressPhone = in.readString();
            addressProvince = in.readString();
            addressCity = in.readString();
            addressArea = in.readString();
            addressXX = in.readString();
            addressZipcode = in.readInt();
            addressDefault = in.readByte() != 0;
        }

        public static final Creator<DeliveryAddressListBean> CREATOR = new Creator<DeliveryAddressListBean>() {
            @Override
            public DeliveryAddressListBean createFromParcel(Parcel in) {
                return new DeliveryAddressListBean(in);
            }

            @Override
            public DeliveryAddressListBean[] newArray(int size) {
                return new DeliveryAddressListBean[size];
            }
        };

        @Override
        public String toString() {
            return "DeliveryAddressListBean{" +
                    "addresslId=" + addresslId +
                    ", addressName='" + addressName + '\'' +
                    ", addressPhone='" + addressPhone + '\'' +
                    ", addressProvince='" + addressProvince + '\'' +
                    ", addressCity='" + addressCity + '\'' +
                    ", addressArea='" + addressArea + '\'' +
                    ", addressXX='" + addressXX + '\'' +
                    ", addressZipcode=" + addressZipcode +
                    ", addressDefault=" + addressDefault +
                    '}';
        }

        public int getAddresslId() {
            return addresslId;
        }

        public void setAddresslId(int addresslId) {
            this.addresslId = addresslId;
        }

        public String getAddressName() {
            return addressName;
        }

        public void setAddressName(String addressName) {
            this.addressName = addressName;
        }

        public String getAddressPhone() {
            return addressPhone;
        }

        public void setAddressPhone(String addressPhone) {
            this.addressPhone = addressPhone;
        }

        public String getAddressProvince() {
            return addressProvince;
        }

        public void setAddressProvince(String addressProvince) {
            this.addressProvince = addressProvince;
        }

        public String getAddressCity() {
            return addressCity;
        }

        public void setAddressCity(String addressCity) {
            this.addressCity = addressCity;
        }

        public String getAddressArea() {
            return addressArea;
        }

        public void setAddressArea(String addressArea) {
            this.addressArea = addressArea;
        }

        public String getAddressXX() {
            return addressXX;
        }

        public void setAddressXX(String addressXX) {
            this.addressXX = addressXX;
        }

        public int getAddressZipcode() {
            return addressZipcode;
        }

        public void setAddressZipcode(int addressZipcode) {
            this.addressZipcode = addressZipcode;
        }

        public boolean isAddressDefault() {
            return addressDefault;
        }

        public void setAddressDefault(boolean addressDefault) {
            this.addressDefault = addressDefault;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(addresslId);
            parcel.writeString(addressName);
            parcel.writeString(addressPhone);
            parcel.writeString(addressProvince);
            parcel.writeString(addressCity);
            parcel.writeString(addressArea);
            parcel.writeString(addressXX);
            parcel.writeInt(addressZipcode);
            parcel.writeByte((byte) (addressDefault ? 1 : 0));
        }
    }
}
