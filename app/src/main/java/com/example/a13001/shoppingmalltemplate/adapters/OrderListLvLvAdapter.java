package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.activitys.ApplyAfterSaleActivity;
import com.example.a13001.shoppingmalltemplate.activitys.GoEvaluateActivity;
import com.example.a13001.shoppingmalltemplate.activitys.LogisticsInfoActivity;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.Order;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Administrator on 2017/8/9.
 */

public class OrderListLvLvAdapter extends BaseAdapter {
    private Context mContext;
    private List<Order.OrderListBean.OrderGoodsListBean> mList;
    private int mOrderStatus;
    public OrderListLvLvAdapter(Context context, List<Order.OrderListBean.OrderGoodsListBean> mList,int orderstatus) {
        this.mContext = context;
        this.mList = mList;
        this.mOrderStatus = orderstatus;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolder vh = null;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.order_item_item, parent, false);
            vh = new ViewHolder(view);
            view.setTag(vh);
            AutoUtils.autoSize(view);
        } else {
            vh = (ViewHolder) view.getTag();
        }
        GlideUtils.setNetImage(mContext, AppConstants.INTERNET_HEAD + mList.get(position).getCartImg(), vh.ivOrderitemGoodimage);
        vh.tvOrderitemGoodsname.setText(mList.get(position).getCommodityName() != null ? mList.get(position).getCommodityName() : "");
        vh.tvOrderitemDescribe.setText(mList.get(position).getCommodityProperty() != null ? mList.get(position).getCommodityProperty() : "");
//        vh.tvOrderitemPrice.setText(mList.get(position).getCommodityProperty()!=null?mList.get(position).getCommodityProperty():"");
        vh.tvOrderitemCount.setText("X" + mList.get(position).getCommodityNumber());
        vh.btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mContext, GoEvaluateActivity.class);
                intent.putExtra("cartid",mList.get(position).getCartId());
                intent.putExtra("commodityId",mList.get(position).getCommodityId());
                mContext.startActivity(intent);
            }
        });
        vh.btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext, ApplyAfterSaleActivity.class));
            }
        });
        //订单状态，1 待支付 2 未发货 3 已发货 4 已签收
        switch (mOrderStatus){
            case 1:
                vh.llButton.setVisibility(View.GONE);
                break;
            case 2:
                vh.llButton.setVisibility(View.GONE);
                break;
            case 3:
                vh.llButton.setVisibility(View.GONE);
                break;
            case 4:

                vh.llButton.setVisibility(View.GONE);


                break;
        }
        return view;
    }




    class ViewHolder {
        @BindView(R.id.iv_orderitem_goodimage)
        ImageView ivOrderitemGoodimage;
        @BindView(R.id.tv_orderitem_goodsname)
        TextView tvOrderitemGoodsname;
        @BindView(R.id.tv_orderitem_price)
        TextView tvOrderitemPrice;
        @BindView(R.id.tv_orderitem_describe)
        TextView tvOrderitemDescribe;
        @BindView(R.id.tv_orderitem_count)
        TextView tvOrderitemCount;
        @BindView(R.id.btn1)
        Button btn1;
        @BindView(R.id.btn2)
        Button btn2;
        @BindView(R.id.ll_button)
        LinearLayout llButton;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
