package com.example.a13001.shoppingmalltemplate.activitys;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePhoneActivity extends BaseActivity {
    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.et_changephone_phone)
    EditText etChangephonePhone;
    @BindView(R.id.et_changephone_code)
    EditText etChangephoneCode;
    @BindView(R.id.tv_changephone_sendcode)
    TextView tvChangephoneSendcode;
    private RelativeLayout mRlBack;
    private TextView mTvTitle;
    private TextView mTvRight;
    private EditText mEtPhone;
    private EditText mEtCode;
    private TextView mTvSendCode;
    private TimeCount time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_phone);
        ButterKnife.bind(this);
        initData();
    }

    /**
     * 初始化控件
     */
    private void initData() {
        time = new TimeCount(60000, 1000);
        tvTitleCenter.setText("换绑手机");
    }

    /**
     * 各控件的点击事件
     *
     * @param view
     */
    @OnClick({R.id.iv_title_back, R.id.tv_changephone_sendcode})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_title_back:
                onBackPressed();
                break;
            //发送验证码
            case R.id.tv_changephone_sendcode:
                time.start();
                break;
        }
    }

    /**
     * 倒计时类
     */
    class TimeCount extends CountDownTimer {

        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            // mBtnGetcode.setBackgroundColor(Color.parseColor("#B6B6D8"));
            mTvSendCode.setClickable(false);
            mTvSendCode.setText("" + millisUntilFinished / 1000);
        }

        @Override
        public void onFinish() {
            mTvSendCode.setText("发送验证码");
            mTvSendCode.setClickable(true);
            //  mBtnGetcode.setBackgroundColor(Color.parseColor("#4EB84A"));
        }
    }
}
