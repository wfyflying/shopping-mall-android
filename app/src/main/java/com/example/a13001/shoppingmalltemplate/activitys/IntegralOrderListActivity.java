package com.example.a13001.shoppingmalltemplate.activitys;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.adapters.IntegralOrderListLvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.Order;
import com.example.a13001.shoppingmalltemplate.mvpview.IntegralOrderListView;
import com.example.a13001.shoppingmalltemplate.presenter.IntegralOrderListPredenter;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class IntegralOrderListActivity extends BaseActivity implements IntegralOrderListLvAdapter.changeOrderStateInterface {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_integralorderlist_weifahuo)
    TextView tvIntegralorderlistWeifahuo;
    @BindView(R.id.tv_integralorderlist_yifahuo)
    TextView tvIntegralorderlistYifahuo;
    @BindView(R.id.tv_integralorderlist_yiqianshou)
    TextView tvIntegralorderlistYiqianshou;
    @BindView(R.id.lv_integralorderlist)
    ListView lvIntegralorderlist;
    @BindView(R.id.srfl_integralorderlist)
    SmartRefreshLayout srflIntegralorderlist;
    private List<Order.OrderListBean> mList;
    private IntegralOrderListLvAdapter mAdapter;
    IntegralOrderListPredenter integralOrderListPredenter=new IntegralOrderListPredenter(IntegralOrderListActivity.this);
    private static final String TAG = "IntegralOrderListActivi";
    private String code;
    private String timestamp;
    private int pageindex=1;
    private String status="2";//查询订单状态，1 待支付 2 未发货 3 已发货 4 已签收
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_integral_order_list);
        ButterKnife.bind(this);
        initData();
    }

    /**
     * 初始化数据源
     */
    private void initData() {
        tvTitleCenter.setText("积分兑换订单");
        integralOrderListPredenter.onCreate();
        integralOrderListPredenter.attachView(integralOrderListView);

        String safetyCode = MyUtils.getMetaValue(IntegralOrderListActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        timestamp = Utils.getTimeStamp();


        mList=new ArrayList<>();
        mAdapter=new IntegralOrderListLvAdapter(IntegralOrderListActivity.this,mList);
        mAdapter.setChangeOrderStateInterface(this);
        tvIntegralorderlistWeifahuo.performClick();
//        integralOrderListPredenter.getOrderList(AppConstants.COMPANY_ID,code,timestamp,AppConstants.PAGE_SIZE,pageindex,status,"","","",AppConstants.FROM_MOBILE);
        lvIntegralorderlist.setAdapter(mAdapter);

        setRefresh();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mList != null) {
            mList.clear();
        }
        pageindex=1;
        integralOrderListPredenter.getOrderList(AppConstants.COMPANY_ID,code,timestamp,AppConstants.PAGE_SIZE,pageindex,status,"","","",AppConstants.FROM_MOBILE);
    }

    private void setRefresh() {
        //刷新
        srflIntegralorderlist.setRefreshHeader(new ClassicsHeader(IntegralOrderListActivity.this));
        srflIntegralorderlist.setRefreshFooter(new ClassicsFooter(IntegralOrderListActivity.this));
//        mRefresh.setEnablePureScrollMode(true);
        srflIntegralorderlist.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                pageindex = 1;
                if (mList != null) {
                    mList.clear();
                }
                integralOrderListPredenter.getOrderList(AppConstants.COMPANY_ID,code,timestamp,AppConstants.PAGE_SIZE,pageindex,status,"","","",AppConstants.FROM_MOBILE);
                refreshlayout.finishRefresh(2000/*,true*/);//传入false表示刷新失败
            }
        });
        srflIntegralorderlist.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                pageindex++;
                integralOrderListPredenter.getOrderList(AppConstants.COMPANY_ID,code,timestamp,AppConstants.PAGE_SIZE,pageindex,status,"","","",AppConstants.FROM_MOBILE);
                refreshlayout.finishLoadMore(2000/*,true*/);//传入false表示加载失败
            }
        });
    }
    IntegralOrderListView integralOrderListView=new IntegralOrderListView() {
        @Override
        public void onSuccessGetIntegralOrderList(Order order) {
            Log.e(TAG, "onSuccessGetIntegralOrderList: "+order.toString());
            int status=order.getStatus();
            if (status>0){
                mList.addAll(order.getOrderList());
                mAdapter.notifyDataSetChanged();
            }else{
                Toast.makeText(IntegralOrderListActivity.this, ""+order.getReturnMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onSuccessAffirmOrder(CommonResult commonResult) {
            Log.e(TAG, "onSuccessAffirmOrder: "+commonResult.toString() );
           int status1= commonResult.getStatus();
           if (status1>0){
               if (mList != null) {
                   mList.clear();
               }
               Log.e(TAG, "onSuccessAffirmOrder: "+status);
               integralOrderListPredenter.getOrderList(AppConstants.COMPANY_ID,code,timestamp,AppConstants.PAGE_SIZE,pageindex,status,"","","",AppConstants.FROM_MOBILE);
           }else {
               Toast.makeText(IntegralOrderListActivity.this, ""+commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();
           }
        }

        @Override
        public void onError(String result) {
            Log.e(TAG, "onError: "+result );
        }
    };
    @OnClick({R.id.iv_title_back, R.id.tv_integralorderlist_weifahuo, R.id.tv_integralorderlist_yifahuo, R.id.tv_integralorderlist_yiqianshou})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_title_back:
                onBackPressed();
                break;
                //未发货
            case R.id.tv_integralorderlist_weifahuo:
                initColor();
                tvIntegralorderlistWeifahuo.setBackgroundColor(getResources().getColor(R.color.ffb422));
                tvIntegralorderlistWeifahuo.setTextColor(getResources().getColor(R.color.white));
                if (mList != null) {
                    mList.clear();
                }
                status="2";
                pageindex=1;
                integralOrderListPredenter.getOrderList(AppConstants.COMPANY_ID,code,timestamp,AppConstants.PAGE_SIZE,pageindex,status,"","","",AppConstants.FROM_MOBILE);
                lvIntegralorderlist.setAdapter(mAdapter);
                break;
                //已发货
            case R.id.tv_integralorderlist_yifahuo:
                initColor();
                tvIntegralorderlistYifahuo.setBackgroundColor(getResources().getColor(R.color.ffb422));
                tvIntegralorderlistYifahuo.setTextColor(getResources().getColor(R.color.white));
                if (mList != null) {
                    mList.clear();
                }
                status="3";
                pageindex=1;
                integralOrderListPredenter.getOrderList(AppConstants.COMPANY_ID,code,timestamp,AppConstants.PAGE_SIZE,pageindex,status,"","","",AppConstants.FROM_MOBILE);
                lvIntegralorderlist.setAdapter(mAdapter);
                break;
                //已签收
            case R.id.tv_integralorderlist_yiqianshou:
                initColor();
                tvIntegralorderlistYiqianshou.setBackgroundColor(getResources().getColor(R.color.ffb422));
                tvIntegralorderlistYiqianshou.setTextColor(getResources().getColor(R.color.white));
                if (mList != null) {
                    mList.clear();
                }
                status="4";
                pageindex=1;
                integralOrderListPredenter.getOrderList(AppConstants.COMPANY_ID,code,timestamp,AppConstants.PAGE_SIZE,pageindex,status,"","","",AppConstants.FROM_MOBILE);
                lvIntegralorderlist.setAdapter(mAdapter);
                break;
        }
    }

    /**
     *
     */
    private void initColor() {
        tvIntegralorderlistWeifahuo.setBackgroundColor(getResources().getColor(R.color.white));
        tvIntegralorderlistYifahuo.setBackgroundColor(getResources().getColor(R.color.white));
        tvIntegralorderlistYiqianshou.setBackgroundColor(getResources().getColor(R.color.white));

        tvIntegralorderlistWeifahuo.setTextColor(getResources().getColor(R.color.t666));
        tvIntegralorderlistYifahuo.setTextColor(getResources().getColor(R.color.t666));
        tvIntegralorderlistYiqianshou.setTextColor(getResources().getColor(R.color.t666));

    }

    @Override
    public void doAffirmOrder(int position, String orderNumber) {
        integralOrderListPredenter.affirmOrder(AppConstants.COMPANY_ID,code,timestamp,orderNumber);
    }
}
