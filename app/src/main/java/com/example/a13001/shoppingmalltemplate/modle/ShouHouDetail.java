package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class ShouHouDetail {

    /**
     * status : 1
     * returnMsg : null
     * CartId : 100001
     * commodityId : 125
     * repairsCheck : 1
     * repairsId : 20180919152728260
     * repairsType : 2
     * repairsImages : //file.site.ify2.cn/site/153/upload/member/100000/201809/627cc4ea-dbe3-44ef-94a7-846e2212d9b2.jpg
     * repairsContent : 我要退货。
     * repairsProof : 2
     * repairsExpress :
     * repairsExpressNO :
     * repairsNumber : 1
     * repairsStatus : 2
     * repairsCreatTime : 2018-09-19 15:32
     * repairsCompleteTime :
     * repairsRefundTime :
     * repairsName : 子非鱼
     * repairsPhone : 18622470028
     * addressProvince : 天津市|10000
     * addressCity : 天津市|17199
     * addressArea : 河西区|10002
     * addressXX : 建国楼
     * repairsZipCode : 300000
     * CartImg : //file.site.ify2.cn/site/153/upload/sccs/upload/201801/2018112715555421.jpg
     * orderDate : 2018-06-14 17:59
     * CommodityName : 我是商品
     * commodityLink : //www.tjsrsh.site.ify2.cn/channel/content.aspx?id=125&preview=301c85222dd1d054593081890f91995d
     * CommodityProperty : 颜色:红色,尺寸:12
     * CommodityNumber : 1
     * RepairsRemarksCount : 1
     * RepairsRemarks : [{"oldStatus":"待审核","newStatus":"等待商品寄回","beizhu":"允许进行退货。","remName":"wfyflying","remTime":"2018-10-10 09:19"}]
     */

    private int status;
    private Object returnMsg;
    private int CartId;
    private int commodityId;
    private int repairsCheck;
    private String repairsId;
    private int repairsType;
    private String repairsImages;
    private String repairsContent;
    private int repairsProof;
    private String repairsExpress;
    private String repairsExpressNO;
    private int repairsNumber;
    private int repairsStatus;
    private String repairsCreatTime;
    private String repairsCompleteTime;
    private String repairsRefundTime;
    private String repairsName;
    private String repairsPhone;
    private String addressProvince;
    private String addressCity;
    private String addressArea;
    private String addressXX;
    private String repairsZipCode;
    private String CartImg;
    private String orderDate;
    private String CommodityName;
    private String commodityLink;
    private String CommodityProperty;
    private int CommodityNumber;
    private int RepairsRemarksCount;
    private List<RepairsRemarksBean> RepairsRemarks;

    @Override
    public String toString() {
        return "ShouHouDetail{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", CartId=" + CartId +
                ", commodityId=" + commodityId +
                ", repairsCheck=" + repairsCheck +
                ", repairsId='" + repairsId + '\'' +
                ", repairsType=" + repairsType +
                ", repairsImages='" + repairsImages + '\'' +
                ", repairsContent='" + repairsContent + '\'' +
                ", repairsProof=" + repairsProof +
                ", repairsExpress='" + repairsExpress + '\'' +
                ", repairsExpressNO='" + repairsExpressNO + '\'' +
                ", repairsNumber=" + repairsNumber +
                ", repairsStatus=" + repairsStatus +
                ", repairsCreatTime='" + repairsCreatTime + '\'' +
                ", repairsCompleteTime='" + repairsCompleteTime + '\'' +
                ", repairsRefundTime='" + repairsRefundTime + '\'' +
                ", repairsName='" + repairsName + '\'' +
                ", repairsPhone='" + repairsPhone + '\'' +
                ", addressProvince='" + addressProvince + '\'' +
                ", addressCity='" + addressCity + '\'' +
                ", addressArea='" + addressArea + '\'' +
                ", addressXX='" + addressXX + '\'' +
                ", repairsZipCode='" + repairsZipCode + '\'' +
                ", CartImg='" + CartImg + '\'' +
                ", orderDate='" + orderDate + '\'' +
                ", CommodityName='" + CommodityName + '\'' +
                ", commodityLink='" + commodityLink + '\'' +
                ", CommodityProperty='" + CommodityProperty + '\'' +
                ", CommodityNumber=" + CommodityNumber +
                ", RepairsRemarksCount=" + RepairsRemarksCount +
                ", RepairsRemarks=" + RepairsRemarks +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getCartId() {
        return CartId;
    }

    public void setCartId(int CartId) {
        this.CartId = CartId;
    }

    public int getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(int commodityId) {
        this.commodityId = commodityId;
    }

    public int getRepairsCheck() {
        return repairsCheck;
    }

    public void setRepairsCheck(int repairsCheck) {
        this.repairsCheck = repairsCheck;
    }

    public String getRepairsId() {
        return repairsId;
    }

    public void setRepairsId(String repairsId) {
        this.repairsId = repairsId;
    }

    public int getRepairsType() {
        return repairsType;
    }

    public void setRepairsType(int repairsType) {
        this.repairsType = repairsType;
    }

    public String getRepairsImages() {
        return repairsImages;
    }

    public void setRepairsImages(String repairsImages) {
        this.repairsImages = repairsImages;
    }

    public String getRepairsContent() {
        return repairsContent;
    }

    public void setRepairsContent(String repairsContent) {
        this.repairsContent = repairsContent;
    }

    public int getRepairsProof() {
        return repairsProof;
    }

    public void setRepairsProof(int repairsProof) {
        this.repairsProof = repairsProof;
    }

    public String getRepairsExpress() {
        return repairsExpress;
    }

    public void setRepairsExpress(String repairsExpress) {
        this.repairsExpress = repairsExpress;
    }

    public String getRepairsExpressNO() {
        return repairsExpressNO;
    }

    public void setRepairsExpressNO(String repairsExpressNO) {
        this.repairsExpressNO = repairsExpressNO;
    }

    public int getRepairsNumber() {
        return repairsNumber;
    }

    public void setRepairsNumber(int repairsNumber) {
        this.repairsNumber = repairsNumber;
    }

    public int getRepairsStatus() {
        return repairsStatus;
    }

    public void setRepairsStatus(int repairsStatus) {
        this.repairsStatus = repairsStatus;
    }

    public String getRepairsCreatTime() {
        return repairsCreatTime;
    }

    public void setRepairsCreatTime(String repairsCreatTime) {
        this.repairsCreatTime = repairsCreatTime;
    }

    public String getRepairsCompleteTime() {
        return repairsCompleteTime;
    }

    public void setRepairsCompleteTime(String repairsCompleteTime) {
        this.repairsCompleteTime = repairsCompleteTime;
    }

    public String getRepairsRefundTime() {
        return repairsRefundTime;
    }

    public void setRepairsRefundTime(String repairsRefundTime) {
        this.repairsRefundTime = repairsRefundTime;
    }

    public String getRepairsName() {
        return repairsName;
    }

    public void setRepairsName(String repairsName) {
        this.repairsName = repairsName;
    }

    public String getRepairsPhone() {
        return repairsPhone;
    }

    public void setRepairsPhone(String repairsPhone) {
        this.repairsPhone = repairsPhone;
    }

    public String getAddressProvince() {
        return addressProvince;
    }

    public void setAddressProvince(String addressProvince) {
        this.addressProvince = addressProvince;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressArea() {
        return addressArea;
    }

    public void setAddressArea(String addressArea) {
        this.addressArea = addressArea;
    }

    public String getAddressXX() {
        return addressXX;
    }

    public void setAddressXX(String addressXX) {
        this.addressXX = addressXX;
    }

    public String getRepairsZipCode() {
        return repairsZipCode;
    }

    public void setRepairsZipCode(String repairsZipCode) {
        this.repairsZipCode = repairsZipCode;
    }

    public String getCartImg() {
        return CartImg;
    }

    public void setCartImg(String CartImg) {
        this.CartImg = CartImg;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getCommodityName() {
        return CommodityName;
    }

    public void setCommodityName(String CommodityName) {
        this.CommodityName = CommodityName;
    }

    public String getCommodityLink() {
        return commodityLink;
    }

    public void setCommodityLink(String commodityLink) {
        this.commodityLink = commodityLink;
    }

    public String getCommodityProperty() {
        return CommodityProperty;
    }

    public void setCommodityProperty(String CommodityProperty) {
        this.CommodityProperty = CommodityProperty;
    }

    public int getCommodityNumber() {
        return CommodityNumber;
    }

    public void setCommodityNumber(int CommodityNumber) {
        this.CommodityNumber = CommodityNumber;
    }

    public int getRepairsRemarksCount() {
        return RepairsRemarksCount;
    }

    public void setRepairsRemarksCount(int RepairsRemarksCount) {
        this.RepairsRemarksCount = RepairsRemarksCount;
    }

    public List<RepairsRemarksBean> getRepairsRemarks() {
        return RepairsRemarks;
    }

    public void setRepairsRemarks(List<RepairsRemarksBean> RepairsRemarks) {
        this.RepairsRemarks = RepairsRemarks;
    }

    public static class RepairsRemarksBean {
        /**
         * oldStatus : 待审核
         * newStatus : 等待商品寄回
         * beizhu : 允许进行退货。
         * remName : wfyflying
         * remTime : 2018-10-10 09:19
         */

        private String oldStatus;
        private String newStatus;
        private String beizhu;
        private String remName;
        private String remTime;

        @Override
        public String toString() {
            return "RepairsRemarksBean{" +
                    "oldStatus='" + oldStatus + '\'' +
                    ", newStatus='" + newStatus + '\'' +
                    ", beizhu='" + beizhu + '\'' +
                    ", remName='" + remName + '\'' +
                    ", remTime='" + remTime + '\'' +
                    '}';
        }

        public String getOldStatus() {
            return oldStatus;
        }

        public void setOldStatus(String oldStatus) {
            this.oldStatus = oldStatus;
        }

        public String getNewStatus() {
            return newStatus;
        }

        public void setNewStatus(String newStatus) {
            this.newStatus = newStatus;
        }

        public String getBeizhu() {
            return beizhu;
        }

        public void setBeizhu(String beizhu) {
            this.beizhu = beizhu;
        }

        public String getRemName() {
            return remName;
        }

        public void setRemName(String remName) {
            this.remName = remName;
        }

        public String getRemTime() {
            return remTime;
        }

        public void setRemTime(String remTime) {
            this.remTime = remTime;
        }
    }
}
