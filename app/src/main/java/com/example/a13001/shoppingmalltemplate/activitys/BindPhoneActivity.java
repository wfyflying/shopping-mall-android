package com.example.a13001.shoppingmalltemplate.activitys;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.mvpview.BindPhoneView;
import com.example.a13001.shoppingmalltemplate.presenter.BindPhonePredenter;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.subscriptions.CompositeSubscription;

public class BindPhoneActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_title_right)
    TextView tvTitleRight;
    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;
    @BindView(R.id.et_bindphone_phone)
    EditText etBindphonePhone;
    @BindView(R.id.et_bindphone_pwd)
    EditText etBindphonePwd;
    @BindView(R.id.et_bindphone_code)
    EditText etBindphoneCode;
    @BindView(R.id.tv_bindphone_sendcode)
    TextView tvBindphoneSendcode;
    @BindView(R.id.tv_bindphone_commit)
    TextView tvBindphoneCommit;
    BindPhonePredenter bindPhonePredenter=new BindPhonePredenter(BindPhoneActivity.this);
    private static final String TAG = "BindPhoneActivity";
    private TimeCount timeCount;

    private String code;
    private String mTimestamp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_phone);
        ButterKnife.bind(this);
        initData();
    }

    /**
     * 初始化数据源
     */
    private void initData() {
        bindPhonePredenter.onCreate();
        bindPhonePredenter.attachView(bindPhoneView);

        timeCount = new TimeCount(60000, 1000);

        String safetyCode = MyUtils.getMetaValue(BindPhoneActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        mTimestamp = Utils.getTimeStamp();
    }
    /**
     * 倒计时类
     */
    class TimeCount extends CountDownTimer {

        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            // mBtnGetcode.setBackgroundColor(Color.parseColor("#B6B6D8"));
            tvBindphoneSendcode.setClickable(false);
            tvBindphoneSendcode.setText("" + millisUntilFinished / 1000);
        }

        @Override
        public void onFinish() {
            tvBindphoneSendcode.setText("获取验证码");
            tvBindphoneSendcode.setClickable(true);
            //  mBtnGetcode.setBackgroundColor(Color.parseColor("#4EB84A"));
        }
    }
    BindPhoneView bindPhoneView=new BindPhoneView() {
        @Override
        public void onSuccessGetCode(CommonResult commonResult) {
            Log.e(TAG, "onSuccessGetCode: "+commonResult.toString() );
            int status=commonResult.getStatus();
            if (status>0){
                Toast.makeText(BindPhoneActivity.this, ""+commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(BindPhoneActivity.this, ""+commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();
//                switch (commonResult.getChkType()){
//                    case 0:
//                        Toast.makeText(BindPhoneActivity.this, "发送成功，请查看您的手机短信获取校验码", Toast.LENGTH_SHORT).show();break;
//                    case 1:
//                        Toast.makeText(BindPhoneActivity.this, "发送失败，手机号码格式不正确", Toast.LENGTH_SHORT).show();break;
//                    case 2:
//                        Toast.makeText(BindPhoneActivity.this, "手机号格式不正确", Toast.LENGTH_SHORT).show();break;
//                    case 3:
//                        Toast.makeText(BindPhoneActivity.this, "", Toast.LENGTH_SHORT).show();break;
//                    case 4:break;
//                    case 5:break;
//                    case 6:break;
//                }
            }
        }

        @Override
        public void onSuccessBindPhone(CommonResult commonResult) {
            Log.e(TAG, "onSuccessBindPhone: "+commonResult.toString() );
            int status=commonResult.getStatus();
            if (status>0){
                Toast.makeText(BindPhoneActivity.this, ""+commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();
                finish();
            }else{
                Toast.makeText(BindPhoneActivity.this, ""+commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();

            }
        }

        @Override
        public void onError(String result) {
            Log.e(TAG, "onError: "+result.toString() );
        }
    };
    @OnClick({R.id.iv_title_back, R.id.tv_bindphone_sendcode, R.id.tv_bindphone_commit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_title_back:
                onBackPressed();
                break;
                //发送验证码
            case R.id.tv_bindphone_sendcode:
                String phone=etBindphonePhone.getText().toString().trim();
                String pwd=etBindphonePwd.getText().toString().trim();
                if (TextUtils.isEmpty(phone)){
                    Toast.makeText(this, "请输入手机号", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(pwd)){
                    Toast.makeText(this, "请输入安全密码", Toast.LENGTH_SHORT).show();
                    return;
                }
                bindPhonePredenter.bindPhoneSendCode(AppConstants.COMPANY_ID,code,mTimestamp,phone,pwd);
                break;
                //提交
            case R.id.tv_bindphone_commit:
                String phone1=etBindphonePhone.getText().toString().trim();
                String code=etBindphoneCode.getText().toString().trim();
                String pwd1=etBindphonePwd.getText().toString().trim();
                if (TextUtils.isEmpty(phone1)){
                    Toast.makeText(this, "请输入手机号", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(code)){
                    Toast.makeText(this, "请输入验证码", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(pwd1)){
                    Toast.makeText(this, "请输入安全密码", Toast.LENGTH_SHORT).show();
                    return;
                }
                bindPhonePredenter.bindPhone(AppConstants.COMPANY_ID,code,mTimestamp,phone1,code,pwd1);
                break;
        }
    }
}
