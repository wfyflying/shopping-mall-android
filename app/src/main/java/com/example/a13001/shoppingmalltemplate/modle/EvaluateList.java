package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class EvaluateList {

    /**
     * status : 1
     * returnMsg : null
     * count : 1
     * pagesize : 20
     * pageindex : 1
     * list : [{"cid":165,"commentName":"子**","commentIP":"111.166.91.*","commentAddress":"天津市","commentTime":"2018-07-30 14:04","commentZc":0,"commentFd":0,"commentContent":"酒店很不错，大家可以来体验下。","commentReplay":"","commentLevel":5,"commentLabel":"干净|漂亮","commentImages":"","commentStatus":0,"memberId":100043,"memberFace":"//q.qlogo.cn/qqapp/101450329/DCF88F6477F04489BEFE4766F300182E/100","memberLevel":"普通会员"}]
     */

    private int status;
    private String returnMsg;
    private int count;
    private int pagesize;
    private int pageindex;
    private List<ListBean> list;

    @Override
    public String toString() {
        return "EvaluateList{" +
                "status=" + status +
                ", returnMsg='" + returnMsg + '\'' +
                ", count=" + count +
                ", pagesize=" + pagesize +
                ", pageindex=" + pageindex +
                ", list=" + list +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public int getPageindex() {
        return pageindex;
    }

    public void setPageindex(int pageindex) {
        this.pageindex = pageindex;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * cid : 165
         * commentName : 子**
         * commentIP : 111.166.91.*
         * commentAddress : 天津市
         * commentTime : 2018-07-30 14:04
         * commentZc : 0
         * commentFd : 0
         * commentContent : 酒店很不错，大家可以来体验下。
         * commentReplay :
         * commentLevel : 5
         * commentLabel : 干净|漂亮
         * commentImages :
         * commentStatus : 0
         * memberId : 100043
         * memberFace : //q.qlogo.cn/qqapp/101450329/DCF88F6477F04489BEFE4766F300182E/100
         * memberLevel : 普通会员
         */

        private int cid;
        private String commentName;
        private String commentIP;
        private String commentAddress;
        private String commentTime;
        private int commentZc;
        private int commentFd;
        private String commentContent;
        private String commentReplay;
        private int commentLevel;
        private String commentLabel;
        private String commentImages;
        private int commentStatus;
        private int memberId;
        private String memberFace;
        private String memberLevel;

        @Override
        public String toString() {
            return "ListBean{" +
                    "cid=" + cid +
                    ", commentName='" + commentName + '\'' +
                    ", commentIP='" + commentIP + '\'' +
                    ", commentAddress='" + commentAddress + '\'' +
                    ", commentTime='" + commentTime + '\'' +
                    ", commentZc=" + commentZc +
                    ", commentFd=" + commentFd +
                    ", commentContent='" + commentContent + '\'' +
                    ", commentReplay='" + commentReplay + '\'' +
                    ", commentLevel=" + commentLevel +
                    ", commentLabel='" + commentLabel + '\'' +
                    ", commentImages='" + commentImages + '\'' +
                    ", commentStatus=" + commentStatus +
                    ", memberId=" + memberId +
                    ", memberFace='" + memberFace + '\'' +
                    ", memberLevel='" + memberLevel + '\'' +
                    '}';
        }

        public int getCid() {
            return cid;
        }

        public void setCid(int cid) {
            this.cid = cid;
        }

        public String getCommentName() {
            return commentName;
        }

        public void setCommentName(String commentName) {
            this.commentName = commentName;
        }

        public String getCommentIP() {
            return commentIP;
        }

        public void setCommentIP(String commentIP) {
            this.commentIP = commentIP;
        }

        public String getCommentAddress() {
            return commentAddress;
        }

        public void setCommentAddress(String commentAddress) {
            this.commentAddress = commentAddress;
        }

        public String getCommentTime() {
            return commentTime;
        }

        public void setCommentTime(String commentTime) {
            this.commentTime = commentTime;
        }

        public int getCommentZc() {
            return commentZc;
        }

        public void setCommentZc(int commentZc) {
            this.commentZc = commentZc;
        }

        public int getCommentFd() {
            return commentFd;
        }

        public void setCommentFd(int commentFd) {
            this.commentFd = commentFd;
        }

        public String getCommentContent() {
            return commentContent;
        }

        public void setCommentContent(String commentContent) {
            this.commentContent = commentContent;
        }

        public String getCommentReplay() {
            return commentReplay;
        }

        public void setCommentReplay(String commentReplay) {
            this.commentReplay = commentReplay;
        }

        public int getCommentLevel() {
            return commentLevel;
        }

        public void setCommentLevel(int commentLevel) {
            this.commentLevel = commentLevel;
        }

        public String getCommentLabel() {
            return commentLabel;
        }

        public void setCommentLabel(String commentLabel) {
            this.commentLabel = commentLabel;
        }

        public String getCommentImages() {
            return commentImages;
        }

        public void setCommentImages(String commentImages) {
            this.commentImages = commentImages;
        }

        public int getCommentStatus() {
            return commentStatus;
        }

        public void setCommentStatus(int commentStatus) {
            this.commentStatus = commentStatus;
        }

        public int getMemberId() {
            return memberId;
        }

        public void setMemberId(int memberId) {
            this.memberId = memberId;
        }

        public String getMemberFace() {
            return memberFace;
        }

        public void setMemberFace(String memberFace) {
            this.memberFace = memberFace;
        }

        public String getMemberLevel() {
            return memberLevel;
        }

        public void setMemberLevel(String memberLevel) {
            this.memberLevel = memberLevel;
        }
    }
}
