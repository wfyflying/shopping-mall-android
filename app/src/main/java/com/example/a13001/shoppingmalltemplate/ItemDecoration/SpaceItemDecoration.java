package com.example.a13001.shoppingmalltemplate.ItemDecoration;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class SpaceItemDecoration extends RecyclerView.ItemDecoration{
    private int space;
    private int leftspace;

    public SpaceItemDecoration(int space, int leftspace) {
        this.space = space;
        this.leftspace = leftspace;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
//        outRect.left=space;
        outRect.bottom=space;
        //由于每行都只有3个，所以第一个都是3的倍数，把左边距设为0
        if (parent.getChildLayoutPosition(view) %2==0) {

        }else{
            outRect.left = leftspace;
        }
//        super.getItemOffsets(outRect, view, parent, state);
    }
}
