package com.example.a13001.shoppingmalltemplate.modle;

public class LoginStatus {

    /**
     * status : 1
     * returnMsg : null
     * memberId : 100013
     * memberFace :
     * memberName : flying
     * memberMobile :
     * memberMail :
     * memberZsname : flying
     * memberCenter : //www.tjsrsh.site.ify2.cn/member/center/
     * memberQuit : //www.tjsrsh.site.ify2.cn/member/json.aspx?action=clear
     * memberQQlogin : null
     * memberWxlogin : null
     * memberSinalogin : null
     */

    private int status;
    private Object returnMsg;
    private int memberId;
    private String memberFace;
    private String memberName;
    private String memberMobile;
    private String memberMail;
    private String memberZsname;
    private String memberCenter;
    private String memberQuit;
    private Object memberQQlogin;
    private Object memberWxlogin;
    private Object memberSinalogin;

    @Override
    public String toString() {
        return "LoginStatus{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", memberId=" + memberId +
                ", memberFace='" + memberFace + '\'' +
                ", memberName='" + memberName + '\'' +
                ", memberMobile='" + memberMobile + '\'' +
                ", memberMail='" + memberMail + '\'' +
                ", memberZsname='" + memberZsname + '\'' +
                ", memberCenter='" + memberCenter + '\'' +
                ", memberQuit='" + memberQuit + '\'' +
                ", memberQQlogin=" + memberQQlogin +
                ", memberWxlogin=" + memberWxlogin +
                ", memberSinalogin=" + memberSinalogin +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public String getMemberFace() {
        return memberFace;
    }

    public void setMemberFace(String memberFace) {
        this.memberFace = memberFace;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberMobile() {
        return memberMobile;
    }

    public void setMemberMobile(String memberMobile) {
        this.memberMobile = memberMobile;
    }

    public String getMemberMail() {
        return memberMail;
    }

    public void setMemberMail(String memberMail) {
        this.memberMail = memberMail;
    }

    public String getMemberZsname() {
        return memberZsname;
    }

    public void setMemberZsname(String memberZsname) {
        this.memberZsname = memberZsname;
    }

    public String getMemberCenter() {
        return memberCenter;
    }

    public void setMemberCenter(String memberCenter) {
        this.memberCenter = memberCenter;
    }

    public String getMemberQuit() {
        return memberQuit;
    }

    public void setMemberQuit(String memberQuit) {
        this.memberQuit = memberQuit;
    }

    public Object getMemberQQlogin() {
        return memberQQlogin;
    }

    public void setMemberQQlogin(Object memberQQlogin) {
        this.memberQQlogin = memberQQlogin;
    }

    public Object getMemberWxlogin() {
        return memberWxlogin;
    }

    public void setMemberWxlogin(Object memberWxlogin) {
        this.memberWxlogin = memberWxlogin;
    }

    public Object getMemberSinalogin() {
        return memberSinalogin;
    }

    public void setMemberSinalogin(Object memberSinalogin) {
        this.memberSinalogin = memberSinalogin;
    }
}
