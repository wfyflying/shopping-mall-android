package com.example.a13001.shoppingmalltemplate.modle;

public class GoodsEvaluateDetail {


    /**
     * status : 1
     * returnMsg : null
     * CartId : 100002
     * commodityId : 125
     * CartImg : //file.site.ify2.cn/site/153/upload/sccs/upload/201801/2018112715555421.jpg
     * orderDate : 2017-07-20 17:35
     * CommodityName : 我是商品
     * commodityLink : //www.tjsrsh.site.ify2.cn/channel/content.aspx?id=125&preview=3a07231246048d9d66721a8819215f54
     * CommodityProperty : 颜色:蓝色,尺寸:16
     * CommodityNumber : 2
     * cid : 0
     * commentContent : 三效合一的好产品。
     * commentTime : 2017-04-11 21:41
     * commentLevel : 2
     * commentPQ : 好用|省水
     * commentImages : //file.site.ify2.cn/site/153/upload/member/100029/201704/8d991b00-576a-4689-a889-fe28e54e9126.jpg|//file.site.ify2.cn/site/153/upload/member/100029/201704/07308dc6-d91f-4ebe-96b5-16f39da38686.jpg
     */

    private int status;
    private String returnMsg;
    private int CartId;
    private int commodityId;
    private String CartImg;
    private String orderDate;
    private String CommodityName;
    private String commodityLink;
    private String CommodityProperty;
    private int CommodityNumber;
    private int cid;
    private String commentContent;
    private String commentTime;
    private int commentLevel;
    private String commentPQ;
    private String commentImages;

    @Override
    public String toString() {
        return "GoodsEvaluateDetail{" +
                "status=" + status +
                ", returnMsg='" + returnMsg + '\'' +
                ", CartId=" + CartId +
                ", commodityId=" + commodityId +
                ", CartImg='" + CartImg + '\'' +
                ", orderDate='" + orderDate + '\'' +
                ", CommodityName='" + CommodityName + '\'' +
                ", commodityLink='" + commodityLink + '\'' +
                ", CommodityProperty='" + CommodityProperty + '\'' +
                ", CommodityNumber=" + CommodityNumber +
                ", cid=" + cid +
                ", commentContent='" + commentContent + '\'' +
                ", commentTime='" + commentTime + '\'' +
                ", commentLevel=" + commentLevel +
                ", commentPQ='" + commentPQ + '\'' +
                ", commentImages='" + commentImages + '\'' +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getCartId() {
        return CartId;
    }

    public void setCartId(int CartId) {
        this.CartId = CartId;
    }

    public int getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(int commodityId) {
        this.commodityId = commodityId;
    }

    public String getCartImg() {
        return CartImg;
    }

    public void setCartImg(String CartImg) {
        this.CartImg = CartImg;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getCommodityName() {
        return CommodityName;
    }

    public void setCommodityName(String CommodityName) {
        this.CommodityName = CommodityName;
    }

    public String getCommodityLink() {
        return commodityLink;
    }

    public void setCommodityLink(String commodityLink) {
        this.commodityLink = commodityLink;
    }

    public String getCommodityProperty() {
        return CommodityProperty;
    }

    public void setCommodityProperty(String CommodityProperty) {
        this.CommodityProperty = CommodityProperty;
    }

    public int getCommodityNumber() {
        return CommodityNumber;
    }

    public void setCommodityNumber(int CommodityNumber) {
        this.CommodityNumber = CommodityNumber;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public String getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(String commentTime) {
        this.commentTime = commentTime;
    }

    public int getCommentLevel() {
        return commentLevel;
    }

    public void setCommentLevel(int commentLevel) {
        this.commentLevel = commentLevel;
    }

    public String getCommentPQ() {
        return commentPQ;
    }

    public void setCommentPQ(String commentPQ) {
        this.commentPQ = commentPQ;
    }

    public String getCommentImages() {
        return commentImages;
    }

    public void setCommentImages(String commentImages) {
        this.commentImages = commentImages;
    }
}
