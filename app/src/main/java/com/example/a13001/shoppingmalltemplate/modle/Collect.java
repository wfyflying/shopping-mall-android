package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class Collect {

    /**
     * status : 1
     * returnMsg : null
     * count : 0
     * pagesize : 10
     * pageindex : 1
     * list : [{"collectlId":33,"commodityId":3,"commodityUrl":"3","collectDate":"2018-06-13 13:14","collectName":"手工牛轧糖","collectImg":"//file.site.ify.cn/site/144/upload/spzs/upload/201701/2017171221372511.jpg"}]
     */

    private int status;
    private Object returnMsg;
    private int count;
    private int pagesize;
    private int pageindex;
    private List<ListBean> list;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public int getPageindex() {
        return pageindex;
    }

    public void setPageindex(int pageindex) {
        this.pageindex = pageindex;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * collectlId : 33
         * commodityId : 3
         * commodityUrl : 3
         * collectDate : 2018-06-13 13:14
         * collectName : 手工牛轧糖
         * collectImg : //file.site.ify.cn/site/144/upload/spzs/upload/201701/2017171221372511.jpg
         */

        private int collectlId;
        private int commodityId;
        private String commodityUrl;
        private String collectDate;
        private String collectName;
        private String collectImg;

        public int getCollectlId() {
            return collectlId;
        }

        public void setCollectlId(int collectlId) {
            this.collectlId = collectlId;
        }

        public int getCommodityId() {
            return commodityId;
        }

        public void setCommodityId(int commodityId) {
            this.commodityId = commodityId;
        }

        public String getCommodityUrl() {
            return commodityUrl;
        }

        public void setCommodityUrl(String commodityUrl) {
            this.commodityUrl = commodityUrl;
        }

        public String getCollectDate() {
            return collectDate;
        }

        public void setCollectDate(String collectDate) {
            this.collectDate = collectDate;
        }

        public String getCollectName() {
            return collectName;
        }

        public void setCollectName(String collectName) {
            this.collectName = collectName;
        }

        public String getCollectImg() {
            return collectImg;
        }

        public void setCollectImg(String collectImg) {
            this.collectImg = collectImg;
        }
    }
}
