package com.example.a13001.shoppingmalltemplate.modle;

public class Goods {
    private int logo;
    private String title;
    private String price;
    private String loveNum;
    public boolean isChoosed;
    public boolean isCheck = false;

    public boolean isChoosed() {
        return isChoosed;
    }

    public void setChoosed(boolean choosed) {
        isChoosed = choosed;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public Goods(int logo, String title, String price, String loveNum) {
        this.logo = logo;
        this.title = title;
        this.price = price;
        this.loveNum = loveNum;
    }

    public Goods() {
    }

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLoveNum() {
        return loveNum;
    }

    public void setLoveNum(String loveNum) {
        this.loveNum = loveNum;
    }
}
