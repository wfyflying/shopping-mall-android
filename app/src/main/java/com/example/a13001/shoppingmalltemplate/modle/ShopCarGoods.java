package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class ShopCarGoods {

    /**
     * status : 1
     * returnMsg : null
     * sum : 1
     * list : [{"CartId":100049,"commodityId":21,"CartImg":"//file.site.ify.cn/site/144/upload/spzs/upload/201709/2017930224012581.jpg","CommodityName":"【自留推荐款】充电暖宫打底裤 火爷打底裤","commodityLink":"//849948.net/mobile/channel/content.aspx?id=21&preview=a11ef6ff91a0475efb076204ebcbb68f","CommodityProperty":"颜色:黑色,尺寸:均码","CommodityNumber":1,"CartListID":"999","CommodityXPrice":119,"CommodityZPrice":119}]
     * login : 0
     * totalPrice : 119
     */

    private int status;
    private Object returnMsg;
    private int sum;
    private int login;
    private double totalPrice;
    private List<ListBean> list;

    @Override
    public String toString() {
        return "ShopCarGoods{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", sum=" + sum +
                ", login=" + login +
                ", totalPrice=" + totalPrice +
                ", list=" + list +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getLogin() {
        return login;
    }

    public void setLogin(int login) {
        this.login = login;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * CartId : 100049
         * commodityId : 21
         * CartImg : //file.site.ify.cn/site/144/upload/spzs/upload/201709/2017930224012581.jpg
         * CommodityName : 【自留推荐款】充电暖宫打底裤 火爷打底裤
         * commodityLink : //849948.net/mobile/channel/content.aspx?id=21&preview=a11ef6ff91a0475efb076204ebcbb68f
         * CommodityProperty : 颜色:黑色,尺寸:均码
         * CommodityNumber : 1
         * CartListID : 999
         * CommodityXPrice : 119
         * CommodityZPrice : 119
         */

        private int CartId;                  //购物车ID
        private int commodityId;//商品ID
        private String CartImg;//商品图片地址
        private String CommodityName;//商品名称
        private String commodityLink;//商品详情页链接
        private String CommodityProperty;//商品属性
        private int CommodityNumber;//购买数量
        private String CartListID;//商品货号
        private double CommodityXPrice;//商品单价
        private double CommodityZPrice;//商品总价
        public boolean isChoosed;//
        @Override
        public String toString() {
            return "ListBean{" +
                    "CartId=" + CartId +
                    ", commodityId=" + commodityId +
                    ", CartImg='" + CartImg + '\'' +
                    ", CommodityName='" + CommodityName + '\'' +
                    ", commodityLink='" + commodityLink + '\'' +
                    ", CommodityProperty='" + CommodityProperty + '\'' +
                    ", CommodityNumber=" + CommodityNumber +
                    ", CartListID='" + CartListID + '\'' +
                    ", CommodityXPrice=" + CommodityXPrice +
                    ", CommodityZPrice=" + CommodityZPrice +
                    '}';
        }

        public boolean isChoosed() {
            return isChoosed;
        }

        public void setChoosed(boolean choosed) {
            isChoosed = choosed;
        }

        public int getCartId() {
            return CartId;
        }

        public void setCartId(int CartId) {
            this.CartId = CartId;
        }

        public int getCommodityId() {
            return commodityId;
        }

        public void setCommodityId(int commodityId) {
            this.commodityId = commodityId;
        }

        public String getCartImg() {
            return CartImg;
        }

        public void setCartImg(String CartImg) {
            this.CartImg = CartImg;
        }

        public String getCommodityName() {
            return CommodityName;
        }

        public void setCommodityName(String CommodityName) {
            this.CommodityName = CommodityName;
        }

        public String getCommodityLink() {
            return commodityLink;
        }

        public void setCommodityLink(String commodityLink) {
            this.commodityLink = commodityLink;
        }

        public String getCommodityProperty() {
            return CommodityProperty;
        }

        public void setCommodityProperty(String CommodityProperty) {
            this.CommodityProperty = CommodityProperty;
        }

        public int getCommodityNumber() {
            return CommodityNumber;
        }

        public void setCommodityNumber(int CommodityNumber) {
            this.CommodityNumber = CommodityNumber;
        }

        public String getCartListID() {
            return CartListID;
        }

        public void setCartListID(String CartListID) {
            this.CartListID = CartListID;
        }

        public double getCommodityXPrice() {
            return CommodityXPrice;
        }

        public void setCommodityXPrice(double CommodityXPrice) {
            this.CommodityXPrice = CommodityXPrice;
        }

        public double getCommodityZPrice() {
            return CommodityZPrice;
        }

        public void setCommodityZPrice(double CommodityZPrice) {
            this.CommodityZPrice = CommodityZPrice;
        }
    }
}
