package com.example.a13001.shoppingmalltemplate.activitys;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.ZprogressHud.ZProgressHUD;
import com.example.a13001.shoppingmalltemplate.adapters.GoodsEvaluateListLvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.modle.GoodsEvaluateDetail;
import com.example.a13001.shoppingmalltemplate.modle.GoodsEvaluateList;
import com.example.a13001.shoppingmalltemplate.mvpview.GoodsEvaluateView;
import com.example.a13001.shoppingmalltemplate.presenter.GoodsEvaluatePredenter;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GoodsEvaluateListActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.view1)
    View view1;
    @BindView(R.id.view2)
    View view2;
    @BindView(R.id.view3)
    View view3;
    @BindView(R.id.tv_title_right)
    TextView tvTitleRight;
    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;
    @BindView(R.id.tv_goodsevaluatelist_all)
    TextView tvGoodsevaluatelistAll;
    @BindView(R.id.tv_goodsevaluatelist_daipingjia)
    TextView tvGoodsevaluatelistDaipingjia;
    @BindView(R.id.tv_goodsevaluatelist_yipingjia)
    TextView tvGoodsevaluatelistYipingjia;
    @BindView(R.id.lv_goodsevaluatelist)
    ListView lvGoodsevaluatelist;
    @BindView(R.id.srfl_goodsevaluatelist)
    SmartRefreshLayout srflGoodsevaluatelist;
    private static final String TAG = "GoodsEvaluateListActivi";
    private GoodsEvaluatePredenter goodsEvaluatePredenter = new GoodsEvaluatePredenter(GoodsEvaluateListActivity.this);
    private List<GoodsEvaluateList.OrderGoodsListBean> mList;
    private GoodsEvaluateListLvAdapter mAdapter;
    private String code;
    private String timeStamp;
    private int pageindex = 1;
    private String status;
    private ZProgressHUD zProgressHUD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods_evaluate_list);
        ButterKnife.bind(this);
        String safetyCode = MyUtils.getMetaValue(GoodsEvaluateListActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        timeStamp = Utils.getTimeStamp();
        tvTitleCenter.setText("商品评价");
        goodsEvaluatePredenter.onCreate();
        goodsEvaluatePredenter.attachView(goodsEvaluateView);
        mList = new ArrayList<>();
        mAdapter = new GoodsEvaluateListLvAdapter(GoodsEvaluateListActivity.this, mList);
        lvGoodsevaluatelist.setAdapter(mAdapter);

        zProgressHUD = new ZProgressHUD(GoodsEvaluateListActivity.this);
        zProgressHUD.show();

            tvGoodsevaluatelistAll.performClick();

        setRefresh();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    GoodsEvaluateView goodsEvaluateView = new GoodsEvaluateView() {

        @Override
        public void onSuccessGetGoodsEvaluateList(GoodsEvaluateList goodsEvaluateList) {
            Log.e(TAG, "onSuccessGetGoodsEvaluateList: "+goodsEvaluateList.toString() );
            zProgressHUD.dismiss();
            int status = goodsEvaluateList.getStatus();
            if (status > 0) {
                mList.addAll(goodsEvaluateList.getOrderGoodsList());
                mAdapter.notifyDataSetChanged();
            } else {

            }
        }

        @Override
        public void onSuccessGetGoodsEvaluateDetail(GoodsEvaluateDetail goodsEvaluateDetail) {

        }

        @Override
        public void onError(String result) {
            Log.e(TAG, "onError: " + result);
            zProgressHUD.dismissWithFailure();
        }
    };

    private void setRefresh() {
        //刷新
        srflGoodsevaluatelist.setRefreshHeader(new ClassicsHeader(GoodsEvaluateListActivity.this));
        srflGoodsevaluatelist.setRefreshFooter(new ClassicsFooter(GoodsEvaluateListActivity.this));
//        mRefresh.setEnablePureScrollMode(true);
        srflGoodsevaluatelist.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                pageindex = 1;
                if (mList != null) {
                    mList.clear();
                }
                goodsEvaluatePredenter.getGoodsEvaluateList(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.PAGE_SIZE, pageindex, status , AppConstants.FROM_MOBILE);
                refreshlayout.finishRefresh(2000/*,true*/);//传入false表示刷新失败
            }
        });
        srflGoodsevaluatelist.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                pageindex++;
                goodsEvaluatePredenter.getGoodsEvaluateList(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.PAGE_SIZE, pageindex, status , AppConstants.FROM_MOBILE);
                refreshlayout.finishLoadMore(2000/*,true*/);//传入false表示加载失败
            }
        });
    }

    @OnClick({R.id.iv_title_back, R.id.tv_goodsevaluatelist_all, R.id.tv_goodsevaluatelist_daipingjia, R.id.tv_goodsevaluatelist_yipingjia})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_title_back:
                onBackPressed();
                break;
            //所有评价	查询评价状态，0 待评价 1 已评价，为空则所有
            case R.id.tv_goodsevaluatelist_all:
                initColor();
                tvGoodsevaluatelistAll.setTextColor(getResources().getColor(R.color.ff2828));
                view1.setVisibility(View.VISIBLE);
                if (mList != null) {
                    mList.clear();
                }
                status = "";
                pageindex=1;
                goodsEvaluatePredenter.getGoodsEvaluateList(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.PAGE_SIZE, pageindex, status , AppConstants.FROM_MOBILE);
                lvGoodsevaluatelist.setAdapter(mAdapter);
                break;
            //待评价
            case R.id.tv_goodsevaluatelist_daipingjia:
                initColor();
                tvGoodsevaluatelistDaipingjia.setTextColor(getResources().getColor(R.color.ff2828));
                view2.setVisibility(View.VISIBLE);
                if (mList != null) {
                    mList.clear();
                }
                status = "0";
                pageindex=1;
                goodsEvaluatePredenter.getGoodsEvaluateList(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.PAGE_SIZE, pageindex, status , AppConstants.FROM_MOBILE);
                lvGoodsevaluatelist.setAdapter(mAdapter);
                break;
            //已评价
            case R.id.tv_goodsevaluatelist_yipingjia:
                initColor();
                tvGoodsevaluatelistYipingjia.setTextColor(getResources().getColor(R.color.ff2828));
                view3.setVisibility(View.VISIBLE);
                if (mList != null) {
                    mList.clear();
                }
                status = "1";
                pageindex=1;
                goodsEvaluatePredenter.getGoodsEvaluateList(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.PAGE_SIZE, pageindex, status , AppConstants.FROM_MOBILE);
                lvGoodsevaluatelist.setAdapter(mAdapter);
                break;

        }
    }

    private void initColor() {
        tvGoodsevaluatelistAll.setTextColor(getResources().getColor(R.color.t333));
        tvGoodsevaluatelistDaipingjia.setTextColor(getResources().getColor(R.color.t333));
        tvGoodsevaluatelistYipingjia.setTextColor(getResources().getColor(R.color.t333));

        view1.setVisibility(View.INVISIBLE);
        view2.setVisibility(View.INVISIBLE);
        view3.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        startActivity(new Intent(AfterSaleListActivity.this, MainActivity.class).putExtra("type","AfterSale"));
//        finish();
    }
}
