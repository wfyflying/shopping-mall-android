package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.Collect;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.mcxtzhang.swipemenulib.SwipeMenuLayout;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CollectGvAdapter extends BaseAdapter {
    private Context mContext;
    private List<Collect.ListBean> mList;
    private doDeleteCollectInterface doDeleteCollectInterface;

    public CollectGvAdapter(Context mContext, List<Collect.ListBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    public void setDoDeleteCollectInterface(doDeleteCollectInterface doDeleteCollectInterface) {
        this.doDeleteCollectInterface = doDeleteCollectInterface;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final ViewHolder myViewHolder;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_lv_collect, viewGroup, false);
            myViewHolder = new ViewHolder(view);
            view.setTag(myViewHolder);
            AutoUtils.autoSize(view);
        } else {
            myViewHolder = (ViewHolder) view.getTag();
        }
        GlideUtils.setNetImage(mContext, AppConstants.INTERNET_HEAD + mList.get(position).getCollectImg(), myViewHolder.ivItemcollectLogo);
        myViewHolder.tvItemcollectGoodsname.setText(mList.get(position).getCollectName() != null ? mList.get(position).getCollectName() : "");
        myViewHolder.tvItemcollectTime.setText(mList.get(position).getCollectDate() != null ? mList.get(position).getCollectDate() : "");
        myViewHolder.tvCollectDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myViewHolder.smlItemcollect.quickClose();
                doDeleteCollectInterface.doDelete(position);
            }
        });
        myViewHolder.llContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doDeleteCollectInterface.doStartDetail(position);
            }
        });
        return view;
    }


    public interface doDeleteCollectInterface {
        void doDelete(int position);
        void doStartDetail(int position);
    }



    class ViewHolder {
        @BindView(R.id.iv_itemcollect_logo)
        ImageView ivItemcollectLogo;
        @BindView(R.id.tv_itemcollect_goodsname)
        TextView tvItemcollectGoodsname;
        @BindView(R.id.tv_itemcollect_time)
        TextView tvItemcollectTime;
        @BindView(R.id.ll_content)
        LinearLayout llContent;
        @BindView(R.id.tv_collect_delete)
        TextView tvCollectDelete;
        @BindView(R.id.sml_itemcollect)
        SwipeMenuLayout smlItemcollect;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
