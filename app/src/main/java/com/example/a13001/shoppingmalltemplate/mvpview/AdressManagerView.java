package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.CloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;

public interface AdressManagerView extends View{
    void onSuccess(Address address);
    void onSuccessDeleteAddress(CommonResult commonResult);
    void onSuccessSetDefaultAddress(CommonResult commonResult);
    void onError(String result);
}
