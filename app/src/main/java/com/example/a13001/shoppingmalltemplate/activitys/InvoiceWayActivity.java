package com.example.a13001.shoppingmalltemplate.activitys;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InvoiceWayActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_invoice_no)
    TextView tvInvoiceNo;
    @BindView(R.id.tv_invoice_persion)
    TextView tvInvoicePersion;
    @BindView(R.id.tv_invoice_company)
    TextView tvInvoiceCompany;
    @BindView(R.id.et_invoice_title)
    EditText etInvoiceTitle;
    @BindView(R.id.btn_invoice_commit)
    Button btnInvoiceCommit;
    private String invoiceType="";
    private String invoiceTitle="";
    private String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_way);
        ButterKnife.bind(this);
        tvTitleCenter.setText("发票信息");
        tvInvoiceNo.performClick();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick({R.id.iv_title_back,R.id.tv_invoice_no, R.id.tv_invoice_persion, R.id.tv_invoice_company, R.id.btn_invoice_commit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_title_back:
                onBackPressed();
                break;
                //不开发票
            case R.id.tv_invoice_no:
                etInvoiceTitle.setVisibility(View.GONE);
                initColor();
                tvInvoiceNo.setBackgroundColor(getResources().getColor(R.color.e1));
                invoiceType="不开发票";
                break;
                //个人
            case R.id.tv_invoice_persion:
                etInvoiceTitle.setVisibility(View.VISIBLE);
                initColor();
                tvInvoicePersion.setBackgroundColor(getResources().getColor(R.color.e1));
                invoiceType="个人";

                break;
                //企业
            case R.id.tv_invoice_company:
                etInvoiceTitle.setVisibility(View.VISIBLE);
                initColor();
                tvInvoiceCompany.setBackgroundColor(getResources().getColor(R.color.e1));
                invoiceType="企业";
                break;
                //保存
            case R.id.btn_invoice_commit:
                if (TextUtils.isEmpty(invoiceType)){
                    Toast.makeText(this, "请选择发票类型", Toast.LENGTH_SHORT).show();
                    return;
                }
                if ("不开发票".equals(invoiceType)){

                }else{
                    title = etInvoiceTitle.getText().toString().trim();
                    if (TextUtils.isEmpty(title)){
                        Toast.makeText(this, "发票抬头不能为空", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                Intent intent=new Intent();
                intent.putExtra("invoicetype",invoiceType);
                if (!TextUtils.isEmpty(title)){

                    intent.putExtra("invoicetitle",title);
                }
                setResult(33,intent);
                finish();
                break;
        }
    }

    /**
     * 初始化背景色
     */
    private void initColor() {
        tvInvoiceNo.setBackgroundColor(getResources().getColor(R.color.white));
        tvInvoicePersion.setBackgroundColor(getResources().getColor(R.color.white));
        tvInvoiceCompany.setBackgroundColor(getResources().getColor(R.color.white));
    }


}
