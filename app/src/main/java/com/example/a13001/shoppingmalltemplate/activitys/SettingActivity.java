package com.example.a13001.shoppingmalltemplate.activitys;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.SelfDialog;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.modle.AppConfig;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.mvpview.SettingView;
import com.example.a13001.shoppingmalltemplate.presenter.SettingPredenter;
import com.example.a13001.shoppingmalltemplate.utils.CleanMessageUtil;
import com.example.a13001.shoppingmalltemplate.utils.SPUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import util.UpdateAppUtils;


public class SettingActivity extends BaseActivity {


    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.ll_setting_modifypwd)
    LinearLayout llSettingModifypwd;
    @BindView(R.id.ll_setting_changephone)
    LinearLayout llSettingChangephone;
    @BindView(R.id.ll_setting_clearcache)
    LinearLayout llSettingClearcache;
    @BindView(R.id.ll_setting_aboutus)
    LinearLayout llSettingAboutus;
    @BindView(R.id.ll_setting_versionup)
    LinearLayout llSettingVersionup;
    @BindView(R.id.ll_setting_exitlogin)
    LinearLayout llSettingExitlogin;
    @BindView(R.id.tv_setting_cache)
    TextView tvSettingCache;
    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_right)
    TextView tvTitleRight;
    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;
    @BindView(R.id.ll_setting_modifyusername)
    LinearLayout llSettingModifyusername;
    @BindView(R.id.ll_setting_modifysafepwd)
    LinearLayout llSettingModifysafepwd;
    @BindView(R.id.ll_setting_bindphone)
    LinearLayout llSettingBindphone;
    @BindView(R.id.tv_setting_new)
    TextView tvSettingNew;
    @BindView(R.id.iv_setting_redpoint)
    ImageView ivSettingRedpoint;
    private SelfDialog selfDialog;
    SettingPredenter settingPredenter = new SettingPredenter(SettingActivity.this);
    private static final String TAG = "SettingActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        tvTitleCenter.setText("设置");

        settingPredenter.onCreate();
        settingPredenter.attachView(settingView);
        settingPredenter.getAppConfig1(AppConstants.COMPANY_ID);

    }

    SettingView settingView = new SettingView() {
        @Override
        public void onSuccessDoLoginOut(CommonResult commonResult) {
            Log.e(TAG, "onSuccessDoLoginOut: " + commonResult.toString());
            int status = commonResult.getStatus();
            if (status > 0) {
                SPUtils.remove(SettingActivity.this, AppConstants.USER_NAME);
                SPUtils.remove(SettingActivity.this, AppConstants.USER_PWD);
                SPUtils.remove(SettingActivity.this, "logintype");
                startActivity(new Intent(SettingActivity.this, LoginActivity.class).putExtra("type", "Setting"));
//                ShoppingMallTemplateApplication.getInstance().out();
            } else {

            }

        }

        @Override
        public void onSuccessGetAppConfig(AppConfig appConfig) {
            Log.e(TAG, "onSuccessGetAppConfig: " + appConfig);
            int status = appConfig.getStatus();
            if (status > 0) {
                String appVersion = appConfig.getAppVersion();
                String appAndroidApk = appConfig.getAppAndroidApk();


                UpdateAppUtils.from(SettingActivity.this)
                        .checkBy(UpdateAppUtils.CHECK_BY_VERSION_CODE) //更新检测方式，默认为VersionCode
                        .serverVersionCode(Integer.parseInt(appVersion))
                        .apkPath(AppConstants.INTERNET_HEAD + appAndroidApk)
                        .showNotification(true) //是否显示下载进度到通知栏，默认为true
//                        .updateInfo(info)  //更新日志信息 String
                        .downloadBy(UpdateAppUtils.DOWNLOAD_BY_APP) //下载方式：app下载、手机浏览器下载。默认app下载
                        .isForce(false) //是否强制更新，默认false 强制更新情况下用户不同意更新则不能使用app
                        .update();

            } else {


            }
        }

        @Override
        public void onSuccessGetAppConfig1(AppConfig appConfig) {
            int status = appConfig.getStatus();
            if (status > 0) {
                String appVersion = appConfig.getAppVersion();
                String appAndroidApk = appConfig.getAppAndroidApk();
                int version = Utils.packageCode(SettingActivity.this);
                if (Integer.valueOf(appVersion) > version) {
                    tvSettingNew.setText("有新版本");
//                    ivSettingRedpoint.setVisibility(View.VISIBLE);
                }else{
                    tvSettingNew.setText("已是最新版本");
//                    ivSettingRedpoint.setVisibility(View.GONE);
                }

            } else {


            }
        }

        @Override
        public void onError(String result) {
            Log.e(TAG, "onSuccessDoLoginOut: " + result);
        }
    };

    @OnClick({R.id.iv_title_back, R.id.ll_setting_changephone, R.id.ll_setting_modifypwd, R.id.ll_setting_clearcache
            , R.id.ll_setting_versionup, R.id.ll_setting_exitlogin, R.id.ll_setting_modifyusername,
            R.id.ll_setting_bindphone, R.id.ll_setting_modifysafepwd})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_title_back:
                onBackPressed();
                break;
            case R.id.ll_setting_modifyusername:
                startActivity(new Intent(SettingActivity.this, ModifyUserNameActivity.class));
                break;
            //绑定手机
            case R.id.ll_setting_bindphone:
                startActivity(new Intent(SettingActivity.this, BindPhoneActivity.class));

                break;
            //换绑手机
            case R.id.ll_setting_changephone:
                startActivity(new Intent(SettingActivity.this, ChangePhoneActivity.class));
                break;
            //修改密码
            case R.id.ll_setting_modifypwd:
                startActivity(new Intent(SettingActivity.this, ModifyPwdActivity.class));
                break;
            case R.id.ll_setting_modifysafepwd:
                startActivity(new Intent(SettingActivity.this, SafePwdActivity.class));
                break;
            //清除缓存
            case R.id.ll_setting_clearcache:
                selfDialog = new SelfDialog(SettingActivity.this);
                selfDialog.setTitle("是否清除缓存");
                selfDialog.setYesOnclickListener("确定", new SelfDialog.onYesOnclickListener() {
                    @Override
                    public void onYesClick() {
                        selfDialog.dismiss();
                        CleanMessageUtil.clearAllCache(SettingActivity.this);
                        try {
                            Utils.cleanVideoCacheDir(SettingActivity.this);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        tvSettingCache.setText("0.0KB");
                    }
                });
                selfDialog.setNoOnclickListener("取消", new SelfDialog.onNoOnclickListener() {
                    @Override
                    public void onNoClick() {

                        selfDialog.dismiss();
                    }
                });
                selfDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        backgroundAlpha(1f);
                    }
                });
                backgroundAlpha(0.6f);
                selfDialog.show();
                break;
            //版本更新
            case R.id.ll_setting_versionup:
                settingPredenter.getAppConfig(AppConstants.COMPANY_ID);
                break;
            //退出登录
            case R.id.ll_setting_exitlogin:
                selfDialog = new SelfDialog(SettingActivity.this);
                selfDialog.setTitle("是否退出登录");
                selfDialog.setYesOnclickListener("确定", new SelfDialog.onYesOnclickListener() {
                    @Override
                    public void onYesClick() {
                        selfDialog.dismiss();
//                        SPUtils.remove(SettingActivity.this,"phone");
//                        SPUtils.remove(SettingActivity.this,"password");
//                        SPUtils.remove(SettingActivity.this,"user_id");
                        settingPredenter.doLoginOut(AppConstants.COMPANY_ID);

//                        finish();
                    }
                });
                selfDialog.setNoOnclickListener("取消", new SelfDialog.onNoOnclickListener() {
                    @Override
                    public void onNoClick() {

                        selfDialog.dismiss();
                    }
                });
                selfDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        backgroundAlpha(1f);
                    }
                });
                backgroundAlpha(0.6f);
                selfDialog.show();
                break;
        }
    }

    /**
     * 设置添加屏幕的背景透明度
     *
     * @param bgAlpha
     */
    public void backgroundAlpha(float bgAlpha) {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        getWindow().setAttributes(lp);
    }

    @Override
    public void onBackPressed() {
        setResult(22);
        super.onBackPressed();
    }
}
