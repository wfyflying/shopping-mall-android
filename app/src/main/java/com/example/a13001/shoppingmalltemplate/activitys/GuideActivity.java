package com.example.a13001.shoppingmalltemplate.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.a13001.shoppingmalltemplate.MainActivity;
import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.adapters.GuideVpAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.utils.SPUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GuideActivity extends BaseActivity {


    @BindView(R.id.vp_guide)
    ViewPager vpGuide;
    private GuideVpAdapter mAdapter;
    private List<View> views;
    private Button btnStart;
    private int[] ids=new int[]{R.id.iv1,R.id.iv2,R.id.iv3};
    private ImageView[] imageViews;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
        ButterKnife.bind(this);
        SPUtils.put(GuideActivity.this,AppConstants.FIRST_OPEN,"false");
        initViewPager();
        initPoint();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SPUtils.put(GuideActivity.this,AppConstants.FIRST_OPEN,"false");
        finish();
    }

    /**
     * 初始化底部小圆点
     */
    private void initPoint() {
        imageViews=new ImageView[views.size()];
        for (int i = 0; i < views.size(); i++) {
            imageViews[i]=findViewById(ids[i]);
        }
    }

    private void initViewPager() {
       LayoutInflater inflater= LayoutInflater.from(GuideActivity.this);
        views = new ArrayList<>();
        views.add(inflater.inflate(R.layout.guide_one,null));
        views.add(inflater.inflate(R.layout.guide_two,null));
        views.add(inflater.inflate(R.layout.guide_three,null));

        mAdapter = new GuideVpAdapter(views,GuideActivity.this);
        vpGuide.setAdapter(mAdapter);

        btnStart = views.get(2).findViewById(R.id.btn_guide_start);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GuideActivity.this, MainActivity.class).putExtra("type","guid"));
                finish();
            }
        });
        vpGuide.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < ids.length; i++) {
                    if (position==i){
                        imageViews[i].setImageResource(R.drawable.point_vp);
                    }else{
                        imageViews[i].setImageResource(R.drawable.point_vp_grey);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}
