package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class Recharge {

    /**
     * status : 1
     * returnMsg : null
     * RechargeCount : 2
     * memberRechargeList : [{"rechareJJ":"-","rechargePrice":"1.00","rechareNumber":"","rechargeDate":"2017-07-20 14:57","rechareCaption":"系统自动扣除提现金额"},{"rechareJJ":"+","rechargePrice":"1.00","rechareNumber":"4007892001201707191637666294","rechargeDate":"2017-07-19 22:44","rechareCaption":"微信余额在线充值"}]
     */

    private int status;
    private Object returnMsg;
    private int RechargeCount;
    private List<MemberRechargeListBean> memberRechargeList;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getRechargeCount() {
        return RechargeCount;
    }

    public void setRechargeCount(int RechargeCount) {
        this.RechargeCount = RechargeCount;
    }

    public List<MemberRechargeListBean> getMemberRechargeList() {
        return memberRechargeList;
    }

    public void setMemberRechargeList(List<MemberRechargeListBean> memberRechargeList) {
        this.memberRechargeList = memberRechargeList;
    }

    public static class MemberRechargeListBean {
        /**
         * rechareJJ : -
         * rechargePrice : 1.00
         * rechareNumber :
         * rechargeDate : 2017-07-20 14:57
         * rechareCaption : 系统自动扣除提现金额
         */

        private String rechareJJ;
        private String rechargePrice;
        private String rechareNumber;
        private String rechargeDate;
        private String rechareCaption;

        public String getRechareJJ() {
            return rechareJJ;
        }

        public void setRechareJJ(String rechareJJ) {
            this.rechareJJ = rechareJJ;
        }

        public String getRechargePrice() {
            return rechargePrice;
        }

        public void setRechargePrice(String rechargePrice) {
            this.rechargePrice = rechargePrice;
        }

        public String getRechareNumber() {
            return rechareNumber;
        }

        public void setRechareNumber(String rechareNumber) {
            this.rechareNumber = rechareNumber;
        }

        public String getRechargeDate() {
            return rechargeDate;
        }

        public void setRechargeDate(String rechargeDate) {
            this.rechargeDate = rechargeDate;
        }

        public String getRechareCaption() {
            return rechareCaption;
        }

        public void setRechareCaption(String rechareCaption) {
            this.rechareCaption = rechareCaption;
        }
    }
}
