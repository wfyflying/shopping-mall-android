package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class GoodsEvaluateList {

    /**
     * status : 1
     * returnMsg : null
     * OrderGoodsCount : 2
     * OrderGoodsList : [{"CartId":100002,"commodityId":125,"CartImg":"//file.site.ify2.cn/site/153/upload/sccs/upload/201801/2018112715555421.jpg","orderDate":"2017-07-20 17:35","CommodityName":"我是商品","commodityLink":"//www.tjsrsh.site.ify2.cn/channel/content.aspx?id=125&preview=3a07231246048d9d66721a8819215f54","CommodityProperty":"颜色:蓝色,尺寸:16","CommodityNumber":2,"CommentStatus":0},{"CartId":100003,"commodityId":125,"CartImg":"//file.site.ify2.cn/site/153/upload/sccs/upload/201801/2018112715555421.jpg","orderDate":"2017-07-20 17:35","CommodityName":"我是商品","commodityLink":"//www.tjsrsh.site.ify2.cn/channel/content.aspx?id=125&preview=3a07231246048d9d66721a8819215f54","CommodityProperty":"颜色:蓝色,尺寸:12","CommodityNumber":3,"CommentStatus":0}]
     */

    private int status;
    private String returnMsg;
    private int OrderGoodsCount;
    private List<OrderGoodsListBean> OrderGoodsList;

    @Override
    public String toString() {
        return "GoodsEvaluateList{" +
                "status=" + status +
                ", returnMsg='" + returnMsg + '\'' +
                ", OrderGoodsCount=" + OrderGoodsCount +
                ", OrderGoodsList=" + OrderGoodsList +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getOrderGoodsCount() {
        return OrderGoodsCount;
    }

    public void setOrderGoodsCount(int OrderGoodsCount) {
        this.OrderGoodsCount = OrderGoodsCount;
    }

    public List<OrderGoodsListBean> getOrderGoodsList() {
        return OrderGoodsList;
    }

    public void setOrderGoodsList(List<OrderGoodsListBean> OrderGoodsList) {
        this.OrderGoodsList = OrderGoodsList;
    }

    public static class OrderGoodsListBean {
        /**
         * CartId : 100002
         * commodityId : 125
         * CartImg : //file.site.ify2.cn/site/153/upload/sccs/upload/201801/2018112715555421.jpg
         * orderDate : 2017-07-20 17:35
         * CommodityName : 我是商品
         * commodityLink : //www.tjsrsh.site.ify2.cn/channel/content.aspx?id=125&preview=3a07231246048d9d66721a8819215f54
         * CommodityProperty : 颜色:蓝色,尺寸:16
         * CommodityNumber : 2
         * CommentStatus : 0
         */

        private int CartId;
        private int commodityId;
        private String CartImg;
        private String orderDate;
        private String CommodityName;
        private String commodityLink;
        private String CommodityProperty;
        private int CommodityNumber;
        private int CommentStatus;

        @Override
        public String toString() {
            return "OrderGoodsListBean{" +
                    "CartId=" + CartId +
                    ", commodityId=" + commodityId +
                    ", CartImg='" + CartImg + '\'' +
                    ", orderDate='" + orderDate + '\'' +
                    ", CommodityName='" + CommodityName + '\'' +
                    ", commodityLink='" + commodityLink + '\'' +
                    ", CommodityProperty='" + CommodityProperty + '\'' +
                    ", CommodityNumber=" + CommodityNumber +
                    ", CommentStatus=" + CommentStatus +
                    '}';
        }

        public int getCartId() {
            return CartId;
        }

        public void setCartId(int CartId) {
            this.CartId = CartId;
        }

        public int getCommodityId() {
            return commodityId;
        }

        public void setCommodityId(int commodityId) {
            this.commodityId = commodityId;
        }

        public String getCartImg() {
            return CartImg;
        }

        public void setCartImg(String CartImg) {
            this.CartImg = CartImg;
        }

        public String getOrderDate() {
            return orderDate;
        }

        public void setOrderDate(String orderDate) {
            this.orderDate = orderDate;
        }

        public String getCommodityName() {
            return CommodityName;
        }

        public void setCommodityName(String CommodityName) {
            this.CommodityName = CommodityName;
        }

        public String getCommodityLink() {
            return commodityLink;
        }

        public void setCommodityLink(String commodityLink) {
            this.commodityLink = commodityLink;
        }

        public String getCommodityProperty() {
            return CommodityProperty;
        }

        public void setCommodityProperty(String CommodityProperty) {
            this.CommodityProperty = CommodityProperty;
        }

        public int getCommodityNumber() {
            return CommodityNumber;
        }

        public void setCommodityNumber(int CommodityNumber) {
            this.CommodityNumber = CommodityNumber;
        }

        public int getCommentStatus() {
            return CommentStatus;
        }

        public void setCommentStatus(int CommentStatus) {
            this.CommentStatus = CommentStatus;
        }
    }
}
