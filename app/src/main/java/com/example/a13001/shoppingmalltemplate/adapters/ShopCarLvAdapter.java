package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.Goods;
import com.example.a13001.shoppingmalltemplate.modle.ShopCarGoods;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.mcxtzhang.swipemenulib.SwipeMenuLayout;

import java.util.List;

public class ShopCarLvAdapter extends BaseAdapter{
    private Context mContext;
    private List<ShopCarGoods.ListBean> mList;
    private boolean isShow=false;//是否显示完成编辑
    private ModifyCountInterface modifyCountInterface;//数量变化接口
    private CheckInterface checkInterface;
    private doDeleteMessageInterface doDeleteMessageInterface;
    public ShopCarLvAdapter(Context mContext, List<ShopCarGoods.ListBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }
    public void setDoDeleteMessageInterface(doDeleteMessageInterface doDeleteMessageInterface) {
        this.doDeleteMessageInterface = doDeleteMessageInterface;
    }
    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder=null;
        if (view == null) {
            view= LayoutInflater.from(mContext).inflate(R.layout.item_shopcar_lv,viewGroup,false);
            holder=new ViewHolder(view);
            view.setTag(holder);
        }else{
            holder= (ViewHolder) view.getTag();
        }
        boolean choosed=mList.get(i).isChoosed;
        if (choosed){
            holder.cbSelected.setChecked(true);
        }else{
            holder.cbSelected.setChecked(false);
        }
        GlideUtils.setNetImage(mContext, AppConstants.INTERNET_HEAD+mList.get(i).getCartImg(),holder.ivLogo);
        holder.tvTitle.setText(mList.get(i).getCommodityName()!=null?mList.get(i).getCommodityName():"");
        holder.tvParamter.setText(mList.get(i).getCommodityProperty()!=null?"已选："+mList.get(i).getCommodityProperty():"");
        holder.tvPrice.setText("￥"+mList.get(i).getCommodityXPrice()+"");
        holder.tvNum.setText(mList.get(i).getCommodityNumber()+"");
        holder.tvEditNum.setText(mList.get(i).getCommodityNumber()+"");
        //判断是否在编辑状态下
        if (isShow) {
            holder.llEditNum.setVisibility(View.VISIBLE);
        } else {
            holder.llEditNum.setVisibility(View.GONE);
        }
        //数量加
        final ViewHolder finalHolder = holder;
        holder.tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modifyCountInterface.doIncrease(i, finalHolder.tvEditNum, finalHolder.cbSelected.isChecked());
            }
        });
        //数量减
        final ViewHolder finalHolder1 = holder;
        holder.tvSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modifyCountInterface.doDecrease(i, finalHolder1.tvEditNum, finalHolder1.cbSelected.isChecked());
            }
        });
        //选择框
        holder.cbSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInterface.checkGrour(i,((CheckBox)view).isChecked());
            }
        });
        //删除
        final ViewHolder finalHolder2 = holder;
        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalHolder2.smlShopcar.quickClose();
                doDeleteMessageInterface.doDelete(mList.get(i).getCartId());
            }
        });
        return view;
    }
    /**
     * 遍历list集合,判断是否全选
     * @return
     */
    private boolean isAllCheck(){
        for (ShopCarGoods.ListBean goods:mList){
            if (!goods.isChoosed){
                return false;
            }
        }
        return true;
    }

    /**
     * 复选框接口
     */
    public interface CheckInterface{
        /**
         * 组选框状态改变触发的事件
         *
         * @param position  元素位置
         * @param isChecked 元素选中与否
         */
        void checkGrour(int position,boolean isChecked);
    }
    /**
     * 改变商品数量接口
     * @param modifyCountInterface
     */
    public void setModifyCountInterface(ModifyCountInterface modifyCountInterface){
        this.modifyCountInterface=modifyCountInterface;
    }
    /**
     * 单选接口
     *
     * @param checkInterface
     */
    public void setCheckInterface(CheckInterface checkInterface) {
        this.checkInterface = checkInterface;
    }
    /**
     * 改变数量的接口
     */
    public interface ModifyCountInterface {
        /**
         * 增加操作
         *
         * @param position      元素位置
         * @param showCountView 用于展示变化后数量的View
         * @param isChecked     子元素选中与否
         */
        void doIncrease(int position, View showCountView, boolean isChecked);

        /**
         * 删减操作
         *
         * @param position      元素位置
         * @param showCountView 用于展示变化后数量的View
         * @param isChecked     子元素选中与否
         */
        void doDecrease(int position, View showCountView, boolean isChecked);

        /**
         * 删除子item
         *
         * @param position
         */
        void childDelete(int position);
    }
    /**
     * 是否显示可编辑
     *
     * @param flag
     */
    public void isShow(boolean flag) {
        isShow = flag;
        notifyDataSetChanged();
    }
    class ViewHolder{
        CheckBox cbSelected;
        TextView tvTitle,tvParamter,tvPrice,tvNum,tvAdd,tvSub,tvEditNum,tvDelete;
        ImageView ivLogo;
        LinearLayout llEditNum,llShopcar;
        SwipeMenuLayout smlShopcar;
        public ViewHolder(View itemView){
            cbSelected=itemView.findViewById(R.id.cb_shopcar_select);//选择框
            tvTitle=itemView.findViewById(R.id.tv_shopcar_title);//标题
            tvParamter=itemView.findViewById(R.id.tv_shopcar_parameter);//商品属性
            tvPrice=itemView.findViewById(R.id.tv_shopcar_price);//价格
            tvNum=itemView.findViewById(R.id.tv_shopcar_shownum);//展示商品数量
            tvAdd=itemView.findViewById(R.id.tv_add);//加号
            tvSub=itemView.findViewById(R.id.tv_sub);//减号
            tvEditNum=itemView.findViewById(R.id.tv_shopcar_editnum);//可编辑商品数量
            ivLogo=itemView.findViewById(R.id.iv_shopcar_logo);//商品图片
            llEditNum=itemView.findViewById(R.id.ll_edit_num);//数量编辑布局
            tvDelete=itemView.findViewById(R.id.tv_shopcar_delete);//数量编辑布局
            llShopcar=itemView.findViewById(R.id.ll_shopcar);//数量编辑布局
            smlShopcar=itemView.findViewById(R.id.sml_shopcar);//数量编辑布局

        }
    }
    public interface doDeleteMessageInterface {
        void doDelete(int cartid);

    }
}
