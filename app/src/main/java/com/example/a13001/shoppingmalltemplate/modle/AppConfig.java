package com.example.a13001.shoppingmalltemplate.modle;

public class AppConfig {

    /**
     * status : 1
     * returnMsg : null
     * appName : 商城APP
     * appLogo : //file.site.ify2.cn/site/153/upload/config/appshoplogo.png
     * appAndroidStore :
     * appAndroidApk : //file.site.ify2.cn/site/153/upload/config/andorid.apk
     * appIosStore :
     * appVersion : 1
     * appContent : 商城APP
     * appStatus : 1
     * appAd : {"status":1,"returnMsg":null,"photoId":100024,"photoName":"广告3","photoPath":"//file.site.ify2.cn/site/153/upload/app/201811/2018115183827751.jpg","photoLowPath":"//file.site.ify2.cn/site/153/upload/app/201811/small-2018115183827751.jpg","photoPreviewPath":"//file.site.ify2.cn/site/153/upload/app/201811/preview-2018115183827751.jpg","photoDescript":"广告3","photoSuffix":"jpg","photoDate":"2018-11-05 18:38","photoConType":2,"photoLink":"http://www.tjsrsh.site.ify2.cn/app/article-100024.aspx"}
     */

    private int status;
    private String returnMsg;
    private String appName;
    private String appLogo;
    private String appAndroidStore;
    private String appAndroidApk;
    private String appIosStore;
    private String appVersion;
    private String appContent;
    private String appTongji;
    private int appStatus;
    private AppAdBean appAd;

    @Override
    public String toString() {
        return "AppConfig{" +
                "status=" + status +
                ", returnMsg='" + returnMsg + '\'' +
                ", appName='" + appName + '\'' +
                ", appLogo='" + appLogo + '\'' +
                ", appAndroidStore='" + appAndroidStore + '\'' +
                ", appAndroidApk='" + appAndroidApk + '\'' +
                ", appIosStore='" + appIosStore + '\'' +
                ", appVersion='" + appVersion + '\'' +
                ", appContent='" + appContent + '\'' +
                ", appTongji='" + appTongji + '\'' +
                ", appStatus=" + appStatus +
                ", appAd=" + appAd +
                '}';
    }

    public String getAppTongji() {
        return appTongji;
    }

    public void setAppTongji(String appTongji) {
        this.appTongji = appTongji;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppLogo() {
        return appLogo;
    }

    public void setAppLogo(String appLogo) {
        this.appLogo = appLogo;
    }

    public String getAppAndroidStore() {
        return appAndroidStore;
    }

    public void setAppAndroidStore(String appAndroidStore) {
        this.appAndroidStore = appAndroidStore;
    }

    public String getAppAndroidApk() {
        return appAndroidApk;
    }

    public void setAppAndroidApk(String appAndroidApk) {
        this.appAndroidApk = appAndroidApk;
    }

    public String getAppIosStore() {
        return appIosStore;
    }

    public void setAppIosStore(String appIosStore) {
        this.appIosStore = appIosStore;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getAppContent() {
        return appContent;
    }

    public void setAppContent(String appContent) {
        this.appContent = appContent;
    }

    public int getAppStatus() {
        return appStatus;
    }

    public void setAppStatus(int appStatus) {
        this.appStatus = appStatus;
    }

    public AppAdBean getAppAd() {
        return appAd;
    }

    public void setAppAd(AppAdBean appAd) {
        this.appAd = appAd;
    }

    public static class AppAdBean {
        /**
         * status : 1
         * returnMsg : null
         * photoId : 100024
         * photoName : 广告3
         * photoPath : //file.site.ify2.cn/site/153/upload/app/201811/2018115183827751.jpg
         * photoLowPath : //file.site.ify2.cn/site/153/upload/app/201811/small-2018115183827751.jpg
         * photoPreviewPath : //file.site.ify2.cn/site/153/upload/app/201811/preview-2018115183827751.jpg
         * photoDescript : 广告3
         * photoSuffix : jpg
         * photoDate : 2018-11-05 18:38
         * photoConType : 2
         * photoLink : http://www.tjsrsh.site.ify2.cn/app/article-100024.aspx
         */

        private int status;
        private Object returnMsg;
        private int photoId;
        private String photoName;
        private String photoPath;
        private String photoLowPath;
        private String photoPreviewPath;
        private String photoDescript;
        private String photoSuffix;
        private String photoDate;
        private int photoConType;
        private String photoLink;

        @Override
        public String toString() {
            return "AppAdBean{" +
                    "status=" + status +
                    ", returnMsg=" + returnMsg +
                    ", photoId=" + photoId +
                    ", photoName='" + photoName + '\'' +
                    ", photoPath='" + photoPath + '\'' +
                    ", photoLowPath='" + photoLowPath + '\'' +
                    ", photoPreviewPath='" + photoPreviewPath + '\'' +
                    ", photoDescript='" + photoDescript + '\'' +
                    ", photoSuffix='" + photoSuffix + '\'' +
                    ", photoDate='" + photoDate + '\'' +
                    ", photoConType=" + photoConType +
                    ", photoLink='" + photoLink + '\'' +
                    '}';
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public Object getReturnMsg() {
            return returnMsg;
        }

        public void setReturnMsg(Object returnMsg) {
            this.returnMsg = returnMsg;
        }

        public int getPhotoId() {
            return photoId;
        }

        public void setPhotoId(int photoId) {
            this.photoId = photoId;
        }

        public String getPhotoName() {
            return photoName;
        }

        public void setPhotoName(String photoName) {
            this.photoName = photoName;
        }

        public String getPhotoPath() {
            return photoPath;
        }

        public void setPhotoPath(String photoPath) {
            this.photoPath = photoPath;
        }

        public String getPhotoLowPath() {
            return photoLowPath;
        }

        public void setPhotoLowPath(String photoLowPath) {
            this.photoLowPath = photoLowPath;
        }

        public String getPhotoPreviewPath() {
            return photoPreviewPath;
        }

        public void setPhotoPreviewPath(String photoPreviewPath) {
            this.photoPreviewPath = photoPreviewPath;
        }

        public String getPhotoDescript() {
            return photoDescript;
        }

        public void setPhotoDescript(String photoDescript) {
            this.photoDescript = photoDescript;
        }

        public String getPhotoSuffix() {
            return photoSuffix;
        }

        public void setPhotoSuffix(String photoSuffix) {
            this.photoSuffix = photoSuffix;
        }

        public String getPhotoDate() {
            return photoDate;
        }

        public void setPhotoDate(String photoDate) {
            this.photoDate = photoDate;
        }

        public int getPhotoConType() {
            return photoConType;
        }

        public void setPhotoConType(int photoConType) {
            this.photoConType = photoConType;
        }

        public String getPhotoLink() {
            return photoLink;
        }

        public void setPhotoLink(String photoLink) {
            this.photoLink = photoLink;
        }
    }
}
