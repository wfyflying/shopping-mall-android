package com.example.a13001.shoppingmalltemplate.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.adapters.GoodsListRvAdapter;
import com.example.a13001.shoppingmalltemplate.adapters.SeckillRvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.Goods;
import com.example.a13001.shoppingmalltemplate.modle.GoodsList;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class SecKillActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.rv_seckill)
    RecyclerView rvSeckill;
    private SeckillRvAdapter mAdapter;
    private List<GoodsList.ListBean> mList;
    private static final String TAG = "SecKillActivity";
    private CompositeSubscription mCompositeSubscription;
    private DataManager manager;
    private int pageIndex=1;
    private String elite="";
    private String xinpin="";
    private String classId="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sec_kill);
        ButterKnife.bind(this);
        initData();
    }

    /**
     * 初始化数据源
     */
    private void initData() {
        tvTitleCenter.setText("促销秒杀");
        manager = new DataManager(SecKillActivity.this);
        mCompositeSubscription = new CompositeSubscription();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SecKillActivity.this);
        rvSeckill.setLayoutManager(linearLayoutManager);
        //添加Android自带的分割线
        DividerItemDecoration dividerItemDecoration=new DividerItemDecoration(this,DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(this,R.drawable.divider));
        rvSeckill.addItemDecoration(dividerItemDecoration);
        mList=new ArrayList<>();
        mAdapter=new SeckillRvAdapter(SecKillActivity.this,mList);
        getGoodList();
        rvSeckill.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new GoodsListRvAdapter.onItemClickListener() {
            @Override
            public void onClick(int position) {
                Intent intent=new Intent(SecKillActivity.this,GoodsDetailActivity.class);
                intent.putExtra("good_id",mList.get(position).getId());
                intent.putExtra("class_id",mList.get(position).getClassid());
                intent.putExtra("type","促销秒杀");

                startActivity(intent);
            }
        });
    }

    /**
     * 获取促销秒杀商品列表
     */
    private void getGoodList() {

        mCompositeSubscription.add(manager.getGoodsList(AppConstants.COMPANY_ID, AppConstants.SECLILL_CHANEL_ID, classId,elite,"",xinpin,"","","","",
                "","","",AppConstants.PAGE_SIZE,pageIndex)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GoodsList>() {
                    @Override
                    public void onCompleted() {
                        Log.e(TAG, "onCompleted: " );
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: "+e.toString() );
                    }

                    @Override
                    public void onNext(GoodsList goodsList) {
                        Log.e(TAG, "onNext: "+goodsList.toString());
                        if (1 == goodsList.getStatus()) {
                            mList.addAll(goodsList.getList());
                            mAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(SecKillActivity.this, ""+goodsList.getReturnMsg(), Toast.LENGTH_SHORT).show();
                        }

                    }
                }));
    }
    /**
     * 各控件的点击事件
     *
     * @param view
     */
    @OnClick(R.id.iv_title_back)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_title_back:
                onBackPressed();
                break;
        }
    }
}
