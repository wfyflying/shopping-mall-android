package com.example.a13001.shoppingmalltemplate.event;

public class AddShopCarEvent {
        private String mMsg;

    public AddShopCarEvent(String msg) {
        this.mMsg = msg;
    }
    public String getMsg(){
        return mMsg;
    }
}
