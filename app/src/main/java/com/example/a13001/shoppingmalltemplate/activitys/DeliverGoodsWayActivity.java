package com.example.a13001.shoppingmalltemplate.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.adapters.DeliverWayLvAdapter;
import com.example.a13001.shoppingmalltemplate.adapters.PaymentWayLvAdapter;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.modle.CloaseAccount;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DeliverGoodsWayActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.lv_delivergoods)
    ListView lvDelivergoods;
    private List<CloaseAccount.ExpressBean> mList = new ArrayList<>();
    private DeliverWayLvAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deliver_goods_way);
        ButterKnife.bind(this);
        tvTitleCenter.setText("送货方式");
        List<CloaseAccount.ExpressBean> list = (List<CloaseAccount.ExpressBean>) getIntent().getSerializableExtra("deliverway");
        mList.addAll(list);
        mAdapter=new DeliverWayLvAdapter(DeliverGoodsWayActivity.this,mList);
        lvDelivergoods.setAdapter(mAdapter);
        lvDelivergoods.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent=new Intent();
                intent.putExtra("deliverwayobj",mList.get(i));
                setResult(22,intent);
                finish();
            }
        });
    }

    @OnClick(R.id.iv_title_back)
    public void onViewClicked() {
        onBackPressed();
    }
}
