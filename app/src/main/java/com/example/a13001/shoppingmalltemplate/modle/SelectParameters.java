package com.example.a13001.shoppingmalltemplate.modle;

public class SelectParameters {
    private int goodId;
    private String parameter1;
    private String parameter2;
    private int position1;
    private int position2;
    private String price;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getGoodId() {
        return goodId;
    }

    public void setGoodId(int goodId) {
        this.goodId = goodId;
    }

    public String getParameter1() {
        return parameter1;
    }

    public void setParameter1(String parameter1) {
        this.parameter1 = parameter1;
    }

    public String getParameter2() {
        return parameter2;
    }

    public void setParameter2(String parameter2) {
        this.parameter2 = parameter2;
    }

    public int getPosition1() {
        return position1;
    }

    public void setPosition1(int position1) {
        this.position1 = position1;
    }

    public int getPosition2() {
        return position2;
    }

    public void setPosition2(int position2) {
        this.position2 = position2;
    }

    @Override
    public String toString() {
        return "SelectParameters{" +
                "goodId=" + goodId +
                ", parameter1='" + parameter1 + '\'' +
                ", parameter2='" + parameter2 + '\'' +
                ", position1=" + position1 +
                ", position2=" + position2 +
                ", price='" + price + '\'' +
                '}';
    }
}
