package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.AnswerList;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;

public interface QuestionAndAnswerView extends View{
    void onSuccessCommitComment(CommonResult commonResult);
    void onSuccessGetCommentList(AnswerList answerList);
    void onError(String result);
}
