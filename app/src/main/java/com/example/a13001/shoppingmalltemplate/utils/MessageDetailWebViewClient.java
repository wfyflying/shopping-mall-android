package com.example.a13001.shoppingmalltemplate.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;


/**
 * Created by Administrator on 2017/3/6.
 */

public class MessageDetailWebViewClient extends WebViewClient {
    Context obj;
    //加载JSON需要参数
    String JsonUrl;
    String companyid="";
    String code="";
    String timestamp="";
    int smsid=0;
    String contentType="";

    public MessageDetailWebViewClient(Context context, String companyid, String code, String timestamp, int smsid, String contentType){
        super();
        this.obj=context;
        this.companyid=companyid;
        this.code=code;
        this.timestamp=timestamp;
        this.smsid=smsid;
        this.JsonUrl=MyUtils.getMetaValue(context, "companyURL");
        this.contentType=contentType;

    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        super.shouldOverrideUrlLoading(view, url);
        if (url.startsWith("tel:")) {
            //调用手机拨号盘
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            obj.startActivity(intent);
        } else if (url.startsWith("smsto:")) {
            //调用短信发送
            Uri uri = Uri.parse(url);
            Intent it = new Intent(Intent.ACTION_SENDTO, uri);
            it.putExtra("sms_body", "");
            obj.startActivity(it);
        } else{
            //判断网络是否正常
//            if (!Utils.isNetworkAvailable(obj)){
//                //弹出提示窗口
//                Toast.makeText(obj, obj.getResources().getString(R.string.error002), Toast.LENGTH_SHORT).show();
//                view.loadUrl("file:///android_asset/404.html");//加载404错误页面
//            } else {
//                if (contentType.length()>0 && contentType.equals("adwebview")){
//                    //广告页允许超链接
//                    view.loadUrl(url);
//                }
//            }
        }
        return true;
    }

    @Override
    public  void onPageFinished(WebView view, String url){
        super.onPageFinished(view, url);
        //如果默认标题为空的话，就使用网页标题
//        if (TextUtils.isEmpty(ThisTitle)) {
//            toolbar.setTitle(view.getTitle());
//        }
        Log.v("Window","contentType:" + contentType + " url:" + url);
        if (contentType.length()>0 && contentType.equals("webview")){
            //页面加载完皆后执行指定命令
            Log.e("Window","contentType:javascript:GetJson('" + JsonUrl + "','" + companyid + "','" + code + "','"+ timestamp + "','"+ smsid  + "');");
            view.loadUrl("javascript:getJSON('" + JsonUrl + "','" + companyid + "','" + code + "','"+ timestamp + "','"+ smsid  + "');");
            //appendJs(view);

        }
    }

    @Override
    public  void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        super.onReceivedError(view, request, error);
        view.stopLoading();
//        view.loadUrl("file:///android_asset/404.html");//加载404错误页面
    }


}
