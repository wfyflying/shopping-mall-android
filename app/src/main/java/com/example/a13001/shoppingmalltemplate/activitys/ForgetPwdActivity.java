package com.example.a13001.shoppingmalltemplate.activitys;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.mvpview.ForgetPwdView;
import com.example.a13001.shoppingmalltemplate.presenter.ForgetPwdPredenter;
import com.example.a13001.shoppingmalltemplate.utils.CodeUtils;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgetPwdActivity extends BaseActivity {

    @BindView(R.id.btn_forgetpwd_sendcode)
    Button btnForgetpwdSendcode;
    @BindView(R.id.btn_forgetpwd_commit)
    Button btnForgetpwdCommit;
    @BindView(R.id.login_phone_et)
    EditText loginPhoneEt;
    @BindView(R.id.login_pw_et)
    EditText loginPwEt;
    @BindView(R.id.et_register_piccode)
    EditText etRegisterPiccode;
    @BindView(R.id.iv_code)
    ImageView ivCode;
    @BindView(R.id.et_forgetpwd_phonecode)
    EditText etForgetpwdPhonecode;
    @BindView(R.id.et_forgetpwd_newpwd)
    EditText etForgetpwdNewpwd;
    @BindView(R.id.imageView4)
    ImageView imageView4;
    private TimeCount timeCount;
    private String code;
    private String mTimestamp;
    private ForgetPwdPredenter forgetPwdPredenter = new ForgetPwdPredenter(ForgetPwdActivity.this);
    private static final String TAG = "ForgetPwdActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pwd);
        ButterKnife.bind(this);
        timeCount = new TimeCount(60000, 1000);
        forgetPwdPredenter.onCreate();
        forgetPwdPredenter.attachView(forgetPwdView);

        String safetyCode = MyUtils.getMetaValue(ForgetPwdActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        mTimestamp = Utils.getTimeStamp();


        final CodeUtils codeUtils = CodeUtils.getInstance();
        Bitmap bitmap = codeUtils.createBitmap();
        ivCode.setImageBitmap(bitmap);
//        GlideUtils.setNetImage(RegisterActivity.this,"https://jiujiukeji002.qkk.cn/ValidateCode.aspx?size=2",ivCode);
        ivCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CodeUtils codeUtils = CodeUtils.getInstance();
                Bitmap bitmap = codeUtils.createBitmap();
                ivCode.setImageBitmap(bitmap);
//                GlideUtils.setNetImageNoCache(RegisterActivity.this,"https://jiujiukeji002.qkk.cn/ValidateCode.aspx?size=2",ivCode);
            }
        });

    }

    ForgetPwdView forgetPwdView = new ForgetPwdView() {
        @Override
        public void onSuccessDoResetPwd(CommonResult commonResult) {
            Log.e(TAG, "onSuccessDoResetPwd: " + commonResult.toString());
            int status = commonResult.getStatus();
            if (status > 0) {
                Toast.makeText(ForgetPwdActivity.this, "密码重置成功，请使用新密码登陆", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(ForgetPwdActivity.this, ""+commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onSuccessGetPicCode(CommonResult commonResult) {
            Log.e(TAG, "onSuccessGetPicCode: " + commonResult.toString());
            int status = commonResult.getStatus();
            if (status > 0) {
                String phone = loginPhoneEt.getText().toString().trim();
                forgetPwdPredenter.getPhoneCode(AppConstants.COMPANY_ID, code, mTimestamp, phone, commonResult.getCode());
            } else {
                Toast.makeText(ForgetPwdActivity.this, "" + commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onSuccessGetPhoneCode(CommonResult commonResult) {
            Log.e(TAG, "onSuccessGetPhoneCode: " + commonResult.toString());
            timeCount.start();
            int status = commonResult.getStatus();
            if (status > 0) {
                timeCount.start();
                Toast.makeText(ForgetPwdActivity.this, "发送成功，请查看您的手机短信获取校验码", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(ForgetPwdActivity.this, "" + commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onError(String result) {
            Log.e(TAG, "onError: " + result);
        }
    };

    @OnClick({R.id.iv_forgetpwd_back, R.id.btn_forgetpwd_sendcode, R.id.btn_forgetpwd_commit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_forgetpwd_back:
                onBackPressed();
                break;
            //获取验证码
            case R.id.btn_forgetpwd_sendcode:
                forgetPwdPredenter.getPicCode(AppConstants.COMPANY_ID, code, mTimestamp);
                break;
            //提交
            case R.id.btn_forgetpwd_commit:
                String phoneCode=etForgetpwdPhonecode.getText().toString().trim();
               String pwd= etForgetpwdNewpwd.getText().toString().trim();
               String pwds=loginPwEt.getText().toString().trim();
                String piccode = CodeUtils.getInstance().getCode();
                String piccode1 = etRegisterPiccode.getText().toString().trim();
                if (TextUtils.isEmpty(phoneCode)) {
                    Toast.makeText(this, "请输入验证码", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(pwd)) {
                    Toast.makeText(this, "请输入新密码", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(pwds)) {
                    Toast.makeText(this, "请输入确认密码", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(piccode1)) {
                    Toast.makeText(this, "请输入图形验证码", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!piccode.equalsIgnoreCase(piccode1)) {
                    Toast.makeText(this, "图形验证码错误，请重新输入", Toast.LENGTH_SHORT).show();
                    return;
                }
                forgetPwdPredenter.doResetPwd(AppConstants.COMPANY_ID, code, mTimestamp,phoneCode,pwd,pwds );
                break;
        }
    }

    /**
     * 倒计时类
     */
    class TimeCount extends CountDownTimer {

        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            // mBtnGetcode.setBackgroundColor(Color.parseColor("#B6B6D8"));
            btnForgetpwdSendcode.setClickable(false);
            btnForgetpwdSendcode.setText("" + millisUntilFinished / 1000);
        }

        @Override
        public void onFinish() {
            btnForgetpwdSendcode.setText("获取验证码");
            btnForgetpwdSendcode.setClickable(true);
            //  mBtnGetcode.setBackgroundColor(Color.parseColor("#4EB84A"));
        }
    }
}
