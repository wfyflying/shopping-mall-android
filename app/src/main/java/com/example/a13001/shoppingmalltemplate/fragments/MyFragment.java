package com.example.a13001.shoppingmalltemplate.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.CircleImageView;
import com.example.a13001.shoppingmalltemplate.View.UserSettingView;
import com.example.a13001.shoppingmalltemplate.ZprogressHud.ZProgressHUD;
import com.example.a13001.shoppingmalltemplate.activitys.AccountBalanceActivity;
import com.example.a13001.shoppingmalltemplate.activitys.AdressManagerActivity;
import com.example.a13001.shoppingmalltemplate.activitys.AfterSaleListActivity;
import com.example.a13001.shoppingmalltemplate.activitys.CollectActivity;
import com.example.a13001.shoppingmalltemplate.activitys.GoodsEvaluateListActivity;
import com.example.a13001.shoppingmalltemplate.activitys.InformActivity;
import com.example.a13001.shoppingmalltemplate.activitys.IntegralOrderListActivity;
import com.example.a13001.shoppingmalltemplate.activitys.LoginActivity;
import com.example.a13001.shoppingmalltemplate.activitys.MessageActivity;
import com.example.a13001.shoppingmalltemplate.activitys.ModifyUserNameActivity;
import com.example.a13001.shoppingmalltemplate.activitys.MyCouponListActivity;
import com.example.a13001.shoppingmalltemplate.activitys.MyIntegralActivity;
import com.example.a13001.shoppingmalltemplate.activitys.OrderListActivity;
import com.example.a13001.shoppingmalltemplate.activitys.SettingActivity;
import com.example.a13001.shoppingmalltemplate.activitys.UserInfoActivity;
import com.example.a13001.shoppingmalltemplate.application.ShoppingMallTemplateApplication;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.LoginStatus;
import com.example.a13001.shoppingmalltemplate.modle.OrderNum;
import com.example.a13001.shoppingmalltemplate.modle.SignIn;
import com.example.a13001.shoppingmalltemplate.modle.UserInfo;
import com.example.a13001.shoppingmalltemplate.mvpview.MyView;
import com.example.a13001.shoppingmalltemplate.presenter.MyPredenter;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.tv_my_allorder)
    TextView tvMyAllorder;
    Unbinder unbinder;
    @BindView(R.id.civ_minef_headimg)
    CircleImageView civMinefHeadimg;

    @BindView(R.id.tv_my_setting)
    TextView tvMySetting;
    @BindView(R.id.usv_my_address)
    UserSettingView usvMyAddress;
    @BindView(R.id.tv_my_signin)
    TextView tvMySignin;
    @BindView(R.id.tv_minef_username)
    TextView tvMinefUsername;
    @BindView(R.id.tv_my_rank)
    TextView tvMyRank;
    @BindView(R.id.ll_mycenterf_obligation)
    LinearLayout llMycenterfObligation;
    @BindView(R.id.ll_mycenterf_shipments)
    LinearLayout llMycenterfShipments;
    @BindView(R.id.ll_mycenterf_takegoods)
    LinearLayout llMycenterfTakegoods;
    @BindView(R.id.ll_mycenterf_together)
    LinearLayout llMycenterfTogether;
    @BindView(R.id.ll_mycenterf_refund)
    LinearLayout llMycenterfRefund;
    @BindView(R.id.usv_my_grouporder)
    UserSettingView usvMyGrouporder;
    @BindView(R.id.usv_my_userinfo)
    UserSettingView usvMyUserinfo;
    @BindView(R.id.usv_my_collect)
    UserSettingView usvMyCollect;
    @BindView(R.id.usv_my_message)
    UserSettingView usvMyMessage;
    @BindView(R.id.usv_my_inform)
    UserSettingView usvMyInform;
    @BindView(R.id.tv_myf_mycredit)
    TextView tvMyfMycredit;
    @BindView(R.id.ll_myf_mycredit)
    LinearLayout llMyfMycredit;
    @BindView(R.id.tv_myf_zmoney)
    TextView tvMyfZmoney;
    @BindView(R.id.ll_myf_zmoney)
    LinearLayout llMyfZmoney;
    @BindView(R.id.tv_myf_ymoney)
    TextView tvMyfYmoney;
    @BindView(R.id.ll_myf_ymoney)
    LinearLayout llMyfYmoney;
    @BindView(R.id.iv_minef_edit)
    ImageView ivMinefEdit;
    @BindView(R.id.usv_my_integralorder)
    UserSettingView usvMyIntegralorder;
    @BindView(R.id.usv_my_shangpinpingjia)
    UserSettingView usvMyShangpinpingjia;
    @BindView(R.id.tv_num_daifuk)
    TextView tvNumDaifuk;
    @BindView(R.id.tv_num_daifahuo)
    TextView tvNumDaifahuo;
    @BindView(R.id.tv_num_daishouhuo)
    TextView tvNumDaishouhuo;
    @BindView(R.id.tv_num_yiqianshou)
    TextView tvNumYiqianshou;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private MyPredenter myPredenter = new MyPredenter(getActivity());
    private String code;//安全码
    private String timeStamp;//时间戳
    private static final String TAG = "MyFragment";
    private int mLoginStatus;
    private ZProgressHUD zProgressHUD;//loading

    public MyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyFragment newInstance(String param1, String param2) {
        MyFragment fragment = new MyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my, container, false);
        unbinder = ButterKnife.bind(this, view);
        initData();
        getData();
        return view;
    }

    /**
     * 初始化数据源
     */
    private void initData() {
        String safetyCode = MyUtils.getMetaValue(getActivity(), "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        timeStamp = Utils.getTimeStamp();
        Log.e(TAG, "onCreateView: " + code + "=timeStamp=" + timeStamp);

        myPredenter.onCreate();
        myPredenter.attachView(myView);
    }

    /**
     * 获取网络数据
     */
    private void getData() {
        //获取用户信息
        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
        //判断登录状态
        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
        //获取订单数量
        myPredenter.getOrderNum(AppConstants.COMPANY_ID, code, timeStamp, "1", "");
    }


    MyView myView = new MyView() {
        //签到
        @Override
        public void onSuccessDoSignIn(SignIn signIn) {
            Log.e(TAG, "onSuccessDoSignIn: " + signIn.toString());
            int status = signIn.getStatus();
            if (status > 0) {
                tvMySignin.setText("已签到");
                Toast.makeText(getActivity(), "签到成功", Toast.LENGTH_SHORT).show();
            } else {
                tvMySignin.setText("签到");
                Toast.makeText(getActivity(), "" + signIn.getReturnMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onError(String result) {
            if (zProgressHUD != null) {

                zProgressHUD.dismissWithFailure();
            }

        }

        //获取会员详细资料
        @Override
        public void onSuccessUserInfo(UserInfo userInfo) {
            Log.e(TAG, "onSuccessUserInfo: " + userInfo.toString());
            if (zProgressHUD != null) {

                zProgressHUD.dismiss();
            }
            int status = userInfo.getStatus();
            if (status > 0) {
                switch (ShoppingMallTemplateApplication.loginType) {
                    case "phone":
                        GlideUtils.setNetImage(getActivity(), AppConstants.INTERNET_HEAD + userInfo.getMemberImg(), civMinefHeadimg);
                        tvMinefUsername.setText(userInfo.getMemberName() != null ? userInfo.getMemberName() : "");
                        break;
                    case "wechat":
                        GlideUtils.setNetImage(getActivity(), userInfo.getMemberImg(), civMinefHeadimg);
                        tvMinefUsername.setText(userInfo.getNickName() != null ? userInfo.getNickName() : "");
                        break;
                    case "qq":
                        GlideUtils.setNetImage(getActivity(), userInfo.getMemberImg(), civMinefHeadimg);
                        tvMinefUsername.setText(userInfo.getNickName() != null ? userInfo.getNickName() : "");
                        break;
                }


                tvMyRank.setText(userInfo.getLevel() != null ? userInfo.getLevel() : "");
                tvMyfMycredit.setText(userInfo.getIntegration() != null ? userInfo.getIntegration() : "0");
                tvMyfZmoney.setText(userInfo.getZMoney() != null ? userInfo.getZMoney() : "0");
                tvMyfYmoney.setText(userInfo.getYMoney() != null ? userInfo.getYMoney() : "0");
                //签到状态，0 未签到 1 已签到
                String signin = userInfo.getSignIn();
                if ("0".equals(signin)) {
                    tvMySignin.setText("签到");
                } else {
                    tvMySignin.setText("已签到");
                }
            } else {

            }
        }

        /**
         * 修改头像
         * @param commonResult
         */
        @Override
        public void onSuccessModifyHeadImg(CommonResult commonResult) {
            Log.e(TAG, "onSuccessModifyHeadImg: " + commonResult.toString());
            int status = commonResult.getStatus();
            if (status > 0) {
                myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
            } else {
                Toast.makeText(getActivity(), "" + commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        //判断会员登录状态
        @Override
        public void onSuccessLoginStatus(LoginStatus loginStatus) {
            Log.e(TAG, "onSuccessLoginStatus: " + loginStatus.toString());
            if (zProgressHUD != null) {

                zProgressHUD.dismiss();
            }
            int status = loginStatus.getStatus();
            if (status > 0) {
                mLoginStatus = 1;
                try {

                    tvMyRank.setVisibility(View.VISIBLE);
                }catch (Exception e){

                }
            } else {
                mLoginStatus = 0;
                tvMinefUsername.setText("登录/注册");
                tvMyfMycredit.setText("0");
                tvMyfZmoney.setText("0");
                tvMyfYmoney.setText("0");
                civMinefHeadimg.setImageResource(R.drawable.head_image);
                tvMinefUsername.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "login"), 111);
                    }
                });

                tvMyRank.setVisibility(View.GONE);
            }
        }

        //获取订单数量
        @Override
        public void onSuccessGetOrderNum(OrderNum orderNum) {
            Log.e(TAG, "onSuccessGetOrderNum: " + orderNum.toString());
            int status = orderNum.getStatus();
            if (status > 0) {
                int orderCount1 = orderNum.getOrderCount1();
                if (orderCount1==0){
                    tvNumDaifuk.setVisibility(View.GONE);
                }else{
                    tvNumDaifuk.setVisibility(View.VISIBLE);
                    tvNumDaifuk.setText(orderCount1+"");
                }
                int orderCount2 = orderNum.getOrderCount2();
                if (orderCount2==0){
                    tvNumDaifahuo.setVisibility(View.GONE);
                }else{
                    tvNumDaifahuo.setVisibility(View.VISIBLE);
                    tvNumDaifahuo.setText(orderCount2+"");
                }
                int orderCount3 = orderNum.getOrderCount3();
                if (orderCount3==0){
                    tvNumDaishouhuo.setVisibility(View.GONE);
                }else{
                    tvNumDaishouhuo.setVisibility(View.VISIBLE);
                    tvNumDaishouhuo.setText(orderCount3+"");
                }
                int orderCount4 = orderNum.getOrderCount4();
                if (orderCount4==0){
                    tvNumYiqianshou.setVisibility(View.GONE);
                }else{
                    tvNumYiqianshou.setVisibility(View.VISIBLE);
                    tvNumYiqianshou.setText(orderCount4+"");
                }

            } else {
                tvNumDaifahuo.setVisibility(View.GONE);
                tvNumDaifahuo.setVisibility(View.GONE);
                tvNumDaishouhuo.setVisibility(View.GONE);
                tvNumYiqianshou.setVisibility(View.GONE);
            }
        }
    };

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.usv_my_integralorder, R.id.ll_myf_mycredit, R.id.ll_myf_zmoney, R.id.ll_myf_ymoney, R.id.tv_my_signin, R.id.tv_my_allorder, R.id.tv_my_setting, R.id.usv_my_address, R.id.iv_minef_edit, R.id.civ_minef_headimg
            , R.id.usv_my_collect, R.id.usv_my_message, R.id.usv_my_inform, R.id.ll_mycenterf_obligation, R.id.ll_mycenterf_shipments
            , R.id.ll_mycenterf_takegoods, R.id.ll_mycenterf_together, R.id.ll_mycenterf_refund, R.id.usv_my_grouporder, R.id.usv_my_userinfo
            , R.id.usv_my_shangpinpingjia})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //头像
            case R.id.civ_minef_headimg:
                if (mLoginStatus > 0) {
                    PictureSelector.create(this)
                            .openGallery(PictureMimeType.ofImage())
                            .selectionMode(PictureConfig.SINGLE)
                            .maxSelectNum(1)
                            .isCamera(true)
                            .enableCrop(true)
                            .circleDimmedLayer(true)
                            .forResult(PictureConfig.CHOOSE_REQUEST);
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 17);
                }

                break;
            //商品评价
            case R.id.usv_my_shangpinpingjia:
                if (mLoginStatus > 0) {
                    startActivity(new Intent(getActivity(), GoodsEvaluateListActivity.class).putExtra("type", "0"));
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 21);
                }
                break;
            //个人资料
            case R.id.usv_my_userinfo:

                if (mLoginStatus > 0) {
                    startActivity(new Intent(getActivity(), UserInfoActivity.class).putExtra("type", "0"));
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 7);
                }
                break;
            //查看全部订单
            case R.id.tv_my_allorder:
                if (mLoginStatus > 0) {
                    startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("type", "0"));
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 1);
                }

                break;
            //优惠券
            case R.id.usv_my_grouporder:
                if (mLoginStatus > 0) {
                    startActivity(new Intent(getActivity(), MyCouponListActivity.class).putExtra("type", "0"));
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 2);
                }

                break;
            //积分订单
            case R.id.usv_my_integralorder:
                if (mLoginStatus > 0) {
                    startActivity(new Intent(getActivity(), IntegralOrderListActivity.class));
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 3);
                }

                break;
            //设置
            case R.id.tv_my_setting:
                if (mLoginStatus > 0) {
                    startActivityForResult(new Intent(getActivity(), SettingActivity.class), 222);
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 18);
                }
                break;
            //地址
            case R.id.usv_my_address:

                if (mLoginStatus > 0) {
                    startActivity(new Intent(getActivity(), AdressManagerActivity.class));
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 4);
                }
                break;
            //我的收藏
            case R.id.usv_my_collect:

                if (mLoginStatus > 0) {
                    startActivity(new Intent(getActivity(), CollectActivity.class));
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 5);
                }
                break;
            //我的消息
            case R.id.usv_my_message:

                if (mLoginStatus > 0) {
                    startActivity(new Intent(getActivity(), MessageActivity.class));
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 6);
                }
                break;
            //会员通知
            case R.id.usv_my_inform:
                if (mLoginStatus > 0) {
                    startActivity(new Intent(getActivity(), InformActivity.class));
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 19);
                }
                break;
            //编辑用户名
            case R.id.iv_minef_edit:
                startActivity(new Intent(getActivity(), ModifyUserNameActivity.class));
                break;
            //待付款
            case R.id.ll_mycenterf_obligation:
                if (mLoginStatus > 0) {
                    startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("type", "1"));
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 8);
                }

                break;
            //待发货
            case R.id.ll_mycenterf_shipments:
                if (mLoginStatus > 0) {
                    startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("type", "2"));
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 9);
                }
                break;
            //待收货
            case R.id.ll_mycenterf_takegoods:
                if (mLoginStatus > 0) {
                    startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("type", "3"));
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 10);
                }
                break;
            //已签收
            case R.id.ll_mycenterf_together:
                if (mLoginStatus > 0) {
                    startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("type", "4"));
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 11);
                }
                break;
            //退款、维权
            case R.id.ll_mycenterf_refund:
                if (mLoginStatus > 0) {
                    startActivity(new Intent(getActivity(), AfterSaleListActivity.class));
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 12);
                }
                break;
            //签到
            case R.id.tv_my_signin:
                if (mLoginStatus > 0) {
                    myPredenter.doSignIn(AppConstants.COMPANY_ID, code, timeStamp);
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 13);
                }
                break;
            //我的积分
            case R.id.ll_myf_mycredit:
                if (mLoginStatus > 0) {
                    startActivity(new Intent(getActivity(), MyIntegralActivity.class));
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 14);
                }
                break;
            //消费总额
            case R.id.ll_myf_zmoney:
                if (mLoginStatus > 0) {
                    startActivity(new Intent(getActivity(), AccountBalanceActivity.class));
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 15);
                }
                break;
            //账户余额
            case R.id.ll_myf_ymoney:
                if (mLoginStatus > 0) {
                    startActivity(new Intent(getActivity(), AccountBalanceActivity.class));
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("type", "allorder"), 16);
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 图片、视频、音频选择结果回调
                    List<LocalMedia> selectList = PictureSelector.obtainMultipleResult(data);
                    String path = selectList.get(0).getCutPath();
                    String path1 = selectList.get(0).getPath();
                    Glide.with(getActivity()).load(path).into(civMinefHeadimg);
                    String ss = MyUtils.imageToBase64(path);
                    myPredenter.modifyHeadImg(AppConstants.COMPANY_ID, code, timeStamp, ss);
                    // 例如 LocalMedia 里面返回三种path
                    // 1.media.getPath(); 为原图path
                    // 2.media.getCutPath();为裁剪后path，需判断media.isCut();是否为true  注意：音视频除外
                    // 3.media.getCompressPath();为压缩后path，需判断media.isCompressed();是否为true  注意：音视频除外
                    // 如果裁剪并压缩了，以取压缩路径为准，因为是先裁剪后压缩的

                    break;


            }
        } else if (resultCode == 11) {
            String login = data.getStringExtra("login");
            if ("登录成功".equals(login)) {
                switch (requestCode) {
                    //所有订单
                    case 1:
                        startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("type", "0"));
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //我的优惠券
                    case 2:
                        startActivity(new Intent(getActivity(), MyCouponListActivity.class).putExtra("type", "0"));
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //积分订单
                    case 3:
                        startActivity(new Intent(getActivity(), IntegralOrderListActivity.class));
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //地址
                    case 4:
                        startActivity(new Intent(getActivity(), AdressManagerActivity.class));
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //我的收藏
                    case 5:
                        startActivity(new Intent(getActivity(), CollectActivity.class));
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //我的消息
                    case 6:
                        startActivity(new Intent(getActivity(), MessageActivity.class));
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //个人资料
                    case 7:
                        startActivity(new Intent(getActivity(), UserInfoActivity.class).putExtra("type", "0"));
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //待付款
                    case 8:
                        startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("type", "1"));
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //待发货
                    case 9:
                        startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("type", "2"));
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //待收货
                    case 10:
                        startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("type", "3"));
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //已签收
                    case 11:
                        startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("type", "4"));
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //退款、维权
                    case 12:
                        startActivity(new Intent(getActivity(), AfterSaleListActivity.class));
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //签到
                    case 13:
                        myPredenter.doSignIn(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //我的积分
                    case 14:
                        startActivity(new Intent(getActivity(), MyIntegralActivity.class));
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //消费总额
                    case 15:
                        startActivity(new Intent(getActivity(), AccountBalanceActivity.class));
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //账户余额
                    case 16:
                        startActivity(new Intent(getActivity(), AccountBalanceActivity.class));
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //修改头像
                    case 17:
                        PictureSelector.create(this)
                                .openGallery(PictureMimeType.ofImage())
                                .selectionMode(PictureConfig.SINGLE)
                                .maxSelectNum(1)
                                .isCamera(true)
                                .enableCrop(true)
                                .circleDimmedLayer(true)
                                .forResult(PictureConfig.CHOOSE_REQUEST);
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //设置
                    case 18:
                        startActivityForResult(new Intent(getActivity(), SettingActivity.class), 222);
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    //站内通知
                    case 19:
                        startActivity(new Intent(getActivity(), InformActivity.class));
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    case 21:
                        startActivity(new Intent(getActivity(), GoodsEvaluateListActivity.class));
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;
                    case 111:
                        myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
                        myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
                        break;


                }

            }
        } else if (resultCode == 22) {

            myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
            myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);

        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        Log.e(TAG, "onHiddenChanged: " + hidden);
        if (hidden) {

        } else {
            zProgressHUD = new ZProgressHUD(getActivity());
            zProgressHUD.show();
            myPredenter.getUserInfo(AppConstants.COMPANY_ID, code, timeStamp);
            myPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timeStamp, AppConstants.FROM_MOBILE);
        }

    }
}
