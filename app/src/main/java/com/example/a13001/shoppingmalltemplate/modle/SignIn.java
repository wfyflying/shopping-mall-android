package com.example.a13001.shoppingmalltemplate.modle;

public class SignIn {

    /**
     * status : 1
     * returnMsg : null
     * integral : 100
     */

    private int status;
    private String returnMsg;
    private int integral;

    @Override
    public String toString() {
        return "SignIn{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", integral=" + integral +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getIntegral() {
        return integral;
    }

    public void setIntegral(int integral) {
        this.integral = integral;
    }
}
