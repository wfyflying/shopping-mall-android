package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;

import java.util.List;

public class ClassifyLvAdapter extends BaseAdapter {
    private Context mContext;
    private List<String> mList;

    public ClassifyLvAdapter(Context mContext, List<String> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            view= LayoutInflater.from(mContext).inflate(R.layout.item_classify,parent,false);
            holder=new ViewHolder(view);
            view.setTag(holder);
        }else{
            holder= (ViewHolder) view.getTag();
        }
        holder.mTvTitle.setText(mList.get(i));
        return view;
    }
    class ViewHolder{

        private final TextView mTvTitle;

        public ViewHolder(View view) {
            mTvTitle = view.findViewById(R.id.tv_itemclassify_title);
        }
    }
}
