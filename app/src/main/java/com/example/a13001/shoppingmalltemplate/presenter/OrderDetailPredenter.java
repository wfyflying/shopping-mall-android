package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;


import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.GoodsEvaluateDetail;
import com.example.a13001.shoppingmalltemplate.modle.OrderDetail;
import com.example.a13001.shoppingmalltemplate.modle.ShouHouDetail;
import com.example.a13001.shoppingmalltemplate.mvpview.OrderDetailView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class OrderDetailPredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private OrderDetailView mOrderDetailView;
    private OrderDetail mOrderDetail;
    private CommonResult mCommonResult;
    private ShouHouDetail mShouHouDetail;
    private GoodsEvaluateDetail mGoodsEvaluateDetail;
    public OrderDetailPredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        mCompositeSubscription=new CompositeSubscription();
        manager=new DataManager(mContext);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mOrderDetailView=(OrderDetailView) view;
    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }


    /**
     * 查看普通订单详情
     * @param companyid   站点ID
     * @param code         安全校验码
     * @param timestamp    时间戳
     * @param ordersNumber  订单号
     * @return
     */
    public void getOrderDetail(String companyid, String code, String timestamp, String ordersNumber, String from) {

        mCompositeSubscription.add(manager.getOrderDetail(companyid,code,timestamp,ordersNumber,from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<OrderDetail>() {
                    @Override
                    public void onCompleted() {
                        if (mOrderDetailView!=null){
                            mOrderDetailView.onSuccessGetOrderDetail(mOrderDetail);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mOrderDetailView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(OrderDetail orderDetail) {
                        mOrderDetail=orderDetail;
                    }
                }));
    }
    /**
     * 取消未支付订单
     * @param companyid   站点ID
     * @param code         安全校验码
     * @param timestamp    时间戳
     * @param ordersNumber  订单号
     * @return
     */
    public void cancelOrder(String companyid, String code, String timestamp,String ordersNumber) {

        mCompositeSubscription.add(manager.cancelOrder(companyid,code,timestamp,ordersNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mOrderDetailView!=null){
                            mOrderDetailView.onSuccessCancelOrder(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mOrderDetailView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
    /**
     * 订单确认收货
     * @param companyid   站点ID
     * @param code         安全校验码
     * @param timestamp    时间戳
     * @param ordersNumber  订单号
     * @return
     */
    public void affirmOrder(String companyid, String code, String timestamp, String ordersNumber) {

        mCompositeSubscription.add(manager.affirmOrder(companyid,code,timestamp,ordersNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mOrderDetailView!=null){
                            mOrderDetailView.onSuccessAffirmOrder(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mOrderDetailView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
    /**
     * ★ 获取售后服务单详情
     * @param companyid
     * @param code
     * @param timestamp
     * @param cartid
     * @param commodityid
     * @param from
     * @return
     */

    public void getShouHouDetail(String companyid, String code, String timestamp, String cartid, String commodityid, String from) {

        mCompositeSubscription.add(manager.getShouHouDetail(companyid,code,timestamp,cartid,commodityid,from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ShouHouDetail>() {
                    @Override
                    public void onCompleted() {
                        if (mOrderDetailView!=null){
                            mOrderDetailView.onSuccessGetShouHouDetail(mShouHouDetail);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mOrderDetailView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(ShouHouDetail shouHouDetail) {
                        mShouHouDetail=shouHouDetail;
                    }
                }));
    }
    /**
     *★ 获取评价商品详情
     * @param companyid
     * @param code
     * @param timestamp
     * @param cartid    商品购物车ID
     * @param from
     * @return
     */

    public void getGoodsEvaluateDetail(String companyid, String code, String timestamp, String cartid,String from) {

        mCompositeSubscription.add(manager.getGoodsEvaluateDetail(companyid,code,timestamp,cartid,from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GoodsEvaluateDetail>() {
                    @Override
                    public void onCompleted() {
                        if (mOrderDetailView!=null){
                            mOrderDetailView.onSuccessGetGoodsEvaluateDetail(mGoodsEvaluateDetail);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mOrderDetailView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(GoodsEvaluateDetail goodsEvaluateDetail) {
                        mGoodsEvaluateDetail=goodsEvaluateDetail;
                    }
                }));
    }
}
