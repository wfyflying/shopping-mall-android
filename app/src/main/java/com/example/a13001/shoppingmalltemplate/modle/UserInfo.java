package com.example.a13001.shoppingmalltemplate.modle;

public class UserInfo {

    /**
     * status : 1
     * returnMsg : null
     * memberId : 100140
     * memberName : lqp
     * memberMobile :
     * memberEmail :
     * nickName :
     * level : 普通会员
     * birthday :
     * sex : 0
     * address :
     * memberZd1 : null
     * memberZd2 : null
     * memberZd3 : null
     * memberZd4 : null
     * memberZd5 : null
     * memberZd6 : null
     * memberZd7 : null
     * memberZd8 : null
     * memberImg : //filecdn.qkk.cn/skin/images/member.gif
     * YMoney : 0.00
     * SMoney : 0.00
     * ZMoney : 0.00
     * integration : 1
     * ulIP : 221.197.104.196
     * ulDate : 2018/6/8 15:05:26
     * buyPassSet : 0
     * signIn :
     */

    private int status;
    private Object returnMsg;
    private String memberId;
    private String memberName;
    private String memberMobile;
    private String memberEmail;
    private String nickName;
    private String level;
    private String birthday;
    private String sex;
    private String address;
    private Object memberZd1;
    private Object memberZd2;
    private Object memberZd3;
    private Object memberZd4;
    private Object memberZd5;
    private Object memberZd6;
    private Object memberZd7;
    private Object memberZd8;
    private String memberImg;
    private String YMoney;
    private String SMoney;
    private String ZMoney;
    private String integration;
    private String ulIP;
    private String ulDate;
    private int buyPassSet;
    private String signIn;

    @Override
    public String toString() {
        return "UserInfo{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", memberId='" + memberId + '\'' +
                ", memberName='" + memberName + '\'' +
                ", memberMobile='" + memberMobile + '\'' +
                ", memberEmail='" + memberEmail + '\'' +
                ", nickName='" + nickName + '\'' +
                ", level='" + level + '\'' +
                ", birthday='" + birthday + '\'' +
                ", sex='" + sex + '\'' +
                ", address='" + address + '\'' +
                ", memberZd1=" + memberZd1 +
                ", memberZd2=" + memberZd2 +
                ", memberZd3=" + memberZd3 +
                ", memberZd4=" + memberZd4 +
                ", memberZd5=" + memberZd5 +
                ", memberZd6=" + memberZd6 +
                ", memberZd7=" + memberZd7 +
                ", memberZd8=" + memberZd8 +
                ", memberImg='" + memberImg + '\'' +
                ", YMoney='" + YMoney + '\'' +
                ", SMoney='" + SMoney + '\'' +
                ", ZMoney='" + ZMoney + '\'' +
                ", integration='" + integration + '\'' +
                ", ulIP='" + ulIP + '\'' +
                ", ulDate='" + ulDate + '\'' +
                ", buyPassSet=" + buyPassSet +
                ", signIn='" + signIn + '\'' +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberMobile() {
        return memberMobile;
    }

    public void setMemberMobile(String memberMobile) {
        this.memberMobile = memberMobile;
    }

    public String getMemberEmail() {
        return memberEmail;
    }

    public void setMemberEmail(String memberEmail) {
        this.memberEmail = memberEmail;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Object getMemberZd1() {
        return memberZd1;
    }

    public void setMemberZd1(Object memberZd1) {
        this.memberZd1 = memberZd1;
    }

    public Object getMemberZd2() {
        return memberZd2;
    }

    public void setMemberZd2(Object memberZd2) {
        this.memberZd2 = memberZd2;
    }

    public Object getMemberZd3() {
        return memberZd3;
    }

    public void setMemberZd3(Object memberZd3) {
        this.memberZd3 = memberZd3;
    }

    public Object getMemberZd4() {
        return memberZd4;
    }

    public void setMemberZd4(Object memberZd4) {
        this.memberZd4 = memberZd4;
    }

    public Object getMemberZd5() {
        return memberZd5;
    }

    public void setMemberZd5(Object memberZd5) {
        this.memberZd5 = memberZd5;
    }

    public Object getMemberZd6() {
        return memberZd6;
    }

    public void setMemberZd6(Object memberZd6) {
        this.memberZd6 = memberZd6;
    }

    public Object getMemberZd7() {
        return memberZd7;
    }

    public void setMemberZd7(Object memberZd7) {
        this.memberZd7 = memberZd7;
    }

    public Object getMemberZd8() {
        return memberZd8;
    }

    public void setMemberZd8(Object memberZd8) {
        this.memberZd8 = memberZd8;
    }

    public String getMemberImg() {
        return memberImg;
    }

    public void setMemberImg(String memberImg) {
        this.memberImg = memberImg;
    }

    public String getYMoney() {
        return YMoney;
    }

    public void setYMoney(String YMoney) {
        this.YMoney = YMoney;
    }

    public String getSMoney() {
        return SMoney;
    }

    public void setSMoney(String SMoney) {
        this.SMoney = SMoney;
    }

    public String getZMoney() {
        return ZMoney;
    }

    public void setZMoney(String ZMoney) {
        this.ZMoney = ZMoney;
    }

    public String getIntegration() {
        return integration;
    }

    public void setIntegration(String integration) {
        this.integration = integration;
    }

    public String getUlIP() {
        return ulIP;
    }

    public void setUlIP(String ulIP) {
        this.ulIP = ulIP;
    }

    public String getUlDate() {
        return ulDate;
    }

    public void setUlDate(String ulDate) {
        this.ulDate = ulDate;
    }

    public int getBuyPassSet() {
        return buyPassSet;
    }

    public void setBuyPassSet(int buyPassSet) {
        this.buyPassSet = buyPassSet;
    }

    public String getSignIn() {
        return signIn;
    }

    public void setSignIn(String signIn) {
        this.signIn = signIn;
    }
}
