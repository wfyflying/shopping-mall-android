package com.example.a13001.shoppingmalltemplate.activitys;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.SpinerPopWindow;
import com.example.a13001.shoppingmalltemplate.adapters.SpinerAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.UserInfo;
import com.example.a13001.shoppingmalltemplate.mvpview.SafeCodeView;
import com.example.a13001.shoppingmalltemplate.presenter.SafeCodePredenter;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SafePwdActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_title_right)
    TextView tvTitleRight;
    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;
    @BindView(R.id.tv_safepwd_yztype)
    TextView tvSafepwdYztype;
    @BindView(R.id.tv_safepwd_yzcode)
    EditText tvSafepwdYzcode;
    @BindView(R.id.tv_safepwd_getcode)
    TextView tvSafepwdGetcode;
    @BindView(R.id.et_safepwd_newpwd)
    EditText etSafepwdNewpwd;
    @BindView(R.id.tv_safepwd_newpwdagin)
    EditText tvSafepwdNewpwdagin;
    @BindView(R.id.tv_safepwd_commit)
    TextView tvSafepwdCommit;
    @BindView(R.id.et_safepwd_oldpwd)
    EditText etSafepwdOldpwd;
    @BindView(R.id.ll_safecode_yzcode)
    LinearLayout llSafecodeYzcode;
    private ArrayList<String> listFour;
    private TimeCount time;
    SafeCodePredenter safeCodePredenter = new SafeCodePredenter(SafePwdActivity.this);
    private static final String TAG = "SafePwdActivity";
    private String code;
    private String mTimestamp;
    private int ifsafe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_safe_pwd);
        ButterKnife.bind(this);
        initData();
    }

    SafeCodeView safeCodeView = new SafeCodeView() {
        @Override
        public void onSuccessGetUserInfo(UserInfo userInfo) {
            Log.e(TAG, "onSuccessGetUserInfo: " + userInfo.toString());
            int status = userInfo.getStatus();
            if (status > 0) {
                //0 未设置安全密码 1 已设置安全密码
                ifsafe = userInfo.getBuyPassSet();
                if (ifsafe ==0){
                    tvSafepwdYztype.setVisibility(View.GONE);
                    etSafepwdOldpwd.setVisibility(View.GONE);
                    llSafecodeYzcode.setVisibility(View.GONE);
                }else if (ifsafe ==1){
                    tvSafepwdYztype.setVisibility(View.VISIBLE);
                    etSafepwdOldpwd.setVisibility(View.VISIBLE);
                    llSafecodeYzcode.setVisibility(View.GONE);
                }
            } else {

            }
        }

        @Override
        public void onSuccessDoResetSafepwd(CommonResult commonResult) {
            Log.e(TAG, "onSuccessDoResetSafepwd: " + commonResult.toString());
            int status = commonResult.getStatus();
            if (status > 0) {
                Toast.makeText(SafePwdActivity.this, "成功", Toast.LENGTH_SHORT).show();
                finish();
            } else {

            }
        }

        @Override
        public void onError(String result) {
            Log.e(TAG, "onError: " + result);
        }
    };

    /**
     * 初始化数据源
     */
    private void initData() {
        tvTitleCenter.setText("设置安全密码");
        time = new TimeCount(60000, 1000);

        String safetyCode = MyUtils.getMetaValue(SafePwdActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        mTimestamp = Utils.getTimeStamp();

        safeCodePredenter.onCreate();
        safeCodePredenter.attachView(safeCodeView);
        safeCodePredenter.getUserInfo(AppConstants.COMPANY_ID,code,mTimestamp);

    }

    /**
     * 发布位置弹框
     */
    private void showSpinWindowAddress() {
        listFour = new ArrayList<String>();
        listFour.add("使用原密码验证");
        listFour.add("使用已绑手机验证");
//        listFour.add("使用已绑邮箱验证");

        SpinerAdapter mAdapter4 = new SpinerAdapter(this, listFour);
        mAdapter4.refreshData(listFour, 0);

        //显示第一条数据
        tvSafepwdYztype.setText(listFour.get(0));


        //初始化PopWindow
        SpinerPopWindow mSpinerPopWindow4 = new SpinerPopWindow(this);
        mSpinerPopWindow4.setAdatper(mAdapter4);
        mSpinerPopWindow4.setItemListener(new SpinerAdapter.IOnItemSelectListener() {
            @Override
            public void onItemClick(int pos) {
                if (pos >= 0 && pos <= listFour.size()) {
                    String value = listFour.get(pos);
                    tvSafepwdYztype.setText(value);
                    if (pos==0){
                        llSafecodeYzcode.setVisibility(View.GONE);
                        etSafepwdOldpwd.setVisibility(View.VISIBLE);
                    }else if (pos==1){
                        llSafecodeYzcode.setVisibility(View.VISIBLE);
                        etSafepwdOldpwd.setVisibility(View.GONE);
                    }
                }
            }
        });
        mSpinerPopWindow4.setWidth(tvSafepwdYztype.getWidth());
        mSpinerPopWindow4.showAsDropDown(tvSafepwdYztype);
    }

    @OnClick({R.id.iv_title_back, R.id.tv_safepwd_getcode, R.id.tv_safepwd_commit, R.id.tv_safepwd_yztype})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_title_back:
                onBackPressed();
                break;
            case R.id.tv_safepwd_getcode:

                break;
            case R.id.tv_safepwd_commit:
                String oldpwd=etSafepwdOldpwd.getText().toString().trim();
                String newpwd=etSafepwdNewpwd.getText().toString().trim();
                String confirmpwd=tvSafepwdNewpwdagin.getText().toString().trim();
               //0 未设置安全密码 1 已设置安全密码
//                if (ifsafe==0){
//
//                }

                safeCodePredenter.doResetSafePwd(AppConstants.COMPANY_ID,code,mTimestamp,oldpwd,newpwd,confirmpwd);
                break;
            case R.id.tv_safepwd_yztype:
                showSpinWindowAddress();
                break;
        }
    }

    /**
     * 倒计时类
     */
    class TimeCount extends CountDownTimer {

        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            // mBtnGetcode.setBackgroundColor(Color.parseColor("#B6B6D8"));
            tvSafepwdGetcode.setClickable(false);
            tvSafepwdGetcode.setText("" + millisUntilFinished / 1000);
        }

        @Override
        public void onFinish() {
            tvSafepwdGetcode.setText("获取验证码");
            tvSafepwdGetcode.setClickable(true);
            //  mBtnGetcode.setBackgroundColor(Color.parseColor("#4EB84A"));
        }
    }
}
