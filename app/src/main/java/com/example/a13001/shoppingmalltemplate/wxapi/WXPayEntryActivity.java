package com.example.a13001.shoppingmalltemplate.wxapi;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;


import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.activitys.OrderListActivity;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.greenrobot.eventbus.EventBus;


public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {

    private static final String TAG = "WXPayEntryActivity";

    private IWXAPI api;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

            setContentView(R.layout.item_paysuccess);

            api = WXAPIFactory.createWXAPI(this, "wxc3137b88302dd911");
            api.handleIntent(getIntent(), this);



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq req) {
    }

    @Override
    public void onResp(BaseResp resp) {

        Log.e("aaa",
            "(WXPayEntryActivity.java:46)"+resp.errStr+""+resp.errCode);
        if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
            if (resp.errCode == -2) {
                Toast.makeText(this, "取消付款！", Toast.LENGTH_LONG).show();
                finish();
            }
            if (resp.errCode == -1) {
                Toast.makeText(this, "支付错误！", Toast.LENGTH_LONG).show();
                finish();
            }
            if (resp.errCode == 0) {
                Toast.makeText(this, "支付成功！", Toast.LENGTH_LONG).show();
                EventBus.getDefault().post("支付成功");
                startActivity(new Intent(WXPayEntryActivity.this, OrderListActivity.class).putExtra("type", "2"));
                    finish();

//                queryOrder();//查询接口调用后台服务器查询是否成功
            }
        }
    }
}