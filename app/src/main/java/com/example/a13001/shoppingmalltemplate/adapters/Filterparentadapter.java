package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.MyGridView;
import com.example.a13001.shoppingmalltemplate.modle.FilterGoods;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

public class Filterparentadapter extends BaseAdapter {
    private Context mContext;
    private List<FilterGoods> mList;
    private FilterChildadapter mAdapter;
    public Filterparentadapter(Context mContext, List<FilterGoods> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view= LayoutInflater.from(mContext).inflate(R.layout.item_right_sideslip_lay,viewGroup,false);
            holder=new ViewHolder();
            holder.itemFrameTitleTv = view.findViewById(R.id.item_frameTv);
            holder.itemFrameSelectTv = view.findViewById(R.id.item_selectTv);
            holder.layoutItem = view.findViewById(R.id.item_select_lay);
            holder.itemFrameGv = view.findViewById(R.id.item_selectGv);
           view.setTag(holder);
            AutoUtils.autoSize(view);
        }else{
            holder= (ViewHolder) view.getTag();
        }
        holder.itemFrameGv.setVisibility(View.VISIBLE);
        holder.itemFrameTitleTv.setText(mList.get(i).getKey());
        /**
         * 筛选子列表
         */
//        mAdapter=new FilterChildadapter(mContext,mList.get(i).getVals());
        holder.itemFrameGv.setAdapter(mAdapter);
        return view;
    }
    class ViewHolder{
        TextView  itemFrameTitleTv,itemFrameSelectTv;
        LinearLayout layoutItem;
        MyGridView itemFrameGv;
    }
}
