package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.modle.IntegrationList;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Administrator on 2017/8/9.
 */

public class MyIntegralLvAdapter extends BaseAdapter {
    private Context context;
    private List<IntegrationList.MemberIntegrationListBean> mList;

    public MyIntegralLvAdapter(Context context, List<IntegrationList.MemberIntegrationListBean> mList) {
        this.context = context;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder vh = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_lv_myintegral, parent, false);
            vh = new ViewHolder(view);
            view.setTag(vh);
            AutoUtils.autoSize(view);
        } else {
            vh = (ViewHolder) view.getTag();
        }
        vh.tvItemmyintegralTitle.setText(mList.get(position).getIntegrationRecord()!=null?mList.get(position).getIntegrationRecord():"");
        vh.tvItemmyintegralDate.setText(mList.get(position).getIntegrationDate()!=null?mList.get(position).getIntegrationDate():"");
        vh.tvItemmyintegralIntegral.setText(mList.get(position).getIntegrationJJ()+mList.get(position).getIntegrationNumber());

        return view;
    }

    static class ViewHolder {
        @BindView(R.id.tv_itemmyintegral_title)
        TextView tvItemmyintegralTitle;
        @BindView(R.id.tv_itemmyintegral_date)
        TextView tvItemmyintegralDate;
        @BindView(R.id.tv_itemmyintegral_integral)
        TextView tvItemmyintegralIntegral;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
