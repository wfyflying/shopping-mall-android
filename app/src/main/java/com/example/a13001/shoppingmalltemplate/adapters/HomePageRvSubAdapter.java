package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.GoodsDetail;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.zhy.autolayout.utils.AutoUtils;

import java.text.DecimalFormat;
import java.util.List;

public class HomePageRvSubAdapter extends BaseAdapter{
    private Context mContext;
    private List<GoodsDetail.ListBean> mList;

    public HomePageRvSubAdapter(Context mContext, List<GoodsDetail.ListBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null&&mList.size()>0) {
            if (mList.size()<=4){

                return mList.size();
            }else{
                return 4;
            }
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        MyViewHolder myViewHolder;
        if (view == null) {
            view= LayoutInflater.from(mContext).inflate(R.layout.item_rv_homepage_sub,viewGroup,false);
            myViewHolder=new MyViewHolder(view);
            view.setTag(myViewHolder);
            AutoUtils.autoSize(view);
        }else{
            myViewHolder= (MyViewHolder) view.getTag();
        }
//        myViewHolder.ivLogo.setImageResource(mList.get(position).getLogo());
//        myViewHolder.ivLogo.loadImage(AppConstants.INTERNET_HEAD+mList.get(position).getImages(),R.mipmap.ic_launcher);
        GlideUtils.setNetImage(mContext,AppConstants.INTERNET_HEAD+mList.get(position).getImages(),myViewHolder.ivLogo);

        myViewHolder.tvContent.setText(mList.get(position).getTitle()!=null?mList.get(position).getTitle():"");
        DecimalFormat df = new DecimalFormat("0.00");//格式化
        String format = df.format(mList.get(position).getPrice());
        myViewHolder.tvPrice.setText(format);
//        myViewHolder.tvPrice.setText(mList.get(position).getPrice());
        myViewHolder.tvLoveNum.setText(mList.get(position).getHits()+"");

        return view;
    }


    class MyViewHolder {

        private final ImageView ivLogo;
        private final TextView tvContent;
        private final TextView tvPrice;
        private final TextView tvLoveNum;

        public MyViewHolder(View itemView) {

            ivLogo = itemView.findViewById(R.id.iv_hompagesub_logo);
            tvContent = itemView.findViewById(R.id.tv_hompagesub_content);
            tvPrice = itemView.findViewById(R.id.tv_hompagesub_price);
            tvLoveNum = itemView.findViewById(R.id.tv_hompagesub_lovenum);
        }
    }
}
