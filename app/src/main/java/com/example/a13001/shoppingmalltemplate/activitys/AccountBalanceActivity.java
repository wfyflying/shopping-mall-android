package com.example.a13001.shoppingmalltemplate.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.adapters.AccountBalanceLvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.Account;
import com.example.a13001.shoppingmalltemplate.modle.IntegrationList;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class AccountBalanceActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_title_right)
    TextView tvTitleRight;
    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;
    @BindView(R.id.tv_accountbalance_zhye)
    TextView tvAccountbalanceZhye;
    @BindView(R.id.tv_accountbalance_xfze)
    TextView tvAccountbalanceXfze;
    @BindView(R.id.tv_accountbalance_lijicz)
    TextView tvAccountbalanceLijicz;
    @BindView(R.id.tv_accountbalance_deposit)
    TextView tvAccountbalanceDeposit;
    @BindView(R.id.lv_accountbalance)
    ListView lvAccountbalance;
    @BindView(R.id.srfl_account)
    SmartRefreshLayout srflAccount;
    private List<Account.MemberTransactionListBean> mList;
    private AccountBalanceLvAdapter mAdapter;
    private static final String TAG = "AccountBalanceActivity";
    private Account mAccount;
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private String code;
    private String timeStamp;
    private int pageindex = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_balance);
        ButterKnife.bind(this);
        initData();
    }

    /**
     * 初始化数据源
     */
    private void initData() {
        tvTitleCenter.setText("账户余额");
        tvTitleRight.setVisibility(View.VISIBLE);
        tvTitleRight.setText("充值明细");

        manager = new DataManager(AccountBalanceActivity.this);
        mCompositeSubscription = new CompositeSubscription();

        String safetyCode = MyUtils.getMetaValue(AccountBalanceActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        timeStamp = Utils.getTimeStamp();
        mList = new ArrayList<>();
        mAdapter = new AccountBalanceLvAdapter(AccountBalanceActivity.this, mList);
        getTransactionList(AppConstants.COMPANY_ID, code, timeStamp,AppConstants.PAGE_SIZE,pageindex,"","");

        lvAccountbalance.setAdapter(mAdapter);

        setRefresh();
    }

    @OnClick({R.id.iv_title_back,R.id.tv_title_right, R.id.tv_accountbalance_lijicz, R.id.tv_accountbalance_deposit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_title_back:
                onBackPressed();
                break;
//                充值明细
            case R.id.tv_title_right:
                startActivity(new Intent(AccountBalanceActivity.this, RechargeSheetActivity.class));
                break;
            //立即充值
            case R.id.tv_accountbalance_lijicz:
                startActivity(new Intent(AccountBalanceActivity.this, RechargeActivity.class));
                break;
            //提现
            case R.id.tv_accountbalance_deposit:
                startActivity(new Intent(AccountBalanceActivity.this, DepositActivity.class));
                break;
        }
    }

    /**
     * ★ 获取会员资金明细     * @param companyid    站点ID
     *
     * @param code      安全校验码
     * @param timestamp 时间戳
     * @return
     */
    public void getTransactionList(String companyid, String code, String timestamp, int pagesize, int pageindex, String startdate, String overdate) {

        mCompositeSubscription.add(manager.getTransactionList(companyid, code, timestamp, pagesize, pageindex, startdate, overdate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Account>() {
                    @Override
                    public void onCompleted() {
                        int status = mAccount.getStatus();
                        if (status > 0) {
                            mList.addAll(mAccount.getMemberTransactionList());
                            mAdapter.notifyDataSetChanged();
                        } else {

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: " + e.toString());
                    }

                    @Override
                    public void onNext(Account account) {
                        mAccount = account;
                    }
                }));
    }
    private void setRefresh() {
        //刷新
        srflAccount.setRefreshHeader(new ClassicsHeader(AccountBalanceActivity.this));
        srflAccount.setRefreshFooter(new ClassicsFooter(AccountBalanceActivity.this));
//        mRefresh.setEnablePureScrollMode(true);
        srflAccount.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                pageindex = 1;
                if (mList != null) {
                    mList.clear();
                }
                getTransactionList(AppConstants.COMPANY_ID, code, timeStamp,AppConstants.PAGE_SIZE,pageindex,"","");

                refreshlayout.finishRefresh(2000/*,true*/);//传入false表示刷新失败
            }
        });
        srflAccount.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                pageindex++;
                getTransactionList(AppConstants.COMPANY_ID, code, timeStamp,AppConstants.PAGE_SIZE,pageindex,"","");
                refreshlayout.finishLoadMore(2000/*,true*/);//传入false表示加载失败
            }
        });
    }
}
