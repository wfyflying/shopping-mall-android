package com.example.a13001.shoppingmalltemplate.modle;

public class Result {

    /**
     * status : 10
     * GoUrl :
     * errMsg : null
     */

    private int status;
    private String GoUrl;
    private String errMsg;
    private String CartId;

    @Override
    public String toString() {
        return "Result{" +
                "status=" + status +
                ", GoUrl='" + GoUrl + '\'' +
                ", errMsg='" + errMsg + '\'' +
                ", CartId='" + CartId + '\'' +
                '}';
    }

    public String getCartId() {
        return CartId;
    }

    public void setCartId(String cartId) {
        CartId = cartId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getGoUrl() {
        return GoUrl;
    }

    public void setGoUrl(String GoUrl) {
        this.GoUrl = GoUrl;
    }

    public Object getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}
