package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.EvaluateList;
import com.example.a13001.shoppingmalltemplate.modle.GoodsParameters;

public interface EvaluateView extends View{
    void onSuccessGetEvaluate(EvaluateList evaluateList);
    void onSuccessGetEvaluateNum(GoodsParameters goodsParameters);
    void onError(String result);
}
