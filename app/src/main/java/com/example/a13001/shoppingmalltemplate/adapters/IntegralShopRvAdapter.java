package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.Goods;
import com.example.a13001.shoppingmalltemplate.modle.GoodsList;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

import cn.iwgang.countdownview.CountdownView;

public class IntegralShopRvAdapter extends RecyclerView.Adapter<IntegralShopRvAdapter.ViewHolder>{
    private Context mContext;
    private List<GoodsList.ListBean> mList;
    GoodsListRvAdapter.onItemClickListener onItemClickListener;
    public IntegralShopRvAdapter(Context mContext, List<GoodsList.ListBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }
    public interface onItemClickListener{
        void onClick(int position);
    }
    public void setOnItemClickListener(GoodsListRvAdapter.onItemClickListener onItemClickListener){
        this.onItemClickListener=onItemClickListener;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.item_rv_seckill,parent,false);
        ViewHolder holder=new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.mLlCount.setVisibility(View.GONE);
        holder.tvPrice.setText("积分："+mList.get(position).getIntegral());
        holder.tvState2.setVisibility(View.GONE);
        GlideUtils.setNetImage(mContext, AppConstants.INTERNET_HEAD+mList.get(position).getImages(),holder.ivLogo);
        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private final ImageView ivLogo;
        private final TextView tvTitle;
        private final TextView tvState;
        private final TextView tvPrice;
        private final TextView tvState2;
        private CountdownView countdownView;
        private LinearLayout mLlCount;
        public ViewHolder(View itemView) {
            super(itemView);
            AutoUtils.autoSize(itemView);
            ivLogo = itemView.findViewById(R.id.iv_itemseckill_logo);
            tvTitle = itemView.findViewById(R.id.tv_itemseckill_title);
            tvState = itemView.findViewById(R.id.tv_itemseckill_state);
            tvPrice = itemView.findViewById(R.id.tv_itemseckill_price);
            tvState2 = itemView.findViewById(R.id.tv_itemseckill_state2);
            countdownView = itemView.findViewById(R.id.countdownview);
            mLlCount = itemView.findViewById(R.id.ll_count);
        }
    }
}
