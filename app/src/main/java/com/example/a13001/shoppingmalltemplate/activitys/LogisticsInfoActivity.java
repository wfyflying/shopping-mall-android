package com.example.a13001.shoppingmalltemplate.activitys;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.adapters.LogisticsInfoLvAdapter;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.modle.Goods;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import butterknife.OnClick;

public class LogisticsInfoActivity extends BaseActivity {

    @BindView(R.id.iv_logisticsinfo_back)
    ImageView ivLogisticsinfoBack;
    @BindView(R.id.textView6)
    TextView textView6;
    @BindView(R.id.tv_logisticsinfo_companyname)
    TextView tvLogisticsinfoCompanyname;
    @BindView(R.id.tv_logisticsinfo_orderid)
    TextView tvLogisticsinfoOrderid;
    @BindView(R.id.tv_logisticsinfo_phone)
    TextView tvLogisticsinfoPhone;
    @BindView(R.id.lv_logisticsinfo)
    ListView lvLogisticsinfo;
    private LogisticsInfoLvAdapter mAdapter;
    private List<Goods> mList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logistics_info);
        ButterKnife.bind(this);
        initData();
    }
    /**
     * 初始化数据源
     */
    private void initData() {
        mList=new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            Goods newBaoJia=new Goods();
            mList.add(newBaoJia);
        }
        mAdapter=new LogisticsInfoLvAdapter(LogisticsInfoActivity.this,mList);
        lvLogisticsinfo.setAdapter(mAdapter);
    }
    @OnClick(R.id.iv_logisticsinfo_back)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
