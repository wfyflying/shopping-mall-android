package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class ShopCouponList {

    /**
     * status : 1
     * returnMsg : null
     * count : 1
     * pagesize : 20
     * pageindex : 1
     * list : [{"Aid":100000,"Atitle":"新人5元券","AhotTime":"2018-07-22 08:16","AstartTime":"2018-07-22 08:16","AoverTime":"2018-07-31 08:16","Areg":2,"Axz1":1,"Axz2":1,"storeid":0,"Qid":100000,"QZjsum":50,"QzjContent":"新人5元券，首次注册会员，并且未进行消费就可以领券。","Qmoney":5,"QxzMoney":100}]
     */

    private int status;
    private String returnMsg;
    private int count;
    private int pagesize;
    private int pageindex;
    private List<ListBean> list;

    @Override
    public String toString() {
        return "ShopCouponList{" +
                "status=" + status +
                ", returnMsg='" + returnMsg + '\'' +
                ", count=" + count +
                ", pagesize=" + pagesize +
                ", pageindex=" + pageindex +
                ", list=" + list +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public int getPageindex() {
        return pageindex;
    }

    public void setPageindex(int pageindex) {
        this.pageindex = pageindex;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * Aid : 100000
         * Atitle : 新人5元券
         * AhotTime : 2018-07-22 08:16
         * AstartTime : 2018-07-22 08:16
         * AoverTime : 2018-07-31 08:16
         * Areg : 2
         * Axz1 : 1
         * Axz2 : 1
         * storeid : 0
         * Qid : 100000
         * QZjsum : 50
         * QzjContent : 新人5元券，首次注册会员，并且未进行消费就可以领券。
         * Qmoney : 5
         * QxzMoney : 100
         */

        private int Aid;
        private String Atitle;
        private String AhotTime;
        private String AstartTime;
        private String AoverTime;
        private int Areg;
        private int Axz1;
        private int Axz2;
        private int storeid;
        private int Qid;
        private int QZjsum;
        private String QzjContent;
        private int Qmoney;
        private int QxzMoney;

        @Override
        public String toString() {
            return "ListBean{" +
                    "Aid=" + Aid +
                    ", Atitle='" + Atitle + '\'' +
                    ", AhotTime='" + AhotTime + '\'' +
                    ", AstartTime='" + AstartTime + '\'' +
                    ", AoverTime='" + AoverTime + '\'' +
                    ", Areg=" + Areg +
                    ", Axz1=" + Axz1 +
                    ", Axz2=" + Axz2 +
                    ", storeid=" + storeid +
                    ", Qid=" + Qid +
                    ", QZjsum=" + QZjsum +
                    ", QzjContent='" + QzjContent + '\'' +
                    ", Qmoney=" + Qmoney +
                    ", QxzMoney=" + QxzMoney +
                    '}';
        }

        public int getAid() {
            return Aid;
        }

        public void setAid(int Aid) {
            this.Aid = Aid;
        }

        public String getAtitle() {
            return Atitle;
        }

        public void setAtitle(String Atitle) {
            this.Atitle = Atitle;
        }

        public String getAhotTime() {
            return AhotTime;
        }

        public void setAhotTime(String AhotTime) {
            this.AhotTime = AhotTime;
        }

        public String getAstartTime() {
            return AstartTime;
        }

        public void setAstartTime(String AstartTime) {
            this.AstartTime = AstartTime;
        }

        public String getAoverTime() {
            return AoverTime;
        }

        public void setAoverTime(String AoverTime) {
            this.AoverTime = AoverTime;
        }

        public int getAreg() {
            return Areg;
        }

        public void setAreg(int Areg) {
            this.Areg = Areg;
        }

        public int getAxz1() {
            return Axz1;
        }

        public void setAxz1(int Axz1) {
            this.Axz1 = Axz1;
        }

        public int getAxz2() {
            return Axz2;
        }

        public void setAxz2(int Axz2) {
            this.Axz2 = Axz2;
        }

        public int getStoreid() {
            return storeid;
        }

        public void setStoreid(int storeid) {
            this.storeid = storeid;
        }

        public int getQid() {
            return Qid;
        }

        public void setQid(int Qid) {
            this.Qid = Qid;
        }

        public int getQZjsum() {
            return QZjsum;
        }

        public void setQZjsum(int QZjsum) {
            this.QZjsum = QZjsum;
        }

        public String getQzjContent() {
            return QzjContent;
        }

        public void setQzjContent(String QzjContent) {
            this.QzjContent = QzjContent;
        }

        public int getQmoney() {
            return Qmoney;
        }

        public void setQmoney(int Qmoney) {
            this.Qmoney = Qmoney;
        }

        public int getQxzMoney() {
            return QxzMoney;
        }

        public void setQxzMoney(int QxzMoney) {
            this.QxzMoney = QxzMoney;
        }
    }
}
