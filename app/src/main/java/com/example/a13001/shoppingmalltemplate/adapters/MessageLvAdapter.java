package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.modle.Message;
import com.mcxtzhang.swipemenulib.SwipeMenuLayout;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Administrator on 2017/8/9.
 */

public class MessageLvAdapter extends BaseAdapter {
    private Context context;
    private List<Message.ListBean> mList;
    private doDeleteMessageInterface doDeleteMessageInterface;
    public MessageLvAdapter(Context context, List<Message.ListBean> mList) {
        this.context = context;
        this.mList = mList;
    }
    public void setDoDeleteMessageInterface(doDeleteMessageInterface doDeleteMessageInterface) {
        this.doDeleteMessageInterface = doDeleteMessageInterface;
    }
    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolder vh = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_lv_message, parent, false);
            vh = new ViewHolder(view);
            view.setTag(vh);
            AutoUtils.autoSize(view);
        } else {
            vh = (ViewHolder) view.getTag();
        }
        vh.tvItemmessageContent.setText(mList.get(position).getSmsTitle() != null ? mList.get(position).getSmsTitle() : "");
        vh.tvItemmessageDate.setText(mList.get(position).getSmsAddtime() != null ? mList.get(position).getSmsAddtime() : "");
        final ViewHolder finalVh = vh;
        vh.tvMessageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalVh.smlItemcollect.quickClose();
               doDeleteMessageInterface.doDelete(position);
            }
        });
        vh.llContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doDeleteMessageInterface.doStartDetail(position);
            }
        });
        return view;
    }

    public interface doDeleteMessageInterface {
        void doDelete(int position);
        void doStartDetail(int position);
    }


    class ViewHolder {
        @BindView(R.id.tv_itemmessage_content)
        TextView tvItemmessageContent;
        @BindView(R.id.tv_itemmessage_date)
        TextView tvItemmessageDate;
        @BindView(R.id.ll_content)
        LinearLayout llContent;
        @BindView(R.id.tv_message_delete)
        TextView tvMessageDelete;
        @BindView(R.id.sml_itemcollect)
        SwipeMenuLayout smlItemcollect;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
