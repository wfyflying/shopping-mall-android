package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.Goods;
import com.example.a13001.shoppingmalltemplate.modle.GoodsList;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

public class GoodsListRvAdapter  extends RecyclerView.Adapter<GoodsListRvAdapter.MyViewHolder>{
   private Context mContext;
   private List<GoodsList.ListBean> mList;
    onItemClickListener onItemClickListener;
    public GoodsListRvAdapter(Context mContext, List<GoodsList.ListBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }
    public interface onItemClickListener{
        void onClick(int position);
    }
    public void setOnItemClickListener(onItemClickListener onItemClickListener){
        this.onItemClickListener=onItemClickListener;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.item_rv_homepage_sub,parent,false);
        MyViewHolder myViewHolder=new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {
        GlideUtils.setNetImage(mContext, AppConstants.INTERNET_HEAD+mList.get(position).getImages(),myViewHolder.ivLogo);
        myViewHolder.tvContent.setText(mList.get(position).getTitle());
        myViewHolder.tvPrice.setText(mList.get(position).getPrice()+"");
        myViewHolder.tvLoveNum.setText(mList.get(position).getHits()+"");
        if (onItemClickListener != null) {
            myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        private final ImageView ivLogo;
        private final TextView tvContent;
        private final TextView tvPrice;
        private final TextView tvLoveNum;
        public MyViewHolder(View itemView) {
            super(itemView);
            AutoUtils.autoSize(itemView);
            ivLogo = itemView.findViewById(R.id.iv_hompagesub_logo);
            tvContent = itemView.findViewById(R.id.tv_hompagesub_content);
            tvPrice = itemView.findViewById(R.id.tv_hompagesub_price);
            tvLoveNum = itemView.findViewById(R.id.tv_hompagesub_lovenum);
        }
    }
}
