package com.example.a13001.shoppingmalltemplate.modle;

public class Information {
    private int logo;
    private String title;
    private String content;
    private String infoNum;
    private String likeNum;

    public Information(int logo, String title, String content, String infoNum, String likeNum) {
        this.logo = logo;
        this.title = title;
        this.content = content;
        this.infoNum = infoNum;
        this.likeNum = likeNum;
    }

    public Information() {
    }

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getInfoNum() {
        return infoNum;
    }

    public void setInfoNum(String infoNum) {
        this.infoNum = infoNum;
    }

    public String getLikeNum() {
        return likeNum;
    }

    public void setLikeNum(String likeNum) {
        this.likeNum = likeNum;
    }
}
