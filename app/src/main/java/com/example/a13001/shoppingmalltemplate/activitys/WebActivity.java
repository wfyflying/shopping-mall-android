package com.example.a13001.shoppingmalltemplate.activitys;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.ProgressWebView;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WebActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_title_right)
    TextView tvTitleRight;
    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;
    @BindView(R.id.webview)
    ProgressWebView webview;
    private String url;
    private String name;
    private static final String TAG = "WebActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        ButterKnife.bind(this);
        if (getIntent() != null) {
            url = getIntent().getStringExtra("url");
            name = getIntent().getStringExtra("name");
        }
        tvTitleCenter.setText(name);
        setWebView();
    }

    private void setWebView() {

        webview.getSettings().setJavaScriptEnabled(true);
        //设置可以访问文件
        webview.getSettings().setAllowFileAccess(true);
        //设置支持缩放
        webview.getSettings().setBuiltInZoomControls(true);
        //自适应屏幕
        webview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setSupportZoom(true);
        webview.getSettings().setDomStorageEnabled(true);
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webview.getSettings().setBlockNetworkImage(false);
//去掉放大缩小按钮
        webview.getSettings().setDisplayZoomControls(false);
        if (!Utils.isNetworkAvailable(WebActivity.this)) {
            webview.loadUrl("file:///android_asset/404.html");
        }else{
            Log.e(TAG, "setWebView: "+url );
            webview.loadUrl(url);
        }
        //设置Web视图
        webview.setWebViewClient(new webViewClient());
        clearWebViewCache();

    }

    public void clearWebViewCache() {
// 清除cookie即可彻底清除缓存
        CookieSyncManager.createInstance(WebActivity.this);
        CookieManager.getInstance().removeAllCookie();
    }

    @OnClick(R.id.iv_title_back)
    public void onViewClicked() {
        onBackPressed();
    }

    //Web视图
    private class webViewClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public boolean onKeyDown(int keyCoder, KeyEvent event) {
        if (keyCoder == KeyEvent.KEYCODE_BACK && webview.canGoBack()) {
            webview.goBack();// 返回前一个页面
            return true;
        }
        return super.onKeyDown(keyCoder, event);
    }
}
