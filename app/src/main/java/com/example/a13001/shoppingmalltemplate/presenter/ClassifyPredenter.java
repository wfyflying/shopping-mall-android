package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.activitys.GoodsListActivity;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.Banner;
import com.example.a13001.shoppingmalltemplate.modle.Classify;
import com.example.a13001.shoppingmalltemplate.modle.GoodsDetail;
import com.example.a13001.shoppingmalltemplate.modle.GoodsList;
import com.example.a13001.shoppingmalltemplate.modle.News;
import com.example.a13001.shoppingmalltemplate.mvpview.ClassifyView;
import com.example.a13001.shoppingmalltemplate.mvpview.NewsView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ClassifyPredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private ClassifyView mClassifyView;
    private Classify mClassify;
    private GoodsList mGoodsList;



    public ClassifyPredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        manager=new DataManager(mContext);
        mCompositeSubscription=new CompositeSubscription();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mClassifyView=(ClassifyView) view;

    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }

    /**
     * 获取一级分类列表
     * @param companyid  站点id
     * @param channelid  频道ID，指定多个频道用“|”线分割

     */
    public void getClassifyList(String companyid,String channelid){
        mCompositeSubscription.add(manager.getClassifyList(companyid,channelid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Classify>() {
                               @Override
                               public void onCompleted() {
                                   if (mClassifyView!=null){
                                       mClassifyView.onSuccess(mClassify);
                                   }
                               }

                               @Override
                               public void onError(Throwable e) {
                                   mClassifyView.onError("请求失败");
                               }

                               @Override
                               public void onNext(Classify classify) {
                                   mClassify=classify;
                               }
                           }

                ));
    }


    /**
     * 获取商品列表
     * @param companyid 站点ID
     * @param channelid  频道ID，指定多个频道用“|”线分割
     * @param classid   栏目ID，指定多个栏目用“|”线分割
     * @param elite    推荐调用方法 1 调用有推荐 2 调用不推荐，为空调用所有
     * @param xinpin   新品调用方法 1 调用新品 2 调用非新品，为空调用所有
     * @param pagesize  每页显示数量
     * @param pageindex  当前页数
     * @param order  	排序方式 1 上架时间正序 2 上架时间倒序 3 价格从低到高 4 价格从高到低
     *                  5 销量从低到高 6 销量从高到低 7 评价从低到高 8 评价从高到低 9 浏览次数从低到高 10 浏览次数从高到低
     */
    public void getGoodList(String companyid, String channelid,String classid,String elite,String xinpin,String order, int pagesize, int pageindex) {
        mCompositeSubscription.add(manager.getGoodsList(companyid, channelid, classid,elite,"",xinpin,"",order,"","","","","",pagesize,pageindex)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GoodsList>() {
                    @Override
                    public void onCompleted() {
                        if (mClassifyView!=null){
                            mClassifyView.onSuccessGoodsList(mGoodsList);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mClassifyView.onError("请求失败");
                    }

                    @Override
                    public void onNext(GoodsList goodsList) {
                       mGoodsList=goodsList;


                    }
                }));
    }
}
