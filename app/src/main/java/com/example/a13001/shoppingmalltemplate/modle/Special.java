package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class Special {

    /**
     * status : 1
     * returnMsg : null
     * channelid : 105
     * channelName : 商品展示
     * channelOrder : 0
     * count : 3
     * pagesize : 20
     * pageindex : 1
     * list : [{"specialid":10002,"specialDir":"gxcy","specialTitle":"个性创意","conCount":1,"specialImages":"/site/536/upload/spzs/upload/gxcy/20189171422255331.jpg","specialRecommend":1,"specialText1":"","specialText2":"","specialText3":"","Meta_title":"","Meta_Keywords":"","Meta_Description":""},{"specialid":10001,"specialDir":"dzcx","specialTitle":"大众畅销","conCount":1,"specialImages":"/site/536/upload/spzs/upload/dzcx/20189171422162791.jpg","specialRecommend":1,"specialText1":"","specialText2":"","specialText3":"","Meta_title":"","Meta_Keywords":"","Meta_Description":""},{"specialid":10002,"specialDir":"hgz","specialTitle":"红贵族","conCount":1,"specialImages":"/site/495/upload/shop/upload/hgz/2018821512582051.jpg","specialRecommend":1,"specialText1":"","specialText2":"","specialText3":"","Meta_title":"","Meta_Keywords":"","Meta_Description":""},{"specialid":10003,"specialDir":"gdss","specialTitle":"高端时尚","conCount":2,"specialImages":"/site/536/upload/spzs/upload/gdss/2018917142255751.jpg","specialRecommend":1,"specialText1":"","specialText2":"","specialText3":"","Meta_title":"","Meta_Keywords":"","Meta_Description":""},{"specialid":10001,"specialDir":"ht","specialTitle":"红提","conCount":1,"specialImages":"/site/495/upload/shop/upload/ht/201882151294191.jpg","specialRecommend":1,"specialText1":"","specialText2":"","specialText3":"","Meta_title":"","Meta_Keywords":"","Meta_Description":""},{"specialid":10000,"specialDir":"htbh","specialTitle":"户太八号","conCount":0,"specialImages":"/site/495/upload/shop/upload/htbh/20187311420182891.jpg","specialRecommend":1,"specialText1":"","specialText2":"","specialText3":"","Meta_title":"","Meta_Keywords":"","Meta_Description":""}]
     */

    private int status;
    private String returnMsg;
    private int channelid;
    private String channelName;
    private int channelOrder;
    private int count;
    private int pagesize;
    private int pageindex;
    private List<ListBean> list;

    @Override
    public String toString() {
        return "Special{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", channelid=" + channelid +
                ", channelName='" + channelName + '\'' +
                ", channelOrder=" + channelOrder +
                ", count=" + count +
                ", pagesize=" + pagesize +
                ", pageindex=" + pageindex +
                ", list=" + list +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getChannelid() {
        return channelid;
    }

    public void setChannelid(int channelid) {
        this.channelid = channelid;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public int getChannelOrder() {
        return channelOrder;
    }

    public void setChannelOrder(int channelOrder) {
        this.channelOrder = channelOrder;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public int getPageindex() {
        return pageindex;
    }

    public void setPageindex(int pageindex) {
        this.pageindex = pageindex;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * specialid : 10002
         * specialDir : gxcy
         * specialTitle : 个性创意
         * conCount : 1
         * specialImages : /site/536/upload/spzs/upload/gxcy/20189171422255331.jpg
         * specialRecommend : 1
         * specialText1 :
         * specialText2 :
         * specialText3 :
         * Meta_title :
         * Meta_Keywords :
         * Meta_Description :
         */

        private int specialid;
        private String specialDir;
        private String specialTitle;
        private int conCount;
        private String specialImages;
        private int specialRecommend;
        private String specialText1;
        private String specialText2;
        private String specialText3;
        private String Meta_title;
        private String Meta_Keywords;
        private String Meta_Description;

        @Override
        public String toString() {
            return "ListBean{" +
                    "specialid=" + specialid +
                    ", specialDir='" + specialDir + '\'' +
                    ", specialTitle='" + specialTitle + '\'' +
                    ", conCount=" + conCount +
                    ", specialImages='" + specialImages + '\'' +
                    ", specialRecommend=" + specialRecommend +
                    ", specialText1='" + specialText1 + '\'' +
                    ", specialText2='" + specialText2 + '\'' +
                    ", specialText3='" + specialText3 + '\'' +
                    ", Meta_title='" + Meta_title + '\'' +
                    ", Meta_Keywords='" + Meta_Keywords + '\'' +
                    ", Meta_Description='" + Meta_Description + '\'' +
                    '}';
        }

        public int getSpecialid() {
            return specialid;
        }

        public void setSpecialid(int specialid) {
            this.specialid = specialid;
        }

        public String getSpecialDir() {
            return specialDir;
        }

        public void setSpecialDir(String specialDir) {
            this.specialDir = specialDir;
        }

        public String getSpecialTitle() {
            return specialTitle;
        }

        public void setSpecialTitle(String specialTitle) {
            this.specialTitle = specialTitle;
        }

        public int getConCount() {
            return conCount;
        }

        public void setConCount(int conCount) {
            this.conCount = conCount;
        }

        public String getSpecialImages() {
            return specialImages;
        }

        public void setSpecialImages(String specialImages) {
            this.specialImages = specialImages;
        }

        public int getSpecialRecommend() {
            return specialRecommend;
        }

        public void setSpecialRecommend(int specialRecommend) {
            this.specialRecommend = specialRecommend;
        }

        public String getSpecialText1() {
            return specialText1;
        }

        public void setSpecialText1(String specialText1) {
            this.specialText1 = specialText1;
        }

        public String getSpecialText2() {
            return specialText2;
        }

        public void setSpecialText2(String specialText2) {
            this.specialText2 = specialText2;
        }

        public String getSpecialText3() {
            return specialText3;
        }

        public void setSpecialText3(String specialText3) {
            this.specialText3 = specialText3;
        }

        public String getMeta_title() {
            return Meta_title;
        }

        public void setMeta_title(String Meta_title) {
            this.Meta_title = Meta_title;
        }

        public String getMeta_Keywords() {
            return Meta_Keywords;
        }

        public void setMeta_Keywords(String Meta_Keywords) {
            this.Meta_Keywords = Meta_Keywords;
        }

        public String getMeta_Description() {
            return Meta_Description;
        }

        public void setMeta_Description(String Meta_Description) {
            this.Meta_Description = Meta_Description;
        }
    }
}
