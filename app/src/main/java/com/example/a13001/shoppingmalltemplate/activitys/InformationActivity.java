package com.example.a13001.shoppingmalltemplate.activitys;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.adapters.ImfromationLvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.modle.Banner;
import com.example.a13001.shoppingmalltemplate.modle.News;
import com.example.a13001.shoppingmalltemplate.mvpview.InformationView;
import com.example.a13001.shoppingmalltemplate.presenter.InformationPredenter;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InformationActivity extends BaseActivity {

    @BindView(R.id.lv_information)
    ListView lvInformation;
    @BindView(R.id.editText)
    EditText editText;
    @BindView(R.id.srfl_information)
    SmartRefreshLayout srflInformation;
    private List<News.ListBean> mList;
    private ImfromationLvAdapter mAdaper;
    private List<String> mListBanner = new ArrayList<>();


    private News mNews;
    private static final String TAG = "InformationActivity";
    private int pageindex = 1;
    String keyword = "";
    InformationPredenter informationPredenter = new InformationPredenter(InformationActivity.this);
    private ConvenientBanner mConvenBanner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
        ButterKnife.bind(this);
        initData();
        setRefresh();
    }

    InformationView informationView = new InformationView() {
        @Override
        public void onSuccess(News mNews) {
            int status = mNews.getStatus();
            if (status > 0) {
                mList.addAll(mNews.getList());
                mAdaper.notifyDataSetChanged();
            } else {
                Toast.makeText(InformationActivity.this, "" + mNews.getReturnMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onSuccessBanner(Banner banner) {
            int status = banner.getStatus();
            if (status > 0) {
                for (int i = 0; i < banner.getFragmentPictureList().size(); i++) {

                    mListBanner.add(AppConstants.INTERNET_HEAD + banner.getFragmentPictureList().get(i).getFragmentPicture());
                }
                Log.e(TAG, "onSuccessBanner: " + mListBanner.toString());
                MyUtils.setConvenientBannerNetWork(mConvenBanner, mListBanner);
            } else {
                Toast.makeText(InformationActivity.this, "" + banner.getReturnMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onError(String result) {

        }
    };

    private void initData() {
        informationPredenter.onCreate();
        informationPredenter.attachView(informationView);
        informationPredenter.getBannerList(AppConstants.COMPANY_ID, AppConstants.BANNER_LABLE);
        View headerview = LayoutInflater.from(InformationActivity.this).inflate(R.layout.include_headerview_banner, lvInformation, false);
        mConvenBanner = (ConvenientBanner) headerview.findViewById(R.id.cb_homepage);
        lvInformation.addHeaderView(headerview);


        mList = new ArrayList<>();
        mAdaper = new ImfromationLvAdapter(InformationActivity.this, mList);
        informationPredenter.getSearchNews(AppConstants.COMPANY_ID, AppConstants.News_ID, keyword, AppConstants.PAGE_SIZE, pageindex);
        lvInformation.setAdapter(mAdaper);

        editText.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //判断是否是“完成”键
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    keyword = editText.getText().toString().trim();
                    if (mList != null) {
                        mList.clear();
                    }
                    informationPredenter.getSearchNews(AppConstants.COMPANY_ID, AppConstants.News_ID, keyword, AppConstants.PAGE_SIZE, pageindex);

//                    //隐藏软键盘
//                    InputMethodManager imm = (InputMethodManager) v
//                            .getContext().getSystemService(
//                                    Context.INPUT_METHOD_SERVICE);
//                    if (imm.isActive()) {
//                        imm.hideSoftInputFromWindow(
//                                v.getApplicationWindowToken(), 0);
//                    }
                    return true;
                }
                return false;
            }
        });

    }
    private void setRefresh() {
        //刷新
        srflInformation.setRefreshHeader(new ClassicsHeader(InformationActivity.this));
        srflInformation.setRefreshFooter(new ClassicsFooter(InformationActivity.this));
//        mRefresh.setEnablePureScrollMode(true);
        srflInformation.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                pageindex = 1;
                if (mList != null) {
                    mList.clear();
                }
                informationPredenter.getSearchNews(AppConstants.COMPANY_ID, AppConstants.News_ID, keyword, AppConstants.PAGE_SIZE, pageindex);
                refreshlayout.finishRefresh(2000/*,true*/);//传入false表示刷新失败
            }
        });
        srflInformation.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                pageindex++;
                informationPredenter.getSearchNews(AppConstants.COMPANY_ID, AppConstants.News_ID, keyword, AppConstants.PAGE_SIZE, pageindex);
                refreshlayout.finishLoadMore(2000/*,true*/);//传入false表示加载失败
            }
        });
    }

}
