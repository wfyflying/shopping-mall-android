package com.example.a13001.shoppingmalltemplate.activitys;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.adapters.EvaluateLvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.modle.EvaluateList;
import com.example.a13001.shoppingmalltemplate.modle.GoodsParameters;
import com.example.a13001.shoppingmalltemplate.mvpview.EvaluateView;
import com.example.a13001.shoppingmalltemplate.presenter.EvaluatePredenter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EvaluateActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_evaluate_all)
    TextView tvEvaluateAll;
    @BindView(R.id.tv_evaluate_numall)
    TextView tvEvaluateNumall;
    @BindView(R.id.ll_evaluate_all)
    LinearLayout llEvaluateAll;
    @BindView(R.id.tv_evaluate_good)
    TextView tvEvaluateGood;
    @BindView(R.id.tv_evaluate_numgood)
    TextView tvEvaluateNumgood;
    @BindView(R.id.ll_evaluate_good)
    LinearLayout llEvaluateGood;
    @BindView(R.id.tv_evaluate_medium)
    TextView tvEvaluateMedium;
    @BindView(R.id.tv_evaluate_nummedium)
    TextView tvEvaluateNummedium;
    @BindView(R.id.ll_evaluate_medium)
    LinearLayout llEvaluateMedium;
    @BindView(R.id.tv_evaluate_bad)
    TextView tvEvaluateBad;
    @BindView(R.id.tv_evaluate_numbad)
    TextView tvEvaluateNumbad;
    @BindView(R.id.ll_evaluate_bad)
    LinearLayout llEvaluateBad;
    @BindView(R.id.lv_evaluate)
    ListView lvEvaluate;
    @BindView(R.id.srfl_evaluate)
    SmartRefreshLayout srflEvaluate;
    private List<EvaluateList.ListBean> mList;
    private EvaluateLvAdapter mAdapter;

    private static final String TAG = "EvaluateActivity";
    private String level = "";//level------评价等级，1 好评 2 中评 3 差评，为空则调用所有
    private int mGoodId;
    private int pageindex = 1;
    private EvaluatePredenter evaluatePredenter = new EvaluatePredenter(EvaluateActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluate);
        ButterKnife.bind(this);
        initData();
    }

    /**
     * 初始化数据源
     */
    private void initData() {
        evaluatePredenter.onCreate();
        evaluatePredenter.attachView(evaluateView);

        tvTitleCenter.setText("商品评价");

        mGoodId = getIntent().getIntExtra("goodid", 0);
        evaluatePredenter.getEvaluateNum(AppConstants.COMPANY_ID, mGoodId);
        llEvaluateAll.performClick();
        mList = new ArrayList<>();
        mAdapter = new EvaluateLvAdapter(EvaluateActivity.this, mList);
        //level------评价等级，1 好评 2 中评 3 差评，为空则调用所有
        evaluatePredenter.getEvaluateList(AppConstants.COMPANY_ID, mGoodId, level, "", AppConstants.PAGE_SIZE, pageindex, AppConstants.FROM_MOBILE);
        lvEvaluate.setAdapter(mAdapter);

        setRefresh();
    }

    EvaluateView evaluateView = new EvaluateView() {
        @Override
        public void onSuccessGetEvaluate(EvaluateList evaluateList) {
            Log.e(TAG, "onSuccessGetEvaluateNum: " + evaluateList.toString());
            int status = evaluateList.getStatus();
            if (status > 0) {
                mList.addAll(evaluateList.getList());
                mAdapter.notifyDataSetChanged();
            } else {

            }
        }

        @Override
        public void onSuccessGetEvaluateNum(GoodsParameters goodsParameters) {
            Log.e(TAG, "onSuccessGetEvaluateNum: " + goodsParameters.toString());
            int status = goodsParameters.getStatus();
            if (status > 0) {
                tvEvaluateNumall.setText(goodsParameters.getEvaluate() + "");
                tvEvaluateNumgood.setText(goodsParameters.getEvaluate_good() + "");
                tvEvaluateNummedium.setText(goodsParameters.getEvaluate_ordinary() + "");
                tvEvaluateNumbad.setText(goodsParameters.getEvaluate_bad() + "");
            } else {

            }
        }

        @Override
        public void onError(String result) {
            Log.e(TAG, "onError: " + result.toString());
        }
    };

    @OnClick({R.id.iv_title_back, R.id.ll_evaluate_all, R.id.ll_evaluate_good, R.id.ll_evaluate_medium, R.id.ll_evaluate_bad})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_title_back:
                onBackPressed();
                break;
            //全部评价
            case R.id.ll_evaluate_all:
                initTxtColor();
                tvEvaluateAll.setTextColor(getResources().getColor(R.color.ff2828));
                tvEvaluateNumall.setTextColor(getResources().getColor(R.color.ff2828));
                if (mList != null) {
                    mList.clear();
                }
                pageindex = 1;
                level = "";
                //level------评价等级，1 好评 2 中评 3 差评，为空则调用所有
                evaluatePredenter.getEvaluateList(AppConstants.COMPANY_ID, mGoodId, level, "", AppConstants.PAGE_SIZE, pageindex, AppConstants.FROM_MOBILE);
                lvEvaluate.setAdapter(mAdapter);
                break;
            //好评
            case R.id.ll_evaluate_good:
                initTxtColor();
                tvEvaluateGood.setTextColor(getResources().getColor(R.color.ff2828));
                tvEvaluateNumgood.setTextColor(getResources().getColor(R.color.ff2828));
                if (mList != null) {
                    mList.clear();
                }
                pageindex = 1;
                level = "1";
                //level------评价等级，1 好评 2 中评 3 差评，为空则调用所有
                evaluatePredenter.getEvaluateList(AppConstants.COMPANY_ID, mGoodId, level, "", AppConstants.PAGE_SIZE, pageindex, AppConstants.FROM_MOBILE);
                lvEvaluate.setAdapter(mAdapter);
                break;
            //中评
            case R.id.ll_evaluate_medium:
                initTxtColor();
                tvEvaluateMedium.setTextColor(getResources().getColor(R.color.ff2828));
                tvEvaluateNummedium.setTextColor(getResources().getColor(R.color.ff2828));
                if (mList != null) {
                    mList.clear();
                }
                pageindex = 1;
                level = "2";
                //level------评价等级，1 好评 2 中评 3 差评，为空则调用所有
                evaluatePredenter.getEvaluateList(AppConstants.COMPANY_ID, mGoodId, level, "", AppConstants.PAGE_SIZE, pageindex, AppConstants.FROM_MOBILE);
                lvEvaluate.setAdapter(mAdapter);
                break;
            //差评
            case R.id.ll_evaluate_bad:
                initTxtColor();
                tvEvaluateBad.setTextColor(getResources().getColor(R.color.ff2828));
                tvEvaluateNumbad.setTextColor(getResources().getColor(R.color.ff2828));
                if (mList != null) {
                    mList.clear();
                }
                pageindex = 1;
                level = "3";
                //level------评价等级，1 好评 2 中评 3 差评，为空则调用所有
                evaluatePredenter.getEvaluateList(AppConstants.COMPANY_ID, mGoodId, level, "", AppConstants.PAGE_SIZE, pageindex, AppConstants.FROM_MOBILE);
                lvEvaluate.setAdapter(mAdapter);
                break;
        }
    }

    /**
     * 初始化顶部标题颜色
     */
    private void initTxtColor() {
        tvEvaluateAll.setTextColor(getResources().getColor(R.color.t333));
        tvEvaluateNumall.setTextColor(getResources().getColor(R.color.t333));
        tvEvaluateGood.setTextColor(getResources().getColor(R.color.t333));
        tvEvaluateNumgood.setTextColor(getResources().getColor(R.color.t333));
        tvEvaluateMedium.setTextColor(getResources().getColor(R.color.t333));
        tvEvaluateNummedium.setTextColor(getResources().getColor(R.color.t333));
        tvEvaluateBad.setTextColor(getResources().getColor(R.color.t333));
        tvEvaluateNumbad.setTextColor(getResources().getColor(R.color.t333));
    }
    private void setRefresh() {
        //刷新
        srflEvaluate.setRefreshHeader(new ClassicsHeader(EvaluateActivity.this));
        srflEvaluate.setRefreshFooter(new ClassicsFooter(EvaluateActivity.this));
//        mRefresh.setEnablePureScrollMode(true);
        srflEvaluate.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                pageindex = 1;
                if (mList != null) {
                    mList.clear();
                }
                evaluatePredenter.getEvaluateList(AppConstants.COMPANY_ID, mGoodId, level, "", AppConstants.PAGE_SIZE, pageindex, AppConstants.FROM_MOBILE);
                refreshlayout.finishRefresh(2000/*,true*/);//传入false表示刷新失败
            }
        });
        srflEvaluate.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                pageindex++;
                evaluatePredenter.getEvaluateList(AppConstants.COMPANY_ID, mGoodId, level, "", AppConstants.PAGE_SIZE, pageindex, AppConstants.FROM_MOBILE);
                refreshlayout.finishLoadMore(2000/*,true*/);//传入false表示加载失败
            }
        });
    }

}
