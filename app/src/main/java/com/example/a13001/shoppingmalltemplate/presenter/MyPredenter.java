package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;

import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.Banner;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.GoodsDetail;
import com.example.a13001.shoppingmalltemplate.modle.LoginStatus;
import com.example.a13001.shoppingmalltemplate.modle.News;
import com.example.a13001.shoppingmalltemplate.modle.OrderNum;
import com.example.a13001.shoppingmalltemplate.modle.SignIn;
import com.example.a13001.shoppingmalltemplate.modle.UserInfo;
import com.example.a13001.shoppingmalltemplate.mvpview.MyView;
import com.example.a13001.shoppingmalltemplate.mvpview.NewsView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class MyPredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private MyView mMyView;
    private SignIn mSignIn;
    private UserInfo mUser;
    private CommonResult mCommonResult;
    private LoginStatus mLoginStatus;
    private OrderNum mOrderNum;
    public MyPredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        manager=new DataManager(mContext);
        mCompositeSubscription=new CompositeSubscription();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mMyView=(MyView) view;

    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }

    /**
     * 签到
     * @param companyid
     * @param code
     * @param timestamp
     */
    public void doSignIn(String companyid,String code,String timestamp){
        mCompositeSubscription.add(manager.doSignIn(companyid,code,timestamp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SignIn>() {
                               @Override
                               public void onCompleted() {
                                   if (mSignIn!=null){
                                       mMyView.onSuccessDoSignIn(mSignIn);
                                   }
                               }

                               @Override
                               public void onError(Throwable e) {
                                   mMyView.onError("请求失败");
                               }

                               @Override
                               public void onNext(SignIn signIn) {
                                   mSignIn=signIn;
                               }
                           }

                ));
    }
    /**
     * 获取会员详细信息
     * @param companyid    站点ID
     * @param code          安全校验码
     * @param timestamp     时间戳
     * @return
     */
    public void getUserInfo(String companyid,String code,String timestamp) {

        mCompositeSubscription.add(manager.getUserInfo(companyid, code,timestamp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserInfo>() {
                    @Override
                    public void onCompleted() {
                        if (mMyView!=null){
                            mMyView.onSuccessUserInfo(mUser);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mMyView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(UserInfo userInfo) {
                        mUser=userInfo;
                    }
                }));
    }
    /**
     * 上传会员头像（APP）
     * @param companyid
     * @param code
     * @param timestamp
     * @param face  头像数据，base64码
     * @return
     */
    public void modifyHeadImg(String companyid, String code, String timestamp, String face) {

        mCompositeSubscription.add(manager.upLoadHeadImg(companyid, code,timestamp,face)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mMyView!=null){
                            mMyView.onSuccessModifyHeadImg(mCommonResult);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mMyView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
    /**
     * 判断登录状态
     * @param companyid  站点ID
     * @param code    安全校验码
     * @param timestamp  时间戳
     * @param from   来源，pc 电脑端 mobile 移动端
     * @return
     */
    public void getLoginStatus(String companyid,String code,String timestamp, String from) {

        mCompositeSubscription.add(manager.getLoginStatus(companyid,code,timestamp, from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<LoginStatus>() {
                    @Override
                    public void onCompleted() {
                        if (mMyView!=null){
                            mMyView.onSuccessLoginStatus(mLoginStatus);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mMyView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(LoginStatus loginStatus) {
                        mLoginStatus=loginStatus;
                    }
                }));
    }
    /**
     * ★ 获取订单数量
     * @param companyid
     * @param code
     * @param timestamp
     * @param type            订单商品类型，1 普通商品 2 虚拟商品，为空则所有
     * @param status        订单状态 1 待支付 2 未发货 3 已发货 4 已签收，为空调返回所有订单状态数量
     * @return
     */
    public void getOrderNum(String companyid, String code, String timestamp, String type,String status) {

        mCompositeSubscription.add(manager.getOrderNum(companyid,code,timestamp, type,status)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<OrderNum>() {
                    @Override
                    public void onCompleted() {
                        if (mMyView!=null){
                            mMyView.onSuccessGetOrderNum(mOrderNum);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mMyView.onError("请求失败"+e.toString());
                    }

                    @Override
                    public void onNext(OrderNum orderNum) {
                        mOrderNum=orderNum;
                    }
                }));
    }
}
