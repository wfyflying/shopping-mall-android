package com.example.a13001.shoppingmalltemplate.modle;

public class AddressOrderId {

    /**
     * status : 1
     * returnMsg : null
     * ReceiptName : feifei
     * ReceiptPhone : 18330695171
     * addressProvince : 北京市
     * addressCity : 北京市
     * addressArea : 东城区
     * addressXX : 102
     * ReciptZipCode : 100000
     */

    private int status;
    private String returnMsg;
    private String ReceiptName;
    private String ReceiptPhone;
    private String addressProvince;
    private String addressCity;
    private String addressArea;
    private String addressXX;
    private String ReciptZipCode;

    @Override
    public String toString() {
        return "AddressOrderId{" +
                "status=" + status +
                ", returnMsg='" + returnMsg + '\'' +
                ", ReceiptName='" + ReceiptName + '\'' +
                ", ReceiptPhone='" + ReceiptPhone + '\'' +
                ", addressProvince='" + addressProvince + '\'' +
                ", addressCity='" + addressCity + '\'' +
                ", addressArea='" + addressArea + '\'' +
                ", addressXX='" + addressXX + '\'' +
                ", ReciptZipCode='" + ReciptZipCode + '\'' +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public String getReceiptName() {
        return ReceiptName;
    }

    public void setReceiptName(String ReceiptName) {
        this.ReceiptName = ReceiptName;
    }

    public String getReceiptPhone() {
        return ReceiptPhone;
    }

    public void setReceiptPhone(String ReceiptPhone) {
        this.ReceiptPhone = ReceiptPhone;
    }

    public String getAddressProvince() {
        return addressProvince;
    }

    public void setAddressProvince(String addressProvince) {
        this.addressProvince = addressProvince;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressArea() {
        return addressArea;
    }

    public void setAddressArea(String addressArea) {
        this.addressArea = addressArea;
    }

    public String getAddressXX() {
        return addressXX;
    }

    public void setAddressXX(String addressXX) {
        this.addressXX = addressXX;
    }

    public String getReciptZipCode() {
        return ReciptZipCode;
    }

    public void setReciptZipCode(String ReciptZipCode) {
        this.ReciptZipCode = ReciptZipCode;
    }
}
