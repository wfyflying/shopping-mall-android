package com.example.a13001.shoppingmalltemplate.activitys;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.MyListView;
import com.example.a13001.shoppingmalltemplate.adapters.ShouHouDetailLvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.modle.GoodsEvaluateDetail;
import com.example.a13001.shoppingmalltemplate.modle.GoodsEvaluateList;
import com.example.a13001.shoppingmalltemplate.mvpview.GoodsEvaluateView;
import com.example.a13001.shoppingmalltemplate.presenter.GoodsEvaluatePredenter;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import am.widget.drawableratingbar.DrawableRatingBar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GooodsEvaluateDetailActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_title_right)
    TextView tvTitleRight;
    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;
    @BindView(R.id.drb_goevaluate_level)
    DrawableRatingBar drbGoevaluateLevel;
    @BindView(R.id.iv_goevaluate_logo)
    ImageView ivGoevaluateLogo;
    @BindView(R.id.iv_goevaluate_number)
    TextView ivGoevaluateNumber;
    @BindView(R.id.tv_goevaluate_goodsname)
    TextView tvGoevaluateGoodsname;
    @BindView(R.id.iv_goevaluate_guige)
    TextView ivGoevaluateGuige;
    private static final String TAG = "GooodsEvaluateDetailAct";
    GoodsEvaluatePredenter goodsEvaluatePredenter = new GoodsEvaluatePredenter(GooodsEvaluateDetailActivity.this);
    @BindView(R.id.tv_goevaluate_lable)
    TextView tvGoevaluateLable;
    @BindView(R.id.tv_goevaluate_xinde)
    TextView tvGoevaluateXinde;
    @BindView(R.id.mlv_tupian)
    MyListView mlvTupian;
    @BindView(R.id.sv_goodsevaluatedetail)
    ScrollView svGoodsevaluatedetail;
    @BindView(R.id.tv_date)
    TextView tvDate;
    private String code;
    private String timeStamp;
    private int mCartid;
    private List<String> mList;
    private ShouHouDetailLvAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_gooods_evaluatedetail);
        ButterKnife.bind(this);
        initData();
    }


    GoodsEvaluateView goodsEvaluateView = new GoodsEvaluateView() {


        @Override
        public void onSuccessGetGoodsEvaluateList(GoodsEvaluateList goodsEvaluateList) {

        }

        @Override
        public void onSuccessGetGoodsEvaluateDetail(GoodsEvaluateDetail goodsEvaluateDetail) {
            Log.e(TAG, "onSuccessGetGoodsEvaluateDetail: " + goodsEvaluateDetail.toString());
            int status = goodsEvaluateDetail.getStatus();
            if (status > 0) {
                GlideUtils.setNetImage(GooodsEvaluateDetailActivity.this, AppConstants.INTERNET_HEAD + goodsEvaluateDetail.getCartImg(), ivGoevaluateLogo);
                ivGoevaluateNumber.setText("数量X" + goodsEvaluateDetail.getCommodityNumber());
                tvGoevaluateGoodsname.setText(goodsEvaluateDetail.getCommodityName() != null ? goodsEvaluateDetail.getCommodityName() : "");
                ivGoevaluateGuige.setText(goodsEvaluateDetail.getCommodityProperty() != null ? goodsEvaluateDetail.getCommodityProperty() : "");
                tvDate.setText(goodsEvaluateDetail.getCommentTime() != null ? goodsEvaluateDetail.getCommentTime() : "");
                tvGoevaluateLable.setText(goodsEvaluateDetail.getCommentPQ() != null ? goodsEvaluateDetail.getCommentPQ() : "");
                tvGoevaluateXinde.setText(goodsEvaluateDetail.getCommentContent() != null ? goodsEvaluateDetail.getCommentContent() : "");
                int commentLevel = goodsEvaluateDetail.getCommentLevel();
                  drbGoevaluateLevel.setRating(commentLevel);

                mList = new ArrayList<>();
                String repairsImages = goodsEvaluateDetail.getCommentImages();
                String[] split = repairsImages.split(Pattern.quote("|"));
                for (int i = 0; i < split.length; i++) {
                    mList.add(AppConstants.INTERNET_HEAD + split[i]);
                }
                mAdapter = new ShouHouDetailLvAdapter(GooodsEvaluateDetailActivity.this, mList);
                mlvTupian.setAdapter(mAdapter);
            } else {

            }
        }

        @Override
        public void onError(String result) {

        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    /**
     * 初始化数据源
     */
    private void initData() {
        goodsEvaluatePredenter.onCreate();
        goodsEvaluatePredenter.attachView(goodsEvaluateView);

        String safetyCode = MyUtils.getMetaValue(GooodsEvaluateDetailActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        timeStamp = Utils.getTimeStamp();

        tvTitleCenter.setText("商品评价");

        mlvTupian.setFocusable(false);
        if (getIntent() != null) {
            mCartid = getIntent().getIntExtra("cartid", 0);
        }
        goodsEvaluatePredenter.getGoodsEvaluateDetail(AppConstants.COMPANY_ID, code, timeStamp, String.valueOf(mCartid), AppConstants.FROM_MOBILE);


    }


    @OnClick({R.id.iv_title_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_title_back:
                onBackPressed();
                break;

        }
    }


}
