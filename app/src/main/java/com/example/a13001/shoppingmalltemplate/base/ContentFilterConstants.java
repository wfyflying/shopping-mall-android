package com.example.a13001.shoppingmalltemplate.base;

public class ContentFilterConstants {
    public static final String orderstatus="1";//是否开启排序搜索，1 开启 0 关闭
    public static final String orderlist="默认|default,上架|time,价格|price,销量|sales,评价|comment,点击|hits";//指定排序方法，请参考示例格式，可以隐藏部分
    public static final String classstatus="1";//是否开启栏目搜索，1 开启 0 关闭
    public static final String parentclassid="0";//上级栏目ID，为0和为空调用一级栏目
    public static final String classtitle="商品分类";//指定栏目搜索名称，不指定则返回默认值
    public static final String specialstatus="1";//是否开启专题搜索，1 开启 0 关闭
    public static final String specialtitle="专题分类";//指定专题搜索名称，不指定则返回默认值
    public static final String fieldstatus="1";//是否开启自定义字段筛选，1 开启 0 关闭
    public static final String pricestatus="1";//是否开启价格区间搜索，1 开启 0 关闭
    public static final String pricetitle="价格区间";//指定价格区间搜索名称，不指定则返回默认值
    public static final String pricelist="0-100,100-500,500-2000,2000-5000,5000-10000,10000-20000,20000-50000,50000以上";//指定价格区间搜索名称，不指定则返回默认值
    public static final String attributestatus="1";//是否开启属性搜索，1 开启 0 关闭
    public static final String attributetitle="内容属性";//指定属性方法搜索名称，不指定则返回默认值
    public static final String attributelist="推荐|elite,热门|hot,新品|xinpin,促销|cuxiao";//指定属性方法，请参考示例格式，可以隐藏部分
}
