package com.example.a13001.shoppingmalltemplate.activitys;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.example.a13001.shoppingmalltemplate.MainActivity;
import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.MyGridView;
import com.example.a13001.shoppingmalltemplate.adapters.GoodsDetailTuiJianLvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.event.AddShopCarEvent;
import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.DetailGoods;
import com.example.a13001.shoppingmalltemplate.modle.GoodsList;
import com.example.a13001.shoppingmalltemplate.modle.GoodsParameters;
import com.example.a13001.shoppingmalltemplate.modle.LoginStatus;
import com.example.a13001.shoppingmalltemplate.modle.Result;
import com.example.a13001.shoppingmalltemplate.modle.SelectParameters;
import com.example.a13001.shoppingmalltemplate.modle.ShopCarGoods;
import com.example.a13001.shoppingmalltemplate.mvpview.GoodsDetailView;
import com.example.a13001.shoppingmalltemplate.presenter.GoodsDetailPredenter;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.SPUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;
import com.google.gson.Gson;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sharesdk.onekeyshare.OnekeyShare;
import rx.subscriptions.CompositeSubscription;

public class GoodsDetailActivity extends BaseActivity {


    @BindView(R.id.cb_goodsdetail)
    ConvenientBanner cbGoodsdetail;
    @BindView(R.id.iv_gooddetail_back)
    ImageView ivGooddetailBack;
    @BindView(R.id.iv_gooddetail_share)
    ImageView ivGooddetailShare;
    @BindView(R.id.tv_goods_name)
    TextView tvGoodsName;
    @BindView(R.id.tv_goods_price)
    TextView tvGoodsPrice;
    @BindView(R.id.iv_collect)
    ImageView ivCollect;
    @BindView(R.id.tv_collect)
    TextView tvCollect;
    @BindView(R.id.ll_collect)
    LinearLayout llCollect;
    @BindView(R.id.tv_add_shopcar)
    TextView tvAddShopcar;
    @BindView(R.id.tv_pay)
    TextView tvPay;
    @BindView(R.id.tv_choosed)
    TextView tvChoosed;
    @BindView(R.id.tv_goodsdetail_text1)
    TextView tvGoodsdetailText1;
    @BindView(R.id.ll_choose_params)
    LinearLayout llChooseParams;
    @BindView(R.id.iv_goodsdetail_jian)
    ImageView ivGoodsdetailJian;
    @BindView(R.id.tv_goodsdetail_num)
    TextView tvGoodsdetailNum;
    @BindView(R.id.iv_goodsdetail_jia)
    ImageView ivGoodsdetailJia;
    @BindView(R.id.ll_choose_num)
    LinearLayout llChooseNum;
    @BindView(R.id.ll_scheme)
    LinearLayout llScheme;
    @BindView(R.id.ll_gooddetail_evaluate)
    LinearLayout llGooddetailEvaluate;
    @BindView(R.id.ll_service)
    LinearLayout llService;
    @BindView(R.id.ll_gooddetail_queandans)
    RelativeLayout llGooddetailQueandans;
    @BindView(R.id.mgv_goodsdetail)
    MyGridView mgvGoodsdetail;
    @BindView(R.id.tv_shopcar_num)
    TextView tvShopcarNum;
    private PopupWindow popWnd;
    private ImageView mIvClose;
    private Button mBtnCommit;
    private int mGoodId;
    private CompositeSubscription mCompositeSubscription;
    private DataManager manager;
    private static final String TAG = "GoodsDetailActivity";
    private String mContent = null;
    private GoodsDetailPredenter goodsDetailPredenter = new GoodsDetailPredenter(GoodsDetailActivity.this);
    private GoodsParameters mGoodsParameters;
    private int mPosition = 0;
    private int mPositionTwo = 0;
    private String mLogoImage;
    private String mPrice;
    private String mChooseOne = "";
    private String mChooseTwo = "";
    private List<String> mListTitle;
    private String mSelectParameters;
    private SelectParameters onj2;
    private int mPosition1;
    private int mPosition2;
    private int type = -1;
    private String mGoodsName;
    private DecimalFormat df;
    private String mType;
    private List<GoodsList.ListBean> mList;
    private GoodsDetailTuiJianLvAdapter mAdapter;
    private String classifyid = "";
    private String elite = "";
    private String hot = "";
    private String xinpin = "";
    private String cuxiao = "";
    private String order = "";
    private String specialid = "";
    private String price1 = "";
    private String price2 = "";
    private String keyword = "";
    private String excludeid = "";
    private int pageindex = 1;
    private int mClass_id;
    private int ifLogin;
    private String code;
    private String timestamp;
    private String clickType="";
    private TextView mTvNum;
    private TagFlowLayout mTfTitle1;
    private TagFlowLayout mTfTitle2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods_detail);
        ButterKnife.bind(this);
        EventBus.getDefault().register(GoodsDetailActivity.this);

        mGoodId = getIntent().getIntExtra("good_id", 0);
        mClass_id = getIntent().getIntExtra("class_id", 0);
        mType = getIntent().getStringExtra("type");
        if ("积分兑换".equals(mType)) {
            llChooseParams.setVisibility(View.GONE);
            llChooseNum.setVisibility(View.VISIBLE);
            tvAddShopcar.setVisibility(View.GONE);
            tvPay.setText("立即兑换");
        } else if ("促销秒杀".equals(mType)) {
            llChooseParams.setVisibility(View.GONE);
            llChooseNum.setVisibility(View.VISIBLE);
            tvAddShopcar.setVisibility(View.GONE);
            tvPay.setText("开始抢购");
        } else {
            llChooseParams.setVisibility(View.VISIBLE);
            llChooseNum.setVisibility(View.GONE);
            tvAddShopcar.setVisibility(View.VISIBLE);
            tvPay.setText("立即购买");
        }

        Log.e(TAG, "onCreate: " + mGoodId);
        goodsDetailPredenter.onCreate();
        goodsDetailPredenter.attachView(goodsDetailView);

        String safetyCode = MyUtils.getMetaValue(GoodsDetailActivity.this, "safetyCode");
        code = Utils.md5(safetyCode+Utils.getTimeStamp());
        timestamp = Utils.getTimeStamp();

        mList = new ArrayList<>();
        mAdapter = new GoodsDetailTuiJianLvAdapter(GoodsDetailActivity.this, mList);
        excludeid = String.valueOf(mGoodId);
        getData();
        Log.e(TAG, "onCreate: " + mGoodId + "==" + classifyid);
        mgvGoodsdetail.setAdapter(mAdapter);




        mSelectParameters = (String) SPUtils.get(String.valueOf(mGoodId), "");
        if (!TextUtils.isEmpty(mSelectParameters)) {
            Gson gson = new Gson();
            onj2 = gson.fromJson(mSelectParameters, SelectParameters.class);
            String parameter2 = onj2.getParameter2();

            if (!TextUtils.isEmpty(parameter2)) {
                tvChoosed.setText("已选：" + onj2.getParameter1() + "   " + parameter2);
            } else {
                tvChoosed.setText("已选：" + onj2.getParameter1() + "   ");
            }

        }

        mgvGoodsdetail.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        Intent intent=new Intent(GoodsDetailActivity.this, GoodsDetailActivity.class);
                        intent.putExtra("good_id",mList.get(i).getId());
                        intent.putExtra("class_id",mList.get(i).getClassid());
                        intent.putExtra("type","a");
                       startActivity(intent);

            }
        });
    }

    /**
     * 获取数据
     */
    private void getData() {
        //判断登录状态
        goodsDetailPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timestamp,AppConstants.FROM_MOBILE);
        //获取商品详情
        goodsDetailPredenter.getGoodDetail(AppConstants.COMPANY_ID, mGoodId);
        //获取推荐商品列表
        goodsDetailPredenter.getGoodList(AppConstants.COMPANY_ID, AppConstants.CLASSIFY_ID, String.valueOf(mClass_id), elite, hot, xinpin, cuxiao, order, specialid, price1, price2, keyword, excludeid, AppConstants.PAGE_SIZE, pageindex);
        //获取商品规格
        goodsDetailPredenter.getGoodsParameters(AppConstants.COMPANY_ID, mGoodId);
        //判断是否收藏
        goodsDetailPredenter.ifCollect(AppConstants.COMPANY_ID, mGoodId);
        //获取购物车商品数量
        goodsDetailPredenter.getShopCarGoods(AppConstants.COMPANY_ID,AppConstants.FROM_MOBILE);
    }


    private GoodsDetailView goodsDetailView = new GoodsDetailView() {

        /**
         * 获取商品规格参数
         * @param goodsParameters
         */
        @Override
        public void onSuccess(GoodsParameters goodsParameters) {
            Log.e(TAG, "onSuccess: " + goodsParameters.toString());
            mGoodsParameters = goodsParameters;
            tvChoosed.setText("已选：" + goodsParameters.getShop_attribute().get(0).getShop_attribute_xh() + goodsParameters.getShop_attribute().get(0).getShop_attribute_data().get(0).getShop_attribute_gg());

        }

        /**
         * 获取购物车商品
         * @param shopCarGoods
         */
        @Override
        public void onSuccessGetShopCarGoods(ShopCarGoods shopCarGoods) {
            Log.e(TAG, "onSuccessGetShopCarGoods: "+shopCarGoods.toString() );
            int status = shopCarGoods.getStatus();
            if (status>0){
                tvShopcarNum.setText(shopCarGoods.getSum()+"");
            }else{

            }
        }

        /**
         * 获取商品详情
         * @param detailGoods
         */
        @Override
        public void onSuccessGoodsDetail(DetailGoods detailGoods) {
            Log.e(TAG, "onSuccessGoodsDetail: " + detailGoods.toString());
            List<String> mList = new ArrayList<>();
            if (detailGoods.getStatus() > 0) {
                classifyid = String.valueOf(detailGoods.getContent().get(0).getClassid());
                mLogoImage = detailGoods.getContent().get(0).getImages();
                if (TextUtils.isEmpty(detailGoods.getContent().get(0).getText1())) {
                    tvGoodsdetailText1.setVisibility(View.GONE);
                } else {
                    tvGoodsdetailText1.setVisibility(View.VISIBLE);
                    tvGoodsdetailText1.setText(detailGoods.getContent().get(0).getText1());
                }
                String images = detailGoods.getContent().get(0).getImagesAll();
                if (images.contains(",")) {
                    String[] split = images.split(",");
                    for (int i = 0; i < split.length; i++) {
                        mList.add(AppConstants.INTERNET_HEAD + split[i]);
                    }
                } else {
                    mList.add(AppConstants.INTERNET_HEAD + images);
                }

                Log.e(TAG, "onNext: " + mList.toString());
                MyUtils.setConvenientBannerNetWork1(cbGoodsdetail, mList);
                //商品名称
                mGoodsName = detailGoods.getContent().get(0).getTitle() != null ? detailGoods.getContent().get(0).getTitle() : "";
                tvGoodsName.setText(mGoodsName);
                //商品优惠后价格
                //格式化
                if ("积分兑换".equals(mType)) {
                    tvGoodsPrice.setText("积分：" + detailGoods.getContent().get(0).getPrice());
                } else {
                    if (onj2 != null) {
                        String price = onj2.getPrice();
                        if (!TextUtils.isEmpty(price)) {
                            tvGoodsPrice.setText("¥" + price);
                        } else {
                            df = new DecimalFormat("0.00");
                            String format = df.format(detailGoods.getContent().get(0).getPrice());
                            tvGoodsPrice.setText("¥" + format);
                            mPrice = format;
                        }
                    } else {
                        df = new DecimalFormat("0.00");
                        String format = df.format(detailGoods.getContent().get(0).getPrice());
                        tvGoodsPrice.setText("¥" + format);
                        mPrice = format;
                    }
                }


                mContent = String.valueOf(detailGoods.getContent().get(0).getId());
            } else {
                Toast.makeText(GoodsDetailActivity.this, "商品详情获取失败", Toast.LENGTH_SHORT).show();
            }
        }

        /**
         * 获取会员登录状态
         * @param loginStatus
         */
        @Override
        public void onSuccessLoginStatus(LoginStatus loginStatus) {
            Log.e(TAG, "onSuccessLoginStatus: "+loginStatus.toString());
            int status=loginStatus.getStatus();
            if (status>0){
                ifLogin=1;
                switch (clickType){
                    case "立即购买":
                        if ("积分兑换".equals(mType)) {
                            type = 2;
                            String num = tvGoodsdetailNum.getText().toString().trim();
                            goodsDetailPredenter.addShopCar(AppConstants.COMPANY_ID, mGoodId, "", Integer.valueOf(num), AppConstants.ADD_BUY, AppConstants.ADD_INTERGRAL, AppConstants.FROM_MOBILE);
                        } else if ("促销秒杀".equals(mType)) {
                            type = 3;
                            String num = tvGoodsdetailNum.getText().toString().trim();
                            goodsDetailPredenter.addShopCar(AppConstants.COMPANY_ID, mGoodId, "", Integer.valueOf(num), AppConstants.ADD_BUY, AppConstants.ADD_SECKILL, AppConstants.FROM_MOBILE);

                        } else {
                            try {
                                type = 1;
                                Log.e(TAG, "onViewClicked: "+ mPosition1+"==>"+mPosition2);
                                String huoghao = mGoodsParameters.getShop_attribute().get(mPosition1).getShop_attribute_data().get(mPosition2).getShop_attribute_hh();
//                  String num=mTvNum.getText().toString().trim();
                                goodsDetailPredenter.addShopCar(AppConstants.COMPANY_ID, mGoodId, huoghao, 1, AppConstants.ADD_BUY, AppConstants.TYPE_COMMON, AppConstants.FROM_MOBILE);
                            }catch (Exception e){

                            }

                        }
                        break;
                    case "加入购物车":
                        showPopUpWindow("2");
                        break;
                }
            }else{
                ifLogin=0;
                switch (clickType){
                    case "立即购买":
                        startActivityForResult(new Intent(GoodsDetailActivity.this, LoginActivity.class).putExtra("type", "allorder"),1);
                        break;
                    case "加入购物车":
                        startActivityForResult(new Intent(GoodsDetailActivity.this, LoginActivity.class).putExtra("type", "allorder"),2);

                        break;
                }

            }



        }

        //加入购物车
        @Override
        public void onSuccessAddShopCar(Result json) {
            Log.e(TAG, "onSuccessAddShopCar: " + json.toString());
            goodsDetailPredenter.getShopCarGoods(AppConstants.COMPANY_ID,AppConstants.FROM_MOBILE);
            int status = json.getStatus();
            switch (status) {
                case -1:
                    Toast.makeText(GoodsDetailActivity.this, "加入购物车失败，发生意外错误", Toast.LENGTH_SHORT).show();
                    break;
                case 0:
                    Toast.makeText(GoodsDetailActivity.this, "加入购物车失败，会员未登录 ", Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    Toast.makeText(GoodsDetailActivity.this, " 加入购物车失败，商品已下架  ", Toast.LENGTH_SHORT).show();
                    break;
                case 2:
                    Toast.makeText(GoodsDetailActivity.this, " 加入购物车失败，超出购买数量 ", Toast.LENGTH_SHORT).show();
                    break;
                case 3:
                    Toast.makeText(GoodsDetailActivity.this, "加入购物车失败，超出库存数量", Toast.LENGTH_SHORT).show();
                    break;
                case 4:
                    Toast.makeText(GoodsDetailActivity.this, "加入购物车失败，积分不足", Toast.LENGTH_SHORT).show();
                    break;
                case 10:
                    if (type == 0) {
                        Toast.makeText(GoodsDetailActivity.this, "加入购物车成功", Toast.LENGTH_SHORT).show();
                        if (popWnd != null) {
                            popWnd.dismiss();
                        }
                        EventBus.getDefault().post(new AddShopCarEvent("成功"));
                    } else if (type == 1) {
                        if (popWnd != null) {
                            popWnd.dismiss();
                        }
                        startActivity(new Intent(GoodsDetailActivity.this, AffirmOrderActivity.class).putExtra("cartId", json.getCartId()));
                    } else if (type == 2) {
                        startActivity(new Intent(GoodsDetailActivity.this, AffirmIntegralOrderActivity.class).putExtra("cartId", json.getCartId()));

                    } else if (type == 3) {
                        startActivity(new Intent(GoodsDetailActivity.this, AffirmOrderActivity.class).putExtra("cartId", json.getCartId()));

                    }
                    break;
                default:
//                    Toast.makeText(GoodsDetailActivity.this, "" + json.getErrMsg(), Toast.LENGTH_SHORT).show();
                    break;
            }
        }

        //是否收藏判断
        @Override
        public void onSuccessIfCollect(CommonResult commonResult) {
            Log.e(TAG, "onSuccessIfCollect: " + commonResult.toString());
            //返回状态：-1 会员未登录 -2 参数ID不正确 0 已收藏 2 未收藏
            int status = commonResult.getStatus();
            switch (status) {
                case 0:
                    ivCollect.setImageResource(R.drawable.img_gooddetail_lover);
                    break;
                case 2:
                    ivCollect.setImageResource(R.drawable.img_gooddetail_love);
                    break;
                case -1:
                    Toast.makeText(GoodsDetailActivity.this, "会员未登录", Toast.LENGTH_SHORT).show();
                    break;
                case -2:
                    Toast.makeText(GoodsDetailActivity.this, "参数ID不正确", Toast.LENGTH_SHORT).show();
                    break;
                default:
//                    Toast.makeText(GoodsDetailActivity.this, "" + commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();
                    break;
            }
        }

        //收藏
        @Override
        public void onSuccessSetCollect(CommonResult commonResult) {
            Log.e(TAG, "onSuccessSetCollect: " + commonResult.toString());
            //返回状态：-1 会员未登录 -2 参数ID不正确 1 收藏成功 2 取消收藏成功F
            int status = commonResult.getStatus();
            switch (status) {
                case 1:
                    goodsDetailPredenter.ifCollect(AppConstants.COMPANY_ID, mGoodId);
                    ivCollect.setImageResource(R.drawable.img_gooddetail_lover);
                    break;
                case 2:
                    goodsDetailPredenter.ifCollect(AppConstants.COMPANY_ID, mGoodId);
                    ivCollect.setImageResource(R.drawable.img_gooddetail_love);
                    break;
                case -1:
                    Toast.makeText(GoodsDetailActivity.this, "会员未登录", Toast.LENGTH_SHORT).show();
                    break;
                case -2:
                    Toast.makeText(GoodsDetailActivity.this, "参数ID不正确", Toast.LENGTH_SHORT).show();
                    break;
                default:
//                    Toast.makeText(GoodsDetailActivity.this, "" + commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();
                    break;
            }
        }

        /**
         * 推荐商品列表
         * @param goodsList
         */
        @Override
        public void onSuccessGetGoodList(GoodsList goodsList) {
            int status = goodsList.getStatus();
            if (status > 0) {
                mList.addAll(goodsList.getList());
                mAdapter.notifyDataSetChanged();
            } else {
//                Toast.makeText(GoodsDetailActivity.this, "" + goodsList.getReturnMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onError(String result) {
            Log.e(TAG, "onSuccessAddShopCar: " + result);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(GoodsDetailActivity.this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getSter(String s) {
        Toast.makeText(this, "" + s, Toast.LENGTH_SHORT).show();
    }

    @OnClick({R.id.ll_service,R.id.ll_gooddetail_wenda,R.id.tv_goodsdetail_more,R.id.iv_goodsdetail_jia, R.id.iv_goodsdetail_jian, R.id.ll_collect, R.id.tv_add_shopcar, R.id.tv_pay, R.id.iv_gooddetail_back, R.id.iv_gooddetail_share, R.id.ll_gooddetail_evaluate, R.id.ll_choose_params, R.id.ll_scheme})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_service:
                startActivity(new Intent(GoodsDetailActivity.this, MainActivity.class).putExtra("type", "shopcar"));
                break;
            //加入购物车
            case R.id.tv_add_shopcar:
                clickType="加入购物车";
                goodsDetailPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timestamp,AppConstants.FROM_MOBILE);

                break;
            //立即购买
            case R.id.tv_pay:
                clickType="立即购买";
                goodsDetailPredenter.getLoginStatus(AppConstants.COMPANY_ID, code, timestamp,AppConstants.FROM_MOBILE);
                break;
            //返回
            case R.id.iv_gooddetail_back:
                onBackPressed();
                break;
            //分享
            case R.id.iv_gooddetail_share:
                showShare();
                break;
            //商品评价
            case R.id.ll_gooddetail_evaluate:
                startActivity(new Intent(GoodsDetailActivity.this, EvaluateActivity.class).putExtra("goodid",mGoodId));
                break;
            //图文详情
            case R.id.ll_scheme:
                Log.e(TAG, "onViewClicked: " + mContent);
                startActivity(new Intent(GoodsDetailActivity.this, DetailActivity.class).putExtra("id", mContent));
                break;
                //问答
            case R.id.ll_gooddetail_wenda:
                Intent intent=new Intent(GoodsDetailActivity.this,QuestionAndAnswerActivity.class);
                Log.e(TAG, "onViewClicked: "+mGoodId );
                intent.putExtra("goodid",mGoodId);
                startActivity(intent);
                break;
            //选择规格
            case R.id.ll_choose_params:
                showPopUpWindow("1");
                break;
            //收藏
            case R.id.ll_collect:
                goodsDetailPredenter.setCollect(AppConstants.COMPANY_ID, mGoodId);
                break;
            //数量加
            case R.id.iv_goodsdetail_jia:
                int currentCount = Integer.parseInt(tvGoodsdetailNum.getText().toString().trim());
                currentCount++;
                tvGoodsdetailNum.setText(currentCount + "");
                break;
            //数量减
            case R.id.iv_goodsdetail_jian:
                int currentCount1 = Integer.parseInt(tvGoodsdetailNum.getText().toString().trim());
                if (currentCount1 == 1) {
                    return;
                }
                currentCount1--;
                tvGoodsdetailNum.setText(currentCount1 + "");
                break;
            case R.id.tv_goodsdetail_more:
                startActivity(new Intent(GoodsDetailActivity.this,GoodsListActivity.class).putExtra("id",mClass_id).putExtra("searchword",""));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==11){
            switch (requestCode){
                case 1:
                    if ("积分兑换".equals(mType)) {
                        type = 2;
                        String num = tvGoodsdetailNum.getText().toString().trim();
                        goodsDetailPredenter.addShopCar(AppConstants.COMPANY_ID, mGoodId, "", Integer.valueOf(num), AppConstants.ADD_BUY, AppConstants.ADD_INTERGRAL, AppConstants.FROM_MOBILE);
                    } else if ("促销秒杀".equals(mType)) {
                        type = 3;
                        String num = tvGoodsdetailNum.getText().toString().trim();
                        goodsDetailPredenter.addShopCar(AppConstants.COMPANY_ID, mGoodId, "", Integer.valueOf(num), AppConstants.ADD_BUY, AppConstants.ADD_SECKILL, AppConstants.FROM_MOBILE);

                    } else {
                        type = 1;
                        Log.e(TAG, "onViewClicked: "+ mPosition1+"==>"+mPosition2);
                        String huoghao = mGoodsParameters.getShop_attribute().get(mPosition1).getShop_attribute_data().get(mPosition2).getShop_attribute_hh();
//                  String num=mTvNum.getText().toString().trim();
                        goodsDetailPredenter.addShopCar(AppConstants.COMPANY_ID, mGoodId, huoghao, 1, AppConstants.ADD_BUY, AppConstants.TYPE_COMMON, AppConstants.FROM_MOBILE);
                    }
                    break;
                case 2:
                    showPopUpWindow("2");
                    break;
                case 3:
                    type = 0;
                    Set<Integer> selectedList = mTfTitle1.getSelectedList();
                    Iterator<Integer> it = selectedList.iterator();
                    while (it.hasNext()) {
                        Integer str = it.next();
                        mPosition1 = str;
                    }
                    Set<Integer> selectedList1 = mTfTitle2.getSelectedList();
                    Iterator<Integer> it1 = selectedList1.iterator();
                    while (it1.hasNext()) {
                        Integer str1 = it1.next();
                        mPosition2 = str1;
                    }
                    Log.e(TAG, "onClick: " + mPosition2 + "------" + mPosition1);
                    String huoghao = mGoodsParameters.getShop_attribute().get(mPosition1).getShop_attribute_data().get(mPosition2).getShop_attribute_hh();
                    String num = mTvNum.getText().toString().trim();
                    Log.e(TAG, "onClick: " + mPosition2 + "------" + mPosition1 + huoghao);
                    Log.e(TAG, "onClick: " + AppConstants.COMPANY_ID + String.valueOf(mGoodId) + huoghao + num + AppConstants.ADD_SUCCESS + AppConstants.TYPE_COMMON + AppConstants.FROM_MOBILE);
                    goodsDetailPredenter.addShopCar(AppConstants.COMPANY_ID, mGoodId, huoghao, Integer.parseInt(num), AppConstants.ADD_SUCCESS, AppConstants.TYPE_COMMON, AppConstants.FROM_MOBILE);
                    break;
                case 4:
                    type = 0;
                    Set<Integer> selectedList11 = mTfTitle1.getSelectedList();
                    Iterator<Integer> it11 = selectedList11.iterator();
                    while (it11.hasNext()) {
                        Integer str = it11.next();
                        mPosition1 = str;
                    }
                    Set<Integer> selectedList111 = mTfTitle2.getSelectedList();
                    Iterator<Integer> it111 = selectedList111.iterator();
                    while (it111.hasNext()) {
                        Integer str1 = it111.next();
                        mPosition2 = str1;
                    }
                    Log.e(TAG, "onClick: " + mPosition2 + "------" + mPosition1);
                    String huoghao11 = mGoodsParameters.getShop_attribute().get(mPosition1).getShop_attribute_data().get(mPosition2).getShop_attribute_hh();
                    String num11 = mTvNum.getText().toString().trim();
                    Log.e(TAG, "onClick: " + mPosition2 + "------" + mPosition1 + huoghao11);
                    Log.e(TAG, "onClick: " + AppConstants.COMPANY_ID + String.valueOf(mGoodId) + huoghao11 + num11 + AppConstants.ADD_SUCCESS + AppConstants.TYPE_COMMON + AppConstants.FROM_MOBILE);
                    goodsDetailPredenter.addShopCar(AppConstants.COMPANY_ID, mGoodId, huoghao11, Integer.parseInt(num11), AppConstants.ADD_SUCCESS, AppConstants.TYPE_COMMON, AppConstants.FROM_MOBILE);
                    break;
                case 5:
                    type = 1;
                    String huoghao22 = mGoodsParameters.getShop_attribute().get(mPosition1).getShop_attribute_data().get(mPosition2).getShop_attribute_hh();
                    String num22 = mTvNum.getText().toString().trim();
                    goodsDetailPredenter.addShopCar(AppConstants.COMPANY_ID, mGoodId, huoghao22, Integer.parseInt(num22), AppConstants.ADD_BUY, AppConstants.TYPE_COMMON, AppConstants.FROM_MOBILE);
                    break;
            }
        }
    }

    /**
     * 规格选择弹框
     */
    private void showPopUpWindow(String ss) {
        View contentView = LayoutInflater.from(GoodsDetailActivity.this).inflate(R.layout.item_pop_parameters, null);
        popWnd = new PopupWindow(GoodsDetailActivity.this);
        popWnd.setContentView(contentView);
        popWnd.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        popWnd.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //关闭
        mIvClose = contentView.findViewById(R.id.iv_popgoodsdetail_close);//关闭

        LinearLayout mLlTitle1 = (LinearLayout) contentView.findViewById(R.id.ll_title1);
        final LinearLayout mLlTitle2 = (LinearLayout) contentView.findViewById(R.id.ll_title2);

        TextView mTvTitle1 = (TextView) contentView.findViewById(R.id.tv_title1);
        final TextView mTvTitle2 = (TextView) contentView.findViewById(R.id.tv_title2);

        mTfTitle1 = (TagFlowLayout) contentView.findViewById(R.id.tf_lvpop);
        mTfTitle2 = (TagFlowLayout) contentView.findViewById(R.id.tf_lvpop_two);

        ImageView mIvLogo = (ImageView) contentView.findViewById(R.id.iv_popgoodsdetail_image);
        final TextView mTvPrice = (TextView) contentView.findViewById(R.id.tv_popgoodsdetail_price);
        final TextView mRealPrice = (TextView) contentView.findViewById(R.id.tv_poppara_realprice);
        TextView mTvGoodsName = (TextView) contentView.findViewById(R.id.tv_poppara_goodsname);
        final TextView mTvKuCun = (TextView) contentView.findViewById(R.id.tv_popgoodsdetail_kucun);//库存

        ImageView mIvSub = ((ImageView) contentView.findViewById(R.id.iv_popgoodsdetail_sub));//减
        ImageView mIvAdd = ((ImageView) contentView.findViewById(R.id.iv_popgoodsdetail_add));//加
        //数量
        mTvNum = ((TextView) contentView.findViewById(R.id.tv_popgoodsdetail_num));

        mBtnCommit = contentView.findViewById(R.id.btn_popgoodsdetail_commit);//确定
        LinearLayout mLlCommit = ((LinearLayout) contentView.findViewById(R.id.ll_popgoodsdetail_commit));
        TextView mTvAddShopCar = ((TextView) contentView.findViewById(R.id.tv_popgoodsdetail_addshopcar));//加入购物车
        TextView mTvBuy = ((TextView) contentView.findViewById(R.id.tv_popgoodsdetail_buy));//立即购买

        mTvGoodsName.setText(mGoodsName);
        mRealPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        mIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popWnd.dismiss();
            }
        });

        final SelectParameters xuanze = new SelectParameters();
        xuanze.setGoodId(mGoodId);

        //商品图片
        if (!TextUtils.isEmpty(mLogoImage)) {
            GlideUtils.setNetImage(GoodsDetailActivity.this, AppConstants.INTERNET_HEAD + mLogoImage, mIvLogo);
        }

        mTvPrice.setText(mPrice + "");
//        Log.e(TAG, "showPopUpWindow: "+mGoodsParameters.toString() );
//        List<Specification> listGuiGe = new ArrayList<>();
//        ListView mLvGuiGe = ((ListView) contentView.findViewById(R.id.lv_pop_guige));
//        final PopLvAdapter popLvAdapter = new PopLvAdapter(GoodsDetailActivity.this,listGuiGe);
//        mLvGuiGe.setAdapter(popLvAdapter);

        if (mGoodsParameters != null) {
            int status = mGoodsParameters.getStatus();
            if (status > 0) {
                final List<GoodsParameters.ShopAttributeBean.ShopAttributeDataBean> mListTitle3 = new ArrayList<>();
                String title1 = mGoodsParameters.getShop_title1();
                final String title2 = mGoodsParameters.getShop_title2();
                if (onj2 != null) {
                    mTvKuCun.setText(mGoodsParameters.getShop_attribute().get(onj2.getPosition1()).getShop_attribute_data().get(onj2.getPosition2()).getShop_attribute_stock() + "");
                    mRealPrice.setText("¥" + mGoodsParameters.getShop_attribute().get(onj2.getPosition1()).getShop_attribute_data().get(onj2.getPosition2()).getShop_attribute_price2());
                    mTvPrice.setText(mGoodsParameters.getShop_attribute().get(onj2.getPosition1()).getShop_attribute_data().get(onj2.getPosition2()).getShop_attribute_price() + "");
                    tvGoodsPrice.setText("¥" + mGoodsParameters.getShop_attribute().get(onj2.getPosition1()).getShop_attribute_data().get(onj2.getPosition2()).getShop_attribute_price());
                } else {
                    mTvKuCun.setText( mGoodsParameters.getShop_attribute().get(0).getShop_attribute_data().get(0).getShop_attribute_stock() + "");
                    mRealPrice.setText("¥" + mGoodsParameters.getShop_attribute().get(0).getShop_attribute_data().get(0).getShop_attribute_price2());
                    mTvPrice.setText(mGoodsParameters.getShop_attribute().get(0).getShop_attribute_data().get(0).getShop_attribute_price());
                    tvGoodsPrice.setText("¥" + mGoodsParameters.getShop_attribute().get(0).getShop_attribute_data().get(0).getShop_attribute_price());
                }
                if (TextUtils.isEmpty(title1)) {
                    mLlTitle1.setVisibility(View.GONE);
                } else {
                    mListTitle = new ArrayList<>();
                    mLlTitle1.setVisibility(View.VISIBLE);
                    mTvTitle1.setText(title1);
                    for (int i = 0; i < mGoodsParameters.getShop_attribute().size(); i++) {
                        mListTitle.add(mGoodsParameters.getShop_attribute().get(i).getShop_attribute_xh());
                    }
                    Log.e(TAG, "showPopUpWindow: " + mListTitle.toString());
                    mTfTitle1.setAdapter(new TagAdapter<String>(mListTitle) {
                        @Override
                        public View getView(FlowLayout parent, int position, String s) {
                            TextView tv = (TextView) LayoutInflater.from(GoodsDetailActivity.this).inflate(R.layout.tv,
                                    mTfTitle1, false);
                            tv.setText(s);
                            return tv;
                        }
                    });

                    mTfTitle1.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
                        @Override
                        public boolean onTagClick(View view, int position, FlowLayout parent) {


                            mPosition = position;
                            if (!TextUtils.isEmpty(title2)) {
                                xuanze.setPosition1(position);
                                xuanze.setPosition2(0);
                                xuanze.setPrice(mGoodsParameters.getShop_attribute().get(position).getShop_attribute_data().get(0).getShop_attribute_price());
                                mLlTitle2.setVisibility(View.VISIBLE);
                                mTvTitle2.setText(title2);
//                                List<GoodsParameters.ShopAttributeBean.ShopAttributeDataBean> mListTitle2=new ArrayList<>();
                                Log.e(TAG, "showPopUpWindow: " + position);
//                                for (int i = 0; i < mGoodsParameters.getShop_attribute().get(position).getShop_attribute_data().size(); i++) {
                                if (mListTitle3 != null) {
                                    mListTitle3.clear();
                                }
                                mListTitle3.addAll(mGoodsParameters.getShop_attribute().get(position).getShop_attribute_data());
//                                }

                                mTfTitle2.setAdapter(new TagAdapter<GoodsParameters.ShopAttributeBean.ShopAttributeDataBean>(mListTitle3) {
                                    @Override
                                    public View getView(FlowLayout parent, int position, GoodsParameters.ShopAttributeBean.ShopAttributeDataBean s) {
                                        TextView tv = (TextView) LayoutInflater.from(GoodsDetailActivity.this).inflate(R.layout.tv,
                                                mTfTitle2, false);
                                        tv.setText(s.getShop_attribute_gg());
                                        return tv;
                                    }
                                });
//                                if (mTfTitle2.getChildAt(0) != null) {
//                                    mTfTitle2.getChildAt(0).performClick();
//                                }
                                tvChoosed.setText("已选：" + mListTitle.get(mPosition));
                            } else {
                                xuanze.setParameter1(mListTitle.get(mPosition));

                                for (int i = 0; i < mGoodsParameters.getShop_attribute().get(position).getShop_attribute_data().size(); i++) {
                                    mTvKuCun.setText(mGoodsParameters.getShop_attribute().get(position).getShop_attribute_data().get(i).getShop_attribute_stock() + "");
                                    mRealPrice.setText("¥" + mGoodsParameters.getShop_attribute().get(position).getShop_attribute_data().get(i).getShop_attribute_price2());
                                    mTvPrice.setText(mGoodsParameters.getShop_attribute().get(position).getShop_attribute_data().get(i).getShop_attribute_price());
                                    tvGoodsPrice.setText("¥" + mGoodsParameters.getShop_attribute().get(position).getShop_attribute_data().get(i).getShop_attribute_price());
                                    xuanze.setPrice(mGoodsParameters.getShop_attribute().get(position).getShop_attribute_data().get(i).getShop_attribute_price());
                                }
                                tvChoosed.setText("已选：" + mListTitle.get(mPosition));
                                String jsonPa = new Gson().toJson(xuanze);
                                SPUtils.put(String.valueOf(mGoodId), jsonPa);
                                mLlTitle2.setVisibility(View.GONE);
                            }
                            return false;
                        }
                    });

                }


                    mTfTitle2.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
                        @Override
                        public boolean onTagClick(View view, int position, FlowLayout parent) {
                            Log.e(TAG, "onTagClick: " + mPosition);
                            xuanze.setPosition2(position);
                            xuanze.setParameter1(mListTitle.get(mPosition));
                            xuanze.setParameter2(mListTitle3.get(position).getShop_attribute_gg());
                            xuanze.setPrice(mListTitle3.get(position).getShop_attribute_price());
                            Log.e(TAG, "onTagClick: ");
                            mTvKuCun.setText(mListTitle3.get(position).getShop_attribute_stock() + "");
                            mRealPrice.setText("¥" + mListTitle3.get(position).getShop_attribute_price2());
                            mTvPrice.setText(mListTitle3.get(position).getShop_attribute_price());
                            tvGoodsPrice.setText("¥" + mListTitle3.get(position).getShop_attribute_price());
                            tvChoosed.setText("已选：" + mListTitle.get(mPosition) + "   " + mListTitle3.get(position).getShop_attribute_gg());
                            String jsonPa = new Gson().toJson(xuanze);
                            SPUtils.put(String.valueOf(mGoodId), jsonPa);
                            return false;
                        }
                    });
//                } else {
//                    mLlTitle2.setVisibility(View.GONE);
//                }
            } else {
                Toast.makeText(this, "数据获取失败", Toast.LENGTH_SHORT).show();
            }
        }

        String sssss = (String) SPUtils.get(GoodsDetailActivity.this, String.valueOf(mGoodId), "");
        Log.e(TAG, "showPopUpWindow--sssss: " + sssss);
        if (!TextUtils.isEmpty(sssss)) {
            Gson gson = new Gson();
            SelectParameters onj1 = gson.fromJson(sssss, SelectParameters.class);
//            int position1 = onj1.getPosition1();
//            int position2 = onj1.getPosition2();
            if (mTfTitle1.getChildAt(onj1.getPosition1()) != null) {
                mTfTitle1.getChildAt(onj1.getPosition1()).performClick();
            }
//            if (onj1.getPosition2() > 0) {

                if (mTfTitle2.getChildAt(onj1.getPosition2()) != null) {
                    mTfTitle2.getChildAt(onj1.getPosition2()).performClick();
//                }

            }
        } else {
            if (mTfTitle1.getChildAt(0) != null) {
                mTfTitle1.getChildAt(0).performClick();
            }
            if (mTfTitle2.getChildAt(0) != null) {
                mTfTitle2.getChildAt(0).performClick();
            }
        }


        //加入购物车
        mBtnCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                clickType="规格加入购物车";
                goodsDetailPredenter.getLoginStatus(AppConstants.COMPANY_ID,code,timestamp,AppConstants.FROM_MOBILE);
                if (ifLogin==1){

                type = 0;
                Set<Integer> selectedList = mTfTitle1.getSelectedList();
                Iterator<Integer> it = selectedList.iterator();
                while (it.hasNext()) {
                    Integer str = it.next();
                    mPosition1 = str;
                }
                Set<Integer> selectedList1 = mTfTitle2.getSelectedList();
                Iterator<Integer> it1 = selectedList1.iterator();
                while (it1.hasNext()) {
                    Integer str1 = it1.next();
                    mPosition2 = str1;
                }
                Log.e(TAG, "onClick: " + mPosition2 + "------" + mPosition1);
                String huoghao = mGoodsParameters.getShop_attribute().get(mPosition1).getShop_attribute_data().get(mPosition2).getShop_attribute_hh();
                String num = mTvNum.getText().toString().trim();
                Log.e(TAG, "onClick: " + mPosition2 + "------" + mPosition1 + huoghao);
                Log.e(TAG, "onClick: " + AppConstants.COMPANY_ID + String.valueOf(mGoodId) + huoghao + num + AppConstants.ADD_SUCCESS + AppConstants.TYPE_COMMON + AppConstants.FROM_MOBILE);
                goodsDetailPredenter.addShopCar(AppConstants.COMPANY_ID, mGoodId, huoghao, Integer.parseInt(num), AppConstants.ADD_SUCCESS, AppConstants.TYPE_COMMON, AppConstants.FROM_MOBILE);

                }else {
                    startActivityForResult(new Intent(GoodsDetailActivity.this, LoginActivity.class).putExtra("type", "allorder"),3);

                }
                }
        });

        //加入购物车
        mTvAddShopCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goodsDetailPredenter.getLoginStatus(AppConstants.COMPANY_ID,code,timestamp,AppConstants.FROM_MOBILE);
                if (ifLogin==1) {
                    type = 0;
                    Set<Integer> selectedList = mTfTitle1.getSelectedList();
                    Iterator<Integer> it = selectedList.iterator();
                    while (it.hasNext()) {
                        Integer str = it.next();
                        mPosition1 = str;
                    }
                    Set<Integer> selectedList1 = mTfTitle2.getSelectedList();
                    Iterator<Integer> it1 = selectedList1.iterator();
                    while (it1.hasNext()) {
                        Integer str1 = it1.next();
                        mPosition2 = str1;
                    }
                    Log.e(TAG, "onClick: " + mPosition2 + "------" + mPosition1);
                    String huoghao = mGoodsParameters.getShop_attribute().get(mPosition1).getShop_attribute_data().get(mPosition2).getShop_attribute_hh();
                    String num = mTvNum.getText().toString().trim();
                    Log.e(TAG, "onClick: " + mPosition2 + "------" + mPosition1 + huoghao);
                    Log.e(TAG, "onClick: " + AppConstants.COMPANY_ID + String.valueOf(mGoodId) + huoghao + num + AppConstants.ADD_SUCCESS + AppConstants.TYPE_COMMON + AppConstants.FROM_MOBILE);
                    goodsDetailPredenter.addShopCar(AppConstants.COMPANY_ID, mGoodId, huoghao, Integer.parseInt(num), AppConstants.ADD_SUCCESS, AppConstants.TYPE_COMMON, AppConstants.FROM_MOBILE);
//                popWnd.dismiss();
                }else{
                    startActivityForResult(new Intent(GoodsDetailActivity.this, LoginActivity.class).putExtra("type", "allorder"),4);

                }
            }
        });
        //立即购买
        mTvBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goodsDetailPredenter.getLoginStatus(AppConstants.COMPANY_ID,code,timestamp,AppConstants.FROM_MOBILE);
                if (ifLogin==1) {
                    type = 1;
                    String huoghao = mGoodsParameters.getShop_attribute().get(mPosition1).getShop_attribute_data().get(mPosition2).getShop_attribute_hh();
                    String num = mTvNum.getText().toString().trim();
                    goodsDetailPredenter.addShopCar(AppConstants.COMPANY_ID, mGoodId, huoghao, Integer.parseInt(num), AppConstants.ADD_BUY, AppConstants.TYPE_COMMON, AppConstants.FROM_MOBILE);
//                startActivity(new Intent(GoodsDetailActivity.this, AffirmOrderActivity.class));
                }else {
                    startActivityForResult(new Intent(GoodsDetailActivity.this, LoginActivity.class).putExtra("type", "allorder"),5);

                }
            }
        });
        //数量减
        mIvSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentCount = Integer.parseInt(mTvNum.getText().toString().trim());
                if (currentCount == 1) {
                    return;
                }
                currentCount--;
                mTvNum.setText(currentCount + "");

            }
        });
        //数量加
        mIvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentCount = Integer.parseInt(mTvNum.getText().toString().trim());
                String kucun=mTvKuCun.getText().toString().trim();
                int kucunq=Integer.valueOf(kucun);
                if (currentCount>=kucunq) {
                    return;
                }
                currentCount++;
                mTvNum.setText(currentCount + "");

            }
        });

        switch (ss) {
            case "1":
                mLlCommit.setVisibility(View.VISIBLE);
                mBtnCommit.setVisibility(View.GONE);
                break;
            case "2":
                mLlCommit.setVisibility(View.GONE);
                mBtnCommit.setVisibility(View.VISIBLE);
                break;
        }
        popWnd.setTouchable(true);
        popWnd.setFocusable(true);
        popWnd.setOutsideTouchable(true);
        popWnd.setBackgroundDrawable(new BitmapDrawable(getResources(), (Bitmap) null));
        backgroundAlpha(0.8f);

        //添加pop窗口关闭事件
        popWnd.setOnDismissListener(new poponDismissListener());

        popWnd.setTouchInterceptor(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    popWnd.dismiss();
                    return true;
                }
                return false;
            }
        });

        //popWnd.showAsDropDown(mTvLine, 200, 0);
        popWnd.showAtLocation(GoodsDetailActivity.this.findViewById(R.id.tv_add_shopcar),
                Gravity.BOTTOM, 0, 0);

    }


    /**
     * 添加弹出的popWin关闭的事件，主要是为了将背景透明度改回来
     *
     * @author cg
     */
    class poponDismissListener implements PopupWindow.OnDismissListener {

        @Override
        public void onDismiss() {
            // TODO Auto-generated method stub
            //Log.v("List_noteTypeActivity:", "我是关闭事件");
            backgroundAlpha(1f);
        }

    }

    /**
     * 设置添加屏幕的背景透明度
     *
     * @param bgAlpha
     */
    public void backgroundAlpha(float bgAlpha) {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        getWindow().setAttributes(lp);
    }

    /**
     * 分享
     */
    private void showShare() {
        OnekeyShare oks = new OnekeyShare();
        //关闭sso授权
        oks.disableSSOWhenAuthorize();

        // title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间等使用
        oks.setTitle("标题");
        // titleUrl是标题的网络链接，QQ和QQ空间等使用
        oks.setTitleUrl("http://sharesdk.cn");
        // text是分享文本，所有平台都需要这个字段
        oks.setText("我是分享文本");
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        //oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片
        // url仅在微信（包括好友和朋友圈）中使用
        oks.setUrl("http://sharesdk.cn");
        // comment是我对这条分享的评论，仅在人人网和QQ空间使用
        oks.setComment("我是测试评论文本");
        // site是分享此内容的网站名称，仅在QQ空间使用
        oks.setSite(getString(R.string.app_name));
        // siteUrl是分享此内容的网站地址，仅在QQ空间使用
        oks.setSiteUrl("http://sharesdk.cn");

        // 启动分享GUI
        oks.show(this);
    }
}
