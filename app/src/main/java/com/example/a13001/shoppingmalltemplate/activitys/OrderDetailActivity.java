package com.example.a13001.shoppingmalltemplate.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.View.MyListView;
import com.example.a13001.shoppingmalltemplate.adapters.OrderDetailLvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.GoodsEvaluateDetail;
import com.example.a13001.shoppingmalltemplate.modle.OrderDetail;
import com.example.a13001.shoppingmalltemplate.modle.ShouHouDetail;
import com.example.a13001.shoppingmalltemplate.mvpview.OrderDetailView;
import com.example.a13001.shoppingmalltemplate.presenter.OrderDetailPredenter;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;

import java.util.ArrayList;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderDetailActivity extends BaseActivity {

    @BindView(R.id.mlv_orderdetail)
    MyListView mlvOrderdetail;
    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.btn1)
    Button btn1;
    @BindView(R.id.btn2)
    Button btn2;
    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_orderdetail_ordernumber)
    TextView tvOrderdetailOrdernumber;
    @BindView(R.id.tv_orderdetail_ordertime)
    TextView tvOrderdetailOrdertime;
    @BindView(R.id.tv_orderdetail_payway)
    TextView tvOrderdetailPayway;
    @BindView(R.id.tv_orderdetail_deliverway)
    TextView tvOrderdetailDeliverway;
    @BindView(R.id.tv_orderdetail_totalprice)
    TextView tvOrderdetailTotalprice;
    @BindView(R.id.tv_orderdetail_yunfei)
    TextView tvOrderdetailYunfei;
    @BindView(R.id.tv_orderdetail_realpay)
    TextView tvOrderdetailRealpay;
    @BindView(R.id.ll_orderdetail_btn)
    LinearLayout llOrderdetailBtn;
    @BindView(R.id.tv_orderstate)
    TextView tvOrderstate;
    @BindView(R.id.tv_title_right)
    TextView tvTitleRight;
    @BindView(R.id.iv_order)
    ImageView ivOrder;
    @BindView(R.id.iv_companyhead)
    ImageView ivCompanyhead;
    @BindView(R.id.tv_companyname)
    TextView tvCompanyname;
    @BindView(R.id.tv_count2)
    TextView tvCount2;
    @BindView(R.id.tv_totalprice)
    TextView tvTotalprice;
    @BindView(R.id.tv_orderdetail_wuliugongsi)
    TextView tvOrderdetailWuliugongsi;
    @BindView(R.id.tv_orderdetail_wkuaididanhao)
    TextView tvOrderdetailWkuaididanhao;
    private ArrayList<OrderDetail.OrderGoodsListBean> mList;
    private OrderDetailLvAdapter mAdapter;
    private String orderNumber;
    private OrderDetailPredenter orderDetailPredenter = new OrderDetailPredenter(OrderDetailActivity.this);
    private static final String TAG = "OrderDetailActivity";
    private String code;
    private String timestamp;
    private int orderstatus;
    private int payid;
    private int ordersId;
  private boolean  ifrepairsCheck=false;
    private String ordersNumber;
    private int mPosition;
    private int mPositionPingJia;
    private boolean ifPingJia=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);
        orderNumber = getIntent().getStringExtra("orderNumber");
        String safetyCode = MyUtils.getMetaValue(OrderDetailActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        timestamp = Utils.getTimeStamp();
        Log.e("aaa", "--orderNumber---->" + orderNumber);
        tvTitleCenter.setText("订单详情");
        orderDetailPredenter.onCreate();
        orderDetailPredenter.attachView(orderDetailView);
        orderDetailPredenter.getOrderDetail(AppConstants.COMPANY_ID, code, timestamp, orderNumber, AppConstants.FROM_MOBILE);


//        tvTitleCenter.setTextColor(getResources().getColor(R.color.t666));
//        rlRoot.setBackgroundColor(getResources().getColor(R.color.white));
//        ivTitleBack.setImageResource(R.drawable.back_grey);
        initData();

    }


    OrderDetailView orderDetailView = new OrderDetailView() {
        //订单详情
        @Override
        public void onSuccessGetOrderDetail(final OrderDetail orderDetail) {
            Log.e(TAG, "onSuccessGetOrderDetail: " + orderDetail.toString());
            int status = orderDetail.getStatus();
            if (status > 0) {
//                orderDetailPredenter.getShouHouDetail(AppConstants.COMPANY_ID,code,timestamp,orderDetail.get);
                ordersId = orderDetail.getOrdersId();

                mList.addAll(orderDetail.getOrderGoodsList());
                mAdapter.notifyDataSetChanged();
                orderstatus = orderDetail.getOrderStatus();
                mAdapter.setmOrderId(ordersId);
                ordersNumber = orderDetail.getOrdersNumber();
                mAdapter.setmOrderNumber(ordersNumber);
                //订单状态，1 待支付 2 未发货 3 已发货 4 已签收
                mAdapter.setmOrderStatus(orderstatus);
                switch (orderstatus) {
                    case 1:
                        tvOrderstate.setText("待付款");
                        btn1.setText("取消订单");
                        btn2.setText("去支付");
                        llOrderdetailBtn.setVisibility(View.VISIBLE);
                        btn1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                orderDetailPredenter.cancelOrder(AppConstants.COMPANY_ID, code, timestamp, orderNumber);
                            }
                        });
                        btn2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent3 = new Intent(OrderDetailActivity.this, OnLinePayActivity.class);
                                intent3.putExtra("orderNumber", orderDetail.getOrdersNumber());
                                intent3.putExtra("totalprice", orderDetail.getOrderAllTotalprice());
                                String paytype = orderDetail.getOrdersZffs();
                                switch (paytype) {
                                    case "微信支付":
                                        payid = 5;
                                        break;
                                    case "余额支付":
                                        payid = 0;
                                        break;
                                    case "银行转账":
                                        payid = 2;
                                        break;
                                }
                                intent3.putExtra("payid", payid);
                                startActivity(intent3);
                            }
                        });
                        tvOrderdetailWuliugongsi.setVisibility(View.GONE);
                        tvOrderdetailWkuaididanhao.setVisibility(View.GONE);
                        break;
                    case 2:
                        tvOrderstate.setText("待发货");
                        btn1.setVisibility(View.GONE);
                        btn2.setVisibility(View.GONE);
                        llOrderdetailBtn.setVisibility(View.GONE);
                        tvOrderdetailWuliugongsi.setVisibility(View.GONE);
                        tvOrderdetailWkuaididanhao.setVisibility(View.GONE);
                        break;
                    case 3:
                        tvOrderstate.setText("待收货");
                        llOrderdetailBtn.setVisibility(View.VISIBLE);
                        btn1.setText("查看物流");
                        btn2.setText("确认收货");
                        btn1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent=new Intent(OrderDetailActivity.this,LogisticsInfoActivity.class);
                                intent.putExtra("ordernumber",orderNumber);

                                //"快递公司|韵达快递,快递单号|89089080",
                                try {
                                    String orderDeliveryInfo = orderDetail.getOrderDeliveryInfo();
                                    String[] split = orderDeliveryInfo.split(",");
                                    String[] split0 = split[0].split(Pattern.quote("|"));
                                    String[] split1 = split[1].split(Pattern.quote("|"));
                                    intent.putExtra("kuaidigongsi",split0[1]);
                                }catch (Exception e){

                                }
                                startActivity(intent);
                            }
                        });
                        btn2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                orderDetailPredenter.affirmOrder(AppConstants.COMPANY_ID, code, timestamp, orderNumber);
                            }
                        });
                        tvOrderdetailWuliugongsi.setVisibility(View.VISIBLE);
                        tvOrderdetailWkuaididanhao.setVisibility(View.VISIBLE);
                        try {
                            String orderDeliveryInfo = orderDetail.getOrderDeliveryInfo();
                            String[] split = orderDeliveryInfo.split(",");
                            String[] split0 = split[0].split(Pattern.quote("|"));
                            String[] split1 = split[1].split(Pattern.quote("|"));
                            tvOrderdetailWuliugongsi.setText("快递公司："+split0[1]);
                            tvOrderdetailWkuaididanhao.setText("快递单号："+split1[1]);
                        }catch (Exception e){

                        }
                        break;
                    case 4:
                        tvOrderstate.setText("已签收");
                        btn1.setText("评价");
                        btn2.setText("售后");
                        llOrderdetailBtn.setVisibility(View.GONE);
                        try {
                            String orderDeliveryInfo = orderDetail.getOrderDeliveryInfo();
                            String[] split = orderDeliveryInfo.split(",");
                            String[] split0 = split[0].split(Pattern.quote("|"));
                            String[] split1 = split[1].split(Pattern.quote("|"));
                            tvOrderdetailWuliugongsi.setVisibility(View.VISIBLE);
                            tvOrderdetailWkuaididanhao.setVisibility(View.VISIBLE);
                            tvOrderdetailWuliugongsi.setText("快递公司："+split0[1]);
                            tvOrderdetailWkuaididanhao.setText("快递单号："+split1[1]);
                        }catch (Exception e){

                        }
                        break;
                }
                tvName.setText("收货人：" + orderDetail.getReceiptName() + "           " + orderDetail.getReceiptPhone());
                tvAddress.setText(orderDetail.getReceiptAddress() != null ? "地址" + orderDetail.getReceiptAddress() : "");
                tvOrderdetailOrdernumber.setText(orderDetail.getOrdersNumber() != null ? "订单编号：" + orderDetail.getOrdersNumber() : "");
                tvOrderdetailOrdertime.setText(orderDetail.getOrderDate() != null ? "下单时间：" + orderDetail.getOrderDate() : "");
                tvOrderdetailPayway.setText(orderDetail.getOrdersZffs() != null ? "支付方式：" + orderDetail.getOrdersZffs() : "");
                tvOrderdetailDeliverway.setText(orderDetail.getOrderDeliveryType() != null ? "送货方式：" + orderDetail.getOrderDeliveryType() : "");
//                tvOrderstate.setText(orderDetail.getOrdersType()!= null ? orderDetail.getOrdersType() : "");
                tvOrderdetailTotalprice.setText("¥" + orderDetail.getOrderTotalprice() + "");
                tvOrderdetailYunfei.setText("+¥" + orderDetail.getOrderFreight() + "");
                tvOrderdetailRealpay.setText(orderDetail.getOrderAllTotalprice() + "");



            } else {
                Toast.makeText(OrderDetailActivity.this, "" + orderDetail.getReturnMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        //取消订单
        @Override
        public void onSuccessCancelOrder(CommonResult commonResult) {
            int status = commonResult.getStatus();
            if (status > 0) {
                Toast.makeText(OrderDetailActivity.this, "" + commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(OrderDetailActivity.this, "" + commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();

            }

        }

        //确认收货
        @Override
        public void onSuccessAffirmOrder(CommonResult commonResult) {
            int status = commonResult.getStatus();
            if (status > 0) {
                Toast.makeText(OrderDetailActivity.this, "" + commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();

                finish();
            } else {
                Toast.makeText(OrderDetailActivity.this, "" + commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();

            }
        }

        @Override
        public void onSuccessGetShouHouDetail(ShouHouDetail shouHouDetail) {
            Log.e(TAG, "onSuccessGetShouHouDetail: "+shouHouDetail.toString() );
            int repairsCheck = shouHouDetail.getRepairsCheck();//售后服务单检测，0 未提交过 1 已提交过
            if (repairsCheck==0){
                ifrepairsCheck=false;
            }else{
                ifrepairsCheck=true;
            }
            mAdapter.setIfSHouHou(ifrepairsCheck);
            Log.e(TAG, "onSuccessGetShouHouDetail: "+mPosition );
            if (ifrepairsCheck){
                Intent intent = new Intent(OrderDetailActivity.this, ShouHouDetailActivity.class);
                intent.putExtra("cartid", mList.get(mPosition).getCartId());
                intent.putExtra("commodityid", mList.get(mPosition).getCommodityId());
                intent.putExtra("orderid", orderNumber);
                startActivity(intent);
            }else{
                Intent intent=new Intent(OrderDetailActivity.this, ApplyAfterSaleActivity.class);
                intent.putExtra("cartid",mList.get(mPosition).getCartId());
                intent.putExtra("commodityId",mList.get(mPosition).getCommodityId());
                intent.putExtra("cartimg",mList.get(mPosition).getCartImg());
                intent.putExtra("number",mList.get(mPosition).getCommodityNumber());
                intent.putExtra("price",mList.get(mPosition).getCommodityXPrice());
                intent.putExtra("property",mList.get(mPosition).getCommodityProperty());
                intent.putExtra("name",mList.get(mPosition).getCommodityName());
                intent.putExtra("orderid",ordersId);
                intent.putExtra("ordernumber",orderNumber);

                startActivity(intent);
            }
        }

        @Override
        public void onSuccessGetGoodsEvaluateDetail(GoodsEvaluateDetail goodsEvaluateDetail) {
            Log.e(TAG, "onSuccessGetGoodsEvaluateDetail: "+goodsEvaluateDetail );
            //商品评价ID，0 表示未评价 >0 是评价ID
            int cid = goodsEvaluateDetail.getCid();
            if (cid>0){
                ifPingJia=true;
            }else{
                ifPingJia=false;
            }
            if (ifPingJia){
                Intent intent=new Intent(OrderDetailActivity.this, GooodsEvaluateDetailActivity.class);
                intent.putExtra("cartid",mList.get(mPositionPingJia).getCartId());
               startActivity(intent);
            }else{
                Intent intent=new Intent(OrderDetailActivity.this, GoEvaluateActivity.class);
                intent.putExtra("cartid",mList.get(mPositionPingJia).getCartId());
                intent.putExtra("commodityId",mList.get(mPositionPingJia).getCommodityId());
                intent.putExtra("cartimg",mList.get(mPositionPingJia).getCartImg());
                intent.putExtra("number",mList.get(mPositionPingJia).getCommodityNumber());
                intent.putExtra("price",mList.get(mPositionPingJia).getCommodityXPrice());
                intent.putExtra("property",mList.get(mPositionPingJia).getCommodityProperty());
                intent.putExtra("name",mList.get(mPositionPingJia).getCommodityName());
               startActivity(intent);
            }

        }

        @Override
        public void onError(String result) {
            Log.e(TAG, "onError: "+result );
        }
    };

    private void initData() {
        mList = new ArrayList<>();
        Log.e("aaa", "initData: " + orderstatus);
        mAdapter = new OrderDetailLvAdapter(OrderDetailActivity.this, mList, orderstatus);
        mlvOrderdetail.setAdapter(mAdapter);
        mAdapter.setBtnClickInterface(new OrderDetailLvAdapter.btnClickInterface() {
            @Override
            public void doClickBtn2(int position) {
                mPosition = position;
                orderDetailPredenter.getShouHouDetail(AppConstants.COMPANY_ID,code,timestamp,String.valueOf(mList.get(position).getCartId()), String.valueOf(mList.get(position).getCommodityId()),AppConstants.FROM_MOBILE);
                Log.e(TAG, "doClickBtn2: "+ifrepairsCheck );

            }

            @Override
            public void doClickBtn1(int position) {
                mPositionPingJia=position;
                orderDetailPredenter.getGoodsEvaluateDetail(AppConstants.COMPANY_ID,code,timestamp,String.valueOf(mList.get(position).getCartId()),AppConstants.FROM_MOBILE);
            }
        });
    }

    @OnClick(R.id.iv_title_back)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_title_back:
                onBackPressed();
                break;
        }
    }


}
