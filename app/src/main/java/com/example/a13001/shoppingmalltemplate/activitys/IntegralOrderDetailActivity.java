package com.example.a13001.shoppingmalltemplate.activitys;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.OrderDetail;
import com.example.a13001.shoppingmalltemplate.mvpview.IntegralOrderDetailView;
import com.example.a13001.shoppingmalltemplate.presenter.IntegralOrderDetailPredenter;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class IntegralOrderDetailActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_title_right)
    TextView tvTitleRight;
    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;
    @BindView(R.id.tv_integralorderdetail_orderid)
    TextView tvIntegralorderdetailOrderid;
    @BindView(R.id.tv_integralorderdetail_state)
    TextView tvIntegralorderdetailState;
    @BindView(R.id.iv_integralorderdetail_goodsimg)
    ImageView ivIntegralorderdetailGoodsimg;
    @BindView(R.id.tv_integralorderdetail_goodsname)
    TextView tvIntegralorderdetailGoodsname;
    @BindView(R.id.tv_integralorderdetail_goodsnum)
    TextView tvIntegralorderdetailGoodsnum;
    @BindView(R.id.tv_integralorderdetail_jifen)
    TextView tvIntegralorderdetailJifen;
    @BindView(R.id.tv_integralorderdetail_address)
    TextView tvIntegralorderdetailAddress;
    @BindView(R.id.tv_integralorderdetail_postcode)
    TextView tvIntegralorderdetailPostcode;
    @BindView(R.id.tv_integralorderdetail_name)
    TextView tvIntegralorderdetailName;
    @BindView(R.id.tv_integralorderdetail_phone)
    TextView tvIntegralorderdetailPhone;
    @BindView(R.id.tv_integralorderdetail_orderdate)
    TextView tvIntegralorderdetailOrderdate;
    @BindView(R.id.tv_integralorderdetail_expresscompany)
    TextView tvIntegralorderdetailExpresscompany;
    @BindView(R.id.tv_integralorderdetail_trackingnumber)
    TextView tvIntegralorderdetailTrackingnumber;
    @BindView(R.id.tv_integralorderdetail_commit)
    TextView tvIntegralorderdetailCommit;
    IntegralOrderDetailPredenter integralOrderDetailPredenter=new IntegralOrderDetailPredenter(IntegralOrderDetailActivity.this);
    private String code;
    private String timestamp;
    private String ordernumber;
    private static final String TAG = "IntegralOrderDetailActi";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_integral_order_detail);
        ButterKnife.bind(this);
        ordernumber = getIntent().getStringExtra("ordernumber");
        tvTitleCenter.setText("积分兑换订单详情");
        integralOrderDetailPredenter.onCreate();
        integralOrderDetailPredenter.attachView(integralOrderDetailView);
        String safetyCode = MyUtils.getMetaValue(IntegralOrderDetailActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        timestamp = Utils.getTimeStamp();
        integralOrderDetailPredenter.getIntegralOrderDetail(AppConstants.COMPANY_ID,code,timestamp,ordernumber,AppConstants.FROM_MOBILE);
    }
    IntegralOrderDetailView integralOrderDetailView=new IntegralOrderDetailView() {
        @Override
        public void onSuccessGetIntegralOrderDetail(OrderDetail orderDetail) {
            Log.e(TAG, "onSuccessGetIntegralOrderDetail: "+orderDetail.toString());
            int status=orderDetail.getStatus();
            if (status>0){
                int orderstatus=orderDetail.getOrderStatus();
                if (orderstatus==3){
                    tvIntegralorderdetailCommit.setVisibility(View.VISIBLE);
                }else{
                    tvIntegralorderdetailCommit.setVisibility(View.GONE);
                }
                tvIntegralorderdetailOrderid.setText(orderDetail.getOrdersNumber()!=null?"订单号："+orderDetail.getOrdersNumber():"订单号："+"");
                tvIntegralorderdetailState.setText(orderDetail.getOrderStatusName()!=null?orderDetail.getOrderStatusName():"");
                GlideUtils.setNetImage(IntegralOrderDetailActivity.this,AppConstants.INTERNET_HEAD+orderDetail.getOrderGoodsList().get(0).getCartImg(),ivIntegralorderdetailGoodsimg);
                tvIntegralorderdetailGoodsname.setText(orderDetail.getOrderGoodsList().get(0).getCommodityName()!=null?orderDetail.getOrderGoodsList().get(0).getCommodityName():"");
                tvIntegralorderdetailGoodsnum.setText("数量：X"+orderDetail.getOrderGoodsCount()+"");
                tvIntegralorderdetailJifen.setText("应付积分："+orderDetail.getOrderGoodsList().get(0).getCommodityZPrice());
                tvIntegralorderdetailAddress.setText(orderDetail.getReceiptAddress()!=null?orderDetail.getReceiptAddress():"");
                tvIntegralorderdetailPostcode.setText(orderDetail.getReciptZipCode()!=null?orderDetail.getReciptZipCode():"");
                tvIntegralorderdetailName.setText(orderDetail.getReceiptName()!=null?orderDetail.getReceiptName():"");
                tvIntegralorderdetailPhone.setText(orderDetail.getReceiptPhone()!=null?orderDetail.getReceiptPhone():"");
                tvIntegralorderdetailOrderdate.setText(orderDetail.getOrderDate()!=null?orderDetail.getOrderDate():"");

                String ss=orderDetail.getOrderDeliveryInfo();
                if (!TextUtils.isEmpty(ss)) {
                    String[] sss = ss.split(",");
                    String ssss = sss[0].substring(5);
                    String ssss1 = sss[1].substring(5);
                    Log.e(TAG, "onSuccessGetIntegralOrderDetail: " + sss[0]+"=="+sss[1]);
                    Log.e(TAG, "onSuccessGetIntegralOrderDetail: " + ssss+ "==" + ssss + "==" + ssss1);
                    tvIntegralorderdetailExpresscompany.setText(ssss);
                    tvIntegralorderdetailTrackingnumber.setText(ssss1);
                }
            }else{

            }
        }

        @Override
        public void onSuccessAffirmOrder(CommonResult commonResult) {
            Log.e(TAG, "onSuccessAffirmOrder: "+commonResult.toString() );
            int status=commonResult.getStatus();
            if (status>0){
                finish();
            }else{
                Toast.makeText(IntegralOrderDetailActivity.this, ""+commonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onError(String result) {
            Log.e(TAG, "onError: "+result.toString() );
        }
    };
    @OnClick({R.id.iv_title_back, R.id.tv_integralorderdetail_commit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_title_back:
                onBackPressed();
                break;
            case R.id.tv_integralorderdetail_commit:
                integralOrderDetailPredenter.affirmOrder(AppConstants.COMPANY_ID,code,timestamp,ordernumber);
                break;
        }
    }
}
