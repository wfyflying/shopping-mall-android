package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.GoodsList;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GoodsDetailTuiJianLvAdapter extends BaseAdapter {
    private Context mContext;
    private List<GoodsList.ListBean> mList;

    public GoodsDetailTuiJianLvAdapter(Context mContext, List<GoodsList.ListBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            if (mList.size()>=6){
                return 6;
            }else{

                return mList.size();
            }
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder myViewHolder;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_rv_goodsdetailtj, viewGroup, false);
            myViewHolder = new ViewHolder(view);
            view.setTag(myViewHolder);
            AutoUtils.autoSize(view);
        } else {
            myViewHolder = (ViewHolder) view.getTag();
        }
        GlideUtils.setNetImage(mContext, AppConstants.INTERNET_HEAD + mList.get(position).getImages(), myViewHolder.ivHompagesubLogo);
        myViewHolder.tvHompagesubContent.setText(mList.get(position).getTitle());
        myViewHolder.tvHompagesubPrice.setText(mList.get(position).getPrice() + "");
        myViewHolder.tvHompagesubLovenum.setText(mList.get(position).getHits()+"");
        return view;
    }


    static class ViewHolder {
        @BindView(R.id.iv_hompagesub_logo)
        ImageView ivHompagesubLogo;
        @BindView(R.id.tv_hompagesub_content)
        TextView tvHompagesubContent;
        @BindView(R.id.textView4)
        TextView textView4;
        @BindView(R.id.tv_hompagesub_price)
        TextView tvHompagesubPrice;
        @BindView(R.id.tv_hompagesub_lovenum)
        TextView tvHompagesubLovenum;
        @BindView(R.id.ll_root)
        LinearLayout llRoot;
        @BindView(R.id.iv_black)
        ImageView ivBlack;
        @BindView(R.id.tv_delete)
        TextView tvDelete;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
