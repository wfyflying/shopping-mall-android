package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.CloaseAccount;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AffirmIntegralOrderLvAdapter extends BaseAdapter {
    private Context mContext;
    private List<CloaseAccount.ShopListBean> mList;

    public AffirmIntegralOrderLvAdapter(Context mContext, List<CloaseAccount.ShopListBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_lv_affirmintegralorder, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        GlideUtils.setNetImage(mContext, AppConstants.INTERNET_HEAD + mList.get(i).getCartImg(), holder.ivItemaffirmorderLogo);
        holder.tvItemaffirmorderJifen.setText("单件积分：" + mList.get(i).getCommodityXPrice());
        holder.tvItemaffirmorderDangqiangjia.setText("所需积分：" + mList.get(i).getCommodityZPrice());
        holder.tvItemaffirmorderCount.setText("数量" + "X" + mList.get(i).getCommodityNumber() + "");
        holder.tvItemaffirmorderGoodsname.setText(mList.get(i).getCommodityName()!=null?mList.get(i).getCommodityName():"");

        return view;
    }



    class ViewHolder {
        @BindView(R.id.tv_itemaffirmorder_goodsname)
        TextView tvItemaffirmorderGoodsname;
        @BindView(R.id.iv_itemaffirmorder_logo)
        ImageView ivItemaffirmorderLogo;
        @BindView(R.id.tv_itemaffirmorder_count)
        TextView tvItemaffirmorderCount;
        @BindView(R.id.tv_itemaffirmorder_jifen)
        TextView tvItemaffirmorderJifen;
        @BindView(R.id.tv_itemaffirmorder_dangqiangjia)
        TextView tvItemaffirmorderDangqiangjia;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
