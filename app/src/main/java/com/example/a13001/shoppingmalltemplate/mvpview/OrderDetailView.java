package com.example.a13001.shoppingmalltemplate.mvpview;


import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.GoodsEvaluateDetail;
import com.example.a13001.shoppingmalltemplate.modle.OrderDetail;
import com.example.a13001.shoppingmalltemplate.modle.ShouHouDetail;

public interface OrderDetailView extends View{
    void onSuccessGetOrderDetail(OrderDetail orderDetail);
    void onSuccessCancelOrder(CommonResult commonResult);
    void onSuccessAffirmOrder(CommonResult commonResult);
    void onSuccessGetShouHouDetail(ShouHouDetail shouHouDetail);
    void onSuccessGetGoodsEvaluateDetail(GoodsEvaluateDetail goodsEvaluateDetail);
    void onError(String result);
}
