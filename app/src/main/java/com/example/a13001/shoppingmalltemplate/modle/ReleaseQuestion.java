package com.example.a13001.shoppingmalltemplate.modle;

public class ReleaseQuestion {

    /**
     * status : 0
     * returnMsg : null
     * commentstatus : 0
     */

    private int status;
    private Object returnMsg;
    private int commentstatus;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getCommentstatus() {
        return commentstatus;
    }

    public void setCommentstatus(int commentstatus) {
        this.commentstatus = commentstatus;
    }
}
