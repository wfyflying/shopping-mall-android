package com.example.a13001.shoppingmalltemplate.activitys;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.adapters.MyIntegralLvAdapter;
import com.example.a13001.shoppingmalltemplate.adapters.RechargeSheetLvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.IntegrationList;
import com.example.a13001.shoppingmalltemplate.modle.Recharge;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class RechargeSheetActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.lv_myintegral)
    ListView lvMyintegral;
    @BindView(R.id.srfl_myintegral)
    SmartRefreshLayout srflMyintegral;
    private List<Recharge.MemberRechargeListBean> mList;
    private RechargeSheetLvAdapter mAdapter;
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Recharge mRecharge;
    private static final String TAG = "MyIntegralActivity";
    private String code;
    private String timeStamp;
    private int pageindex=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_integral);
        ButterKnife.bind(this);
        initData();
    }

    /**
     * 初始化数据源
     */
    private void initData() {
        manager=new DataManager(RechargeSheetActivity.this);
        mCompositeSubscription=new CompositeSubscription();
        tvTitleCenter.setText("充值明细");
        String safetyCode = MyUtils.getMetaValue(RechargeSheetActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        timeStamp = Utils.getTimeStamp();

        mList = new ArrayList<>();
        mAdapter = new RechargeSheetLvAdapter(RechargeSheetActivity.this, mList);
        getIntegraTionList(AppConstants.COMPANY_ID, code, timeStamp,AppConstants.PAGE_SIZE,pageindex,"","");
        lvMyintegral.setAdapter(mAdapter);

        setRefresh();
    }

    @OnClick(R.id.iv_title_back)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_title_back:
                onBackPressed();
                break;
        }

    }
    /**
     * 获取会员详细信息
     * @param companyid    站点ID
     * @param code          安全校验码
     * @param timestamp     时间戳
     * @return
     */
    public void getIntegraTionList(String companyid, String code, String timestamp, int pagesize, int pageindex, String startdate, String overdate) {

        mCompositeSubscription.add(manager.getRechargeList(companyid,code,timestamp,pagesize,pageindex,startdate,overdate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Recharge>() {
                    @Override
                    public void onCompleted() {
                        int status=mRecharge.getStatus();
                        if (status>0){
                            mList.addAll(mRecharge.getMemberRechargeList());
                            mAdapter.notifyDataSetChanged();
                        }else{

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: "+e.toString() );
                    }

                    @Override
                    public void onNext(Recharge recharge) {
                        mRecharge=recharge;
                    }
                }));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }
    private void setRefresh() {
        //刷新
        srflMyintegral.setRefreshHeader(new ClassicsHeader(RechargeSheetActivity.this));
        srflMyintegral.setRefreshFooter(new ClassicsFooter(RechargeSheetActivity.this));
//        mRefresh.setEnablePureScrollMode(true);
        srflMyintegral.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                pageindex = 1;
                if (mList != null) {
                    mList.clear();
                }
                getIntegraTionList(AppConstants.COMPANY_ID, code, timeStamp,AppConstants.PAGE_SIZE,pageindex,"","");

                refreshlayout.finishRefresh(2000/*,true*/);//传入false表示刷新失败
            }
        });
        srflMyintegral.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                pageindex++;
                getIntegraTionList(AppConstants.COMPANY_ID, code, timeStamp,AppConstants.PAGE_SIZE,pageindex,"","");
                refreshlayout.finishLoadMore(2000/*,true*/);//传入false表示加载失败
            }
        });
    }
}
