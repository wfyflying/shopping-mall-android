package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class IntegralCloaseAccount {


    /**
     * status : 1
     * returnMsg : null
     * addressCount : 1
     * address : [{"addresslId":7,"addressName":"子非鱼","addressPhone":"18622470028","addressProvince":"天津市","addressCity":"天津市","addressArea":"河西区","addressXX":"建国楼","addressZipcode":300000,"addressDefault":true}]
     * Count : 1
     * shopSum : 2
     * shopList : [{"CartId":100001,"commodityId":134,"CartImg":"//file.site.ify2.cn/site/153/upload/sccs/upload/201801/2018112107373851.jpg","CommodityName":"添加个新商品","commodityLink":"//www.tjsrsh.site.ify2.cn/channel/content.aspx?id=134&preview=6e8c08bea0328d612569d30dc24ffb4e","CommodityProperty":"规格:10克","CommodityNumber":2,"CartListID":"E24BAB001","CommodityXPrice":100,"CommodityYhPrice":0,"CommodityZPrice":200}]
     * totalZongPrice : 400
     */

    private int status;
    private Object returnMsg;
    private int addressCount;
    private int Count;
    private int shopSum;
    private int totalZongPrice;
    private List<AddressBean> address;
    private List<ShopListBean> shopList;

    @Override
    public String toString() {
        return "IntegralCloaseAccount{" +
                "status=" + status +
                ", returnMsg=" + returnMsg +
                ", addressCount=" + addressCount +
                ", Count=" + Count +
                ", shopSum=" + shopSum +
                ", totalZongPrice=" + totalZongPrice +
                ", address=" + address +
                ", shopList=" + shopList +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(Object returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getAddressCount() {
        return addressCount;
    }

    public void setAddressCount(int addressCount) {
        this.addressCount = addressCount;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int Count) {
        this.Count = Count;
    }

    public int getShopSum() {
        return shopSum;
    }

    public void setShopSum(int shopSum) {
        this.shopSum = shopSum;
    }

    public int getTotalZongPrice() {
        return totalZongPrice;
    }

    public void setTotalZongPrice(int totalZongPrice) {
        this.totalZongPrice = totalZongPrice;
    }

    public List<AddressBean> getAddress() {
        return address;
    }

    public void setAddress(List<AddressBean> address) {
        this.address = address;
    }

    public List<ShopListBean> getShopList() {
        return shopList;
    }

    public void setShopList(List<ShopListBean> shopList) {
        this.shopList = shopList;
    }

    public static class AddressBean {
        /**
         * addresslId : 7
         * addressName : 子非鱼
         * addressPhone : 18622470028
         * addressProvince : 天津市
         * addressCity : 天津市
         * addressArea : 河西区
         * addressXX : 建国楼
         * addressZipcode : 300000
         * addressDefault : true
         */

        private int addresslId;
        private String addressName;
        private String addressPhone;
        private String addressProvince;
        private String addressCity;
        private String addressArea;
        private String addressXX;
        private int addressZipcode;
        private boolean addressDefault;

        @Override
        public String toString() {
            return "AddressBean{" +
                    "addresslId=" + addresslId +
                    ", addressName='" + addressName + '\'' +
                    ", addressPhone='" + addressPhone + '\'' +
                    ", addressProvince='" + addressProvince + '\'' +
                    ", addressCity='" + addressCity + '\'' +
                    ", addressArea='" + addressArea + '\'' +
                    ", addressXX='" + addressXX + '\'' +
                    ", addressZipcode=" + addressZipcode +
                    ", addressDefault=" + addressDefault +
                    '}';
        }

        public int getAddresslId() {
            return addresslId;
        }

        public void setAddresslId(int addresslId) {
            this.addresslId = addresslId;
        }

        public String getAddressName() {
            return addressName;
        }

        public void setAddressName(String addressName) {
            this.addressName = addressName;
        }

        public String getAddressPhone() {
            return addressPhone;
        }

        public void setAddressPhone(String addressPhone) {
            this.addressPhone = addressPhone;
        }

        public String getAddressProvince() {
            return addressProvince;
        }

        public void setAddressProvince(String addressProvince) {
            this.addressProvince = addressProvince;
        }

        public String getAddressCity() {
            return addressCity;
        }

        public void setAddressCity(String addressCity) {
            this.addressCity = addressCity;
        }

        public String getAddressArea() {
            return addressArea;
        }

        public void setAddressArea(String addressArea) {
            this.addressArea = addressArea;
        }

        public String getAddressXX() {
            return addressXX;
        }

        public void setAddressXX(String addressXX) {
            this.addressXX = addressXX;
        }

        public int getAddressZipcode() {
            return addressZipcode;
        }

        public void setAddressZipcode(int addressZipcode) {
            this.addressZipcode = addressZipcode;
        }

        public boolean isAddressDefault() {
            return addressDefault;
        }

        public void setAddressDefault(boolean addressDefault) {
            this.addressDefault = addressDefault;
        }
    }

    public static class ShopListBean {
        /**
         * CartId : 100001
         * commodityId : 134
         * CartImg : //file.site.ify2.cn/site/153/upload/sccs/upload/201801/2018112107373851.jpg
         * CommodityName : 添加个新商品
         * commodityLink : //www.tjsrsh.site.ify2.cn/channel/content.aspx?id=134&preview=6e8c08bea0328d612569d30dc24ffb4e
         * CommodityProperty : 规格:10克
         * CommodityNumber : 2
         * CartListID : E24BAB001
         * CommodityXPrice : 100
         * CommodityYhPrice : 0
         * CommodityZPrice : 200
         */

        private int CartId;
        private int commodityId;
        private String CartImg;
        private String CommodityName;
        private String commodityLink;
        private String CommodityProperty;
        private int CommodityNumber;
        private String CartListID;
        private int CommodityXPrice;
        private int CommodityYhPrice;
        private int CommodityZPrice;

        @Override
        public String toString() {
            return "ShopListBean{" +
                    "CartId=" + CartId +
                    ", commodityId=" + commodityId +
                    ", CartImg='" + CartImg + '\'' +
                    ", CommodityName='" + CommodityName + '\'' +
                    ", commodityLink='" + commodityLink + '\'' +
                    ", CommodityProperty='" + CommodityProperty + '\'' +
                    ", CommodityNumber=" + CommodityNumber +
                    ", CartListID='" + CartListID + '\'' +
                    ", CommodityXPrice=" + CommodityXPrice +
                    ", CommodityYhPrice=" + CommodityYhPrice +
                    ", CommodityZPrice=" + CommodityZPrice +
                    '}';
        }

        public int getCartId() {
            return CartId;
        }

        public void setCartId(int CartId) {
            this.CartId = CartId;
        }

        public int getCommodityId() {
            return commodityId;
        }

        public void setCommodityId(int commodityId) {
            this.commodityId = commodityId;
        }

        public String getCartImg() {
            return CartImg;
        }

        public void setCartImg(String CartImg) {
            this.CartImg = CartImg;
        }

        public String getCommodityName() {
            return CommodityName;
        }

        public void setCommodityName(String CommodityName) {
            this.CommodityName = CommodityName;
        }

        public String getCommodityLink() {
            return commodityLink;
        }

        public void setCommodityLink(String commodityLink) {
            this.commodityLink = commodityLink;
        }

        public String getCommodityProperty() {
            return CommodityProperty;
        }

        public void setCommodityProperty(String CommodityProperty) {
            this.CommodityProperty = CommodityProperty;
        }

        public int getCommodityNumber() {
            return CommodityNumber;
        }

        public void setCommodityNumber(int CommodityNumber) {
            this.CommodityNumber = CommodityNumber;
        }

        public String getCartListID() {
            return CartListID;
        }

        public void setCartListID(String CartListID) {
            this.CartListID = CartListID;
        }

        public int getCommodityXPrice() {
            return CommodityXPrice;
        }

        public void setCommodityXPrice(int CommodityXPrice) {
            this.CommodityXPrice = CommodityXPrice;
        }

        public int getCommodityYhPrice() {
            return CommodityYhPrice;
        }

        public void setCommodityYhPrice(int CommodityYhPrice) {
            this.CommodityYhPrice = CommodityYhPrice;
        }

        public int getCommodityZPrice() {
            return CommodityZPrice;
        }

        public void setCommodityZPrice(int CommodityZPrice) {
            this.CommodityZPrice = CommodityZPrice;
        }
    }
}
