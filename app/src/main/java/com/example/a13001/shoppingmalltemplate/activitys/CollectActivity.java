package com.example.a13001.shoppingmalltemplate.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.adapters.CollectGvAdapter;
import com.example.a13001.shoppingmalltemplate.adapters.GoodsListRvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.Collect;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.Goods;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class CollectActivity extends BaseActivity implements CollectGvAdapter.doDeleteCollectInterface {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.gv_collect)
    ListView gvCollect;
    private ArrayList<Collect.ListBean> mList;
    private CollectGvAdapter mAdapter;
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Collect mCollect;
    private int pageindex=1;
    private String code;
    private String timestamp;
    private CommonResult mCommonResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect);
        ButterKnife.bind(this);
        mCompositeSubscription=new CompositeSubscription();
        manager=new DataManager(CollectActivity.this);
        tvTitleCenter.setText("我的收藏");
        initData();
        String safetyCode = MyUtils.getMetaValue(CollectActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        timestamp = Utils.getTimeStamp();
        getLoveList(AppConstants.COMPANY_ID,code,timestamp,AppConstants.PAGE_SIZE,pageindex);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    /**
     * 初始化数据源
     */
    private void initData() {
        mList = new ArrayList<>();
        mAdapter = new CollectGvAdapter(CollectActivity.this, mList);
        mAdapter.setDoDeleteCollectInterface(this);
        gvCollect.setAdapter(mAdapter);


    }

    @OnClick(R.id.iv_title_back)
    public void onViewClicked(View view) {
        switch (view.getId()){
            case R.id.iv_title_back:
                onBackPressed();
                break;
        }
    }
    /**
     * 获取会员收藏列表
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @return
     */
    public void getLoveList(String companyid, String code, String timestamp,int pagesize,int pageindex) {

        mCompositeSubscription.add(manager.getLoveList(companyid,code,timestamp,pagesize,pageindex)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Collect>() {
                    @Override
                    public void onCompleted() {
                        if (mCollect != null) {
                            int status=mCollect.getStatus();
                            if (status>0){
                                mList.addAll(mCollect.getList());
                                mAdapter.notifyDataSetChanged();
                            }else{
                                Toast.makeText(CollectActivity.this, ""+mCollect.getReturnMsg(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(CollectActivity.this, "请求失败"+e.toString(), Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onNext(Collect collect) {
                       mCollect=collect;
                    }
                }));
    }
    /**
     * 删除会员收藏列表
     * @param companyid
     * @param code
     * @param timestamp

     * @return
     */
    public void doDeleteCollect(final String companyid, final String code, final String timestamp, int collectlId) {

        mCompositeSubscription.add(manager.doDeleteCollection(companyid,code,timestamp,collectlId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResult>() {
                    @Override
                    public void onCompleted() {
                        if (mCommonResult != null) {
                            int status=mCommonResult.getStatus();
                            if (status>0){
                                if (mList != null) {
                                    mList.clear();
                                }
                                getLoveList(companyid,code,timestamp,AppConstants.PAGE_SIZE,pageindex);
                            }else{
                                Toast.makeText(CollectActivity.this, ""+mCommonResult.getReturnMsg(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(CollectActivity.this, "请求失败"+e.toString(), Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onNext(CommonResult commonResult) {
                        mCommonResult=commonResult;
                    }
                }));
    }
        //删除
    @Override
    public void doDelete(int position) {
        doDeleteCollect(AppConstants.COMPANY_ID,code,timestamp,mList.get(position).getCollectlId());
    }

    @Override
    public void doStartDetail(int position) {
        Intent intent=new Intent(CollectActivity.this,GoodsDetailActivity.class);
        intent.putExtra("good_id",mList.get(position).getCommodityId());
        intent.putExtra("type","a");
        startActivity(intent);
    }
}
