package com.example.a13001.shoppingmalltemplate.activitys;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DepositActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_deposit_all)
    TextView tvDepositAll;
    @BindView(R.id.tv_deposit_sqtx)
    TextView tvDepositSqtx;
    @BindView(R.id.tv_deposit_txsb)
    TextView tvDepositTxsb;
    @BindView(R.id.tv_deposit_txcg)
    TextView tvDepositTxcg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposit);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.iv_title_back, R.id.tv_deposit_all, R.id.tv_deposit_sqtx, R.id.tv_deposit_txsb, R.id.tv_deposit_txcg})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_title_back:
                onBackPressed();
                break;
                //全部记录
            case R.id.tv_deposit_all:
                initColor();
                tvDepositAll.setBackgroundColor(getResources().getColor(R.color.ffb422));
                tvDepositAll.setTextColor(getResources().getColor(R.color.white));
                break;
                //申请提现
            case R.id.tv_deposit_sqtx:
                initColor();
                tvDepositSqtx.setBackgroundColor(getResources().getColor(R.color.ffb422));
                tvDepositSqtx.setTextColor(getResources().getColor(R.color.white));
                break;
                //提现失败
            case R.id.tv_deposit_txsb:
                initColor();
                tvDepositTxsb.setBackgroundColor(getResources().getColor(R.color.ffb422));
                tvDepositTxsb.setTextColor(getResources().getColor(R.color.white));
                break;
                //提现成功
            case R.id.tv_deposit_txcg:
                initColor();
                tvDepositTxcg.setBackgroundColor(getResources().getColor(R.color.ffb422));
                tvDepositTxcg.setTextColor(getResources().getColor(R.color.white));
                break;
        }
    }
    /**
     *
     */
    private void initColor() {
        tvDepositAll.setBackgroundColor(getResources().getColor(R.color.white));
        tvDepositSqtx.setBackgroundColor(getResources().getColor(R.color.white));
        tvDepositTxsb.setBackgroundColor(getResources().getColor(R.color.white));
        tvDepositTxcg.setBackgroundColor(getResources().getColor(R.color.white));

        tvDepositAll.setTextColor(getResources().getColor(R.color.t666));
        tvDepositSqtx.setTextColor(getResources().getColor(R.color.t666));
        tvDepositTxsb.setTextColor(getResources().getColor(R.color.t666));
        tvDepositTxcg.setTextColor(getResources().getColor(R.color.t666));

    }
}
