package com.example.a13001.shoppingmalltemplate.activitys;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.MyWebViewClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.webview_detail)
    WebView webviewDetail;
    private static final String TAG = "DetailActivity";
    private String mContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        initData();
        setWebView();

    }

    /**
     * 初始化数据源
     */
    private void initData() {
        mContent = getIntent().getStringExtra("id");

        Log.e("bbb","---id--->"+mContent);
        tvTitleCenter.setText("图文详情");
    }
    private void setWebView() {

        WebSettings webSettings=webviewDetail.getSettings();
        webSettings.setSaveFormData(false);
        webSettings.setJavaScriptEnabled(true);//能够执行javascript脚本

        webSettings.setSupportZoom(false);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        //改写客户端UA
        String ua = webSettings.getUserAgentString();
        webSettings.setUserAgentString(ua.replace("Android", "tjtv5android"));
//        webview.addJavascriptInterface(new DemoJavaScriptInterface(Window.this, webview, model), "Tjtv5API");
        webviewDetail.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY); //阻止滚动条出现白边
//        if (!Utils.isNetworkAvailable(Window.this)){
//            webview.loadUrl("file:///android_asset/404.html");
//        }else{
            //加载URL
        webviewDetail.loadUrl("file:///android_asset/content.html");
        webviewDetail.setWebViewClient(new MyWebViewClient(DetailActivity.this, "144", mContent, "webview"));
//        }
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
            webviewDetail.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
//        webviewDetail.clearCache(true);
//        webviewDetail.loadDataWithBaseURL("",mContent,"text/html" , "utf-8", null);
//        String jsonUrl = MyUtils.getMetaValue(this, "companyJSON");
//        webviewDetail.loadUrl("file:///android_asset/content.html");
//        String jsonUrl = MyUtils.getMetaValue(this, "companyURL");
//        webviewDetail.loadUrl("javascript:GetJson('" + jsonUrl + "','" + 144 + "','" + 3 + "',"  + ")");
//        webviewDetail.loadUrl("javascript:GetJson('" + jsonUrl + "','" + "144" + "','" + "3" + "');");
        //设置Web视图
//        webviewDetail.setWebViewClient(new MyWebViewClient(DetailActivity.this,AppConstants.COMPANY_ID,mContent,"webview"));
//        clearWebViewCache();

    }
    public void clearWebViewCache() {
// 清除cookie即可彻底清除缓存
        CookieSyncManager.createInstance(DetailActivity.this);
        CookieManager.getInstance().removeAllCookie();
    }
    //Web视图
    private class webViewClient extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            imgReset();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    /**
     * 循环遍历标签中的图片
     * js 语法
     */
    private void imgReset() {
        webviewDetail.loadUrl("javascript:(function(){" +
                "var objs = document.getElementsByTagName('img'); " +
                "for(var i=0;i<objs.length;i++)  " +
                "{"
                + "var img = objs[i];   " +
                "    img.style.maxWidth = '100%';   " +
                "}" +
                "})()");
    }
    @Override
    public boolean onKeyDown(int keyCoder,KeyEvent event){
        if (keyCoder == KeyEvent.KEYCODE_BACK && webviewDetail.canGoBack()) {
            webviewDetail.goBack();// 返回前一个页面
            return true;
        }
        return super.onKeyDown(keyCoder, event);
    }
    /**
     * 各控件的点击事件
     *
     * @param view
     */
    @OnClick(R.id.iv_title_back)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_title_back:
                onBackPressed();
                break;
        }
    }
}
