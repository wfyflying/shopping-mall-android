package com.example.a13001.shoppingmalltemplate.modle;

import java.util.List;

public class MyCouponList {

    /**
     * status : 1
     * returnMsg : null
     * count : 1
     * pagesize : 20
     * pageindex : 1
     * list : [{"SnNewId":16,"SnPh":"0877-0736-1002","SnStatus":1,"SnAddTime":"2018-07-22 10:13","SnEndTime":"2018-07-27 10:13","memberId":100000,"Atitle":"满100减20元","QzjContent":"满100减20元","Qmoney":20,"QxzMoney":100}]
     */

    private int status;
    private String returnMsg;
    private int count;
    private int pagesize;
    private int pageindex;
    private List<ListBean> list;

    @Override
    public String toString() {
        return "MyCouponList{" +
                "status=" + status +
                ", returnMsg='" + returnMsg + '\'' +
                ", count=" + count +
                ", pagesize=" + pagesize +
                ", pageindex=" + pageindex +
                ", list=" + list +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public int getPageindex() {
        return pageindex;
    }

    public void setPageindex(int pageindex) {
        this.pageindex = pageindex;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * SnNewId : 16
         * SnPh : 0877-0736-1002
         * SnStatus : 1
         * SnAddTime : 2018-07-22 10:13
         * SnEndTime : 2018-07-27 10:13
         * memberId : 100000
         * Atitle : 满100减20元
         * QzjContent : 满100减20元
         * Qmoney : 20
         * QxzMoney : 100
         */

        private int SnNewId;
        private String SnPh;
        private int SnStatus;
        private String SnAddTime;
        private String SnEndTime;
        private int memberId;
        private String Atitle;
        private String QzjContent;
        private int Qmoney;
        private int QxzMoney;

        @Override
        public String toString() {
            return "ListBean{" +
                    "SnNewId=" + SnNewId +
                    ", SnPh='" + SnPh + '\'' +
                    ", SnStatus=" + SnStatus +
                    ", SnAddTime='" + SnAddTime + '\'' +
                    ", SnEndTime='" + SnEndTime + '\'' +
                    ", memberId=" + memberId +
                    ", Atitle='" + Atitle + '\'' +
                    ", QzjContent='" + QzjContent + '\'' +
                    ", Qmoney=" + Qmoney +
                    ", QxzMoney=" + QxzMoney +
                    '}';
        }

        public int getSnNewId() {
            return SnNewId;
        }

        public void setSnNewId(int SnNewId) {
            this.SnNewId = SnNewId;
        }

        public String getSnPh() {
            return SnPh;
        }

        public void setSnPh(String SnPh) {
            this.SnPh = SnPh;
        }

        public int getSnStatus() {
            return SnStatus;
        }

        public void setSnStatus(int SnStatus) {
            this.SnStatus = SnStatus;
        }

        public String getSnAddTime() {
            return SnAddTime;
        }

        public void setSnAddTime(String SnAddTime) {
            this.SnAddTime = SnAddTime;
        }

        public String getSnEndTime() {
            return SnEndTime;
        }

        public void setSnEndTime(String SnEndTime) {
            this.SnEndTime = SnEndTime;
        }

        public int getMemberId() {
            return memberId;
        }

        public void setMemberId(int memberId) {
            this.memberId = memberId;
        }

        public String getAtitle() {
            return Atitle;
        }

        public void setAtitle(String Atitle) {
            this.Atitle = Atitle;
        }

        public String getQzjContent() {
            return QzjContent;
        }

        public void setQzjContent(String QzjContent) {
            this.QzjContent = QzjContent;
        }

        public int getQmoney() {
            return Qmoney;
        }

        public void setQmoney(int Qmoney) {
            this.Qmoney = Qmoney;
        }

        public int getQxzMoney() {
            return QxzMoney;
        }

        public void setQxzMoney(int QxzMoney) {
            this.QxzMoney = QxzMoney;
        }
    }
}
