package com.example.a13001.shoppingmalltemplate.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.adapters.GoodsListRvAdapter;
import com.example.a13001.shoppingmalltemplate.adapters.MyCouponListRvAdapter;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.base.BaseActivity;
import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.MyCouponList;
import com.example.a13001.shoppingmalltemplate.utils.MyUtils;
import com.example.a13001.shoppingmalltemplate.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class CanUseCouponActivity extends BaseActivity {

    @BindView(R.id.iv_title_back)
    ImageView ivTitleBack;
    @BindView(R.id.tv_title_center)
    TextView tvTitleCenter;
    @BindView(R.id.tv_title_right)
    TextView tvTitleRight;
    @BindView(R.id.rv_canusecoupon)
    RecyclerView rvCanusecoupon;
    private static final String TAG = "CanUseCouponActivity";
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private MyCouponList mMyCouponList;
    private List<MyCouponList.ListBean> mList;
    private MyCouponListRvAdapter mAdapter;
    private String code;
    private String timeStamp;
    private int pageindex;
    private String couponStatus = "1";//优惠券状态，1 未使用 2 已使用，为空则所有
    private String money="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_can_use_coupon);
        ButterKnife.bind(this);
        money= String.valueOf(getIntent().getDoubleExtra("money",0));
        initData();
    }

    /**
     * 初始化数据源
     */
    private void initData() {
        mCompositeSubscription = new CompositeSubscription();
        manager = new DataManager(CanUseCouponActivity.this);

        tvTitleCenter.setText("可用优惠券");

        String safetyCode = MyUtils.getMetaValue(CanUseCouponActivity.this, "safetyCode");
        code = Utils.md5(safetyCode + Utils.getTimeStamp());
        timeStamp = Utils.getTimeStamp();


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CanUseCouponActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvCanusecoupon.setLayoutManager(linearLayoutManager);
        //添加自定义分割线
//        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
//        divider.setDrawable(ContextCompat.getDrawable(this, R.drawable.line_recycler));
//        rvMycouponlist.addItemDecoration(divider);
//        rvMycouponlist.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        mList = new ArrayList<>();
        mAdapter = new MyCouponListRvAdapter(CanUseCouponActivity.this, mList);
        getMyCouponList(AppConstants.COMPANY_ID, code, timeStamp, "", money, couponStatus, AppConstants.PAGE_SIZE, pageindex);

        rvCanusecoupon.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new GoodsListRvAdapter.onItemClickListener() {
            @Override
            public void onClick(int position) {

                Intent intent=new Intent();
                intent.putExtra("snph", mList.get(position).getSnPh());
                intent.putExtra("qmoney", mList.get(position).getQmoney());
                setResult(55,intent);
                onBackPressed();
            }
        });
    }

    @OnClick({R.id.iv_title_back, R.id.tv_title_right})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_title_back:
                onBackPressed();
                break;
            case R.id.tv_title_right:
                break;
        }
    }
    /**
     * ★ 获取会员商城优惠券列表
     *
     * @param companyid
     * @param code
     * @param timestamp
     * @param storeid
     * @param money
     * @param status
     * @param pagesize
     * @param pageindex
     * @return
     */
    public void getMyCouponList(String companyid, String code, String timestamp,
                                String storeid, String money, String status, int pagesize, int pageindex) {

        mCompositeSubscription.add(manager.getMyCouponList(companyid, code, timestamp, storeid, money, status, pagesize, pageindex)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MyCouponList>() {
                    @Override
                    public void onCompleted() {
                        if (mMyCouponList != null) {
                            Log.e(TAG, "onCompleted: "+mMyCouponList.toString());
                            int status = mMyCouponList.getStatus();
                            if (status > 0) {
                                mList.addAll(mMyCouponList.getList());
                                mAdapter.notifyDataSetChanged();
                            } else {
//                                Toast.makeText(MyCouponListActivity.this, "" + mMyCouponList.getReturnMsg(), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: " + e.toString());
                    }

                    @Override
                    public void onNext(MyCouponList myCouponList) {
                        mMyCouponList = myCouponList;
                    }
                }));
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCompositeSubscription.hasSubscriptions()) {
            mCompositeSubscription.unsubscribe();
        }
    }
}
