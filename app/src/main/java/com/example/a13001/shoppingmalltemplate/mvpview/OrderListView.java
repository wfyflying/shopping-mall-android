package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.CloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.Order;

public interface OrderListView extends View{
    void onGetOrderList(Order order);
    void onSuccessCancelOrder(CommonResult commonResult);
    void onSuccessAffirmOrder(CommonResult commonResult);
    void onError(String result);
}
