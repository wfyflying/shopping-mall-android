package com.example.a13001.shoppingmalltemplate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.a13001.shoppingmalltemplate.R;
import com.example.a13001.shoppingmalltemplate.base.AppConstants;
import com.example.a13001.shoppingmalltemplate.modle.GoodsList;
import com.example.a13001.shoppingmalltemplate.utils.GlideUtils;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClassifyTwoLvAdapter extends BaseAdapter {
    private Context mContext;
    private List<GoodsList.ListBean> mList;
    private boolean isShow = false;//是否显示完成编辑

    public ClassifyTwoLvAdapter(Context mContext, List<GoodsList.ListBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_rv_homepage_sub1, viewGroup, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
            AutoUtils.autoSize(view);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        GlideUtils.setNetImage(mContext, AppConstants.INTERNET_HEAD + mList.get(i).getImages(), holder.ivHompagesubLogo);
        holder.tvHompagesubContent.setText(mList.get(i).getTitle() != null ? mList.get(i).getTitle() : "");
//        holder.tvClassifyDescribe.setText(mList.get(i).getText1() != null ? mList.get(i).getText1() : "");
        holder.tvHompagesubPrice.setText(mList.get(i).getPrice() + "");
        return view;
    }


    static class ViewHolder {
        @BindView(R.id.iv_hompagesub_logo)
        ImageView ivHompagesubLogo;
        @BindView(R.id.tv_hompagesub_content)
        TextView tvHompagesubContent;
        @BindView(R.id.tv_hompagesub_price)
        TextView tvHompagesubPrice;
        @BindView(R.id.tv_hompagesub_lovenum)
        TextView tvHompagesubLovenum;
        @BindView(R.id.ll_root)
        LinearLayout llRoot;
        @BindView(R.id.iv_black)
        ImageView ivBlack;
        @BindView(R.id.tv_delete)
        TextView tvDelete;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
