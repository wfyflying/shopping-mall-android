package com.example.a13001.shoppingmalltemplate.mvpview;

import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.DetailGoods;
import com.example.a13001.shoppingmalltemplate.modle.GoodsList;
import com.example.a13001.shoppingmalltemplate.modle.GoodsParameters;
import com.example.a13001.shoppingmalltemplate.modle.LoginStatus;
import com.example.a13001.shoppingmalltemplate.modle.Result;
import com.example.a13001.shoppingmalltemplate.modle.ShopCarGoods;

public interface GoodsDetailView extends View{
    void onSuccess(GoodsParameters goodsParameters);
    void onSuccessGetShopCarGoods(ShopCarGoods shopCarGoods);
    void onSuccessGoodsDetail(DetailGoods detailGoods);
    void onSuccessLoginStatus(LoginStatus loginStatus);
    void onSuccessAddShopCar(Result result);
    void onSuccessIfCollect(CommonResult commonResult);
    void onSuccessSetCollect(CommonResult commonResult);
    void onSuccessGetGoodList(GoodsList goodsList);
    void onError(String result);
}
