package com.example.a13001.shoppingmalltemplate.presenter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.a13001.shoppingmalltemplate.manager.DataManager;
import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.EvaluateList;
import com.example.a13001.shoppingmalltemplate.modle.GoodsParameters;
import com.example.a13001.shoppingmalltemplate.mvpview.EvaluateView;
import com.example.a13001.shoppingmalltemplate.mvpview.EvaluateView;
import com.example.a13001.shoppingmalltemplate.mvpview.View;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class EvaluatePredenter implements Presenter{
    private DataManager manager;
    private CompositeSubscription mCompositeSubscription;
    private Context mContext;
    private EvaluateView mEvaluateView;
    private EvaluateList mEvaluateList;
    private GoodsParameters mGoodsParameters;



    public EvaluatePredenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        mCompositeSubscription=new CompositeSubscription();
        manager=new DataManager(mContext);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (mCompositeSubscription.hasSubscriptions()){
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void attachView(View view) {
        mEvaluateView=(EvaluateView) view;
    }

    @Override
    public void attachIncomingIntent(Intent intent) {

    }
    /**
     * 获取商品评价列表信息
     * @param companyid
     * @param id         评价商品ID
     * @param level      评价等级，1 好评 2 中评 3 差评，为空则调用所有
     * @param pagesize
     * @param pageindex
     * @param order           排序方式 0 按评价时间正序 1 按评价时间倒序 2 按支持数正序 3 按支持数倒序
     * @param from
     * @return
     */
    public void getEvaluateList(String companyid, int id,String level, String order,int pagesize, int pageindex, String from) {

        mCompositeSubscription.add(manager.getEvaluateList(companyid,id,level,order,pagesize,pageindex,from)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new rx.Observer<EvaluateList>() {
                    @Override
                    public void onCompleted() {

                        if (mEvaluateView!=null){
                            mEvaluateView.onSuccessGetEvaluate(mEvaluateList);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mEvaluateView.onError("请求失败"+e.toString());

                    }

                    @Override
                    public void onNext(EvaluateList evaluateList) {
                        mEvaluateList=evaluateList;
                    }
                }));
    }
    /**
     * 获取商品规格参数
     * @param companyid
     * @param id
     */
    public void getEvaluateNum(String companyid,int id){
        mCompositeSubscription.add(manager.getGoodsParemters(companyid, id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GoodsParameters>() {
                    @Override
                    public void onCompleted() {
                        if (mEvaluateView!=null){
                            mEvaluateView.onSuccessGetEvaluateNum(mGoodsParameters);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mEvaluateView.onError("请求失败");
                    }

                    @Override
                    public void onNext(GoodsParameters goodsParameters) {
                        mGoodsParameters=goodsParameters;
                    }
                }));
    }
}
