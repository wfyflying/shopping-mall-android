package retrofit;

import com.example.a13001.shoppingmalltemplate.modle.Account;
import com.example.a13001.shoppingmalltemplate.modle.Address;
import com.example.a13001.shoppingmalltemplate.modle.AddressOrderId;
import com.example.a13001.shoppingmalltemplate.modle.AfterSale;
import com.example.a13001.shoppingmalltemplate.modle.Alipay;
import com.example.a13001.shoppingmalltemplate.modle.AnswerList;
import com.example.a13001.shoppingmalltemplate.modle.AppConfig;
import com.example.a13001.shoppingmalltemplate.modle.Banner;
import com.example.a13001.shoppingmalltemplate.modle.Book;
import com.example.a13001.shoppingmalltemplate.modle.Classify;
import com.example.a13001.shoppingmalltemplate.modle.CloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.Collect;
import com.example.a13001.shoppingmalltemplate.modle.CommonResult;
import com.example.a13001.shoppingmalltemplate.modle.ContentFilter;
import com.example.a13001.shoppingmalltemplate.modle.CouponCount;
import com.example.a13001.shoppingmalltemplate.modle.DetailGoods;
import com.example.a13001.shoppingmalltemplate.modle.EvaluateList;
import com.example.a13001.shoppingmalltemplate.modle.GoodsDetail;
import com.example.a13001.shoppingmalltemplate.modle.GoodsEvaluateDetail;
import com.example.a13001.shoppingmalltemplate.modle.GoodsEvaluateList;
import com.example.a13001.shoppingmalltemplate.modle.GoodsList;
import com.example.a13001.shoppingmalltemplate.modle.GoodsParameters;
import com.example.a13001.shoppingmalltemplate.modle.IntegralCloaseAccount;
import com.example.a13001.shoppingmalltemplate.modle.IntegrationList;
import com.example.a13001.shoppingmalltemplate.modle.LoginStatus;
import com.example.a13001.shoppingmalltemplate.modle.Message;
import com.example.a13001.shoppingmalltemplate.modle.MessageDetail;
import com.example.a13001.shoppingmalltemplate.modle.MyCouponList;
import com.example.a13001.shoppingmalltemplate.modle.NewBanner;
import com.example.a13001.shoppingmalltemplate.modle.News;
import com.example.a13001.shoppingmalltemplate.modle.Notice;
import com.example.a13001.shoppingmalltemplate.modle.NoticeDetail;
import com.example.a13001.shoppingmalltemplate.modle.Order;
import com.example.a13001.shoppingmalltemplate.modle.OrderDetail;
import com.example.a13001.shoppingmalltemplate.modle.OrderNum;
import com.example.a13001.shoppingmalltemplate.modle.Recharge;
import com.example.a13001.shoppingmalltemplate.modle.RefreshYunFei;
import com.example.a13001.shoppingmalltemplate.modle.Result;
import com.example.a13001.shoppingmalltemplate.modle.ShopCarGoods;
import com.example.a13001.shoppingmalltemplate.modle.ShopCouponList;
import com.example.a13001.shoppingmalltemplate.modle.ShouHouDetail;
import com.example.a13001.shoppingmalltemplate.modle.SignIn;
import com.example.a13001.shoppingmalltemplate.modle.UpFile;
import com.example.a13001.shoppingmalltemplate.modle.User;
import com.example.a13001.shoppingmalltemplate.modle.UserInfo;
import com.example.a13001.shoppingmalltemplate.modle.WechatPay;


import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import rx.Observable;

public interface RetrofitService {
    /**
     * 注册
     */
    @FormUrlEncoded
    @POST("/api/json/member/member.ashx?action=register")
    Observable<User> doRegister(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @FieldMap Map<String, String> map
    );
    /**
     * ★ 获取图形验证码
     */
    @GET("/api/json/config/config.ashx?action=txyzm")
    Observable<CommonResult> getPicCode(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp
    );
    /**
     * ★  验证会员注册信息-发送手机校验码
     */
    @GET("/api/json/member/member.ashx?action=chkreg&chktype=106")
    Observable<CommonResult> getPhoneCode(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("chkdata1") String chkdata1,
            @Query("chkdata2") String chkdata2,
            @Query("from") String from
    );

    /**
     * 找回密码-发送手机校验码
     * @param companyid
     * @param code
     * @param timestamp
     * @param chkData1       手机号
     * @param chkData2      图形验证码
     * @return
     */
    @FormUrlEncoded
    @POST("/api/json/member/member.ashx?action=getpass&chktype=102")
    Observable<CommonResult> getForgetPwdPhoneCode(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Field("chkData1") String chkData1,
            @Field("chkData2") String chkData2
    );

    /**
     *★ 找回密码-设置新密码
     * @param companyid
     * @param code
     * @param timestamp
     * @param chkData1        手机（邮箱）校验码
     * @param chkData2       密码
     * @param chkData3       确认密码
     * @return
     */
    @FormUrlEncoded
    @POST("/api/json/member/member.ashx?action=getpass&chktype=106")
    Observable<CommonResult> doResetPwd(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Field("chkData1") String chkData1,
            @Field("chkData2") String chkData2,
            @Field("chkData3") String chkData3
    );

    /**
     * ★ 修改会员安全密码
     * @param companyid
     * @param code
     * @param timestamp
     * @param oldpass      旧密码，未设置交易密码时允许为空
     * @param newpass     新密码
     * @param confirmpass   确认新密码
     * @return
     */
    @FormUrlEncoded
    @POST("/api/json/member/safe.ashx?action=paymentpass")
    Observable<CommonResult> doResetSafePwd(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Field("oldpass") String oldpass,
            @Field("newpass") String newpass,
            @Field("confirmpass") String confirmpass
    );
    /**
     * 登录
     * @param companyid  站点ID
     * @param code   安全校验码
     * @param timestamp  时间戳
     * @param name    会员名称，用户名/手机/邮箱
     * @param pwd    登录密码
     * @param from   来源，pc 电脑端 mobile 移动端
     * @return
     */
    @GET("/api/json/member/member.ashx?action=login")
    Observable<User> doLogin(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("name") String name,
            @Query("pwd") String pwd,
            @Query("from") String from
    );

    /**
     * 退出登录
     * @param companyid
     * @return
     */
    @GET("/api/json/member/member.ashx?action=logout")
    Observable<CommonResult> doLoginOut(
            @Query("companyid") String companyid
    );
    /**
     * 第三方登录
     * @param companyid   站点ID
     * @param code        安全校验码
     * @param timestamp   时间戳
     * @param type        qqlogin QQ登录， wxlogin 微信登录，sinalogin 新浪微博登录
     * @param openid      第三方登录用户唯一值
     * @param nickname    会员昵称
     * @param sex          会员性别，1 男，2 女
     * @param headimgurl   头像地址
     * @return
     */
    @GET("/api/json/member/member.ashx?action=otherlogin")
    Observable<User> doThirdLogin(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("type") String type,
            @Query("openid") String openid,
            @Query("nickname") String nickname,
            @Query("sex") String sex,
            @Query("headimgurl") String headimgurl,
            @Query("appos") String appos,
            @Query("appkey") String appkey
    );

    /**
     * ★ 修改会员登录密码
     * @param companyid
     * @param code
     * @param timestamp
     * @param oldpass       旧密码，如果是第三方登录首次修改密码，值可以为空
     * @param newpass       新密码
     * @param confirmpass    确认新密码
     * @return
     */
    @FormUrlEncoded
    @POST("/api/json/member/safe.ashx?action=editpass")
    Observable<CommonResult> modifyPwd(
        @Query("companyid") String companyid,
        @Query("code") String code,
        @Query("timestamp") String timestamp,
        @Field("oldpass") String oldpass,
        @Field("newpass") String newpass,
        @Field("confirmpass") String confirmpass
            );

    /**
     *★ 绑定手机号码
     * @param companyid
     * @param code
     * @param timestamp
     * @param chkData1     手机号
     * @param chkData2      手机验证码
     * @param chkData3       安全密码
     * @return
     */
    @FormUrlEncoded
    @POST("/api/json/member/safe.ashx?action=editmobileoremail&chkType=106")
    Observable<CommonResult> bindPhone(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Field("chkData1") String chkData1,
            @Field("chkData2") String chkData2,
            @Field("chkData3") String chkData3
    );
    /**
     *★ 绑定手机号码-发送手机校验码
     * @param companyid
     * @param code
     * @param timestamp
     * @param chkData1     手机号
     * @param chkData2      安全密码
     *
     * @return
     */
    @FormUrlEncoded
    @POST("/api/json/member/safe.ashx?action=editmobileoremail&chkType=102")
    Observable<CommonResult> bindPhoneSendCode(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Field("chkData1") String chkData1,
            @Field("chkData2") String chkData2
    );
    /**
     * 获取资讯列表
     * @param companyid  站点ID
     * @param channelid  频道ID，指定多个频道用“|”线分割
     * @param pagesize  每页显示数量
     * @param pageindex  当前页数
     * @return
     */
    @GET("/api/json/content/content.ashx?action=list")
    Observable<News> getSearchNews(
            @Query("companyid") String companyid,
            @Query("channelid") String channelid,
            @Query("keyword") String keyword,
            @Query("pagesize") int pagesize,
            @Query("pageindex") int pageindex);

    /**
     * 获取首页轮播图
     * @param companyid  站点ID
     * @param label     碎片文件标识ID
     * @return
     */
    @GET("/api/json/common/common.ashx?action=fragment")
    Observable<Banner> getBannerList(
            @Query("companyid") String companyid,
            @Query("label") String label
    );

    /**
     * 获取首页底部分类商品列表
     * @param companyid   站点ID
     * @param channelid   频道ID，指定多个频道用“|”线分割
     * @param pagesize    每页显示数量
     * @param pageindex   当前页数
     * @return
     */
    @GET("/api/json/content/content.ashx?action=list")
    Observable<GoodsDetail> getClassifyList(
            @Query("companyid") String companyid,
            @Query("channelid") String channelid,
            @Query("pagesize") int pagesize,
            @Query("pageindex") int pageindex
    );

    /**
     * 获取特定栏目下商品列表
     * @param companyid 站点ID
     * @param channelid  频道ID，指定多个频道用“|”线分割
     * @param classid   栏目ID，指定多个栏目用“|”线分割
     * @param elite    推荐调用方法 1 调用有推荐 2 调用不推荐，为空调用所有
     * @param xinpin   新品调用方法 1 调用新品 2 调用非新品，为空调用所有
     * @param pagesize  每页显示数量
     * @param pageindex  当前页数
     * @param order  	排序方式 1 上架时间正序 2 上架时间倒序 3 价格从低到高 4 价格从高到低
     *                  5 销量从低到高 6 销量从高到低 7 评价从低到高 8 评价从高到低 9 浏览次数从低到高 10 浏览次数从高到低
     * @param hot          热门调用方法 1 调用热门 2 调用不热门，为空调用所有
     *@param cuxiao         促销调用方法 1 调用促销 2 调用非促销，为空调用所有
     *   @param  specialid    专题ID，指定多个专题用“|”线分割
     *      @param    price1     最低价格，用于价格区间查询，需要配合price2
     *   @param    price2          最高价格，用于价格区间查询，需要配合price1
     *     @param  keyword         关键词搜索，模糊搜索
     * @return
     */
    @GET("/api/json/content/content.ashx?action=list")
    Observable<GoodsList> getGoodsList(
            @Query("companyid") String companyid,
            @Query("channelid") String channelid,
            @Query("classid") String classid,
            @Query("elite") String elite,
            @Query("hot") String hot,
            @Query("xinpin") String xinpin,
            @Query("cuxiao") String cuxiao,
            @Query("order") String order,
            @Query("specialid") String specialid,
            @Query("price1") String price1,
            @Query("price2") String price2,
            @Query("keyword") String keyword,
            @Query("excludeid") String excludeid,
            @Query("pagesize") int pagesize,
            @Query("pageindex") int pageindex

    );

    /**
     * 获取商品详情
     * @param companyid  站点ID
     * @param id    内容ID
     * @return
     */
    @GET("/api/json/content/content.ashx?action=content")
    Observable<DetailGoods> getGoodsDetail(
            @Query("companyid") String companyid,
            @Query("id") int id
    );

    /**
     * 获取商品属性
     * @param companyid  站点ID
     * @param id   内容ID
     * @return
     */
    @GET("/api/json/shop/shop.ashx?action=shopdata")
    Observable<GoodsParameters> getGoodsPramter(
            @Query("companyid") String companyid,
            @Query("id") int id
    );

    /**
     * 获取一级分类
     * @param companyid
     * @param channelid
     * @return
     */
    @GET("/api/json/channel/channel.ashx?action=class")
    Observable<Classify> getClassify(
            @Query("companyid") String companyid,
            @Query("channelid") String channelid
            );

    /**
     * 加入购物车
     * @param companyid 站点ID
     * @param id   内容ID
     * @param h    货号
     * @param s     数量
     * @param r    操作类型，1 立刻购买 2 购买并进入购物车页面 3 加入购物车成功
     * @param t    商品类型 0 普通商品 1 促销秒杀 3 积分兑换
     * @param from  来源，pc 电脑端 mobile 移动端
     * @return -1 加入购物车失败，发生意外错误
    0 加入购物车失败，会员未登录
    1 加入购物车失败，商品已下架
    2 加入购物车失败，超出购买数量
    3 加入购物车失败，超出库存数量
    4 加入购物车失败，积分不足
    10 加入购物车成功
     */
    @GET("/api/json/shop/shop.ashx?action=shoping")
    Observable<Result> addShopCar(
          @Query("companyid") String companyid,
          @Query("id") int id,
          @Query("h") String h,
          @Query("s") int s,
          @Query("r") int r,
          @Query("t") int t,
          @Query("from") String from
    );

    /**
     * 更新购物车数量
     * @param companyid   站点ID
     * @param id          内容ID
     * @param sum        更新后数量
     * @return
     */
    @GET("/api/json/shop/shop.ashx?action=updateshopcart")
    Observable<CommonResult> updateShopCarNum(
            @Query("companyid") String companyid,
            @Query("id") int id,
            @Query("sum") int sum
    );

    /**
     * 提交商品订单（实物商品）
     * @param companyid    站点ID
     * @param code         安全校验码
     * @param timestamp 时间戳
     * @param cartid      购物车结算ID，多个用“,”号分割
     * @param addressid    收货人地址ID
     * @param paymentname    支付方式ID
     * @param ordertype    送货方式
     * @param invoicestatus   是否开票发票 1 开发票 2 不开发票
     * @param invoicename     发票抬头，invoicestatus为1时必填
     * @param remark           备注
     * @param from           来源，pc 电脑端 mobile 移动端
     * @return
     */
    @FormUrlEncoded
    @POST("/api/json/shop/shop.ashx?action=ordersubmit")
    Observable<CommonResult> doCommitOrder(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Field("cartid") String cartid,
            @Field("addressid") int addressid,
            @Field("paymentname") String paymentname,
            @Field("ordertype") String ordertype,
            @Field("invoicestatus") int invoicestatus,
            @Field("invoicename") String invoicename,
            @Field("snph") String snph,
            @Field("remark") String remark,
            @Field("from") String from
    );
    /**
     * ★ 提交积分兑换订单
     * @param companyid    站点ID
     * @param code         安全校验码
     * @param timestamp 时间戳
     * @param cartid      购物车结算ID，多个用“,”号分割
     * @param addressid    收货人地址ID
     * @param remark           备注
     * @param from           来源，pc 电脑端 mobile 移动端
     * @return
     */
    @FormUrlEncoded
    @POST("/api/json/shop/shop.ashx?action=integralexchangesubmit")
    Observable<CommonResult> doCommitIntegralOrder(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Field("cartid") String cartid,
            @Field("addressid") int addressid,
            @Field("remark") String remark,
            @Field("from") String from
    );
    /**
     * 结算
     * @param companyid   站点ID
     * @param code        安全校验码
     * @param timestamp   时间戳
     * @param cartid     购物车结算ID，多个用“,”号分割
     * @param from       来源，pc 电脑端 mobile 移动端
     * @return
     */
    @FormUrlEncoded
    @POST("/api/json/shop/shop.ashx?action=checkout")
    Observable<CloaseAccount> doCloaseAccount(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Field("cartid") String cartid,
            @Field("from") String from
    );
    /**
     * ★ 进行积分兑换结算
     * @param companyid   站点ID
     * @param code        安全校验码
     * @param timestamp   时间戳
     * @param cartid     购物车结算ID，多个用“,”号分割
     * @param from       来源，pc 电脑端 mobile 移动端
     * @return
     */
    @FormUrlEncoded
    @POST("/api/json/shop/shop.ashx?action=integralexchange")
    Observable<CloaseAccount> doCloaseIntegralAccount(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Field("cartid") String cartid,
            @Field("from") String from
    );
    /**
     *删除购物车商品
     * @param companyid  站点ID
     * @param id         商品ID，多个用“,”号分割
     * @return
     */
    @GET("/api/json/shop/shop.ashx?action=deleteshopcart")
    Observable<CommonResult> deleteShopCar(
            @Query("companyid") String companyid,
            @Query("id") String id
    );
    /**
     * 获取购物车商品信息
     * @param companyid  站点ID
     * @param from   来源，pc 电脑端 mobile 移动端
     * @return
     */
    @GET("/api/json/shop/shop.ashx?action=shopcart")
    Observable<ShopCarGoods> getShopCarGoods(
            @Query("companyid") String companyid,
            @Query("from") String from
            );

    /**
     * 判断登录状态
     * @param companyid  站点ID
     * @param code    安全校验码
     * @param timestamp  时间戳
     * @param from   来源，pc 电脑端 mobile 移动端
     * @return
     */
    @GET("/api/json/member/member.ashx?action=loginStatus")
    Observable<LoginStatus> getLoginStatus(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("from") String from
    );

    /**
     * 获取会员详细信息
     * @param companyid    站点ID
     * @param code          安全校验码
     * @param timestamp     时间戳
     * @return
     */
    @GET("/api/json/member/member.ashx?action=memberinfo")
    Observable<UserInfo> getuserInfo(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp
    );

    /**
     * 会员资料修改
     * @param companyid
     * @param code
     * @param timestamp
     * @param map
     * @return
     */
    @FormUrlEncoded
    @POST("/api/json/member/member.ashx?action=editinfo")
    Observable<CommonResult> modifyUserInfo(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @FieldMap Map<String, String> map
    );
    /**
     *  获取会员收货地址
     * @param companyid   站点ID
     * @param code   安全校验码
     * @param timestamp  时间戳
     * @param pagesize   每页显示数量
     * @param pageindex   当前页数
     * @param addressid   收货地址ID
     * @param main   为1时加载默认收货地址，其它调用所有
     * @return
     */
    @GET("/api/json/member/member.ashx?action=address")
    Observable<Address> getAddress(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("pagesize") int pagesize,
            @Query("pageindex") int pageindex,
            @Query("main") int main,
            @Query("addressid") String addressid
    );

    /**
     * 编辑收货地址
     * @param companyid    站点ID
     * @param code         安全校验码
     * @param timestamp    时间戳
     * @param addressid 收货地址ID，为0时是添加新地址，大于0是修改指定ID收货地址
     * @param addressName  收货人名字
     * @param addressPhone  收货人手机
     * @param addressProvince  收货人所在省/市
     * @param addressCity      收货人所在市/区
     * @param addressArea      收货人所在区/县
     * @param addressXX        收货人详细地址
     * @param addressZipcode   收货人邮编
     * @param addressDefault    默认收货地址 0 否 1 是

     * @return
     */
    @FormUrlEncoded
    @POST("/api/json/member/member.ashx?action=editaddress")
    Observable<CommonResult> editAddress(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Field("addressid") int addressid,
            @Field("addressName") String addressName,
            @Field("addressPhone") String addressPhone,
            @Field("addressProvince") String addressProvince,
            @Field("addressCity") String addressCity,
            @Field("addressArea") String addressArea,
            @Field("addressXX") String addressXX,
            @Field("addressZipcode") String addressZipcode,
            @Field("addressDefault") int addressDefault

            );

    /**
     * 删除会员收货地址
     * @param companyid   站点ID
     * @param code      安全校验码
     * @param timestamp  时间戳
     * @param addressid   收货地址ID
     * @return
     */
    @GET("/api/json/member/member.ashx?action=deleteaddress")
    Observable<CommonResult> deleteAddress(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("addressid") String addressid
    );

    /**
     * 设置默认会员收货地址
     * @param companyid   站点ID
     * @param code        安全校验码
     * @param timestamp   时间戳
     * @param addressid   收货地址ID
     * @return
     */
    @GET("/api/json/member/member.ashx?action=setdefaultaddress")
    Observable<CommonResult> setDefaultAddress(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("addressid") String addressid
    );

    /**
     *  获取普通订单列表
     * @param companyid   站点ID
     * @param code        安全校验码
     * @param timestamp   时间戳
     * @param pagesize    每页显示数量
     * @param pageindex   当前页数
     * @param status    查询订单状态，1 待支付 2 未发货 3 已发货 4 已签收
     * @param ordersNumber   查询订单号，支持模糊查询
     * @param starttime    指定开始时间（订单下单时间区间查询）
     * @param endtime     指定结束时间（订单下单时间区间查询）
     * @param endtime     指定结束时间（订单下单时间区间查询）
     * @param type    订单商品类型，1 普通商品 2 虚拟商品，为空则所有
     * @return
     */
    @GET("/api/json/member/order.ashx?action=orderlist")
    Observable<Order> getOrderList(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("pagesize") int pagesize,
            @Query("pageindex") int pageindex,
            @Query("status") String status,
            @Query("ordersNumber") String ordersNumber,
            @Query("starttime") String starttime,
            @Query("endtime") String endtime,
            @Query("type") String type,
            @Query("from") String from
    );

    /**
     * 查看普通订单详情
     * @param companyid   站点ID
     * @param code         安全校验码
     * @param timestamp    时间戳
     * @param ordersNumber  订单号
     * @return
     */
    @GET("/api/json/member/order.ashx?action=ordercontent")
    Observable<OrderDetail> getOrderDetail(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("ordersNumber") String ordersNumber,
            @Query("from") String from
    );

    /**
     * 取消未支付订单
     * @param companyid   站点ID
     * @param code         安全校验码
     * @param timestamp    时间戳
     * @param ordersNumber  订单号
     * @return
     */
    @GET("/api/json/member/order.ashx?action=orderdelete")
    Observable<CommonResult> cancelOrder(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("ordersNumber") String ordersNumber
    );
    /**
     * 订单确认收货
     * @param companyid   站点ID
     * @param code         安全校验码
     * @param timestamp    时间戳
     * @param ordersNumber  订单号
     * @return
     */
    @GET("/api/json/member/order.ashx?action=ordershouhuo")
    Observable<CommonResult> affirmOrder(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("ordersNumber") String ordersNumber
    );
    /**
     * 内容收藏
     * @param companyid
     * @param id   内容ID
     * @return
     */
    @GET("/api/json/content/content.ashx?action=collection")
    Observable<CommonResult> setCollect(
            @Query("companyid") String companyid,
            @Query("id") int id
    );
    /**
     * 内容收藏判断
     * @param companyid
     * @param id   内容ID
     * @return
     */
    @GET("/api/json/content/content.ashx?action=checkcollection")
    Observable<CommonResult> ifCollect(
            @Query("companyid") String companyid,
            @Query("id") int id
    );

    /**
     * 微信支付
     * @param companyid   站点ID
     * @param code    安全校验码
     * @param timestamp  timestamp
     * @param ordernumber ordernumber
     * @param ip   ip
     * @return
     */
    @GET("/api/json/shop/shop.ashx?action=weixinapppay")
    Observable<WechatPay> wechatPay(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("ordernumber") String ordernumber,
            @Query("ip") String ip
    );

    /**
     * 获取会员收藏列表
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @return
     */
    @GET("/api/json/member/member.ashx?action=collectionlist")
    Observable<Collect> getLoveList(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("pagesize") int pagesize,
            @Query("pageindex") int pageindex
    );

    /**
     *  删除会员收藏内容
     * @param companyid
     * @param code
     * @param timestamp
     * @param collectlId
     * @return
     */
    @GET("/api/json/member/member.ashx?action=collectiondelete")
    Observable<CommonResult> doDeleteCollection(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("collectlId") int collectlId
    );

    /**
     * 上传会员头像（APP）
     * @param companyid
     * @param code
     * @param timestamp
     * @param face  头像数据，base64码
     * @return
     */
    @FormUrlEncoded
    @POST("/api/json/member/member.ashx?action=memberface")
    Observable<CommonResult> upLoadHeadImg(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Field("face") String face
    );

    /**
     * ★ 获取会员消息列表
     * @param companyid
     * @param code
     * @param timestamp
     * @param status    会员消息状态 0 未读 1 已读，为空则查询所有
     * @param pagesize
     * @param pageindex
     * @return
     */
    @GET("/api/json/member/member.ashx?action=smslist")
    Observable<Message> getSmsList(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("status") String status,
            @Query("pagesize") int pagesize,
            @Query("pageindex") int pageindex
    );
    /**
     * ★ 获取会员消息详情
     * @param companyid
     * @param code
     * @param timestamp
     * @param smsid   会员消息ID
     * @return
     */
    @GET("/api/json/member/member.ashx?action=smscontent")
    Observable<MessageDetail> getSmsContent(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("smsid") int smsid

    );
    /**
     * ★ 删除会员消息
     * @param companyid
     * @param code
     * @param timestamp
     * @param smsid   会员消息ID
     * @return
     */
    @GET("/api/json/member/member.ashx?action=smsdelete")
    Observable<CommonResult> doSmsDelete(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("smsid") int smsid

    );

    /**
     * 获取会员通知列表
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @return
     */
    @GET("/api/json/member/member.ashx?action=noticelist")
    Observable<Notice> getNoticeList(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("pagesize") int pagesize,
            @Query("pageindex") int pageindex
    );

    /**
     *
     * @param companyid
     * @param code
     * @param timestamp
     * @param noticeid 会员通知ID
     * @return
     */
    @GET("/api/json/member/member.ashx?action=noticecontent")
    Observable<NoticeDetail> getNoticeDetail(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("noticeid") int noticeid

    );

    /**
     * 内容高级筛选
     * @param companyid
     * @param channelid          频道ID
     * @param orderstatus        是否开启排序搜索，1 开启 0 关闭
     * @param orderlist          指定排序方法，请参考示例格式，可以隐藏部分
     * @param classstatus       是否开启栏目搜索，1 开启 0 关闭
     * @param parentclassid      上级栏目ID，为0和为空调用一级栏目
     * @param classtitle         指定栏目搜索名称，不指定则返回默认值
     * @param specialstatus     是否开启专题搜索，1 开启 0 关闭
     * @param specialtitle      指定专题搜索名称，不指定则返回默认值
     * @param fieldstatus          是否开启自定义字段筛选，1 开启 0 关闭
     * @param pricestatus           是否开启价格区间搜索，1 开启 0 关闭
     * @param pricetitle          指定价格区间搜索名称，不指定则返回默认值
     * @param pricelist           指定价格区间金额，请参考示例格式，可以隐藏部分
     * @param attributestatus        是否开启属性搜索，1 开启 0 关闭
     * @param attributetitle       指定属性方法搜索名称，不指定则返回默认值
     * @param attributelist        指定属性方法，请参考示例格式，可以隐藏部分
     * @return
     */
    @GET("/api/json/content/content.ashx?action=searchselect")
    Observable<ContentFilter> getFilterList(
            @Query("companyid") String companyid,
            @Query("channelid") String channelid,
            @Query("orderstatus") String orderstatus,
            @Query("orderlist") String orderlist,
            @Query("classstatus") String classstatus,
            @Query("parentclassid") String parentclassid,
            @Query("classtitle") String classtitle,
            @Query("specialstatus") String specialstatus,
            @Query("specialtitle") String specialtitle,
            @Query("fieldstatus") String fieldstatus,
            @Query("pricestatus") String pricestatus,
            @Query("pricetitle") String pricetitle,
            @Query("pricelist") String pricelist,
            @Query("attributestatus") String attributestatus,
            @Query("attributetitle") String attributetitle,
            @Query("attributelist") String attributelist

    );

    /**
     * 签到
     * @param companyid
     * @param code
     * @param timestamp
     * @return
     */
    @GET("/api/json/member/member.ashx?action=signin")
    Observable<SignIn>  doSignIn(
            @Query("companyid")  String companyid,
            @Query("code")  String code,
            @Query("timestamp")  String timestamp
    );

    /**
     *★ 获取积分兑换订单列表
     * @param companyid  站点ID
     * @param code           安全校验码
     * @param timestamp     时间戳
     * @param pagesize     每页显示数量
     * @param pageindex    当前页数
     * @param status        查询订单状态，1 待支付 2 未发货 3 已发货 4 已签收
     * @param ordersNumber  查询订单号，支持模糊查询
     * @param starttime      指定开始时间（订单下单时间区间查询）
     * @param endtime       指定结束时间（订单下单时间区间查询）
     * @param from            来源，pc 电脑端 mobile 移动端
     * @return
     */
    @GET("/api/json/member/order.ashx?action=integralorderlist")
    Observable<Order>  getIntegralOrderList(
            @Query("companyid")  String companyid,
            @Query("code")  String code,
            @Query("timestamp")  String timestamp,
            @Query("pagesize")  int pagesize,
            @Query("pageindex")  int pageindex,
            @Query("status")  String status,
            @Query("ordersNumber")  String ordersNumber,
            @Query("starttime")  String starttime,
            @Query("endtime")  String endtime,
            @Query("from")  String from
    );

    /**
     * ★ 查看积分兑换订单详情
     * @param companyid    站点ID
     * @param code          安全校验码
     * @param timestamp       时间戳
     * @param ordersNumber    订单号
     * @param from      来源，pc 电脑端 mobile 移动端
     * @return
     */
    @GET("/api/json/member/order.ashx?action=integralordercontent")
    Observable<OrderDetail>  getIntegralOrderDetail(
            @Query("companyid")  String companyid,
            @Query("code")  String code,
            @Query("timestamp")  String timestamp,
            @Query("ordersNumber")  String ordersNumber,
            @Query("from")  String from
    );

    /**
     * ★ 积分兑换订单确认收货
     * @param companyid
     * @param code
     * @param timestamp
     * @param ordersNumber
     * @return
     */
    @GET("/api/json/member/order.ashx?action=integralordershouhuo")
    Observable<CommonResult>  doSureIntegralOrder(
            @Query("companyid")  String companyid,
            @Query("code")  String code,
            @Query("timestamp")  String timestamp,
            @Query("ordersNumber")  String ordersNumber

    );

    /**
     * ★ 发布评论/咨询信息
     * @param companyid
     * @param code
     * @param timestamp
     * @param id                     评论内容ID
     * @param commentname            评论人名字，为空时会取会员昵称，如果未登录，则默认为游客
     * @param commentcontent           评论内容
     * @param commentimages              评论图片，多个用“,”号分割
     * @return
     */
    @FormUrlEncoded
    @POST("/api/json/common/common.ashx?action=commentadd")
    Observable<CommonResult>  doCommentAdd(
            @Query("companyid")  String companyid,
            @Query("code")  String code,
            @Query("timestamp")  String timestamp,
            @Field("id")  String id,
            @Field("commentname")  String commentname,
            @Field("commentcontent")  String commentcontent,
            @Field("commentimages")  String commentimages

    );

    /**
     * 获取评论/咨询列表信息
     * @param companyid      站点ID
     * @param id            评论内容ID
     * @param keyword       搜索关键词，模糊搜索评论人或评论内容
     * @param order         排序方式 0 按评论时间正序 1 按评论时间倒序 2 按支持数正序 3 按支持数倒序
     * @param pagesize      每页显示数量
     * @param pageindex      当前页数
     * @param from           来源，pc 电脑端 mobile 移动端 app app端 xcx 微信小程序
     * @return
     */
    @GET("/api/json/common/common.ashx?action=commentlist")
    Observable<AnswerList>  getCommentList(
            @Query("companyid")  String companyid,
            @Query("id")  String id,
            @Query("keyword")  String keyword,
            @Query("order")  String order,
            @Query("pagesize")  int pagesize,
            @Query("pageindex")  int pageindex,
            @Query("from")  String from
    );

    /**
     * 获取商城优惠券列表
     * @param companyid       站点ID(必传)
     * @param storeid        商家分店ID（非必传）
     * @param type           限制类型 1 仅限注册会员 2 仅限新会员（新注册，未消费过）（非必传）
     * @param pagesize      每页显示数量（非必传）
     * @param pageindex         当前页数	（非必传）
     * @return
     */
    @GET("/api/json/vouchers/vouchers.ashx?action=coupon")
    Observable<ShopCouponList>  getCouponList(
            @Query("companyid")  String companyid,
            @Query("storeid")  String storeid,
            @Query("type")  String type,
            @Query("pagesize")  int pagesize,
            @Query("pageindex")  int pageindex

    );

    /**
     * ★ 领取商城优惠券
     * @param companyid   站点ID
     * @param code
     * @param timestamp
     * @param aid           优惠券活动ID
     * @return
     */
    @GET("/api/json/vouchers/vouchers.ashx?action=couponobtain")
    Observable<CommonResult>  obtainCoupon(
            @Query("companyid")  String companyid,
            @Query("code")  String code,
            @Query("timestamp")  String timestamp,
            @Query("aid")  int aid
    );

    /**
     * ★ 获取会员商城优惠券数量
     * @param companyid
     * @param code
     * @param timestamp
     * @param storeid             商家分店ID
     * @param money            筛选符合订单金额的优惠券（大于等于该金额）
     * @param status          优惠券状态，1 未使用 2 已使用，为空则所有
     * @return
     */
    @GET("/api/json/member/vouchers.ashx?action=couponcount")
    Observable<CouponCount>  getCouponCount(
            @Query("companyid")  String companyid,
            @Query("code")  String code,
            @Query("timestamp")  String timestamp,
            @Query("storeid")  int storeid,
            @Query("money")  int money,
            @Query("status")  int status
    );

    /**
     * ★ 获取会员商城优惠券列表
     * @param companyid
     * @param code
     * @param timestamp
     * @param storeid
     * @param money
     * @param status
     * @param pagesize
     * @param pageindex
     * @return
     */
    @GET("/api/json/member/vouchers.ashx?action=couponlist")
    Observable<MyCouponList>  getMyCouponList(
            @Query("companyid")  String companyid,
            @Query("code")  String code,
            @Query("timestamp")  String timestamp,
            @Query("storeid")  String storeid,
            @Query("money")  String money,
            @Query("status")  String status,
            @Query("pagesize")  int pagesize,
            @Query("pageindex")  int pageindex
    );

    /**
     * ★ 获取会员积分明细
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @param startdate    指定开始时间
     * @param overdate       指定结束时间
     * @return
     */
    @GET("/api/json/member/member.ashx?action=integration")
    Observable<IntegrationList>  getIntegrationList(
            @Query("companyid")  String companyid,
            @Query("code")  String code,
            @Query("timestamp")  String timestamp,
            @Query("pagesize")  int pagesize,
            @Query("pageindex")  int pageindex,
            @Query("startdate")  String startdate,
            @Query("overdate")  String overdate
    );
    /**
     ★ 获取会员资金明细
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @param startdate    指定开始时间
     * @param overdate       指定结束时间
     * @return
     */
    @GET("/api/json/member/member.ashx?action=transaction")
    Observable<Account>  getTransactionList(
            @Query("companyid")  String companyid,
            @Query("code")  String code,
            @Query("timestamp")  String timestamp,
            @Query("pagesize")  int pagesize,
            @Query("pageindex")  int pageindex,
            @Query("startdate")  String startdate,
            @Query("overdate")  String overdate
    );

    /**
     * ★ 获取会员充值交易列表
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @param startdate
     * @param overdate
     * @return
     */
    @GET("/api/json/member/member.ashx?action=recharge")
    Observable<Recharge>  getRechargeList(
            @Query("companyid")  String companyid,
            @Query("code")  String code,
            @Query("timestamp")  String timestamp,
            @Query("pagesize")  int pagesize,
            @Query("pageindex")  int pageindex,
            @Query("startdate")  String startdate,
            @Query("overdate")  String overdate
    );

    /**
     * 获取商品评价列表信息
     * @param companyid
     * @param id         评价商品ID
     * @param level      评价等级，1 好评 2 中评 3 差评，为空则调用所有
     * @param pagesize
     * @param pageindex
     * @param order           排序方式 0 按评价时间正序 1 按评价时间倒序 2 按支持数正序 3 按支持数倒序
     * @param from
     * @return
     */
    @GET("/api/json/common/common.ashx?action=commodityevaluation")
    Observable<EvaluateList>  getEvaluateList(
            @Query("companyid")  String companyid,
            @Query("id")  int id,
            @Query("level")  String level,
            @Query("order")  String order,
            @Query("pagesize")  int pagesize,
            @Query("pageindex")  int pageindex,
            @Query("from")  String from
    );

    /**
     * ★ 提交商品评价信息
     * @param companyid
     * @param code
     * @param timestamp
     * @param cartid           商品购物车ID
     * @param commodityid      商品ID
     * @param commentcontent   商品评价内容
     * @param commentlevel        商品评价星级
     * @param commentlabel       商品评价标签，多个用“|”线分割
     * @param commentimages        商品评价晒图
     * @return
     */
    @FormUrlEncoded
    @POST("/api/json/member/order.ashx?action=saveordergoodcomment")
    Observable<CommonResult>  commitEvaluate(
            @Query("companyid")  String companyid,
            @Query("code")  String code,
            @Query("timestamp")  String timestamp,
            @Field("cartid")  int cartid,
            @Field("commodityid")  int commodityid,
            @Field("commentcontent")  String commentcontent,
            @Field("commentlevel")  int commentlevel,
            @Field("commentlabel")  String commentlabel,
            @Field("commentimages")  String commentimages
    );
    @Multipart
    @POST("/api/json/upload/upload.ashx?action=memberimages")
    Observable<UpFile>  upFile(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Part  MultipartBody.Part file
    );
    /**
     *售后商品列表
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @param status
     * @param repairsid
     * @param startdate
     * @param overdate
     * @param from
     * @return
     */
    @GET("/api/json/member/aftersale.ashx?action=list")
    Observable<AfterSale>  getAfterSaleList(
            @Query("companyid")  String companyid,
            @Query("code")  String code,
            @Query("timestamp")  String timestamp,
            @Query("pagesize")  int pagesize,
            @Query("pageindex")  int pageindex,
            @Query("status")  int status,
            @Query("repairsid")  String repairsid,
            @Query("startdate")  String startdate,
            @Query("overdate")  String overdate,
            @Query("from")  String from
    );

    /**
     * ★ 订单号获取商品收货地址
     * @param companyid
     * @param code
     * @param timestamp
     * @param ordersid
     * @return
     */
    @GET("/api/json/member/aftersale.ashx?action=orderaddress")
    Observable<AddressOrderId>  getAddressOrderId(
            @Query("companyid")  String companyid,
            @Query("code")  String code,
            @Query("timestamp")  String timestamp,
            @Query("ordersid")  String ordersid

    );
    /**
     * ★ 提交商品售后服务
     * @param companyid
     * @param code
     * @param timestamp
     * @param cartid        商品购物车ID
     * @param commodityid    商品ID
     * @param ordersid       商品订单ID
     * @param repairstype   售后服务类型，1 退货 2 换货 3 维修
     * @param repairsnumber      申请售后商品数量
     * @param repairsproof          申请售后商品申请凭据
     * @param repairscontent     申请售后商品原因
     * @param repairsimages      申请售后商品图片，多张图片用“|”线分割
     * @param repairsname       申请人名字
     * @param repairsphone           申请人电话
     * @param Receiptaddress      申请人地址（格式：省/市,市/区,区/县,详细地址）
     * @param repairszipcode     申请人邮编
     * @return
     */
    @FormUrlEncoded
    @POST("/api/json/member/aftersale.ashx?action=addaftersale")
    Observable<CommonResult>  commitShouHou(
            @Query("companyid")  String companyid,
            @Query("code")  String code,
            @Query("timestamp")  String timestamp,
            @Field("cartid")  int cartid,
            @Field("commodityid")  int commodityid,
            @Field("ordersid")  String ordersid,
            @Field("repairstype")  int repairstype,
            @Field("repairsnumber")  int repairsnumber,
            @Field("repairsproof")  String repairsproof,
            @Field("repairscontent")  String repairscontent,
            @Field("repairsimages")  String repairsimages,
            @Field("repairsname")  String repairsname,
            @Field("repairsphone")  String repairsphone,
            @Field("Receiptaddress")  String Receiptaddress,
            @Field("repairszipcode")  String repairszipcode
    );
    /**
     * ★ 运费刷新（更换收货地址时）
     * @param companyid
     * @param code
     * @param timestamp
     * @param cartid
     * @param addresslid
     * @return
     */
    @GET("/api/json/shop/shop.ashx?action=newfreight")
    Observable<RefreshYunFei> refreshYunFei(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("cartid") String cartid,
            @Query("addresslid") String addresslid
    );
    /**
     * ★ 获取售后服务单详情
     * @param companyid
     * @param code
     * @param timestamp
     * @param cartid
     * @param commodityid
     * @param from
     * @return
     */
    @GET("/api/json/member/aftersale.ashx?action=info")
    Observable<ShouHouDetail> getShouHouDetail(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("cartid") String cartid,
            @Query("commodityid") String commodityid,
            @Query("from") String from
    );
    /**
     * ★ 支付宝支付（APP）
     * @param companyid
     * @param code
     * @param timestamp
     * @param ordernumber
     * @return
     */
    @GET("/api/json/shop/shop.ashx?action=ailpayapppay")
    Observable<Alipay> doAlipay(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("ordernumber") String ordernumber

    );
    /**
     * ★ 获取评价商品列表
     * @param companyid
     * @param code
     * @param timestamp
     * @param pagesize
     * @param pageindex
     * @param status            查询评价状态，0 待评价 1 已评价，为空则所有
     * @param from
     * @return
     */
    @GET("/api/json/member/order.ashx?action=ordergoodslist")
    Observable<GoodsEvaluateList> getGoodsEvaluateList(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("pagesize") int pagesize,
            @Query("pageindex") int pageindex,
            @Query("status") String status,
            @Query("from") String from

    );

    /**
     *★ 获取评价商品详情
     * @param companyid
     * @param code
     * @param timestamp
     * @param cartid    商品购物车ID
     * @param from
     * @return
     */
    @GET("/api/json/member/order.ashx?action=ordergoodcomment")
    Observable<GoodsEvaluateDetail> getGoodsEvaluateDetail(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("cartid") String cartid,
            @Query("from") String from

    );

    /**
     *获取原生版定制APP配置
     * @param companyid
     * @return
     */
    @GET("/api/json/app/app.ashx?action=config")
    Observable<AppConfig> getAppConfig(
            @Query("companyid") String companyid
    );

    /**
     *获取原生版定制APP轮播图
     * @param companyid
     * @return
     */
    @GET("/api/json/app/app.ashx?action=slide")
    Observable<NewBanner> getNewBanner(
            @Query("companyid") String companyid
    );
    /**
     * ★ 获取订单数量
     * @param companyid
     * @param code
     * @param timestamp
     * @param type            订单商品类型，1 普通商品 2 虚拟商品，为空则所有
     * @param status        订单状态 1 待支付 2 未发货 3 已发货 4 已签收，为空调返回所有订单状态数量
     * @return
     */
    @GET("/api/json/member/order.ashx?action=ordercount")
    Observable<OrderNum> getOrderNum(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Query("type") String type,
            @Query("status") String status
    );
    /**
     * 统计原生版定制APP安装
     * @param companyid
     * @param code
     * @param timestamp
     * @param appos             APP类型，IOS 苹果 Android 安卓
     * @param appkey            APP唯一编码
     * @param appipaddress     IP地址
     * @return
     */
    @FormUrlEncoded
    @POST("/api/json/app/app.ashx?action=tongji")
    Observable<CommonResult> getAppTongji(
            @Query("companyid") String companyid,
            @Query("code") String code,
            @Query("timestamp") String timestamp,
            @Field("appos") String appos,
            @Field("appkey") String appkey,
            @Field("appipaddress") String appipaddress
    );
}
